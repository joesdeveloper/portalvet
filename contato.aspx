﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contato.aspx.cs" Inherits="contato" %>

<%@ Register src="modulos/rodape.ascx" tagname="rodape" tagprefix="uc1" %>
<%@ Register src="modulos/cabecalho.ascx" tagname="cabecalho" tagprefix="uc2" %>
<%@ Register src="modulos/aside-app-prescricao.ascx" tagname="prescricao" tagprefix="uc3" %>
<%@ Register src="modulos/aside-pesquisar.ascx" tagname="pesquisar" tagprefix="uc4" %>
<%@ Register src="modulos/aside-menu.ascx" tagname="menu" tagprefix="uc5" %>
<%@ Register src="modulos/section-destaque.ascx" tagname="destaque" tagprefix="uc6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Portal Vet - Contato e Representantes</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=ddlEstado.ClientID %>').change(function () {
                var estado = $('#<%=ddlEstado.ClientID%>').val()
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "_servicos/servicos.aspx/Representantes",
                    data: "{'estado':'" + estado + "'}",
                    dataType: "json",
                    success: function (data) {
                        var html = '';
                        var obj = $.parseJSON(data.d);
                        $.each(obj, function () {
                            html +=
                                '<p>' +
                                '<span class="titulo">' + this['nome'] + '</span>' +
                                '<span class="cidade">' + ' - ' + this['estado'] + '</span>' +
                                '<br>' +
                                this['endereco'] + ', ' + this['numero']
                                ;

                            if (this['complemento'].length > 0)
                                html += ' - ' + this['complemento'];

                            html +='<br>CEP: ' + this['cep'];

                            if (this['telefone1'].length > 0)
                                html += '<br>Tel: ' + this['telefone1'];

                            if (this['telefone2'].length > 0)
                                html += ' / ' + this['telefone2'];

                            if (this['telefone3'].length > 0)
                                html += ' / ' + this['telefone3'];

                            if (this['telefone4'].length > 0)
                                html += ' / ' + this['telefone4'];

                            if (this['celular'].length > 0)
                                html += ' / ' + this['celular'];

                            if (this['fax'].length > 0)
                                html += '<br>Fax:' + this['fax'];

                            if (this['nextelid'].length > 0)
                                html += '<br>Id Nextel:' + this['nextelid'];
                                
                            if (this['email1'].length > 0)
                                html += '<br>E-mail: ' + this['email1'];

                            if (this['email2'].length > 0)
                                html += ' / ' + this['email2'];

                            html += '</p>';
                                    
                        });

                        $('.representantes').html(html);

                    },
                    error: function (result) {
                        //alert("Error");
                    }
                });
            });
        });
    </script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="principal">
       <uc2:cabecalho ID="cabecalho1" runat="server" />
        <div id="conteudo">
            <section>
                <div class="contato">
                    <h1>
                        <div>
                            Contato
                        </div>
                    </h1>

                    <p>Para entrar em contato com nosso SAC, preencha os campos abaixo para enviar suas mensagem.</p>

                    <div class="campos">
                        <div class="nome"><span>Seu nome:</span><asp:TextBox ID="txtNome" runat="server" MaxLength="80"></asp:TextBox></div>
                        <div class="email"><span>Seu e-mail:</span><asp:TextBox ID="txtEmail" runat="server" MaxLength="100"></asp:TextBox></div>
                        <div class="telefone"><span>Seu telefone:</span><asp:TextBox ID="txtTelefone" runat="server" MaxLength="20"></asp:TextBox></div>
                        <div class="assunto"><span>Assunto:</span><asp:TextBox ID="txtAssunto" runat="server" MaxLength="50"></asp:TextBox></div>
                        <div class="mensagem"><span>Mensagem:</span><asp:TextBox ID="txtMensagem" runat="server" TextMode="MultiLine" Rows="6"></asp:TextBox></div>
                        <asp:Button ID="btnEnviar" runat="server" Text="enviar mensagem" 
                            onclick="btnEnviar_Click" />
                    </div>

                    <a href="#" class="banner-chat-online"><img src="layout/imagens/img-banner-chat-online.png" /></a>

                    <h1>
                        <div>
                            Representantes
                        </div>
                    </h1>
                    <p>Gostaria de comprar produtos ou solicitar uma visita técnica, selecione sua região abaixo e localize o distribuidor mais próximo.</p>
                    <div class="campos">
                        <div class="seletor">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="estado" oninit="ddlEstado_Init" >
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="representantes">
                        <%--<p>
                            <span class="titulo">Distribuidora</span>
                            <span class="cidade"> Blumenal - SC</span>
                            <br>
                            Rua Bahia, 7799 - Bloco A - Sala 2 - Salto Weissbach
                            <br>
                            CEP: 89032-002
                            <br>
                            Tel: (47) 3330-5747
                            <br>
                            E-mail: agroscat@terra.com.br
                        </p>--%>
                    </div>
                </div>
            </section>
               
            <aside>
                <uc3:prescricao ID="prescricao1" runat="server" />
                <uc4:pesquisar ID="pesquisar1" runat="server" />
                <uc5:menu ID="menu1" runat="server" />
            </aside>
            
        </div>
        
        <uc1:rodape ID="rodape1" runat="server" />
    </div>
    </form>
</body>
</html>
