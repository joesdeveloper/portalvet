﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Globalization;
using System.Text;

public partial class faq : System.Web.UI.Page
{
    BLL.PerguntasRespostas bFaq = new BLL.PerguntasRespostas();

    UTIL.Validacao oVal = new UTIL.Validacao();

    protected void Page_Load(object sender, EventArgs e)
    {
        /*System.Web.HttpBrowserCapabilities browser = Request.Browser;

        Response.Write(
            "Browser Capabilities<br>"
        + "Type = " + browser.Type + "<br>"
        + "Name = " + browser.Browser + "<br>"
        + "Version = " + browser.Version + "<br>"
        + "Major Version = " + browser.MajorVersion + "<br>"
        + "Minor Version = " + browser.MinorVersion + "<br>"
        + "Platform = " + browser.Platform + "<br>"
        + "Is Beta = " + browser.Beta + "<br>"
        + "Is Crawler = " + browser.Crawler + "<br>"
        + "Is AOL = " + browser.AOL + "<br>"
        + "Is Win16 = " + browser.Win16 + "<br>"
        + "Is Win32 = " + browser.Win32 + "<br>"
        + "Supports Frames = " + browser.Frames + "<br>"
        + "Supports Tables = " + browser.Tables + "<br>"
        + "Supports Cookies = " + browser.Cookies + "<br>"
        + "Supports VBScript = " + browser.VBScript + "<br>"
        + "Supports JavaScript = " +
            browser.EcmaScriptVersion.ToString() + "<br>"
        + "Supports Java Applets = " + browser.JavaApplets + "<br>"
        + "Supports ActiveX Controls = " + browser.ActiveXControls
              + "<br>"
        + "Supports JavaScript Version = " +
            browser["JavaScriptVersion"] + "<br>"
            );

         var bw = browser.Browser.ToLower();
         if (bw.Contains("iphone") || bw.Contains("ipad") || bw.Contains("ipod") || bw.Contains("safari"))
             Response.Write("<br>ok");
         else
             Response.Write("<br>normal");*/

    }


    public string Faq()
    {

        var lista = bFaq.Pesquisar(0, 10000, "", "Pergunta", true);

        var listaAux = (from aux in lista
                        where aux.Ativo == true
                        orderby aux.Categoria.Nome ascending
                        select aux).ToList();


        var html = new StringBuilder();

        for (var i = 0; i < listaAux.Count; i++)
        {
            var registro = listaAux[i];

            html.Append(
                "<table class=\"lista\" cellspacing=\"0\">" +
                "   <tr class=\"cabecalho\">" +
                "       <th>" +
                "           " + registro.Categoria.Nome +
                "       </th>" +
                "   </tr>"
                );

            
            for (var j = i; j < listaAux.Count; j++)
            {
                var registroAux = listaAux[j];



                if (registro.Categoria.IdCategoria != registroAux.Categoria.IdCategoria)
                    break;
               

                html.Append(
                        "   <tr>" +
                        "       <td>" +
                        "           <a href=\"#" + registroAux.IdPerguntaResposta + "\" class=\"resposta-item\">" +
                        "               <p class=\"texto-nome\"><strong>" + registroAux.Pergunta + "</strong></p>" +
                        "               <label id=\"" + registroAux.IdPerguntaResposta + "\" class=\"texto-do-item\" >" + registroAux.Resposta + "</label>" +
                        "           </a>" +
                        "       </td>" +
                        "   </tr>"
                        );
                i = j;
                
              }

            html.Append("</table>");
        }

        return html.ToString();
    }

}