﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_administracao : System.Web.UI.MasterPage
{
    System.Security.Principal.IPrincipal usuario = System.Web.HttpContext.Current.User;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["ADMINISTRADORES"] == null)
            Response.Redirect("~/_admin/");

        var nome = Request.Cookies["ADMINISTRADORES"]["nome"];
        var dataUltimoAcesso = Request.Cookies["ADMINISTRADORES"]["dataUltimoAcesso"];

        var nomeAux = nome.Split(new char[]{' '});

        switch(nomeAux.Length)
        {
            case 1:
                lblNome.Text = nomeAux[0];
                break;
            case 2:
                lblNome.Text = nomeAux[0] + " " + nomeAux[1];
                break;
            default:
                lblNome.Text = nomeAux[0] + " " + nomeAux[1];
                break;
        }
    }

    //protected void Page_Error( object sender, EventArgs e )
    //{
    //    try
    //    {
    //        var erro = Server.GetLastError();
    //        //    Server.ClearError();
    //        //    Response.Write(erro.Message);
    //    }
    //    catch { }

    //}

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        Page.Header.DataBind();
        //this.Page.Error += new EventHandler(Page_Error);
    }

    protected void btnSair_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/_admin/login.aspx");
    }

    protected void treeLateral_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
    {
        var node = e.Node.DataItem as SiteMapNode;

        if (!string.IsNullOrEmpty(node["showInTree"]))
        {
            bool isVisible;
            if (bool.TryParse(node["showInTree"], out isVisible))
            {
                if (!isVisible)
                {
                    e.Node.Parent.ChildNodes.Remove(e.Node);
                }
            }
        } 
    }
}
