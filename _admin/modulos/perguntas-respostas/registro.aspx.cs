﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_perguntas_respostas_registro : System.Web.UI.Page
{
    BLL.PerguntasRespostas bPer = new BLL.PerguntasRespostas();
    VO.PerguntaResposta vPer = new VO.PerguntaResposta();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        txtResposta.config.toolbar = new object[]
			{
				new object[] { "Source", "-", "NewPage", "Preview", "-", "Templates" },
				new object[] { "Cut", "Copy", "Paste", "PasteText", "PasteFromWord" },
				new object[] { "Undo", "Redo", "-", "Find", "Replace", "-", "SelectAll", "RemoveFormat" },
                new object[] { "Image", "Table", "HorizontalRule", "SpecialChar", "PageBreak", "Iframe" },
				"/",
				new object[] { "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				new object[] { "NumberedList", "BulletedList", "-", "Outdent", "Indent", "Blockquote", "CreateDiv" },
				new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				new object[] { "BidiLtr", "BidiRtl" },
				new object[] { "Link", "Unlink", "Anchor" },
				new object[] { "TextColor", "BGColor" }
			};

        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vPer.IdPerguntaResposta = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vPer = bPer.Consultar(vPer.IdPerguntaResposta);

        if (vPer != null)
        {
            LimparDados();
            try
            {
                ddlCategoria.Items.FindByValue(vPer.Categoria.IdCategoria.ToString()).Selected = true;
            }
            catch { }
            txtPergunta.Text = vPer.Pergunta;
            txtResposta.Text = vPer.Resposta;
            cbxAtivo.Checked = vPer.Ativo;
        }
    }


    private void LimparDados()
    {
        ddlCategoria.SelectedIndex = -1;
        txtPergunta.Text = "";
        txtResposta.Text = "";
        cbxAtivo.Checked = false;
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
        {
            vPer.IdPerguntaResposta = Convert.ToInt32(Request["id"]);
        }

        vPer.Categoria = new VO.PerguntaRespostaCategoria();
        vPer.Categoria.IdCategoria = Convert.ToInt32(ddlCategoria.SelectedValue);

        vPer.Pergunta = txtPergunta.Text;
        vPer.Resposta = txtResposta.Text;
        vPer.Ativo = cbxAtivo.Checked;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        //if (ValidarDados())
        //{
            vPer = bPer.Salvar(vPer);

            if (!Notificacao.Erro)
            {
                if (Request["id"] == null)
                    Response.Redirect("registro.aspx?id=" + vPer.IdPerguntaResposta);
            }
        //}

        ExibirMensagem();
    }
    

    protected void ddlCategoria_Init(object sender, EventArgs e)
    {
        ddlCategoria.Items.Clear();

        var lista = bPer.ListarCategorias();

        foreach (var registro in lista)
        {
            ddlCategoria.Items.Add(new ListItem(registro.Nome, registro.IdCategoria.ToString()));
        }
    }
}