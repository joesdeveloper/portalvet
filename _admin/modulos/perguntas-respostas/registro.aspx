﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_perguntas_respostas_registro" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <table class="formulario">
            <tr>
                <td class="texto">Categoria:</td>
                <td>
                    <asp:DropDownList ID="ddlCategoria" runat="server" CssClass="campo-curto" oninit="ddlCategoria_Init">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">Pergunta:</td>
                <td>
                    <asp:TextBox ID="txtPergunta" runat="server" CssClass="campo" required Rows="4" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Resposta:
                </td>
                <td>
                    <CKEditor:CKEditorControl ID="txtResposta" runat="server" Width="95%" Height="350px"></CKEditor:CKEditorControl>
                    <%--<asp:TextBox ID="txtResposta" runat="server" CssClass="campo" required Rows="15" TextMode="MultiLine"></asp:TextBox>--%>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Ativo:
                </td>
                <td>
                    <asp:CheckBox ID="cbxAtivo" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

