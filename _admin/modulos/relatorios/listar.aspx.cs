﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_relatorios_listar : System.Web.UI.Page
{
    const int _registrosPorPagina = 15;
    BLL.Receitas bRecObs = new BLL.Receitas();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["coluna"] = null;
            IniciarGridView();
        }
    }


    private void InicarSessao()
    {
        if (ViewState["coluna"] == null)
        {
            txtDataInicial.Text = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");
            txtDataFinal.Text = DateTime.Now.ToString("dd/MM/yyyy");

            ViewState["filtro"] = "";
            ViewState["paginaAtual"] = 0;
            ViewState["coluna"] = "DataInclusao";
            ViewState["ascendente"] = false;

            ViewState["totalPaginas"] = 0;
            ViewState["totalDeRegistros"] = 0;
        }
    }

    private void IniciarGridView()
    {
        InicarSessao();

        var idTipoDeRelatorio = ddlTipoDeRelatorio.SelectedValue;
        var idProduto = Convert.ToInt32(ddlProdutos.SelectedValue);
        var idEspecie = Convert.ToInt32(ddlEspecies.SelectedValue);
        var dataIncial = Convert.ToDateTime(txtDataInicial.Text + " 00:00:00");
        var dataFinal = Convert.ToDateTime(txtDataFinal.Text + " 23:59:59");

        bool ascendente = Convert.ToBoolean(ViewState["ascendente"]);
        int paginaAtual = Convert.ToInt32(ViewState["paginaAtual"]);
        int totalPaginas = 0;
        string valor = ViewState["filtro"].ToString();
        string coluna = ViewState["coluna"].ToString();

        var lista = bRecObs.Pesquisar(paginaAtual, _registrosPorPagina, valor, coluna, ascendente, idTipoDeRelatorio, idEspecie, idProduto, dataIncial, dataFinal);

        int totalRegistros = bRecObs.TotalDeRegistros(valor, idTipoDeRelatorio, idEspecie, idProduto, dataIncial, dataFinal);
        ViewState["totalDeRegistros"] = totalRegistros;

        totalPaginas = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal((decimal)totalRegistros / (decimal)_registrosPorPagina)));
        ViewState["totalPaginas"] = totalPaginas;

        grdwListar.PageSize = _registrosPorPagina;
        grdwListar.PageIndex = 0;
        grdwListar.DataSource = lista;
        grdwListar.DataBind();
    }


    protected void grdwListar_Sorting(object sender, GridViewSortEventArgs e)
    {
        string coluna = e.SortExpression;

        if (coluna == ViewState["coluna"].ToString())
        {
            if (Convert.ToBoolean(ViewState["ascendente"]))
                ViewState["ascendente"] = false;
            else
                ViewState["ascendente"] = true;
        }
        else
            ViewState["coluna"] = coluna;

        IniciarGridView();
    }



    protected void btnPrimeiro_Click(object sender, ImageClickEventArgs e)
    {
        int pag = 0;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }

    protected void btnAnterior_Click(object sender, ImageClickEventArgs e)
    {
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        pag--;

        if (pag < 0)
            pag = 0;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void btnIr_Click(object sender, EventArgs e)
    {
        int total = Convert.ToInt32(ViewState["totalPaginas"]);
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        try
        {
            pag = Convert.ToInt32(((TextBox)grdwListar.BottomPagerRow.FindControl("txtPaginaAtual")).Text);
        }
        catch { }

        total--;

        if (pag < 0)
            pag = 0;

        if (pag > total)
            pag = total;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void btnProximo_Click(object sender, ImageClickEventArgs e)
    {
        int total = Convert.ToInt32(ViewState["totalPaginas"]);
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        pag++;

        total--;
        if (pag > total)
            pag = total;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }

    protected void btnUltimo_Click(object sender, ImageClickEventArgs e)
    {
        int pag = Convert.ToInt32(ViewState["totalPaginas"]);
        pag--;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void grdwListar_PreRender(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ViewState["totalPaginas"]) > 0)
            grdwListar.BottomPagerRow.Visible = true;

    }


    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            /*var imgAlterar = (ImageButton)e.Row.FindControl("imgAlterar");
            imgAlterar.Attributes.Add("idRegistro", DataBinder.Eval(e.Row.DataItem, "IdProduto").ToString());*/
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            var txtPaginaAtual = (TextBox)e.Row.FindControl("txtPaginaAtual");
            txtPaginaAtual.Text = (Convert.ToInt32(ViewState["paginaAtual"]) + 1).ToString();

            var lblTotalDePaginas = (Literal)e.Row.FindControl("lblTotalDePaginas");
            lblTotalDePaginas.Text = ViewState["totalPaginas"].ToString();

            var lblTotalDeIRegistros = (Literal)e.Row.FindControl("lblTotalDeIRegistros");
            lblTotalDeIRegistros.Text = ViewState["totalDeRegistros"].ToString();
        }
    }



    protected void grdwListar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        var image = new Image();
        var separador = new Image();

        if (e.Row != null && e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                if (cell.HasControls())
                {
                    var button = cell.Controls[0] as LinkButton;
                    if (button != null)
                    {
                        if (ViewState["coluna"].ToString().Trim() == button.CommandArgument)
                        {
                            if (Convert.ToBoolean(ViewState["ascendente"]))
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_cima.png";
                            else
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_baixo.png";

                            image.AlternateText = "Middle";
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }
    }


    protected void imgAlterar_Click(object sender, EventArgs e)
    {
        var imgAlterar = (ImageButton)sender;
        Response.Redirect("registro.aspx?id=" + imgAlterar.Attributes["idRegistro"]);
    }


    protected void btnTodos_Click(object sender, EventArgs e)
    {
        txtPesquisar.Text = "";
        ViewState["filtro"] = "";
        ViewState["paginaAtual"] = 0;
        IniciarGridView();
    }

    /*protected void btnLocalizar_Click(object sender, ImageClickEventArgs e)
    {
        Localizar();
    }*/

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        Localizar();
    }

    private void Localizar()
    {
        var valor = txtPesquisar.Text;
        ViewState["filtro"] = valor;
        ViewState["paginaAtual"] = 0;
        IniciarGridView();
    }

    protected void txtPesquisar_TextChanged(object sender, EventArgs e)
    {
        Localizar();
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        var idTipoDeRelatorio = ddlTipoDeRelatorio.SelectedValue;
        var idProduto = Convert.ToInt32(ddlProdutos.SelectedValue);
        var idEspecie = Convert.ToInt32(ddlEspecies.SelectedValue);
        var dataIncial = Convert.ToDateTime(txtDataInicial.Text + " 00:00:00");
        var dataFinal = Convert.ToDateTime(txtDataFinal.Text + " 23:59:59");
        string valor = ViewState["filtro"].ToString();

        var lista = bRecObs.Pesquisar(0, 1000000, valor, "DataInclusao", false, idTipoDeRelatorio, idEspecie, idProduto, dataIncial, dataFinal);

        var listaAux = (from aux in lista
                 select new
                 {
                     aux.IdReceita,
                     aux.TipoDeRelatorio,
                     aux.Especie,
                     aux.Raca,

                     Logo = (aux.Logo == "" ? "false" : "true"),
                     aux.NomeClinica,
                     aux.Cpf,
                     aux.NomeDoVeterinario,
                     aux.SobrenomeDoVeterinario,
                     aux.Crmv,
                     aux.CrmvUf,

                     aux.EmailDoVeterinario,
                     aux.EspecialidadeAreaAtuacao,
                     aux.Endereco,
                     aux.Numero,
                     aux.Complemento,
                     aux.Bairro,
                     aux.Cidade,
                     aux.Cep,
                     aux.Estado,
                     aux.TelefoneComercial,

                     aux.NomeDoProprietario,
                     aux.EmailDoProprietario,

                     aux.NomeDoAnimal,
                     aux.Sexo,
                     aux.Porte,
                     aux.ScoreCorporal,
                     aux.PesoAtual,
                     aux.PesoPretendido,
                     aux.QuantidadeDeSemanas,

                     aux.Doenca,
                     aux.AlimentoSecoNome,
                     aux.AlimentoSecoQuantidade,
                     aux.AlimentoUmidoNome,
                     aux.AlimentoUmidoQuantidade,

                     aux.RecomendacoesAdicionais,
                     aux.EnviadoPorEmail,
                     aux.Impresso,
                     DataInclusao = aux.DataInclusao.ToShortDateString(),
                     HoraInclusao = aux.DataInclusao.ToLongTimeString()
                 }
                 ).ToList();


        if (lista != null)
            new UTIL.Exportacao().CSV(listaAux, "Obesidade-Exportacao-" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".csv");
    }

    protected void ddlEspecies_Init(object sender, EventArgs e)
    {
        var lista = new BLL.Especies().Listar();

        ddlEspecies.Items.Clear();
        ddlEspecies.Items.Add(new ListItem("Todos", "0"));

        foreach (var registro in lista)
        {
            ddlEspecies.Items.Add(new ListItem(registro.Nome, registro.IdEspecie.ToString()));
        }
    }

    protected void ddlProdutos_Init(object sender, EventArgs e)
    {
        var lista = new BLL.Produtos().Listar();

        ddlProdutos.Items.Clear();
        ddlProdutos.Items.Add(new ListItem("Todos", "0"));

        foreach (var registro in lista)
        {
            ddlProdutos.Items.Add(new ListItem(registro.Nome, registro.IdProduto.ToString()));
        }
    }

}