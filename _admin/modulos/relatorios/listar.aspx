﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="listar.aspx.cs" Inherits="_admin_modulos_relatorios_listar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=txtDataInicial.ClientID%>').mask('99/99/9999');
            $('#<%=txtDataFinal.ClientID%>').mask('99/99/9999');
         });
     </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkExcel" runat="server" onclick="btnExcel_Click"><span><asp:Image ID="Image3" ImageUrl="~/_admin/layout/imagens/ico-excel.png" runat="server" />Excel CSV</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
<%--    <ul>
        <li>Tip.: <asp:DropDownList ID="ddlTipoDeRelatorio" runat="server" Width="80">
                        <asp:ListItem Value="" Text="Todos"></asp:ListItem>
                        <asp:ListItem Value="o" Text="Obesidade"></asp:ListItem>
                        <asp:ListItem Value="e" Text="Outras Enfermidades "></asp:ListItem>
                   </asp:DropDownList></li>
        <li>&nbsp;&nbsp;|&nbsp;&nbsp;Prod.: <asp:DropDownList ID="ddlProdutos" runat="server" OnInit="ddlProdutos_Init" Width="80"></asp:DropDownList></li>
        <li>&nbsp;&nbsp;|&nbsp;&nbsp;Esp.: <asp:DropDownList ID="ddlEspecies" runat="server" OnInit="ddlEspecies_Init"></asp:DropDownList></li>
        <li>&nbsp;&nbsp;|&nbsp;&nbsp;Dt Inicio: <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campo" MaxLength="10" Width="70"></asp:TextBox>  Dt Final: <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campo" MaxLength="10" Width="70"></asp:TextBox></li>
        <li>&nbsp;&nbsp;|&nbsp;&nbsp;Busca: <asp:TextBox ID="txtPesquisar" runat="server" AutoPostBack="True" ontextchanged="txtPesquisar_TextChanged" Width="80"></asp:TextBox> <asp:LinkButton ID="btnPesquisar" runat="server" onclick="btnPesquisar_Click"><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-search.png" runat="server" /></asp:LinkButton></li>
    </ul>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Listagem
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
            <fieldset id="filtros">
                <legend>Filtros para pesquisa</legend>
                <div >Tipo de relatório:
                    <asp:DropDownList ID="ddlTipoDeRelatorio" runat="server">
                        <asp:ListItem Value="" Text="Todos"></asp:ListItem>
                        <asp:ListItem Value="o" Text="Obesidade"></asp:ListItem>
                        <asp:ListItem Value="e" Text="Outras Enfermidades "></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div >Produtos:<asp:DropDownList ID="ddlProdutos" runat="server" OnInit="ddlProdutos_Init"></asp:DropDownList></div>
                <div >Espécies:<asp:DropDownList ID="ddlEspecies" runat="server" OnInit="ddlEspecies_Init"></asp:DropDownList></div>
                <div >Data de Inicio:<asp:TextBox ID="txtDataInicial" runat="server" CssClass="campo" MaxLength="10" Width="70"></asp:TextBox>  Data Final: <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campo" MaxLength="10" Width="70"></asp:TextBox></div>
                <div >Palavra chave: <asp:TextBox ID="txtPesquisar" runat="server" AutoPostBack="True" Width="150" OnTextChanged="txtPesquisar_TextChanged"></asp:TextBox></div>
                <div ><asp:Button ID="btnFiltrar" runat="server" Text="Pesquisar" OnClick="btnFiltrar_Click" /></div>

            </fieldset>

    <div class="margem">
        <%--<table>
            <tr>
                <td style="width:120px;">Tipo de relatório:</td>
                <td>
                    <asp:DropDownList ID="ddlTipoDeRelatorio" runat="server">
                            <asp:ListItem Value="" Text="Todos"></asp:ListItem>
                            <asp:ListItem Value="o" Text="Obesidade"></asp:ListItem>
                            <asp:ListItem Value="e" Text="Outras Enfermidades "></asp:ListItem>
                       </asp:DropDownList>
                </td>
                <td style="width:120px;">Produtos:</td><td><asp:DropDownList ID="ddlProdutos" runat="server" OnInit="ddlProdutos_Init"></asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width:120px;">Espécies:</td><td><asp:DropDownList ID="ddlEspecies" runat="server" OnInit="ddlEspecies_Init"></asp:DropDownList></td>
                <td style="width:120px;">Data de Inicio:</td><td><asp:TextBox ID="txtDataInicial" runat="server" CssClass="campo" MaxLength="10" Width="70"></asp:TextBox>  Data Final: <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campo" MaxLength="10" Width="70"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width:120px;">Palavra chave: </td><td><asp:TextBox ID="txtPesquisar" runat="server" AutoPostBack="True" Width="150"></asp:TextBox></td>
                <td style="width:120px;"><asp:Button ID="btnFiltrar" runat="server" Text="Pesquisar" OnClick="btnFiltrar_Click" /></td><td></td>
            </tr>
        </table>--%>



        <!-- Grid Inicio -->
        <asp:GridView ID="grdwListar" runat="server" Width="100%" PagerSettings-Visible="True"
            AutoGenerateColumns="false" AllowPaging="True" 
            CssClass="listagem" OnRowDataBound="grdwListar_RowDataBound"
            GridLines="None" onprerender="grdwListar_PreRender" AllowSorting="True" 
            onsorting="grdwListar_Sorting" onrowcreated="grdwListar_RowCreated">
            <Columns>
                <asp:TemplateField HeaderText="Tipo de Relatório" SortExpression="TipoDeRelatorio">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "TipoDeRelatorio")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="130px" />
                    <HeaderStyle HorizontalAlign="Left" Width="130px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Especies">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Especie")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"  Width="60px" />
                    <HeaderStyle HorizontalAlign="Left"  Width="60px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nome da Clinica" SortExpression="NomeClinica">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "NomeClinica")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="190px" />
                    <HeaderStyle HorizontalAlign="Left" Width="190px" />
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Nome do MV" SortExpression="NomeDoVeterinario">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "NomeDoVeterinario")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Alimento Seco">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "AlimentoSecoNome")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"  Width="200px" />
                    <HeaderStyle HorizontalAlign="Left"  Width="200px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Alimento Úmido">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "AlimentoUmidoNome")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"  Width="200px" />
                    <HeaderStyle HorizontalAlign="Left"  Width="200px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Data Inclusao" SortExpression="DataInclusao">
                    <ItemTemplate>
                        <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "DataInclusao")).ToString("dd/MM/yyyy HH:mm") %>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="130px" />
                    <HeaderStyle HorizontalAlign="Center" Width="130px" />
                </asp:TemplateField>
            </Columns>

            <PagerTemplate>
                <div class="total-de-registros" >
                    <asp:Literal ID="lblTotalDeIRegistros" runat="server"></asp:Literal>
                    registros
                </div>
                <div class="paginacao-setas">
                    <asp:ImageButton ID="btnPrimeiro" runat="server" AlternateText="Primeira página" ToolTip="Primeira página" 
                        ImageUrl="~/_admin/layout/imagens/ico-first-dark.png" 
                        onclick="btnPrimeiro_Click" />
                    <asp:ImageButton ID="btnAnterior" runat="server" AlternateText="Página anterior" ToolTip="Página anterior" 
                        ImageUrl="~/_admin/layout/imagens/ico-back-dark.png" 
                        onclick="btnAnterior_Click" />

                    <asp:TextBox ID="txtPaginaAtual" runat="server" Width="25" MaxLength="3"></asp:TextBox>
                    de
                    <asp:Literal ID="lblTotalDePaginas" runat="server"></asp:Literal>
                    <asp:Button ID="btnIr" runat="server" Text=" Ir " onclick="btnIr_Click" />

                    <asp:ImageButton ID="btnProximo" runat="server" AlternateText="Próxima página" ToolTip="Próxima página" 
                        ImageUrl="~/_admin/layout/imagens/ico-next-dark.png" 
                        onclick="btnProximo_Click" />
                    <asp:ImageButton ID="btnUltimo" runat="server" AlternateText="Ultima página" ToolTip="Ultima página" 
                        ImageUrl="~/_admin/layout/imagens/ico-last-dark.png" 
                        onclick="btnUltimo_Click" />
                </div>
            </PagerTemplate>

            <HeaderStyle CssClass="cabecalho" />
            <AlternatingRowStyle CssClass="registros" />
            <RowStyle CssClass="registros" />
            <PagerStyle CssClass="paginacao" />
        </asp:GridView>

        <!-- Grid Final -->
    </div>
</asp:Content>

