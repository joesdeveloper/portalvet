﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_produtos_doencas_registro : System.Web.UI.Page
{
    BLL.Produtos bPro = new BLL.Produtos();
    BLL.Doencas bDoe = new BLL.Doencas();
    VO.Doenca vDoe = new VO.Doenca();
    List<int> idCategorias = new List<int>();
    List<int> idProdutos = new List<int>();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }

    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vDoe.IdDoenca = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }

    private void IniciarFormulario()
    {
        vDoe = bDoe.Consultar(vDoe.IdDoenca);

        if (vDoe != null)
        {
            LimparDados();

            try
            {
                ddlEspecie.Items.FindByValue(vDoe.IdEspecie.ToString()).Selected = true;

                IniciarProdutos(vDoe.IdEspecie);
            }
            catch { }

            try
            {
                var lCat = bDoe.ListarPorDoenca(vDoe.IdDoenca);

                for (int i = 0; i < lCat.Count; i++)
                {
                    lstCategorias.Items.FindByValue(lCat[i].IdCategoria.ToString()).Selected = true;
                }
            }
            catch { }


            
                var lPro = bDoe.ListarProdutosPorDoenca(vDoe.IdDoenca);
            
                for (int i = 0; i < lPro.Count; i++)
                {
                    lstProdutos.Items.FindByValue(lPro[i].IdProduto.ToString()).Selected = true;
                }
            
            txtNome.Text = vDoe.Nome;
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        ddlEspecie.SelectedIndex = -1;
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
            vDoe.IdDoenca = Convert.ToInt32(Request["id"]);

        vDoe.Nome = txtNome.Text;
        vDoe.IdEspecie = Convert.ToInt32(ddlEspecie.SelectedValue);

        for (int i = 0; i < lstCategorias.Items.Count; i++)
        {
            if (lstCategorias.Items[i].Selected)
                idCategorias.Add(Convert.ToInt32(lstCategorias.Items[i].Value));
        }

        for (int i = 0; i < lstProdutos.Items.Count; i++)
        {
            if (lstProdutos.Items[i].Selected)
                idProdutos.Add(Convert.ToInt32(lstProdutos.Items[i].Value));
        }
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        bDoe.Salvar(vDoe, idCategorias, idProdutos);

        if (!Notificacao.Erro)
        {
            if (Request["id"] == null)
                Response.Redirect("registro.aspx?id=" + vDoe.IdDoenca);
        }

        ExibirMensagem();
    }



    protected void ddlEspecie_Init(object sender, EventArgs e)
    {
        var lista = new BLL.Especies().Listar();

        var idEspecie = 0;

        ddlEspecie.Items.Clear();
        foreach (var registro in lista)
        {
            if (idEspecie == 0)
                idEspecie = registro.IdEspecie;

            ddlEspecie.Items.Add(new ListItem(registro.Nome, registro.IdEspecie.ToString()));
        }

        IniciarProdutos(idEspecie);
    }

    protected void lstCategorias_Init(object sender, EventArgs e)
    {
        lstCategorias.Items.Clear();

        var lista = bPro.ListarCategorias();

        if (lista != null)
        {
            var listaAux = (from aux in lista
                            orderby aux.Nome
                            select aux).ToList();

            foreach (var registro in listaAux)
            {
                lstCategorias.Items.Add(new ListItem(registro.Nome, registro.IdCategoria.ToString()));
            }
        }
    }


    private void IniciarProdutos(int idEspecie)
    {
        lstProdutos.Items.Clear();

        var lista = bPro.Listar();

        if (lista != null)
        {
            var listaAux = (from aux in lista
                            where aux.Especie.IdEspecie == idEspecie
                            orderby aux.Nome
                            select aux).ToList();

            foreach (var registro in listaAux)
            {
                lstProdutos.Items.Add(new ListItem(registro.Nome + " ( " + registro.Tipo.Nome + " )", registro.IdProduto.ToString()));
            }
        }
    }

    protected void ddlEspecie_SelectedIndexChanged(object sender, EventArgs e)
    {
        IniciarProdutos(Convert.ToInt32(ddlEspecie.SelectedValue));
    }
}