﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_produtos_termos : System.Web.UI.UserControl
{
    BLL.Termos bTer = new BLL.Termos();
    int idProduto = 0;
    List<int> idTermos = new List<int>();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }

    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            idProduto = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        var lista = bTer.ListarPorProduto(idProduto);

        if (lista != null)
        {
            LimparDados();

            for (var i = 0; i < cbxTermos.Items.Count; i++)
            {
                var registro = (from aux in lista
                                where aux.IdTermo == Convert.ToInt32(cbxTermos.Items[i].Value)
                                select aux).ToList();

                if (registro.Count > 0)
                    cbxTermos.Items[i].Selected = true;
            }
        }
    }


    private void LimparDados()
    {
        cbxTermos.SelectedIndex = -1;
    }

    private void CaptarDados()
    {
        idProduto = Convert.ToInt32(Request["id"]);

        for (var i = 0; i < cbxTermos.Items.Count; i++)
        {
            if (cbxTermos.Items[i].Selected)
                idTermos.Add(Convert.ToInt32(cbxTermos.Items[i].Value));
        }
    }

    private bool ValidarDados()
    {
        if (Notificacao.Erro)
            return false;

        return true;
    }

    public void Salvar(int idProduto)
    {
        CaptarDados();

        if (ValidarDados())
        {
            bTer.SalvarPorProduto(idProduto, idTermos);
        }
    }

    protected void cbxTermos_Init(object sender, EventArgs e)
    {
        var lista = bTer.Pesquisar(0, 10000, "", "Nome", true);

        if (lista != null)
        {
            cbxTermos.Items.Clear();

            foreach (var registro in lista)
            {
                cbxTermos.Items.Add(new ListItem(registro.Nome, registro.IdTermo.ToString()));
            }
        }
    }
}