﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_produtos_registro" %>


<%@ Register src="geral.ascx" tagname="geral" tagprefix="uc1" %>
<%@ Register src="termos.ascx" tagname="termos" tagprefix="uc2" %>
<%@ Register src="enfermidades.ascx" tagname="enfermidades" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        $(document).ready(function () {
            $(".sombra").fancybox({
                maxWidth: 800,
                maxHeight: 600,
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <div class="aba-painel">
            <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" 
                onmenuitemclick="Menu1_MenuItemClick" CssClass="aba">
                <Items>
                    <asp:MenuItem Text="Geral" Value="Geral" Selected="true"></asp:MenuItem>
                    <%--<asp:MenuItem Text="Termos" Value="Termos"></asp:MenuItem>--%>
                    <%--<asp:MenuItem Text="Enfermidades" Value="Enfermidades"></asp:MenuItem>--%>
                </Items>
            </asp:Menu>
        </div>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="Geral" runat="server">
                <uc1:geral ID="geral1" runat="server" />
            </asp:View>
            <%--<asp:View ID="Termos" runat="server">
                <uc2:termos ID="termos1" runat="server" />
            </asp:View>--%>
<%--            <asp:View ID="Enfermidades" runat="server">
                <uc2:enfermidades ID="enfermidades1" runat="server" />
            </asp:View>--%>
        </asp:MultiView>
    </div>
</asp:Content>
