﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class _admin_modulos_produtos_geral : System.Web.UI.UserControl
{
    BLL.Doencas bDoe = new BLL.Doencas();
    BLL.Produtos bPro = new BLL.Produtos();
    VO.Produto vPro = new VO.Produto();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\produtos\";

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        txtInformacao.config.toolbar = new object[]
			{
				new object[] { "Source", "-", "NewPage", "Preview", "-", "Templates" },
				new object[] { "Cut", "Copy", "Paste", "PasteText", "PasteFromWord" },
				new object[] { "Undo", "Redo", "-", "Find", "Replace", "-", "SelectAll", "RemoveFormat" },
                new object[] { "Image", "Table", "HorizontalRule", "SpecialChar", "PageBreak", "Iframe" },
				"/",
				new object[] { "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				new object[] { "NumberedList", "BulletedList", "-", "Outdent", "Indent", "Blockquote", "CreateDiv" },
				new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				new object[] { "BidiLtr", "BidiRtl" },
				new object[] { "Link", "Unlink", "Anchor" },
				new object[] { "TextColor", "BGColor" },
                "/",
                new object[] { "Styles", "Format", "Font", "FontSize" }
			};

        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vPro.IdProduto = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
        else
        {
            txtIcones.Text =
                "<table>" +
                "	<tbody>" +
                "		<tr>" +
                "			<td><img alt=\"\" src=\"arquivos/imagens/ico.png\" /></td>" +
                "			<td style=\"text-align: justify;\">xxxxxxxxxxxxxxxxxxxxxx</td>" +
                "		</tr>" +
                "		<tr>" +
                "			<td><img alt=\"\" src=\"arquivos/imagens/ico.png\" /></td>" +
                "			<td style=\"text-align: justify;\">xxxxxxxxxxxxxxxxxxxxxx</td>" +
                "		</tr>" +
                "		<tr>" +
                "			<td><img alt=\"\" src=\"arquivos/imagens/ico.png\" /></td>" +
                "			<td style=\"text-align: justify;\">xxxxxxxxxxxxxxxxxxxxxx</td>" +
                "		</tr>" +
                "	</tbody>" +
                "</table>"
                ;

            txtInformacao.Text =
                "<div>" +
                "	<span class=\"titulo-produto\">INDICA&Ccedil;&Otilde;ES</span></div>" +
                "<div style=\"text-align: justify;\">" +
                    "xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx.</div>" +
                "<div>" +
                "	&nbsp;</div>" +
                "<div id=\"product_banner\">" +
                    "<span class=\"titulo-produto\">CONTRAINDICA&Ccedil;&Atilde;O</span></div>" +
                "<div style=\"text-align: justify;\">" +
                    "xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx.</div>" +
                "<div>" +
                "	&nbsp;</div>" +
                "<div>" +
                "	<span class=\"titulo-produto\">PER&Iacute;ODO DE UTILIZA&Ccedil;&Atilde;O</span></div>" +
                "<div style=\"text-align: justify;\">" +
                "xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx.<br />" +
                "<br />" +
                "<span class=\"titulo-produto\">RECOMENDA&Ccedil;&Otilde;ES E INFORMA&Ccedil;&Otilde;ES ADICIONAIS</span><br />" +
                "<span class=\"nota\">xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx.</span><br />" +
                "<br />" +
                "<span class=\"titulo-produto\">MODO DE USAR</span><br />" +
                "xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx.<br />" +
                "</div>" +
                "<div>" +
                "	<table class=\"tabela-produto\">" +
                        "<thead>" +
                "			<tr>" +
                "				<th colspan=\"2\" scope=\"col\">" +
                                    "N&Iacute;VEIS DE GARANTIA</th>" +
                            "</tr>" +
                        "</thead>" +
                        "<tbody>" +
                "			<tr>" +
                "				<td>" +
                "					Umidade (m&aacute;x.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Prote&iacute;na bruta (m&iacute;n.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Extrato et&eacute;reo (m&iacute;n.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Mat&eacute;ria fibrosa (m&aacute;x.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Mat&eacute;ria mineral (m&aacute;x.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					C&aacute;lcio (m&iacute;n.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					C&aacute;lcio (m&aacute;x.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					F&oacute;sforo (m&iacute;n.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					S&oacute;dio (m&iacute;n.)</td>" +
                                "<td style=\"text-align: center;\">" +
                                    "&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Cloro (m&iacute;n.)</td>" +
                                "<td>" +
                "					&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Pot&aacute;ssio (m&iacute;n.)</td>" +
                                "<td>" +
                "					&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Magn&eacute;sio (m&iacute;n.)</td>" +
                                "<td>" +
                "					&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Taurina (m&iacute;n)</td>" +
                                "<td>" +
                "					&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					L-carnitina (m&iacute;n.)</td>" +
                                "<td>" +
                "					&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td>" +
                "					Metionina (m&iacute;n.)</td>" +
                                "<td>" +
                "					&nbsp;</td>" +
                            "</tr>" +
                            "<tr>" +
                "				<td colspan=\"2\" style=\"text-align: center;\">" +
                                    "Energia metaboliz&aacute;vel: <strong>0000 kcal/kg</strong></td>" +
                            "</tr>" +
                        "</tbody>" +
                    "</table>" +
                "</div>" +
                "<p>" +
                "	&nbsp;</p>" +
                "<table class=\"tabela-produto\">" +
                    "<thead>" +
                "		<tr>" +
                "			<th colspan=\"2\" scope=\"col\">" +
                                "ENRIQUECIMENTO POR KILOGRAMA (m&iacute;n.)</th>" +
                        "</tr>" +
                    "</thead>" +
                    "<tbody>" +
                "		<tr>" +
                "			<td>" +
                "				Vitamina A</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Vitamina D3</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Vitamina E</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Vitamina C</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Vitamina B1</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Vitamina B2</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Vitamina B6</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Vitamina B12</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Vitamina PP</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				&Aacute;cido pantot&ecirc;nico</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				&Aacute;cido f&oacute;lico</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Biotina</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Colina</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Cobre</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Ferro</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Mangan&ecirc;s</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Iodo</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Zinco</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				Sel&ecirc;nio</td>" +
                            "<td>" +
                "				&nbsp;</td>" +
                        "</tr>" +
                    "</tbody>" +
                "</table>" +
                "<p>" +
                "	&nbsp;</p>" +
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"tabela-produto\">" +
                    "<tbody>" +
                "		<tr>" +
                "			<td colspan=\"4\" style=\"text-align: center; background-color: rgb(244, 244, 244);\">" +
                                "<h3>" +
                "					Quantidades di&aacute;rias recomendadas (g/dia):</h3>" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td colspan=\"4\" style=\"text-align: center; background-color: rgb(181, 205, 54);\">" +
                                "<h1>" +
                "					<span style=\"color:#ffffff;\">XXXXX XXXXX</span></h1>" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td>" +
                "				<p style=\"text-align: center;\">" +
                                    "<img alt=\"\" src=\"arquivos/imagens/ico-cao-peso-do-cao.png\" /><br />" +
                                    "Peso do<br />" +
                                    "c&atilde;o (kg)</p>" +
                            "</td>" +
                            "<td style=\"text-align: center;\">" +
                                "<img alt=\"\" src=\"arquivos/imagens/ico-cao-peso-magro.png\" /><br />" +
                                "Magro<br />" +
                                "(g)</td>" +
                            "<td style=\"text-align: center;\">" +
                                "<img alt=\"\" src=\"arquivos/imagens/ico-cao-peso-ideal.png\" /><br />" +
                                "Peso ideal<br />" +
                                "(g)</td>" +
                            "<td style=\"text-align: center;\">" +
                                "<img alt=\"\" src=\"arquivos/imagens/ico-cao-peso-sobrepeso.png\" /><br />" +
                                "Sobrepeso<br />" +
                                "(g)</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "2</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "4</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "6</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "8</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "10</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "15</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "20</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "25</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "30</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "35</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "40</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "50</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "60</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                "			<td style=\"text-align: center;\">" +
                                "70</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                            "<td style=\"text-align: center;\">" +
                                "&nbsp;</td>" +
                        "</tr>" +
                    "</tbody>" +
                "</table>" +
                "<p style=\"text-align: justify;\">" +
                    "<br />" +
                    "<span class=\"titulo-produto\">COMPOSI&Ccedil;&Atilde;O B&Aacute;SICA</span><br />" +
                    "xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx.<br />" +
                    "<span class=\"citacao\">* xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxxxxxxxxx xxxxxxx xxxxxxx xxxxxxx xxxxxxx.</span></p>"
                ;

        }
    }


    private void IniciarFormulario()
    {
        vPro = bPro.Consultar(vPro.IdProduto);

        if (vPro != null)
        {
            LimparDados();

            ddlEspecie.Items.FindByValue(vPro.Especie.IdEspecie.ToString()).Selected = true;
            ddlOutraVersao.Items.FindByValue(vPro.IdOutraVersao.ToString()).Selected = true;
            txtNome.Text = vPro.Nome;
            txtIcones.Text = vPro.Icones;
            txtDescricao.Text = vPro.Descricao;
            txtEmbalagens.Text = vPro.Embalagem;
            txtInformacao.Text = vPro.Informacao;
            txtEnergiaMetabolizavel.Text = vPro.EnergiaMetabolizavel.ToString();

            txtFatorSobrepeso.Text = vPro.FatorSobrepeso.ToString();
            txtFatorPesoideial.Text = vPro.FatorPesoideial.ToString();
            txtFatorMagro.Text = vPro.FatorMagro.ToString();
            txtIndiceDeCalculo.Text = vPro.IndiceDeCalculo.ToString();

            //ddlCategoria.Items.FindByValue(vPro.Categoria.IdCategoria.ToString()).Selected = true;
            ddlTipo.Items.FindByValue(vPro.Tipo.IdTipo.ToString()).Selected = true;
            cbxObesidade.Checked = vPro.Obesidade;
            txtCor.Text = vPro.Cor;
            cbxAtivo.Checked = vPro.Ativo;
            ddlLocalExibicao.Items.FindByValue(vPro.LocalExibicao).Selected = true;
            txtTabelaQuantidadeDiaria.Text = vPro.TabelaQuantidadeDiaria;

            //ListarEnfermidades(vPro.Categoria.IdCategoria, vPro.Especie.IdEspecie);
            ListarCategorias(vPro.Especie.IdEspecie);
            ListarEnfermidades(vPro.Especie.IdEspecie);

            try
            {
                foreach (var registro in vPro.Categorias)
                {
                    lstCategorias.Items.FindByValue(registro.IdCategoria.ToString()).Selected = true;
                }
            }
            catch { }

            try
            {
                foreach (var registro in vPro.Doenca)
                {
                    lstEnfermidades.Items.FindByValue(registro.IdDoenca.ToString()).Selected = true;
                }
            }
            catch { }
        }
    }


    private void LimparDados()
    {
        ddlOutraVersao.SelectedIndex = -1;
        //ddlCategoria.SelectedIndex = -1;
        ddlEspecie.SelectedIndex = -1;
        txtNome.Text = "";
        txtIcones.Text = "";
        txtDescricao.Text = "";
        txtEmbalagens.Text = "";
        txtInformacao.Text = "";
        txtEnergiaMetabolizavel.Text = "";
        txtFatorSobrepeso.Text = "";
        txtFatorPesoideial.Text = "";
        txtFatorMagro.Text = "";
        txtIndiceDeCalculo.Text = "";
        ddlTipo.SelectedIndex = -1;
        cbxObesidade.Checked = false;
        txtCor.Text = "";
        cbxAtivo.Checked = false;
        lstCategorias.SelectedIndex = -1;
        lstEnfermidades.SelectedIndex = -1;
        ddlLocalExibicao.SelectedIndex = -1;
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
            vPro.IdProduto = Convert.ToInt32(Request["id"]);

        vPro.Especie = new VO.Especie();
        vPro.Especie.IdEspecie = Convert.ToInt32(ddlEspecie.SelectedValue);

        //vPro.Categoria = new VO.ProdutoCategoria();
        //vPro.Categoria.IdCategoria = Convert.ToInt32(ddlCategoria.SelectedValue);

        vPro.IdOutraVersao = Convert.ToInt32(ddlOutraVersao.SelectedValue);
        vPro.Nome = txtNome.Text;
        vPro.Icones = txtIcones.Text;
        vPro.Descricao = txtDescricao.Text;
        vPro.Embalagem = txtEmbalagens.Text;
        vPro.Informacao = txtInformacao.Text;
        try
        {
            vPro.EnergiaMetabolizavel = Convert.ToInt32(txtEnergiaMetabolizavel.Text);
        }
        catch { }
        try
        {
            vPro.FatorSobrepeso = Convert.ToDecimal(txtFatorSobrepeso.Text);
        }
        catch { }
        try
        {
            vPro.FatorPesoideial = Convert.ToDecimal(txtFatorPesoideial.Text);
        }
        catch { }
        try
        {
            vPro.FatorMagro = Convert.ToDecimal(txtFatorMagro.Text);
        }
        catch { }
        try
        {
            vPro.IndiceDeCalculo = Convert.ToDecimal(txtIndiceDeCalculo.Text);
        }
        catch { }

        vPro.Tipo = new VO.ProdutoTipo();
        vPro.Tipo.IdTipo = ddlTipo.SelectedValue;
        vPro.Obesidade = cbxObesidade.Checked;
        vPro.Cor = txtCor.Text;
        vPro.Ativo = cbxAtivo.Checked;
        vPro.LocalExibicao = ddlLocalExibicao.SelectedValue;
        vPro.TabelaQuantidadeDiaria = txtTabelaQuantidadeDiaria.Text;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    //protected void lnkSalvar_Click(object sender, EventArgs e)
    public VO.Produto Salvar()
    {
        CaptarDados();

        vPro = bPro.Salvar(vPro);

        SalvarCategorias(vPro.IdProduto);
        SalvarEnfermidades(vPro.IdProduto);

        if (!Notificacao.Erro)
        {
            Upload();

            if (Request["id"] == null)
                Response.Redirect("registro.aspx?id=" + vPro.IdProduto);

            IniciarPagina();
        }

        return vPro;
        //ExibirMensagem();
    }


    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        CaptarDados();

        _erro = @"Foto foi excluida com sucesso.";

        Notificacao.Erro = false;
        Notificacao.Status = Notificacao.Tipo.Sucesso;
        Notificacao.Texto = _erro;

        if (File.Exists(caminho + vPro.IdProduto + ".png"))
            File.Delete(caminho + vPro.IdProduto + ".png");

        IniciarPagina();

        ExibirMensagem();
    }


    private void SalvarCategorias(int idProduto)
    {
        var idCategoria = new List<int>();

        for (var i = 0; i < lstCategorias.Items.Count; i++)
        {
            if (lstCategorias.Items[i].Selected)
                idCategoria.Add(Convert.ToInt32(lstCategorias.Items[i].Value));
        }

        bPro.SalvarCategorias(idProduto, idCategoria);
    }


    private void SalvarEnfermidades(int idProduto)
    {
        var IdDoencas = new List<int>();

        for (var i = 0; i < lstEnfermidades.Items.Count; i++)
        {
            if (lstEnfermidades.Items[i].Selected)
                IdDoencas.Add(Convert.ToInt32(lstEnfermidades.Items[i].Value));
        }

        bDoe.Salvar(idProduto, IdDoencas);
    }


    private void Upload()
    {
        var _nome = "";
        var _extensao = "";
        var caminhoAux = caminho + vPro.IdProduto + @"\";

        if (!Directory.Exists(caminhoAux))
            Directory.CreateDirectory(caminhoAux);

        if (uplLogo.HasFile)
        {
            _nome = uplLogo.FileName;
            _extensao = Path.GetExtension(_nome).ToLower();

            uplLogo.PostedFile.SaveAs(caminhoAux + "t-logo" + _extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminhoAux + "t-logo" + _extensao, caminhoAux + "logo.png", 200, 100, UTIL.EdicaoDeImagem.Formato.Png);

            try
            {
                File.Delete(caminhoAux + "t-logo" + _extensao);
            }
            catch { }
        }


        if (uplFoto.HasFile)
        {
            _nome = uplFoto.FileName;
            _extensao = Path.GetExtension(_nome).ToLower();

            uplFoto.PostedFile.SaveAs(caminhoAux + "t-foto" + _extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminhoAux + "t-foto" + _extensao, caminhoAux + "foto.png", 114, 300, UTIL.EdicaoDeImagem.Formato.Png);

            try
            {
                File.Delete(caminhoAux + "t-foto" + _extensao);
            }
            catch { }
        }

        if (uplPetisco.HasFile)
        {
            _nome = uplPetisco.FileName;
            _extensao = Path.GetExtension(_nome).ToLower();

            uplPetisco.PostedFile.SaveAs(caminhoAux + "t-petisco" + _extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminhoAux + "t-petisco" + _extensao, caminhoAux + "petisco.png", 300, 300, UTIL.EdicaoDeImagem.Formato.Png);

            try
            {
                File.Delete(caminhoAux + "t-petisco" + _extensao);
            }
            catch { }
        }

        IniciarPagina();
    }


    #region Inicializacao
    protected void ddlEspecie_Init(object sender, EventArgs e)
    {
        var lista = new BLL.Especies().Listar();

        if (lista != null)
        {
            var listaAux = (from aux in lista
                            orderby aux.Nome
                            select aux).ToList();

            ddlEspecie.Items.Clear();

            foreach (var registro in listaAux)
            {
                ddlEspecie.Items.Add(new ListItem(registro.Nome, registro.IdEspecie.ToString()));
            }


            ListarCategorias(Convert.ToInt32(ddlEspecie.Items[0].Value));
            ListarEnfermidades(Convert.ToInt32(ddlEspecie.Items[0].Value));
        }
    }

    protected void ddlEspecie_SelectedIndexChanged(object sender, EventArgs e)
    {
        var idEspecie = Convert.ToInt32(ddlEspecie.SelectedItem.Value);
        ListarCategorias(idEspecie);
        ListarEnfermidades(idEspecie);
    }


    //protected void ddlCategoria_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    var idCategoria = Convert.ToInt32(ddlCategoria.SelectedItem.Value);
    //    ListarEnfermidades(idCategoria, Convert.ToInt32(ddlEspecie.SelectedItem.Value));
    //}


    protected void ListarCategorias(int idEspecie)
    {
        lstCategorias.Items.Clear();

        var lista = bPro.ListarCategorias();

        if (lista != null)
        {
            var listaAux = (from aux in lista
                            //where aux.IdEspecie == idEspecie
                            orderby aux.Nome
                            select aux).ToList();

            foreach (var registro in listaAux)
            {
                lstCategorias.Items.Add(new ListItem(registro.Nome, registro.IdCategoria.ToString()));
            }

            //ListarEnfermidades(Convert.ToInt32(ddlCategoria.Items[0].Value), idEspecie);
        }
    }


    //protected void ListarEnfermidades(int idCategoria, int idEspecie)
    //{
    //    var lista = bDoe.ListarPorCategoria(idCategoria);

    //    if (lista != null)
    //    {
    //        var listarAux = (from aux in lista
    //                         where aux.IdEspecie == idEspecie
    //                         select aux);

    //        lstEnfermidades.Items.Clear();

    //        foreach (var registro in listarAux)
    //        {
    //            lstEnfermidades.Items.Add(new ListItem(registro.Nome, registro.IdDoenca.ToString()));
    //        }
    //    }
    //}

    protected void ListarEnfermidades(int idEspecie)
    {
        var lista = bDoe.Pesquisar(0, 10000, "", "Nome", true);

        if (lista != null)
        {
            var listarAux = (from aux in lista
                             where aux.IdEspecie == idEspecie
                             orderby aux .Nome
                             select aux);

            lstEnfermidades.Items.Clear();

            foreach (var registro in listarAux)
            {
                lstEnfermidades.Items.Add(new ListItem(registro.Nome, registro.IdDoenca.ToString()));
            }
        }
    }

    protected void ddlTipo_Init(object sender, EventArgs e)
    {
        var lista = new BLL.Produtos().ListarTipo();

        if (lista != null)
        {
            var listaAux = (from aux in lista
                            orderby aux.Nome
                            select aux).ToList();

            ddlTipo.Items.Clear();

            foreach (var registro in listaAux)
            {
                ddlTipo.Items.Add(new ListItem(registro.Nome, registro.IdTipo));
            }
        }
    }


    protected void ddlOutraVersao_Init(object sender, EventArgs e)
    {
        var idProduto = -1;

        if (Request["id"] != null)
            idProduto = Convert.ToInt32(Request["id"]);

        var lista = bPro.Listar();

        if (lista != null)
        {
            var listaAux = (from aux in lista
                            orderby aux.Nome
                            select aux).ToList();

            ddlOutraVersao.Items.Clear();
            ddlOutraVersao.Items.Add(new ListItem("Versão Única", "0"));

            foreach (var registro in listaAux)
            {
                if (idProduto != registro.IdProduto)
                    ddlOutraVersao.Items.Add(new ListItem(registro.Nome, registro.IdProduto.ToString()));
            }
        }
    }


    #endregion
}