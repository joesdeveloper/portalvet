﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_produtos_registro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
        int i = 0;
        for (i = 0; i < Menu1.Items.Count; i++)
        {
            if (Menu1.Items[i].Selected)
                break;
        }
        MultiView1.ActiveViewIndex = i;
    }

    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        Notificacao.Erro = false;
        Notificacao.Status = Notificacao.Tipo.Sucesso;
        Notificacao.Texto = "Registro salvo com sucesso";
        
        var vPro = geral1.Salvar();

        //termos1.Salvar(vPro.IdProduto);
        //enfermidades1.Salvar(vPro.IdProduto);

        if (Request["id"] == null && vPro.IdProduto > 0)
            Response.Redirect("registro.aspx?id=" + vPro.IdProduto);

        ExibirMensagem();
    }
}