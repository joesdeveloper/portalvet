﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_produtos_enfermidades : System.Web.UI.UserControl
{
    BLL.Doencas bDoe = new BLL.Doencas();
    int IdProduto = 0;
    List<int> IdDoencas = new List<int>();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }

    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            IdProduto = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        var lista = bDoe.ListarPorProduto(IdProduto);

        if (lista != null)
        {
            LimparDados();

            for (var i = 0; i < cbxEnfermidades.Items.Count; i++)
            {
                var registro = (from aux in lista
                                where aux.IdDoenca == Convert.ToInt32(cbxEnfermidades.Items[i].Value)
                                select aux).ToList();

                if (registro.Count > 0)
                    cbxEnfermidades.Items[i].Selected = true;
            }
        }
    }


    private void LimparDados()
    {
        cbxEnfermidades.SelectedIndex = -1;
    }

    private void CaptarDados()
    {
        IdProduto = Convert.ToInt32(Request["id"]);

        for (var i = 0; i < cbxEnfermidades.Items.Count; i++)
        {
            if (cbxEnfermidades.Items[i].Selected)
                IdDoencas.Add(Convert.ToInt32(cbxEnfermidades.Items[i].Value));
        }
    }

    private bool ValidarDados()
    {
        if (Notificacao.Erro)
            return false;

        return true;
    }

    public void Salvar(int IdProduto)
    {
        CaptarDados();

        if (ValidarDados())
        {
            bDoe.Salvar(IdProduto, IdDoencas);
        }
    }


    protected void cbxEnfermidades_Init(object sender, EventArgs e)
    {
        var lista = bDoe.Pesquisar(0, 10000, "", "Nome", true);

        if (lista != null)
        {
            cbxEnfermidades.Items.Clear();

            foreach (var registro in lista)
            {
                cbxEnfermidades.Items.Add(new ListItem(registro.Nome, registro.IdDoenca.ToString()));
            }
        }
    }
}