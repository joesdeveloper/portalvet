﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_produtos_categorias_registro : System.Web.UI.Page
{
    BLL.Produtos bPro = new BLL.Produtos();
    VO.ProdutoCategoria vProCat = new VO.ProdutoCategoria();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }

    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vProCat.IdCategoria = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }

    private void IniciarFormulario()
    {
        vProCat = bPro.ConsultarCategoria(vProCat.IdCategoria);

        if (vProCat != null)
        {
            LimparDados();

            txtNome.Text = vProCat.Nome;
            txtClasse.Text = vProCat.Classe;
            cbxObesidade.Checked = vProCat.Obesidade ;
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        txtClasse.Text = "";
        cbxObesidade.Checked = false;
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
            vProCat.IdCategoria = Convert.ToInt32(Request["id"]);

        vProCat.Nome = txtNome.Text;
        vProCat.Classe = txtClasse.Text;
        vProCat.Obesidade = cbxObesidade.Checked;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        bPro.SalvarCategorias(vProCat);

        if (!Notificacao.Erro)
        {
            if (Request["id"] == null)
                Response.Redirect("registro.aspx?id=" + vProCat.IdCategoria);
        }

        ExibirMensagem();
    }
}