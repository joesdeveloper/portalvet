﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_produtos_categorias_registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo_direito" Runat="Server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <table class="formulario">
            <tr>
                <td class="texto">
                    Nome: 
                </td>
                <td>
                    <asp:TextBox ID="txtNome" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Classe CSS: 
                </td>
                <td>
                    <asp:TextBox ID="txtClasse" runat="server" CssClass="campo" MaxLength="80" 
                        Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Prod. para Obesidade:
                </td>
                <td>
                    <asp:CheckBox ID="cbxObesidade" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

