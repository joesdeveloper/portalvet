﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="geral.ascx.cs" Inherits="_admin_modulos_produtos_geral" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

    <div class="margem">
        <table class="formulario">
            <tr>
                <td class="texto">Espécie:</td>
                <td>
                    <asp:DropDownList ID="ddlEspecie" runat="server" oninit="ddlEspecie_Init" 
                        onselectedindexchanged="ddlEspecie_SelectedIndexChanged" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">Categoria:</td>
                <td>
                    <%--<asp:DropDownList ID="ddlCategoria" runat="server" CssClass="campo-curto" 
                        AutoPostBack="True" 
                        onselectedindexchanged="ddlCategoria_SelectedIndexChanged">
                    </asp:DropDownList>--%>
                    <asp:ListBox ID="lstCategorias" runat="server" Rows="10" Width="100%"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Enfermidades:</td>
                <td>
                    <asp:ListBox ID="lstEnfermidades" runat="server" Rows="10" Width="100%" 
                        SelectionMode="Multiple"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Cor Padrão:</td>
                <td>
                    <asp:TextBox ID="txtCor" runat="server" CssClass="campo" MaxLength="6" Width="80px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Nome: 
                </td>
                <td>
                    <asp:TextBox ID="txtNome" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Outra versão:</td>
                <td>
                    <asp:DropDownList ID="ddlOutraVersao" runat="server" 
                        oninit="ddlOutraVersao_Init">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Logo do produto:</td>
                <td>
                    <asp:FileUpload ID="uplLogo" runat="server" />
                    <br />(Tamanho: 200x40px)
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Foto do Produto:</td>
                <td>
                    <asp:FileUpload ID="uplFoto" runat="server" />
                    <br />(Tamanho: 115x240px)
                    <%--<a class="sombra" id="lnkFotoDoProduto" data-fancybox-type="iframe" data-id="23" href="../imagens/selecionar.aspx?id=conteudo_idFotoDoProduto&url=conteudo_urlFotoDeProduto&img=conteudo_lnkFotoDoProduto" runat="server"></a>
                    <asp:HiddenField ID="idFotoDoProduto" runat="server" />
                    <asp:HiddenField ID="urlFotoDeProduto" runat="server" />--%>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Foto do Petisco:</td>
                <td>
                    <asp:FileUpload ID="uplPetisco" runat="server" />
                    <br />(Tamanho: 100x100px)
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Tamanho das embalagens:</td>
                <td>
                    <asp:TextBox ID="txtEmbalagens" runat="server" CssClass="campo" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Descrição:</td>
                <td>
                    <asp:TextBox ID="txtDescricao" runat="server" CssClass="campo" MaxLength="60"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Ícones:</td>
                <td>
                    <CKEditor:CKEditorControl ID="txtIcones" runat="server" Width="95%" Height="200px" Language="pt"></CKEditor:CKEditorControl>
                </td>
            </tr>
            <tr>
                <td class="texto">Informações:</td>
                <td>
                    <CKEditor:CKEditorControl ID="txtInformacao" runat="server" Width="95%" Height="350px" Language="pt"></CKEditor:CKEditorControl>
                </td>
            </tr>
            <tr>
                <td class="texto">Tabela Qtd Diária Recomendada (g/dia):</td>
                <td>
                    <CKEditor:CKEditorControl ID="txtTabelaQuantidadeDiaria" runat="server" Width="95%" Height="350px" Language="pt"></CKEditor:CKEditorControl>
                </td>
            </tr>
            <tr>
               <td class="texto">
                    En. Metab.:  
                </td>
                <td>
                    <asp:TextBox ID="txtEnergiaMetabolizavel" runat="server" CssClass="campo" MaxLength="8" 
                        Width="70"></asp:TextBox>
                </td>
            </tr>
            <tr>
               <td class="texto">
                    Fator Sobrepeso:  
                </td>
                <td>
                    <asp:TextBox ID="txtFatorSobrepeso" runat="server" CssClass="campo" MaxLength="8" 
                        Width="70"></asp:TextBox>
                </td>
            </tr>
            <tr>
               <td class="texto">
                    Fator Peso Ideal:  
                </td>
                <td>
                    <asp:TextBox ID="txtFatorPesoideial" runat="server" CssClass="campo" MaxLength="8" 
                        Width="70"></asp:TextBox>
                </td>
            </tr>
            <tr>
               <td class="texto">
                    Fator Magro:  
                </td>
                <td>
                    <asp:TextBox ID="txtFatorMagro" runat="server" CssClass="campo" MaxLength="8" 
                        Width="70"></asp:TextBox>
                </td>
            </tr>
            <tr>
               <td class="texto">
                    Índice Calculo:  
                </td>
                <td>
                    <asp:TextBox ID="txtIndiceDeCalculo" runat="server" CssClass="campo" MaxLength="8" 
                        Width="70"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Tipo:</td>
                <td>
                    <asp:DropDownList ID="ddlTipo" runat="server" oninit="ddlTipo_Init">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Obesidade:
                </td>
                <td>
                    <asp:CheckBox ID="cbxObesidade" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Exibir em:
                </td>
                <td>
                    <asp:DropDownList ID="ddlLocalExibicao" runat="server" AutoPostBack="True">
                        <asp:ListItem Text="Todos as páginas" Value="t"></asp:ListItem>
                        <asp:ListItem Text="Apenas no aplicativo de indicação" Value="a"></asp:ListItem>
                        <asp:ListItem Text="Apenas na página de produtos" Value="p"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Ativo:
                </td>
                <td>
                    <asp:CheckBox ID="cbxAtivo" runat="server" />
                </td>
            </tr>
        </table>
    </div>
