﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_artigos_autores_registro : System.Web.UI.Page
{
    BLL.Autores bAut = new BLL.Autores();
    VO.Autor vAut = new VO.Autor();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vAut.IdAutor = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vAut = bAut.Consultar(vAut.IdAutor);

        if (vAut != null)
        {
            LimparDados();

            txtAssinatura.Text = vAut.Assinatura;
            txtNome.Text = vAut.Nome;
            txtEmail.Text = vAut.Email;
            txtCurriculo.Text = vAut.Curriculo;
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        txtAssinatura.Text = "";
        txtEmail.Text = "";
        txtCurriculo.Text = "";
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
        {
            vAut.IdAutor = Convert.ToInt32(Request["id"]);
        }


        vAut.Assinatura = txtAssinatura.Text;
        vAut.Nome = txtNome.Text;
        vAut.Email = txtEmail.Text;
        vAut.Curriculo = txtCurriculo.Text;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        vAut = bAut.Salvar(vAut);

        if (!Notificacao.Erro)
        {
            if (Request["id"] == null)
                Response.Redirect("registro.aspx?id=" + vAut.IdAutor);
        }

        ExibirMensagem();
    }
}