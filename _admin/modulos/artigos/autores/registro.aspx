﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_artigos_autores_registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <table class="formulario">
            <tr>
                <td class="texto">Assinatura:</td>
                <td><asp:TextBox ID="txtAssinatura" runat="server" CssClass="campo" required MaxLength="80"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">Nome:</td>
                <td><asp:TextBox ID="txtNome" runat="server" CssClass="campo" required MaxLength="80"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">Email:</td>
                <td><asp:TextBox ID="txtEmail" runat="server" CssClass="campo" required MaxLength="80"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">Mini Curriculo:</td>
                <td><asp:TextBox ID="txtCurriculo" runat="server" CssClass="campo" required MaxLength="50" TextMode="MultiLine" Rows="6"></asp:TextBox></td>
            </tr>
        </table>
    </div>
</asp:Content>