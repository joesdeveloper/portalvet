﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Drawing;

public partial class _admin_modulos_artigos_geral : System.Web.UI.UserControl
{
    BLL.Artigos bArt = new BLL.Artigos();
    VO.Artigo vArt = new VO.Artigo();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\artigos\";

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        txtTexto.config.toolbar = new object[]
			{
				new object[] { "Source", "-", "NewPage", "Preview", "-", "Templates" },
				new object[] { "Cut", "Copy", "Paste", "PasteText", "PasteFromWord" },
				new object[] { "Undo", "Redo", "-", "Find", "Replace", "-", "SelectAll", "RemoveFormat" },
                new object[] { "Image", "Table", "HorizontalRule", "SpecialChar", "PageBreak", "Iframe" },
				"/",
				new object[] { "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				new object[] { "NumberedList", "BulletedList", "-", "Outdent", "Indent", "Blockquote", "CreateDiv" },
				new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				new object[] { "BidiLtr", "BidiRtl" },
				new object[] { "Link", "Unlink", "Anchor" },
				new object[] { "TextColor", "BGColor" },
                "/",
                new object[] { "Styles", "Format", "Font", "FontSize" }
			};

        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vArt.IdArtigo = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vArt = bArt.Consultar(vArt.IdArtigo);

        if (vArt != null)
        {
            LimparDados();

            //if (vArt.ArtigoImagem != null)
            //{
            //    idFotoDeDestaque.Value = vArt.ArtigoImagem.IdImagem.ToString();
            //    urlFotoDeDestaque.Value = oVal.UrlBase() + vArt.ArtigoImagem.Url;
            //}

            //if (vArt.ArtigoImagem != null)
            //    lnkRemover.Visible = true;

            lblImagem.Text = "<div class=\"imagem\" style=\"background-image:url('" + oVal.UrlBase() + "arquivos/artigos/" + vArt.IdArtigo + "/imagem.png');\" data-url=\"" + oVal.UrlBase() + "arquivos/artigos/" + vArt.IdArtigo + "/imagem.png\"></div>";
            //txtUrlParcial.Text = "arquivos/vitrines/" + vVit.IdVitrine + ".png";
            //txtUrlCompleta.Text = oVal.UrlBase() + "arquivos/vitrines/" + vVit.IdVitrine + ".png";
            try
            {
                var bmp = new Bitmap(Server.MapPath("~/arquivos/artigos/" + vArt.IdArtigo + "/imagem.png"));
                lblInformacoes.Text =
                    "<div class=\"imagens-info\"><strong>Nome do arquivo:</strong> " + Path.GetFileName("~/arquivos/artigos/" + vArt.IdArtigo + "/imagem.png") + "</div>" +
                    "<div class=\"imagens-info\"><strong>Tipo do arquivo:</strong> " + Path.GetExtension("~/arquivos/artigos/" + vArt.IdArtigo + "/imagem.png") + "</div>" +
                    "<div class=\"imagens-info\"><strong>Data de upload:</strong> " + vArt.DataPublicacao.ToString("dd/MM/yyyy HH:mm:ss") + "</div>" +
                    "<div class=\"imagens-info\"><strong>Dimensões:</strong> " + bmp.Width + " x " + bmp.Height + "</div>" +
                    "<div class=\"imagens-info\"><strong>Url parcial:</strong> arquivos/artigos/" + vArt.IdArtigo + "/imagem.png" + "</div>" +
                    "<div class=\"imagens-info\"><strong>Url completa:</strong> " + oVal.UrlBase() + "arquivos/artigos/" + vArt.IdArtigo + "/imagem.png" + "</div>";
            }
            catch { }

            try
            {
                ddlRelacionamento.Items.FindByValue(vArt.Relacionamento.IdCategoria.ToString()).Selected = true;
            }
            catch { }

            ddlTipo.Items.FindByValue(vArt.Tipo.IdTipo.ToString()).Selected = true;
            SetarTipo(vArt.Tipo.IdTipo);

            try
            {
                ddlAutor.Items.FindByValue(vArt.Autor.IdAutor.ToString()).Selected = true;
            }
            catch { }

            try
            {
                ddlCategoria.Items.FindByValue(vArt.IdCategoria.ToString()).Selected = true;
            }
            catch { }

            txtTitulo.Text = vArt.Titulo;
            txtTexto.Text = vArt.Texto;
            cbxDestaque.Checked = vArt.Destaque;
            cbxAtivo.Checked = vArt.Ativo;
            txtDataPublicacao.Text = vArt.DataPublicacao.ToString();
        }
    }


    private void LimparDados()
    {
        ddlRelacionamento.SelectedIndex = -1;
        ddlTipo.SelectedIndex = -1;
        ddlAutor.SelectedIndex = -1;
        ddlCategoria.SelectedIndex = -1;
        txtTitulo.Text = "";
        txtTexto.Text = "";
        cbxDestaque.Checked = false;
        cbxAtivo.Checked = false;
        txtDataPublicacao.Text = DateTime.Now.ToString();
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
        {
            vArt.IdArtigo = Convert.ToInt32(Request["id"]);
        }

        vArt.Relacionamento = new VO.ProdutoCategoria();
        vArt.Relacionamento.IdCategoria = Convert.ToInt32(ddlRelacionamento.SelectedValue);

        vArt.Tipo = new VO.ArtigoTipo();
        vArt.Tipo.IdTipo = Convert.ToInt32(ddlTipo.SelectedValue);

        vArt.Autor = new VO.Autor();
        vArt.Autor.IdAutor = Convert.ToInt32(ddlAutor.SelectedValue);

        vArt.IdCategoria = Convert.ToInt32(ddlCategoria.SelectedValue);

        vArt.Titulo = txtTitulo.Text;
        vArt.Texto = txtTexto.Text;
        vArt.Destaque = cbxDestaque.Checked;
        vArt.Ativo = cbxAtivo.Checked;

        try
        {
            vArt.DataPublicacao = Convert.ToDateTime(txtDataPublicacao.Text);
        }
        catch { vArt.DataPublicacao = DateTime.MaxValue; }
    }


    private bool ValidarDados()
    {
        if (Notificacao.Erro)
            return false;
        
        if (vArt.IdArtigo == 0 && !uplImagem.HasFile)
            _erro += @"- Imagem não pode ser nula.<br />";

        if (vArt.Titulo.Trim().Length == 0)
            _erro += @"- Titulo não pode ser nulo.<br />";

        if (vArt.Texto.Trim().Length == 0)
            _erro += @"- Texto não pode ser nulo.<br />";

        if (vArt.DataPublicacao.ToString() == DateTime.MaxValue.ToString())
            _erro += @"- Data Publicação é inválida.<br />";
        

        if (_erro.Length > 0)
        {
            Notificacao.Erro = true;
            Notificacao.Status = Notificacao.Tipo.Atencao;
            Notificacao.Texto = _erro;

            return false;
        }

        return true;
    }


    public VO.Artigo Salvar()
    {
        CaptarDados();

        if (ValidarDados())
        {
            vArt = bArt.Salvar(vArt);

            if (Notificacao.Erro == false)
                Upload();

            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            
            //Upload();

            IniciarFormulario();
        }

        return vArt;
    }

    /*
    protected void lnkRemover_Click(object sender, EventArgs e)
    {
        CaptarDados();

        bArt.RemoverImagem(vArt.IdArtigo);

        _erro = @"Imagem foi removida com sucesso.";

        Notificacao.Erro = false;
        Notificacao.Status = Notificacao.Tipo.Erro;
        Notificacao.Texto = _erro;

        IniciarPagina();
    }
     * */


    private void Upload()
    {
        var _nome = "";
        var _extensao = "";
        var caminhoAux = caminho + vArt.IdArtigo + @"\";

        if (!Directory.Exists(caminhoAux))
            Directory.CreateDirectory(caminhoAux);


        if (uplImagem.HasFile)
        {
            _nome = uplImagem.FileName;
            _extensao = Path.GetExtension(_nome).ToLower();

            uplImagem.PostedFile.SaveAs(caminhoAux + "t-imagem" + _extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminhoAux + "t-imagem" + _extensao, caminhoAux + "imagem.png", 700, 1000, UTIL.EdicaoDeImagem.Formato.Png);

            try
            {
                File.Delete(caminhoAux + "t-imagem" + _extensao);
            }
            catch { }
        }

        IniciarPagina();
    }




    protected void ddlTipo_Init(object sender, EventArgs e)
    {
        var listar = new BLL.Artigos().ListarTipo();

        if (listar != null)
        {
            listar = (from aux in listar
                      orderby aux.Nome
                      select aux).ToList();

            ddlTipo.Items.Clear();
            foreach (var registro in listar)
            {
                ddlTipo.Items.Add(new ListItem(registro.Nome, registro.IdTipo.ToString()));
            }
        }
    }


    protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
    {
        var idTipo = Convert.ToInt32(ddlTipo.SelectedValue);

        SetarTipo(idTipo);
    }


    protected void ddlCategoria_Init(object sender, EventArgs e)
    {
        var listar = (from aux in bArt.ListarCategorias()
                      orderby aux.Nome
                      select aux);

        if (listar != null)
        {
            ddlCategoria.Items.Clear();

            foreach (var registro in listar)
            {
                ddlCategoria.Items.Add(new ListItem(registro.Nome, registro.IdCategoria.ToString()));
            }
        }
    }
    

    protected void ddlAutor_Init(object sender, EventArgs e)
    {
        var listar = new BLL.Autores().Pesquisar(0,10000,"", "Nome", true);

        if (listar != null)
        {
            ddlAutor.Items.Clear();
            ddlAutor.Items.Add(new ListItem("", "0"));

            foreach (var registro in listar)
            {
                ddlAutor.Items.Add(new ListItem(registro.Nome, registro.IdAutor.ToString()));
            }
        }
    }


    protected void ddlRelacionamento_Init(object sender, EventArgs e)
    {
        var listar = (from aux in new BLL.Produtos().ListarCategorias()
                      orderby aux.Nome ascending
                      select aux);

        if (listar != null)
        {
            ddlRelacionamento.Items.Clear();
            ddlRelacionamento.Items.Add(new ListItem("", "0"));

            foreach (var registro in listar)
            {
                ddlRelacionamento.Items.Add(new ListItem(registro.Nome, registro.IdCategoria.ToString()));
            }
        }
    }


    private void SetarTipo(int idTipo)
    {
        pnlCategoria.Visible = false;

        if (idTipo == 3 || idTipo == 6)
            pnlCategoria.Visible = true;
    }
}