﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_artigos_registro" %>
<%@ Register src="geral.ascx" tagname="geral" tagprefix="uc1" %>
<%@ Register src="termos.ascx" tagname="termos" tagprefix="uc2" %>

<%@ Register src="arquivos.ascx" tagname="arquivos" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        $(function () {
            $('.imagem').click(function () {
                var url = $(this).attr('data-url');
                window.open(url, 'imagem', '');
            });
        });
        </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <div class="aba-painel">
            <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" 
                onmenuitemclick="Menu1_MenuItemClick" CssClass="aba">
                <Items>
                    <asp:MenuItem Text="Geral" Value="Geral" Selected="true"></asp:MenuItem>
                    <%--<asp:MenuItem Text="Termos" Value="Termos"></asp:MenuItem>--%>
                    <asp:MenuItem Text="Arquivos" Value="Arquivos"></asp:MenuItem>
                </Items>
            </asp:Menu>
        </div>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="Geral" runat="server">
                <uc1:geral ID="geral1" runat="server" />
            </asp:View>
            <%--<asp:View ID="Termos" runat="server">
                <uc2:termos ID="termos1" runat="server" />
            </asp:View>--%>
            <asp:View ID="Arquivos" runat="server">
                <uc3:arquivos ID="arquivos1" runat="server" />
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>