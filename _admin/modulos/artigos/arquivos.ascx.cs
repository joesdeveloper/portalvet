﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class _admin_modulos_artigos_arquivos : System.Web.UI.UserControl
{
    BLL.Artigos bArt = new BLL.Artigos();
    VO.ArquivoPorArtigo vArt = new VO.ArquivoPorArtigo();

    const int _registrosPorPagina = 1000;
    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\downloads\";

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarGridView();
    }


    #region Lista
    private void InicarSessao()
    {
        if (ViewState["coluna"] == null)
        {
            ViewState["coluna"] = "DataInclusao";
            ViewState["ascendente"] = false;
        }
    }

    private void IniciarGridView()
    {
        InicarSessao();

        if (Request["id"] != null)
        {
            var idArtigo = Convert.ToInt32(Request["id"]);

            bool ascendente = Convert.ToBoolean(ViewState["ascendente"]);
            string coluna = ViewState["coluna"].ToString();

            var lista = bArt.ListarArquivos(idArtigo, coluna, ascendente);

            grdwListar.PageSize = _registrosPorPagina;
            grdwListar.PageIndex = 0;
            grdwListar.DataSource = lista;
            grdwListar.DataBind();
        }
    }


    protected void grdwListar_Sorting(object sender, GridViewSortEventArgs e)
    {
        string coluna = e.SortExpression;

        if (coluna == ViewState["coluna"].ToString())
        {
            if (Convert.ToBoolean(ViewState["ascendente"]))
                ViewState["ascendente"] = false;
            else
                ViewState["ascendente"] = true;
        }
        else
            ViewState["coluna"] = coluna;

        IniciarGridView();
    }


    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var btnExcluir = (ImageButton)e.Row.FindControl("btnExcluir");
            btnExcluir.Attributes.Add("idRegistro", DataBinder.Eval(e.Row.DataItem, "IdArquivo").ToString());
            btnExcluir.Attributes.Add("data-url", DataBinder.Eval(e.Row.DataItem, "Url").ToString());
            //btnExcluir.Attributes.Add("onclick", "SetarThumbnail('" + Request["id"] + "','" + Request["url"] + "','" + Request["img"] + "','" + DataBinder.Eval(e.Row.DataItem, "IdArquivo") + "','" + oVal.UrlBase() + DataBinder.Eval(e.Row.DataItem, "Url") + "'); parent.jQuery.fancybox.close(); ");

            var btnDownload = (ImageButton)e.Row.FindControl("btnDownload");
            btnDownload.Attributes.Add("onclick", "window.open('../../../download.aspx?q=" + DataBinder.Eval(e.Row.DataItem, "Url") + "', '_blank'); return false;");

            var lblDataInclusao = (Literal)e.Row.FindControl("lblDataInclusao");
            if (DataBinder.Eval(e.Row.DataItem, "DataInclusao") != null)
                lblDataInclusao.Text = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DataInclusao")).ToString("dd/MM/yy HH:mm");
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            var txtPaginaAtual = (TextBox)e.Row.FindControl("txtPaginaAtual");
            txtPaginaAtual.Text = (Convert.ToInt32(ViewState["paginaAtual"]) + 1).ToString();

            var lblTotalDePaginas = (Literal)e.Row.FindControl("lblTotalDePaginas");
            lblTotalDePaginas.Text = ViewState["totalPaginas"].ToString();

            var lblTotalDeIRegistros = (Literal)e.Row.FindControl("lblTotalDeIRegistros");
            lblTotalDeIRegistros.Text = ViewState["totalDeRegistros"].ToString();
        }
    }

    protected void grdwListar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        var image = new Image();
        var separador = new Image();

        if (e.Row != null && e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                if (cell.HasControls())
                {
                    var button = cell.Controls[0] as LinkButton;
                    if (button != null)
                    {
                        if (ViewState["coluna"].ToString().Trim() == button.CommandArgument)
                        {
                            if (Convert.ToBoolean(ViewState["ascendente"]))
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_cima.png";
                            else
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_baixo.png";

                            image.AlternateText = "Middle";
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }
    }


    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        var botao = (ImageButton)sender;

        var id = Convert.ToInt32(botao.Attributes["idRegistro"]);
        var caminho = Server.MapPath("~/" + botao.Attributes["data-url"]);

        bArt.ExcluirArquivo(id);

        if (File.Exists(caminho))
            File.Delete(caminho);

        IniciarGridView();
    }

    #endregion



    private bool ValidarDados()
    {
        if (Notificacao.Erro)
            return false;

        return true;
    }

    //protected void lnkUpload_Click(object sender, EventArgs e)
    
    public void Salvar(int idArtigo)
    {
        vArt.IdArtigo = idArtigo;

        if (!Directory.Exists(caminho))
            Directory.CreateDirectory(caminho);

        if (ValidarDados())
        {
            if (uplImagem.HasFile)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var arquivo = (HttpPostedFile)Request.Files[i];

                    Upload(arquivo);
                }

                IniciarGridView();
            }
        }
    }



    private void Upload(HttpPostedFile arquivo)
    {
        var nome = Path.GetFileName(arquivo.FileName);
        var extensao = Path.GetExtension(arquivo.FileName).ToLower();

        nome = nome.Replace(extensao, "");
        vArt.Titulo = nome;
        nome = oVal.CaracteresEspeciais(nome);
        nome = oVal.Acentuacao(nome).Replace(" ", "-");

        var caminhoAux = caminho + nome + extensao;

        if (File.Exists(caminhoAux))
        {
            var i = 0;
            for (; ; )
            {
                i++;
                caminhoAux = caminho + nome + i + extensao;
                if (!File.Exists(caminhoAux))
                {
                    nome = nome + i;
                    break;
                }
            }
        }

        arquivo.SaveAs(caminho + nome + extensao);

        vArt.Url = @"arquivos/downloads/" + nome + extensao;

        bArt.Salvar(vArt);
    }

}