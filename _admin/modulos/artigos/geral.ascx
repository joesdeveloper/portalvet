﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="geral.ascx.cs" Inherits="_admin_modulos_artigos_geral" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

     <script type="text/javascript">
         $(document).ready(function () {
             $('#<%=txtDataPublicacao.ClientID%>').mask('99/99/9999 99:99:99');
         });
     </script>

<%--        <script>
            $(function () {
                $(".sombra").fancybox({
                    maxWidth: 800,
                    maxHeight: 600,
                    fitToView: false,
                    width: '70%',
                    height: '70%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none'
                });
                $(".botao").fancybox({
                    maxWidth: 800,
                    maxHeight: 600,
                    fitToView: false,
                    width: '70%',
                    height: '70%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none'
                });
            });
        </script>--%>
        <table class="formulario">
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="texto">
                                <asp:Literal ID="lblImagem" runat="server"></asp:Literal><br />
                            </td>
                            <td style=" vertical-align:text-top;">
                                <asp:Literal ID="lblInformacoes" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="texto">Imagem:</td>
                <td>
                    <asp:FileUpload ID="uplImagem" runat="server" />
                    <br />(Tamanho: 800x600px)
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Destaque:
                </td>
                <td>
                    <asp:CheckBox ID="cbxDestaque" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="texto">Titulo:</td>
                <td><asp:TextBox ID="txtTitulo" runat="server" CssClass="campo" MaxLength="150"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">Tipo:</td>
                <td><asp:DropDownList ID="ddlTipo" runat="server" oninit="ddlTipo_Init" 
                        AutoPostBack="True" onselectedindexchanged="ddlTipo_SelectedIndexChanged"></asp:DropDownList></td>
            </tr>
            <tr runat="server" id="pnlCategoria" visible="false">
                <td class="texto">Download Categoria:</td>
                <td><asp:DropDownList ID="ddlCategoria" runat="server" oninit="ddlCategoria_Init"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="texto">
                    Texto:
                </td>
                <td>
                    <CKEditor:CKEditorControl ID="txtTexto" runat="server" Width="95%" Height="400px"></CKEditor:CKEditorControl>
                </td>
            </tr>
            <tr>
                <td class="texto">Autor:</td>
                <td><asp:DropDownList ID="ddlAutor" runat="server" oninit="ddlAutor_Init"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="texto">Relacionar com categoria:</td>
                <td><asp:DropDownList ID="ddlRelacionamento" runat="server" oninit="ddlRelacionamento_Init"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="texto">Data Publicação:</td>
                <td><asp:TextBox ID="txtDataPublicacao" runat="server" CssClass="campo" MaxLength="19"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">
                    Ativo:
                </td>
                <td>
                    <asp:CheckBox ID="cbxAtivo" runat="server" />
                </td>
            </tr>
        </table>