﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class _admin_modulos_administradores_registro : System.Web.UI.Page
{
    BLL.Administradores bAdm = new BLL.Administradores();
    BLL.Permissoes bPer = new BLL.Permissoes();
    BLL.PermissoesPorAdministradores bPerAdm = new BLL.PermissoesPorAdministradores();

    VO.Administrador vAdm = new VO.Administrador();
    VO.Permissao vPer = new VO.Permissao();
    VO.PermissaoPorAdministrador vPerAdm = new VO.PermissaoPorAdministrador();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\administradores\";

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vAdm.IdAdministrador = new Guid(Request["id"].ToString());
            IniciarFormulario();
        }
        else
        {
            Random rd = new Random();
            txtSenha.Text = rd.Next(9) + "" + rd.Next(9) + "" + rd.Next(9) + "" + rd.Next(9) + "" + rd.Next(9) + "" + rd.Next(9);
        }
    }


    private void IniciarFormulario()
    {
        vAdm = bAdm.Consultar(vAdm.IdAdministrador);

        if (vAdm != null)
        {
            LimparDados();

            txtNome.Text = vAdm.Nome;
            txtEmail.Text = vAdm.Email;
            cbxAtivo.Checked = vAdm.Ativo;

            lblTotalAcessos.Text = vAdm.TotalAcessos.ToString();
            lblDataUltimoAcesso.Text = vAdm.DataUltimoAcesso.ToString();

            imgFoto.ImageUrl = "~/arquivos/administradores/" + vAdm.IdAdministrador + ".png";
            pnlFoto.Visible = false;
            if (File.Exists(caminho + vAdm.IdAdministrador + ".png"))
                pnlFoto.Visible = true;

            vPerAdm = bPerAdm.Consultar(vAdm.IdAdministrador);
            ddlPermissao.Items.FindByValue(vPerAdm.IdPermissao.ToString()).Selected = true;
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        txtEmail.Text = "";
        txtSenha.Text = "";
        cbxAtivo.Checked = false;
        lblTotalAcessos.Text = "0";
        lblDataUltimoAcesso.Text = "";
        ddlPermissao.SelectedIndex = -1;
    }


    private void CaptarDados()
    {
        Notificacao.Erro = false;
        Notificacao.Status = Notificacao.Tipo.Sucesso;
        Notificacao.Texto = "Registro salvo com sucesso";

        if (Request["id"] != null)
        {
            vAdm.IdAdministrador = new Guid(Request["id"].ToString());
            vPerAdm.IdAdministrador = vAdm.IdAdministrador;
        }

        vAdm.Nome = txtNome.Text;
        vAdm.Email = txtEmail.Text;
        vAdm.Senha = txtSenha.Text;
        vAdm.Ativo = cbxAtivo.Checked;

        vPerAdm.IdPermissao = Convert.ToInt32(ddlPermissao.SelectedValue);
    }


    private bool ValidarDados()
    {
        if (vAdm.Nome.Trim().Length == 0)
            _erro += @"- Nome não pode ser nulo.<br />";

        if (vAdm.Email.Trim().Length == 0)
            _erro += @"- Email não pode ser nulo.<br />";
        else
        {
            if (!oVal.Email(vAdm.Email))
                _erro += @"- Email inválido.<br />";
        }

        if (vPerAdm.IdPermissao == -1)
            _erro += @"- Grupo não pode ser nulo.<br />";

        if (vAdm.IdAdministrador == null)
        {
            if (vAdm.Senha.Trim().Length == 0)
                _erro += @"- Senha não pode ser nula.<br />";
            else
            {
                if (!oVal.Senha(vAdm.Senha, 6, 20))
                    _erro += @"- A senha deve ter em 6 a 20 caracteres.<br />";
            }
        }
        else
        {
            if (vAdm.Senha.Trim().Length > 0)
            {
                if (!oVal.Senha(vAdm.Senha, 6, 20))
                    _erro += @"- A senha deve ter em 6 a 20 caracteres.<br />";
            }
        }

        if (_erro.Length > 0)
        {
            Notificacao.Erro = true;
            Notificacao.Status = Notificacao.Tipo.Atencao;
            Notificacao.Texto = _erro;

            return false;
        }

        return true;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        try
        {
            if (ValidarDados())
            {
                var vAdmAux = bAdm.Consultar(vAdm.Email);

                if (vAdm.IdAdministrador.ToString() != "00000000-0000-0000-0000-000000000000")
                {
                    if (vAdmAux == null)
                    {
                        bAdm.Alterar(vAdm);
                        bPerAdm.Salvar(vPerAdm);
                        AlterarSenha();
                        IniciarFormulario();
                        Upload();
                        //_erro = @"PARABÉNS:<br />O registro foi salvo com sucesso.<br />";
                    }
                    else
                    {
                        if (vAdmAux.IdAdministrador == vAdm.IdAdministrador)
                        {
                            bAdm.Alterar(vAdm);
                            bPerAdm.Salvar(vPerAdm);
                            AlterarSenha();
                            IniciarFormulario();
                            Upload();
                            //_erro = @"PARABÉNS:<br />O registro foi salvo com sucesso.<br />";
                        }
                        else
                        {
                            _erro += @"- Este e-mail já é cadastrado.<br />";

                            Notificacao.Erro = true;
                            Notificacao.Status = Notificacao.Tipo.Atencao;
                            Notificacao.Texto = _erro;
                        }
                    }
                }
                else
                {
                    if (vAdmAux == null)
                    {
                        vAdm.IdAdministrador = bAdm.Incluir(vAdm);
                        vPerAdm.IdAdministrador = vAdm.IdAdministrador;
                        bPerAdm.Salvar(vPerAdm);
                        Upload();
                        //_erro = @"PARABÉNS:<br />O registro foi salvo com sucesso.<br />";
                        Response.Redirect("registro.aspx?id=" + vAdm.IdAdministrador);
                    }
                    else
                    {
                        _erro += @"- Este e-mail já é cadastrado.<br />";

                        Notificacao.Erro = true;
                        Notificacao.Status = Notificacao.Tipo.Atencao;
                        Notificacao.Texto = _erro;
                    }
                }
            }
        }
        catch (Exception er)
        {
            _erro += er.Message;

            Notificacao.Erro = true;
            Notificacao.Status = Notificacao.Tipo.Atencao;
            Notificacao.Texto = _erro;
        }

        ExibirMensagem();
    }


    private void AlterarSenha()
    {
        if (vAdm.Senha.Trim().Length > 0)
            bAdm.AlterarSenha(vAdm);
    }


    protected void ddlPermissao_Init(object sender, EventArgs e)
    {
        ddlPermissao.Items.Clear();

        var lista = bPer.Listar();

        ddlPermissao.Items.Add(new ListItem("Selecione a permissão", "-1"));

        foreach (var registro in lista)
        {
            ddlPermissao.Items.Add(new ListItem(registro.Nome, registro.IdPermissao.ToString()));
        }
    }


    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        CaptarDados();

        _erro = @"Foto foi excluida com sucesso.";

        Notificacao.Erro = false;
        Notificacao.Status = Notificacao.Tipo.Sucesso;
        Notificacao.Texto = _erro;
        
        if (File.Exists(caminho + vAdm.IdAdministrador + ".png"))
            File.Delete(caminho + vAdm.IdAdministrador + ".png");

        IniciarPagina();

        ExibirMensagem();
    }


    private void Upload()
    {
        if (!Directory.Exists(caminho))
            Directory.CreateDirectory(caminho);

        if (uplFoto.HasFile)
        {
            var _nome = uplFoto.FileName;
            var _extensao = Path.GetExtension(_nome).ToLower();

            uplFoto.PostedFile.SaveAs(caminho + vAdm.IdAdministrador + "_t" + _extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminho + vAdm.IdAdministrador + "_t" + _extensao, caminho + vAdm.IdAdministrador + ".png", 800, 600, UTIL.EdicaoDeImagem.Formato.Png);

            try
            {
                File.Delete(caminho + vAdm.IdAdministrador + "_t" + _extensao);
            }
            catch { }

            IniciarPagina();

            _erro = @"A foto foi salva com sucesso.<br />";

            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = _erro;
        }
    }
}