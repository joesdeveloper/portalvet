﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="listar.aspx.cs" Inherits="_admin_modulos_administradores_listar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><a href="registro.aspx"><span><asp:Image ID="Image3" ImageUrl="~/_admin/layout/imagens/ico-add.png" runat="server" />Novo</span></a></li><li><asp:LinkButton ID="lnkTodos" runat="server" onclick="btnTodos_Click"><span><asp:Image ID="Image2" ImageUrl="~/_admin/layout/imagens/ico-all.png" runat="server" />Todos</span></asp:LinkButton></li>
        <%--<li><asp:LinkButton ID="lnkExcluir" runat="server" onclick="lnkExcluir_Click" OnClientClick="return confirm('Para excluir os registros selecionados clique OK?');"><span><asp:Image ID="Image4" ImageUrl="~/_admin/layout/imagens/ico-trash.png" runat="server" />Excluir</span></asp:LinkButton></li>--%>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
    <ul>
        <li><asp:TextBox ID="txtPesquisar" runat="server" AutoPostBack="True" 
                ontextchanged="txtPesquisar_TextChanged"></asp:TextBox>
            <asp:LinkButton ID="btnPesquisar" runat="server" onclick="btnPesquisar_Click"><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-search.png" runat="server" />Pesquisar</asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Listagem
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <!-- Grid Inicio -->
        <asp:GridView ID="grdwListar" runat="server" Width="100%" PagerSettings-Visible="True"
            AutoGenerateColumns="false" AllowPaging="True" 
            CssClass="listagem" OnRowDataBound="grdwListar_RowDataBound"
            GridLines="None" onprerender="grdwListar_PreRender" AllowSorting="True" 
            onsorting="grdwListar_Sorting" onrowcreated="grdwListar_RowCreated">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkTodos" runat="server" />
                    </HeaderTemplate> 
                    <ItemTemplate>
                        <asp:CheckBox ID="chkID" runat="server"/>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="22px" />
                    <HeaderStyle HorizontalAlign="Center" Width="22px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="A" SortExpression="Ativo">
                    <ItemTemplate>
                        <asp:Image ID="imgAtivo" runat="server" ImageUrl="~/_admin/layout/imagens/ico-pin-grey.png" CssClass="ativo" AlternateText="Ativo/Inativo" ToolTip="Ativo/Inativo" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="22px" />
                    <HeaderStyle HorizontalAlign="Center" Width="22px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nome" SortExpression="Nome">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "nome")%>
                    </ItemTemplate>
                    <ItemStyle Width="250px" HorizontalAlign="Left" />
                    <HeaderStyle Width="250px" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="E-mail" SortExpression="Email">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "email")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Acessos" SortExpression="TotalAcessos">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "TotalAcessos")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="65px" />
                    <HeaderStyle HorizontalAlign="Center" Width="65px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Dt. Acesso" SortExpression="DataUltimoAcesso">
                    <ItemTemplate>
                        <asp:Literal ID="lblDataAcesso" runat="server"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" Width="90px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Op&#231;&#245;es">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgAlterar" runat="server" ImageUrl="~/_admin/layout/imagens/ico-edit-dark.png" ImageAlign="absbottom" OnClick="imgAlterar_Click" CssClass="opcoes"  ToolTip="Alterar registro" AlternateText="Alterar registro" />
                        <%--<asp:ImageButton ID="btnExcluir" runat="server" ImageUrl="~/_admin/layout/imagens/ico-delete-dark.png" ImageAlign="absbottom" OnClick="btnExcluir_Click" CssClass="opcoes" ToolTip="Excluir registro" AlternateText="Excluir registro" OnClientClick="return confirm('Para excluir o registro selecionado clique OK?');" />--%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" Width="90px" />
                </asp:TemplateField>
            </Columns>

            <PagerTemplate>
                <div class="total-de-registros" >
                    <asp:Literal ID="lblTotalDeIRegistros" runat="server"></asp:Literal>
                    registros
                </div>
                <div class="paginacao-setas">
                    <asp:ImageButton ID="btnPrimeiro" runat="server" AlternateText="Primeira página" ToolTip="Primeira página" 
                        ImageUrl="~/_admin/layout/imagens/ico-first-dark.png" 
                        onclick="btnPrimeiro_Click" />
                    <asp:ImageButton ID="btnAnterior" runat="server" AlternateText="Página anterior" ToolTip="Página anterior" 
                        ImageUrl="~/_admin/layout/imagens/ico-back-dark.png" 
                        onclick="btnAnterior_Click" />

                    <asp:TextBox ID="txtPaginaAtual" runat="server" Width="25" MaxLength="3"></asp:TextBox>
                    de
                    <asp:Literal ID="lblTotalDePaginas" runat="server"></asp:Literal>
                    <asp:Button ID="btnIr" runat="server" Text=" Ir " onclick="btnIr_Click" />

                    <asp:ImageButton ID="btnProximo" runat="server" AlternateText="Próxima página" ToolTip="Próxima página" 
                        ImageUrl="~/_admin/layout/imagens/ico-next-dark.png" 
                        onclick="btnProximo_Click" />
                    <asp:ImageButton ID="btnUltimo" runat="server" AlternateText="Ultima página" ToolTip="Ultima página" 
                        ImageUrl="~/_admin/layout/imagens/ico-last-dark.png" 
                        onclick="btnUltimo_Click" />
                </div>
            </PagerTemplate>

            <HeaderStyle CssClass="cabecalho" />
            <AlternatingRowStyle CssClass="registros" />
            <RowStyle CssClass="registros" />
            <PagerStyle CssClass="paginacao" />
        </asp:GridView>

        <!-- Grid Final -->
    </div>
</asp:Content>

