﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_administradores_registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">

    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    

    <div class="margem">
        <table class="formulario">
            <tr>
                <td class="texto">Nome:</td>
                <td><asp:TextBox ID="txtNome" runat="server" CssClass="campo" MaxLength="80" required></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">
                    E-mail:
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="campo" MaxLength="80" pattern="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Z a-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-z A-Z]{2,6}$" placeholder="usuario@dominio.com" required></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Senha:
                </td>
                <td>
                    <asp:TextBox ID="txtSenha" runat="server" CssClass="campo-curto" MaxLength="20" Width="200">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Grupo:</td>
                <td>
                    <asp:DropDownList ID="ddlPermissao" runat="server" CssClass="campo-curto" oninit="ddlPermissao_Init">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Foto:</td>
                <td>
                    <asp:FileUpload ID="uplFoto" runat="server" />
                    <div runat="server" id="pnlFoto" visible="false">
                        <asp:Image ID="imgFoto" runat="server" CssClass="foto" />
                        <br />
                        <asp:Button ID="btnExcluir" runat="server" onclick="btnExcluir_Click" 
                            OnClientClick="return confirm('Confirma a exclusão da foto?');" Text="Excluir" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Total Acessos:
                </td>
                <td>
                    <asp:Label ID="lblTotalAcessos" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Dt. Ult. Acessos:
                </td>
                <td>
                    <asp:Label ID="lblDataUltimoAcesso" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Ativo:
                </td>
                <td>
                    <asp:CheckBox ID="cbxAtivo" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

