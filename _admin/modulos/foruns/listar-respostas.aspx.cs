﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_foruns_listar_respostas : System.Web.UI.Page
{
    const int _registrosPorPagina = 15;
    int idPergunta = 0;
    int idForum = 0;
    BLL.Foruns bFor = new BLL.Foruns();
    BLL.ForunsPerguntas bForPer = new BLL.ForunsPerguntas();
    BLL.ForunsRespostas bForRes = new BLL.ForunsRespostas();

    UTIL.Validacao oVal = new UTIL.Validacao();

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["idPergunta"] = Request.QueryString["idPergunta"];
        Session["idForum"] = Request.QueryString["idForum"];
        SiteMap.SiteMapResolve += new SiteMapResolveEventHandler(this.ExpandForumPaths);

        if (!IsPostBack)
        {
            ViewState["coluna"] = null;
            IniciarGridView();
        }
    }


    private void InicarSessao()
    {
        idPergunta = Convert.ToInt32(Request["idPergunta"]);
        idForum = Convert.ToInt32(Request["idForum"]);

        if (ViewState["coluna"] == null)
        {
            ViewState["filtro"] = "";
            ViewState["paginaAtual"] = 0;
            ViewState["coluna"] = "DataInclusao";
            ViewState["ascendente"] = true;

            ViewState["totalPaginas"] = 0;
            ViewState["totalDeRegistros"] = 0;
        }
    }

    private void IniciarGridView()
    {
        InicarSessao();

        lblForum.Text = bFor.Consultar(idForum).Nome + " > " + bForPer.Consultar(idPergunta).Nome;

        bool ascendente = Convert.ToBoolean(ViewState["ascendente"]);
        int paginaAtual = Convert.ToInt32(ViewState["paginaAtual"]);
        int totalPaginas = 0;
        string valor = ViewState["filtro"].ToString();
        string coluna = ViewState["coluna"].ToString();

        var lista = bForRes.Pesquisar(paginaAtual, _registrosPorPagina, valor, coluna, ascendente, idPergunta);

        int totalRegistros = bForRes.TotalDeRegistros(valor, idPergunta);
        ViewState["totalDeRegistros"] = totalRegistros;

        totalPaginas = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal((decimal)totalRegistros / (decimal)_registrosPorPagina)));
        ViewState["totalPaginas"] = totalPaginas;

        grdwListar.PageSize = _registrosPorPagina;
        grdwListar.PageIndex = 0;
        grdwListar.DataSource = lista;
        grdwListar.DataBind();
    }


    protected void grdwListar_Sorting(object sender, GridViewSortEventArgs e)
    {
        string coluna = e.SortExpression;

        if (coluna == ViewState["coluna"].ToString())
        {
            if (Convert.ToBoolean(ViewState["ascendente"]))
                ViewState["ascendente"] = false;
            else
                ViewState["ascendente"] = true;
        }
        else
            ViewState["coluna"] = coluna;

        IniciarGridView();
    }



    protected void btnPrimeiro_Click(object sender, ImageClickEventArgs e)
    {
        int pag = 0;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }

    protected void btnAnterior_Click(object sender, ImageClickEventArgs e)
    {
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        pag--;

        if (pag < 0)
            pag = 0;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void btnIr_Click(object sender, EventArgs e)
    {
        int total = Convert.ToInt32(ViewState["totalPaginas"]);
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        try
        {
            pag = Convert.ToInt32(((TextBox)grdwListar.BottomPagerRow.FindControl("txtPaginaAtual")).Text);
        }
        catch { }

        total--;

        if (pag < 0)
            pag = 0;

        if (pag > total)
            pag = total;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void btnProximo_Click(object sender, ImageClickEventArgs e)
    {
        int total = Convert.ToInt32(ViewState["totalPaginas"]);
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        pag++;

        total--;
        if (pag > total)
            pag = total;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }

    protected void btnUltimo_Click(object sender, ImageClickEventArgs e)
    {
        int pag = Convert.ToInt32(ViewState["totalPaginas"]);
        pag--;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void grdwListar_PreRender(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ViewState["totalPaginas"]) > 0)
            grdwListar.BottomPagerRow.Visible = true;

    }


    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var chkID = (CheckBox)e.Row.FindControl("chkID");
            chkID.Attributes.Add("idRegistro", DataBinder.Eval(e.Row.DataItem, "IdResposta").ToString());

            var imgAtivo = (Image)e.Row.FindControl("imgAtivo");
            imgAtivo.ImageUrl = "~/_admin/layout/imagens/ico-pin-grey.png";
            if (Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "ativo")))
                imgAtivo.ImageUrl = "~/_admin/layout/imagens/ico-pin-green.png";

            var lblTexto = (Literal)e.Row.FindControl("lblTexto");
            lblTexto.Text = oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Texto").ToString()), 150);

            var lblDataInclusao = (Literal)e.Row.FindControl("lblDataInclusao");
            lblDataInclusao.Text = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DataInclusao")).ToString("dd/MM/yy HH:mm");

            var imgAlterar = (ImageButton)e.Row.FindControl("imgAlterar");
            imgAlterar.Attributes.Add("idRegistro", DataBinder.Eval(e.Row.DataItem, "IdResposta").ToString());

            //var btnExcluir = (ImageButton)e.Row.FindControl("btnExcluir");
            //btnExcluir.Attributes.Add("idRegistro", DataBinder.Eval(e.Row.DataItem, "IdResposta").ToString());
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            var txtPaginaAtual = (TextBox)e.Row.FindControl("txtPaginaAtual");
            txtPaginaAtual.Text = (Convert.ToInt32(ViewState["paginaAtual"]) + 1).ToString();

            var lblTotalDePaginas = (Literal)e.Row.FindControl("lblTotalDePaginas");
            lblTotalDePaginas.Text = ViewState["totalPaginas"].ToString();

            var lblTotalDeIRegistros = (Literal)e.Row.FindControl("lblTotalDeIRegistros");
            lblTotalDeIRegistros.Text = ViewState["totalDeRegistros"].ToString();
        }
    }



    protected void grdwListar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        var image = new Image();
        var separador = new Image();

        if (e.Row != null && e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                if (cell.HasControls())
                {
                    var button = cell.Controls[0] as LinkButton;
                    if (button != null)
                    {
                        if (ViewState["coluna"].ToString().Trim() == button.CommandArgument)
                        {
                            if (Convert.ToBoolean(ViewState["ascendente"]))
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_cima.png";
                            else
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_baixo.png";

                            image.AlternateText = "Middle";
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }
    }


    protected void imgAlterar_Click(object sender, EventArgs e)
    {
        var imgAlterar = (ImageButton)sender;
        Response.Redirect("registro-resposta.aspx?idForum=" + Request["idForum"] + "&idPergunta=" + Request["idPergunta"] + "&id=" + imgAlterar.Attributes["idRegistro"]);
    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        var botao = (ImageButton)sender;
        Excluir(Convert.ToInt32(botao.Attributes["idRegistro"]));
    }

    protected void lnkExcluir_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow linha in grdwListar.Rows)
        {
            var chk = (CheckBox)linha.FindControl("chkID");

            if (chk.Checked)
            {
                var chkID = (CheckBox)linha.FindControl("chkID");
                Excluir(Convert.ToInt32(chkID.Attributes["idRegistro"]));
            }
        }
    }

    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }

    private void Excluir(int id)
    {
        InicarSessao();

        bForRes.Excluir(id);

        ExibirMensagem();

        IniciarGridView();
    }

    protected void btnTodos_Click(object sender, EventArgs e)
    {
        txtPesquisar.Text = "";
        ViewState["filtro"] = "";
        ViewState["paginaAtual"] = 0;
        IniciarGridView();
    }

    protected void btnLocalizar_Click(object sender, ImageClickEventArgs e)
    {
        Localizar();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        Localizar();
    }

    private void Localizar()
    {
        var valor = txtPesquisar.Text;
        ViewState["filtro"] = valor;
        ViewState["paginaAtual"] = 0;
        IniciarGridView();
    }

    protected void txtPesquisar_TextChanged(object sender, EventArgs e)
    {
        Localizar();
    }

    private SiteMapNode ExpandForumPaths(Object sender, SiteMapResolveEventArgs e)
    {
        var currentNode = SiteMap.CurrentNode.Clone(true);

        var tempNode = currentNode;

        if (tempNode.Url.EndsWith("registro-resposta.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"] + "&idPergunta=" + Session["idPergunta"];
            tempNode.ParentNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        if (tempNode.Url.EndsWith("listar-respostas.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        if (tempNode.Url.EndsWith("registro-pergunta.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        //SiteMap.SiteMapResolve -= new SiteMapResolveEventHandler(this.ExpandForumPaths);

        return currentNode;
    }
}