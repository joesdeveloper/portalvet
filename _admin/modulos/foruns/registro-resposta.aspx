﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro-resposta.aspx.cs" Inherits="_admin_modulos_foruns_registro_resposta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro <asp:Literal ID="lblForum" runat="server"></asp:Literal>
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb" RenderCurrentNodeAsLink="true"></asp:SiteMapPath>
    </div>
    <div class="margem">
        <table class="formulario">
            <tr>
                <td class="texto">Nome:</td>
                <td><asp:TextBox ID="txtNome" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">Texto:</td>
                <td><asp:TextBox ID="txtTexto" runat="server" CssClass="campo" Rows="15" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">Ativo:</td>
                <td><asp:CheckBox ID="cbxAtivo" runat="server" /></td>
            </tr>
        </table>
    </div>
</asp:Content>