﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_foruns_registro_resposta : System.Web.UI.Page
{
    BLL.Foruns bFor = new BLL.Foruns();
    BLL.ForunsPerguntas bForPer = new BLL.ForunsPerguntas();
    BLL.ForunsRespostas bForRes = new BLL.ForunsRespostas();
    VO.ForumResposta vForRes = new VO.ForumResposta();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["idPergunta"] = Request["idPergunta"];
        Session["idForum"] = Request["idForum"];
        SiteMap.SiteMapResolve += new SiteMapResolveEventHandler(this.ExpandForumPaths);

        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        CaptarDados();

        lblForum.Text = bFor.Consultar(Convert.ToInt32(Session["idForum"])).Nome + " > " + bForPer.Consultar(Convert.ToInt32(Session["idPergunta"])).Nome;

        if (Request["id"] != null)
            IniciarFormulario();
    }


    private void IniciarFormulario()
    {
        CaptarDados();
        
        vForRes = bForRes.Consultar(vForRes.IdResposta);

        if (vForRes != null)
        {
            LimparDados();

            txtNome.Text = vForRes.Nome;
            txtTexto.Text = vForRes.Texto;
            cbxAtivo.Checked = vForRes.Ativo;
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        txtTexto.Text = "";
        cbxAtivo.Checked = false;
    }


    private void CaptarDados()
    {
        vForRes.IdPergunta = Convert.ToInt32(Request["idPergunta"]);

        if (Request["id"] != null)
            vForRes.IdResposta = Convert.ToInt32(Request["id"]);

        vForRes.Texto = txtTexto.Text;
        vForRes.Nome = txtNome.Text;
        vForRes.Ativo = cbxAtivo.Checked;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        vForRes = bForRes.Salvar(vForRes);

        if (!Notificacao.Erro)
        {
            if (Request["id"] == null)
                Response.Redirect("registroTopico.aspx?idForum=" + Convert.ToInt32(Session["idForum"]) + "&IdPergunta=" + vForRes.IdPergunta + "&id=" + vForRes.IdPergunta);
        }

        ExibirMensagem();
    }


    private SiteMapNode ExpandForumPaths(Object sender, SiteMapResolveEventArgs e)
    {
        var currentNode = SiteMap.CurrentNode.Clone(true);

        var tempNode = currentNode;

        if (tempNode.Url.EndsWith("registro-resposta.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"] + "&idPergunta=" + Session["idPergunta"];
            tempNode.ParentNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        if (tempNode.Url.EndsWith("listar-respostas.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        if (tempNode.Url.EndsWith("registro-pergunta.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        return currentNode;
    }
}