﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_foruns_registro : System.Web.UI.Page
{
    BLL.Foruns bFor = new BLL.Foruns();
    VO.Forum vFor = new VO.Forum();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vFor.IdForum = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vFor = bFor.Consultar(vFor.IdForum);

        if (vFor != null)
        {
            LimparDados();

            txtNome.Text = vFor.Nome;
            txtDescricao.Text = vFor.Descricao;
            cbxAtivo.Checked = vFor.Ativo;
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        txtDescricao.Text = "";
        cbxAtivo.Checked = false;
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
        {
            vFor.IdForum = Convert.ToInt32(Request["id"]);
        }

        vFor.Nome = txtNome.Text;
        vFor.Descricao = txtDescricao.Text;
        vFor.Ativo = cbxAtivo.Checked;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        vFor = bFor.Salvar(vFor);

        if (!Notificacao.Erro)
        {
            if (Request["id"] == null)
                Response.Redirect("registro.aspx?id=" + vFor.IdForum);
        }

        ExibirMensagem();
    }
}