﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_foruns_registro_pergunta : System.Web.UI.Page
{
    BLL.Foruns bFor = new BLL.Foruns();
    BLL.ForunsPerguntas bForPer = new BLL.ForunsPerguntas();
    VO.ForumPergunta vForPer = new VO.ForumPergunta();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["idPergunta"] = Request["idPergunta"];
        Session["idForum"] = Request["idForum"];
        SiteMap.SiteMapResolve += new SiteMapResolveEventHandler(this.ExpandForumPaths);

        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        CaptarDados();

        vForPer.IdForum = Convert.ToInt32(Request["idForum"]);
        lblForum.Text = bFor.Consultar(vForPer.IdForum).Nome;

        if (Request["id"] != null)
        {
            vForPer.IdPergunta = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vForPer = bForPer.Consultar(vForPer.IdPergunta);

        if (vForPer != null)
        {
            LimparDados();

            txtNome.Text = vForPer.Nome;
            txtTitulo.Text = vForPer.Titulo;
            txtTexto.Text = vForPer.Texto;
            cbxAtivo.Checked = vForPer.Ativo;
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        cbxAtivo.Checked = false;
    }


    private void CaptarDados()
    {
        vForPer.IdForum = Convert.ToInt32(Request["idForum"]);

        if (Request["id"] != null)
        {
            vForPer.IdPergunta = Convert.ToInt32(Request["id"]);
        }

        vForPer.Nome = txtNome.Text;
        vForPer.Titulo = txtTitulo.Text;
        vForPer.Texto = txtTexto.Text;
        vForPer.Ativo = cbxAtivo.Checked;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        vForPer = bForPer.Salvar(vForPer);

        if (!Notificacao.Erro)
        {
            if (Request["id"] == null)
                Response.Redirect("registro-pergunta.aspx?idForum=" + vForPer.IdForum + "&id=" + vForPer.IdPergunta);
        }

        ExibirMensagem();
    }


    private SiteMapNode ExpandForumPaths(Object sender, SiteMapResolveEventArgs e)
    {
        var currentNode = SiteMap.CurrentNode.Clone(true);

        var tempNode = currentNode;

        if (tempNode.Url.EndsWith("registro-resposta.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"] + "&idPergunta=" + Session["idPergunta"];
            tempNode.ParentNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        if (tempNode.Url.EndsWith("listar-respostas.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        if (tempNode.Url.EndsWith("registro-pergunta.aspx"))
        {
            tempNode.Url = "";
            tempNode.ParentNode.Url += "?idForum=" + Session["idForum"];
        }

        //SiteMap.SiteMapResolve -= new SiteMapResolveEventHandler(this.ExpandForumPaths);

        return currentNode;
    }
}