﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_vitrines_registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
            <script>
                $(function () {
                    $('.imagem').click(function () {
                        var url = $(this).attr('data-url');
                        window.open(url, 'imagem', '');
                    });
                    $('#<%=txtDataPublicacao.ClientID%>').mask('99/99/9999 99:99:99');
                });
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <table class="formulario">
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="texto">
                                <asp:Literal ID="lblImagem" runat="server"></asp:Literal><br />
                            </td>
                            <td style=" vertical-align:text-top;">
                                <asp:Literal ID="lblInformacoes" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="texto">Imagem:</td>
                <td>
                    <asp:FileUpload ID="uplImagem" runat="server" />
                    <br />(Tamanho: 981x274px)
                </td>
            </tr>
                <tr>
                <td class="texto">Titulo:</td>
                <td><asp:TextBox ID="txtTitulo" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">Url:</td>
                <td><asp:TextBox ID="txtUrl" runat="server" CssClass="campo" MaxLength="250"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">Target:</td>
                <td>
                    <asp:DropDownList ID="ddlTarget" runat="server">
                        <asp:ListItem Text="_blank"></asp:ListItem>
                        <asp:ListItem Text="_top"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">Data Publicação:</td>
                <td><asp:TextBox ID="txtDataPublicacao" runat="server" CssClass="campo" MaxLength="19"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">
                    Ativo:
                </td>
                <td>
                    <asp:CheckBox ID="cbxAtivo" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

