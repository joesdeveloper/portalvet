﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Drawing;

public partial class _admin_modulos_vitrines_registro : System.Web.UI.Page
{
    BLL.Vitrines bVit = new BLL.Vitrines();
    VO.Vitrine vVit = new VO.Vitrine();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\vitrines\";

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vVit.IdVitrine = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vVit = bVit.Consultar(vVit.IdVitrine);

        if (vVit != null)
        {
            LimparDados();

            try {
                lblImagem.Text = "<div class=\"imagem\" style=\"background-image:url('" + oVal.UrlBase() + "arquivos/vitrines/" + vVit.IdVitrine + ".png');\" data-url=\"" + oVal.UrlBase() + "arquivos/vitrines/" + vVit.IdVitrine + ".png\"></div>";
                var bmp = new Bitmap(Server.MapPath("~/arquivos/vitrines/" + vVit.IdVitrine + ".png"));

                lblInformacoes.Text =
                    "<div class=\"imagens-info\"><strong>Nome do arquivo:</strong> " + Path.GetFileName("~/arquivos/vitrines/" + vVit.IdVitrine + ".png") + " </div>" +
                    "<div class=\"imagens-info\"><strong>Tipo do arquivo:</strong> " + Path.GetExtension("~/arquivos/vitrines/" + vVit.IdVitrine + ".png") + " </div>" +
                    "<div class=\"imagens-info\"><strong>Data de upload:</strong> " + vVit.DataInclusao.ToString("dd/MM/yyyy HH:mm:ss") + "</div>" +
                    "<div class=\"imagens-info\"><strong>Dimensões:</strong> " + bmp.Width + " x " + bmp.Height + "</div>" +
                    "<div class=\"imagens-info\"><strong>Url parcial:</strong> arquivos/vitrines/" + vVit.IdVitrine + ".png" + "</div>" +
                    "<div class=\"imagens-info\"><strong>Url completa:</strong> " + oVal.UrlBase() + "arquivos/vitrines/" + vVit.IdVitrine + ".png" + "</div>";
            }
            catch { }

            txtTitulo.Text = vVit.Titulo;
            txtUrl.Text = vVit.Url;
            ddlTarget.Items.FindByValue(vVit.Target).Selected = true;
            cbxAtivo.Checked = vVit.Ativo;
            txtDataPublicacao.Text = vVit.DataPublicacao.ToString();
        }
    }


    private void LimparDados()
    {
        txtTitulo.Text = "";
        txtUrl.Text = "";
        ddlTarget.SelectedIndex = -1;
        cbxAtivo.Checked = false;
        txtDataPublicacao.Text = DateTime.Now.ToString();
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
            vVit.IdVitrine = Convert.ToInt32(Request["id"]);

        vVit.Titulo = txtTitulo.Text;
        vVit.Url = txtUrl.Text.TrimStart().TrimEnd();
        vVit.Target = ddlTarget.SelectedValue;
        vVit.Ativo = cbxAtivo.Checked;

        try
        {
            vVit.DataPublicacao = Convert.ToDateTime(txtDataPublicacao.Text);
        }
        catch { vVit.DataPublicacao = DateTime.MaxValue; }
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        vVit = bVit.Salvar(vVit);

        if (!Notificacao.Erro)
        {
            Upload();

            if (Request["id"] == null)
                Response.Redirect("registro.aspx?id=" + vVit.IdVitrine);

            IniciarPagina();
        }

        ExibirMensagem();
    }

    private void Upload()
    {
        var _nome = "";
        var _extensao = "";
        var caminhoAux = caminho + @"\";

        if (!Directory.Exists(caminhoAux))
            Directory.CreateDirectory(caminhoAux);


        if (uplImagem.HasFile)
        {
            _nome = uplImagem.FileName;
            _extensao = Path.GetExtension(_nome).ToLower();

            uplImagem.PostedFile.SaveAs(caminhoAux + "t-imagem" + _extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminhoAux + "t-imagem" + _extensao, caminhoAux + vVit.IdVitrine + ".png", 981, 274, UTIL.EdicaoDeImagem.Formato.Png);

            try
            {
                File.Delete(caminhoAux + "t-imagem" + _extensao);
            }
            catch { }
        }

        IniciarPagina();
    }
}