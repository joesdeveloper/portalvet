﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Drawing;
using System.IO;

public partial class _admin_modulos_eventos_registro : System.Web.UI.Page
{
    BLL.Eventos bEve = new BLL.Eventos();
    VO.Evento vEve = new VO.Evento();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\eventos\";

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vEve.IdEvento = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vEve = bEve.Consultar(vEve.IdEvento);

        if (vEve != null)
        {
            LimparDados();

            try
            {
                lblImagem.Text = "<div class=\"imagem\" style=\"background-image:url('" + oVal.UrlBase() + "arquivos/eventos/" + vEve.IdEvento + "/imagem.png');\" data-url=\"" + oVal.UrlBase() + "arquivos/eventos/" + vEve.IdEvento + "/imagem.png\"></div>";
                var bmp = new Bitmap(Server.MapPath("~/arquivos/eventos/" + vEve.IdEvento + "/imagem.png"));
                lblInformacoes.Text =
                    "<div class=\"imagens-info\"><strong>Nome do arquivo:</strong> " + Path.GetFileName("~/arquivos/eventos/" + vEve.IdEvento + "/imagem.png") + " </div>" +
                    "<div class=\"imagens-info\"><strong>Tipo do arquivo:</strong> " + Path.GetExtension("~/arquivos/eventos/" + vEve.IdEvento + "/imagem.png") + " </div>" +
                    "<div class=\"imagens-info\"><strong>Data de upload:</strong> " + vEve.DataInclusao.ToString("dd/MM/yyyy HH:mm:ss") + "</div>" +
                    "<div class=\"imagens-info\"><strong>Dimensões:</strong> " + bmp.Width + " x " + bmp.Height + "</div>" +
                    "<div class=\"imagens-info\"><strong>Url parcial:</strong> arquivos/eventos/" + vEve.IdEvento + "/imagem.png" + "</div>" +
                    "<div class=\"imagens-info\"><strong>Url completa:</strong> " + oVal.UrlBase() + "arquivos/eventos/" + vEve.IdEvento + "/imagem.png" + "</div>";
            }
            catch { }

            txtNome.Text = vEve.Nome;
            txtDescricao.Text = vEve.Descricao;
            txtLocal.Text = vEve.Local;
            txtEndereco.Text = vEve.Endereco;
            txtNumero.Text = vEve.Numero;
            txtComplemento.Text = vEve.Complemento;
            txtBairro.Text = vEve.Bairro;
            txtCidade.Text = vEve.Cidade;
            ddlEstado.Items.FindByValue(vEve.Estado).Selected = true;
            txtCep.Text = vEve.Cep;
            txtDataEventoInicial.Text = vEve.DataEventoInicial.ToString("dd/MM/yyyy");
            txtDataEventoFinal.Text = vEve.DataEventoFinal.ToString("dd/MM/yyyy");
            cbxAtivo.Checked = vEve.Ativo;
            txtDataPublicacao.Text = vEve.DataPublicacao.ToString();
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        txtDescricao.Text = "";
        txtLocal.Text = "";
        txtEndereco.Text = "";
        txtNumero.Text = "";
        txtComplemento.Text = "";
        txtBairro.Text = "";
        txtCidade.Text = "";
        ddlEstado.SelectedIndex = -1;
        txtCep.Text = "";
        txtDataEventoInicial.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtDataEventoFinal.Text = DateTime.Now.ToString("dd/MM/yyyy");
        cbxAtivo.Checked = false;
        txtDataPublicacao.Text = DateTime.Now.ToString();
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
            vEve.IdEvento = Convert.ToInt32(Request["id"]);

        vEve.Nome = txtNome.Text;
        vEve.Descricao = txtDescricao.Text;
        vEve.Local = txtLocal.Text;
        vEve.Endereco = txtEndereco.Text;
        vEve.Numero = txtNumero.Text;
        vEve.Complemento = txtComplemento.Text;
        vEve.Bairro = txtBairro.Text;
        vEve.Cidade = txtCidade.Text;
        vEve.Estado = ddlEstado.SelectedValue;
        vEve.Cep = txtCep.Text;
        try
        {
            vEve.DataEventoInicial = Convert.ToDateTime(txtDataEventoInicial.Text);
        }
        catch { }

        try
        {
            vEve.DataEventoFinal = Convert.ToDateTime(txtDataEventoFinal.Text);
        }
        catch { }

        vEve.Ativo = cbxAtivo.Checked;

        try
        {
            vEve.DataPublicacao = Convert.ToDateTime(txtDataPublicacao.Text);
        }
        catch { vEve.DataPublicacao = DateTime.MaxValue; }
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        vEve = bEve.Salvar(vEve);

        if (!Notificacao.Erro)
        {
            Upload();

            if (Request["id"] == null)
                Response.Redirect("registro.aspx?id=" + vEve.IdEvento);

            IniciarPagina();
        }

        ExibirMensagem();
    }


    protected void lnkRemover_Click(object sender, EventArgs e)
    {
        CaptarDados();

        bEve.RemoverImagem(vEve.IdEvento);

        _erro = @"Imagem foi removida com sucesso.";

        Notificacao.Erro = false;
        Notificacao.Status = Notificacao.Tipo.Erro;
        Notificacao.Texto = _erro;

        IniciarPagina();

        ExibirMensagem();
    }

    private void Upload()
    {
        var _nome = "";
        var _extensao = "";
        var caminhoAux = caminho + vEve.IdEvento + @"\";

        if (!Directory.Exists(caminhoAux))
            Directory.CreateDirectory(caminhoAux);

        if (uplImagem.HasFile)
        {
            _nome = uplImagem.FileName;
            _extensao = Path.GetExtension(_nome).ToLower();

            uplImagem.PostedFile.SaveAs(caminhoAux + "t-imagem" + _extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminhoAux + "t-imagem" + _extensao, caminhoAux + "imagem.png", 700, 700, UTIL.EdicaoDeImagem.Formato.Png);

            try
            {
                File.Delete(caminhoAux + "t-imagem" + _extensao);
            }
            catch { }
        }

        IniciarPagina();
    }
}