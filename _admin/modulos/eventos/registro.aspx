﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_eventos_registro" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script>
    $(document).ready(function () {
        $('.imagem').click(function () {
            var url = $(this).attr('data-url');
            window.open(url, 'imagem', '');
        });
        $('#<%=txtDataPublicacao.ClientID%>').mask('99/99/9999 99:99:99');
        $("#conteudo_txtDataEventoInicial").mask("99/99/9999");
        $("#conteudo_txtDataEventoFinal").mask("99/99/9999");
    });
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <table class="formulario">
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="texto">
                                <asp:Literal ID="lblImagem" runat="server"></asp:Literal><br />
                            </td>
                            <td style=" vertical-align:text-top;">
                                <asp:Literal ID="lblInformacoes" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Foto do Evento:</td>
                <td>
                    <asp:FileUpload ID="uplImagem" runat="server" />
                    <br />(Tamanho: 800x600px)
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Nome: 
                </td>
                <td>
                    <asp:TextBox ID="txtNome" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Descrição:</td>
                <td>
                    <CKEditor:CKEditorControl ID="txtDescricao" runat="server" Width="95%" Height="250px"></CKEditor:CKEditorControl>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Nome do Local:
                </td>
                <td>
                    <asp:TextBox ID="txtLocal" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Endereço:
                </td>
                <td>
                    <asp:TextBox ID="txtEndereco" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Numero:
                </td>
                <td>
                    <asp:TextBox ID="txtNumero" runat="server" CssClass="campo" MaxLength="10" Width="100px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Complemento:
                </td>
                <td>
                    <asp:TextBox ID="txtComplemento" runat="server" CssClass="campo" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Bairro:
                </td>
                <td>
                    <asp:TextBox ID="txtBairro" runat="server" CssClass="campo" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Cidade:
                </td>
                <td>
                    <asp:TextBox ID="txtCidade" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Estado:
                </td>
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server">
                        <asp:ListItem>AC</asp:ListItem>  
                        <asp:ListItem>AL</asp:ListItem>  
                        <asp:ListItem>AM</asp:ListItem>  
                        <asp:ListItem>AP</asp:ListItem>  
                        <asp:ListItem>BA</asp:ListItem>  
                        <asp:ListItem>CE</asp:ListItem>  
                        <asp:ListItem>DF</asp:ListItem>  
                        <asp:ListItem>ES</asp:ListItem>  
                        <asp:ListItem>GO</asp:ListItem>  
                        <asp:ListItem>MA</asp:ListItem>  
                        <asp:ListItem>MG</asp:ListItem>  
                        <asp:ListItem>MS</asp:ListItem>  
                        <asp:ListItem>MT</asp:ListItem>  
                        <asp:ListItem>PA</asp:ListItem>  
                        <asp:ListItem>PB</asp:ListItem>  
                        <asp:ListItem>PE</asp:ListItem>  
                        <asp:ListItem>PI</asp:ListItem>  
                        <asp:ListItem>PR</asp:ListItem>  
                        <asp:ListItem>RJ</asp:ListItem>  
                        <asp:ListItem>RN</asp:ListItem>  
                        <asp:ListItem>RO</asp:ListItem>  
                        <asp:ListItem>RR</asp:ListItem>  
                        <asp:ListItem>RS</asp:ListItem>  
                        <asp:ListItem>SC</asp:ListItem>  
                        <asp:ListItem>SE</asp:ListItem>  
                        <asp:ListItem>SP</asp:ListItem>  
                        <asp:ListItem>TO</asp:ListItem>  
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Cep:
                </td>
                <td>
                    <asp:TextBox ID="txtCep" runat="server" CssClass="campo" MaxLength="8" Width="100px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Data Inicial:
                </td>
                <td>
                    <asp:TextBox ID="txtDataEventoInicial" CssClass="Data" MaxLength="10" runat="server" Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Data Final:
                </td>
                <td>
                    <asp:TextBox ID="txtDataEventoFinal" CssClass="Data" MaxLength="10" runat="server" Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Data Publicação:</td>
                <td><asp:TextBox ID="txtDataPublicacao" runat="server" CssClass="campo" MaxLength="19"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">
                    Ativo:
                </td>
                <td>
                    <asp:CheckBox ID="cbxAtivo" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

