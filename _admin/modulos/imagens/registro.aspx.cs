﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Drawing;

public partial class _admin_modulos_imagens_registro : System.Web.UI.Page
{
    BLL.Imagens bIma = new BLL.Imagens();
    VO.Imagem vIma = new VO.Imagem();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\imagens\";
    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        liUpload.Visible = true;
        tbUpload.Visible = true;
        liSalvar.Visible = false;
        tbSalvar.Visible = false;

        if (Request["id"] != null)
        {
            vIma.IdImagem = Convert.ToInt32(Request["id"]);
            
            IniciarFormulario();

            liUpload.Visible = false;
            tbUpload.Visible = false;
            liSalvar.Visible = true;
            tbSalvar.Visible = true;
        }
    }


    private void IniciarFormulario()
    {
        vIma = bIma.Consultar(vIma.IdImagem);

        if (vIma != null)
        {
            LimparDados();

            lblImagem.Text = "<div class=\"imagem\" style=\"background-image:url('" + oVal.UrlBase() + vIma.Url + "');\" data-url=\"" + oVal.UrlBase() + vIma.Url + "\"></div>";
            txtTitulo.Text = vIma.Titulo;
            txtUrlParcial.Text = vIma.Url;
            txtUrl.Text = oVal.UrlBase() + vIma.Url;
            

            var bmp = new Bitmap(Server.MapPath("~/" + vIma.Url));
            lblInformacoes.Text =
                "<strong>Nome do arquivo:</strong> " + Path.GetFileName(vIma.Url) + " <br /><br />" +
                "<strong>Tipo do arquivo:</strong> " + Path.GetExtension(vIma.Url) + " <br /><br />" +
                "<strong>Data de upload:</strong> " + vIma.DataInclusao.ToString("dd/MM/yyyy HH:mm:ss") + "<br /><br />" +
                "<strong>Dimensões:</strong> " + bmp.Width + " x " + bmp.Height;
        }
    }


    private void LimparDados()
    {
        lblImagem.Text = "";
        txtTitulo.Text = "";
        txtUrl.Text = "";
        txtUrlParcial.Text = "";

    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
            vIma.IdImagem = Convert.ToInt32(Request["id"]);

        vIma.Titulo = txtTitulo.Text;
        vIma.Url = txtUrl.Text;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        vIma = bIma.Salvar(vIma);

        ExibirMensagem();
    }


    protected void lnkUpload_Click(object sender, EventArgs e)
    {
        if (!Directory.Exists(caminho))
            Directory.CreateDirectory(caminho);

        if (uplImagem.HasFile)
        {
            lblErro.Text = "";
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var arquivo = (HttpPostedFile)Request.Files[i];

                Upload(arquivo);
            }

            ExibirMensagem();
        }
    }



    private void Upload(HttpPostedFile arquivo)
    {
        var vIma = new VO.Imagem();

        var nome = Path.GetFileName(arquivo.FileName);
        var extensao = Path.GetExtension(arquivo.FileName).ToLower();

        if (oVal.Imagem(extensao))
        {
            nome = nome.Replace(extensao, "");
            vIma.Titulo = nome;
            nome = oVal.CaracteresEspeciais(nome);
            nome = oVal.Acentuacao(nome).Replace(" ", "-");

            var caminhoAux = caminho + nome + ".png";

            if (File.Exists(caminhoAux))
            {
                var i = 0;
                for (; ; )
                {
                    i++;
                    caminhoAux = caminho + nome + i + ".png";
                    if (!File.Exists(caminhoAux))
                    {
                        nome = nome + i;
                        break;
                    }
                }
            }

            arquivo.SaveAs(caminho + nome + "-o" + extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminho + nome + "-o" + extensao, caminho + nome + ".png", 1920, 1080, UTIL.EdicaoDeImagem.Formato.Png);

            vIma.Url = @"arquivos/imagens/" + nome + ".png";

            txtUrl.Text = vIma.Url;

            try
            {
                File.Delete(caminho + nome + "-o" + extensao);
            }
            catch { }

            vIma = bIma.Salvar(vIma);

            _erro = @"A imagem foi salva com sucesso.<br />";

            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = _erro;
        }
        else
            lblErro.Text += "<br/><font color=\"red\">Arquivo <strong>" + nome.ToUpper() + "</strong> não é um formato válido.</font>";
    }
}