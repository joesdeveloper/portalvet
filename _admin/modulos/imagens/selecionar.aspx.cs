﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class _admin_modulos_imagens_selecionar : System.Web.UI.Page
{
    const int _registrosPorPagina = 6;
    BLL.Imagens bIma = new BLL.Imagens();
    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\imagens\";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["coluna"] = null;
            IniciarGridView();
        }
    }


    private void InicarSessao()
    {
        if (ViewState["coluna"] == null)
        {
            ViewState["filtro"] = "";
            ViewState["paginaAtual"] = 0;
            ViewState["coluna"] = "DataInclusao";
            ViewState["ascendente"] = false;

            ViewState["totalPaginas"] = 0;
            ViewState["totalDeRegistros"] = 0;
        }
    }

    private void IniciarGridView()
    {
        InicarSessao();

        bool ascendente = Convert.ToBoolean(ViewState["ascendente"]);
        int paginaAtual = Convert.ToInt32(ViewState["paginaAtual"]);
        int totalPaginas = 0;
        string valor = ViewState["filtro"].ToString();
        string coluna = ViewState["coluna"].ToString();

        var lista = bIma.Pesquisar(paginaAtual, _registrosPorPagina, valor, coluna, ascendente);

        int totalRegistros = bIma.TotalDeRegistros(valor);
        ViewState["totalDeRegistros"] = totalRegistros;

        totalPaginas = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal((decimal)totalRegistros / (decimal)_registrosPorPagina)));
        ViewState["totalPaginas"] = totalPaginas;

        grdwListar.PageSize = _registrosPorPagina;
        grdwListar.PageIndex = 0;
        grdwListar.DataSource = lista;
        grdwListar.DataBind();
    }


    protected void grdwListar_Sorting(object sender, GridViewSortEventArgs e)
    {
        string coluna = e.SortExpression;

        if (coluna == ViewState["coluna"].ToString())
        {
            if (Convert.ToBoolean(ViewState["ascendente"]))
                ViewState["ascendente"] = false;
            else
                ViewState["ascendente"] = true;
        }
        else
            ViewState["coluna"] = coluna;

        IniciarGridView();
    }



    protected void btnPrimeiro_Click(object sender, ImageClickEventArgs e)
    {
        int pag = 0;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }

    protected void btnAnterior_Click(object sender, ImageClickEventArgs e)
    {
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        pag--;

        if (pag < 0)
            pag = 0;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void btnIr_Click(object sender, EventArgs e)
    {
        int total = Convert.ToInt32(ViewState["totalPaginas"]);
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        try
        {
            pag = Convert.ToInt32(((TextBox)grdwListar.BottomPagerRow.FindControl("txtPaginaAtual")).Text);
        }
        catch { }

        total--;

        if (pag < 0)
            pag = 0;

        if (pag > total)
            pag = total;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void btnProximo_Click(object sender, ImageClickEventArgs e)
    {
        int total = Convert.ToInt32(ViewState["totalPaginas"]);
        int pag = Convert.ToInt32(ViewState["paginaAtual"]);
        pag++;

        total--;
        if (pag > total)
            pag = total;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }

    protected void btnUltimo_Click(object sender, ImageClickEventArgs e)
    {
        int pag = Convert.ToInt32(ViewState["totalPaginas"]);
        pag--;

        ViewState["paginaAtual"] = pag;

        IniciarGridView();
    }


    protected void grdwListar_PreRender(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ViewState["totalPaginas"]) > 0)
            grdwListar.BottomPagerRow.Visible = true;
    }


    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var imgSelecionar = (ImageButton)e.Row.FindControl("imgSelecionar");
            imgSelecionar.Attributes.Add("idRegistro", DataBinder.Eval(e.Row.DataItem, "IdImagem").ToString());
            imgSelecionar.Attributes.Add("onclick", "SetarThumbnail('" + Request["id"] + "','" + Request["url"] + "','" + Request["img"] + "','" + DataBinder.Eval(e.Row.DataItem, "IdImagem") + "','" + oVal.UrlBase() + DataBinder.Eval(e.Row.DataItem, "Url") + "'); parent.jQuery.fancybox.close(); ");
            //imgSelecionar.Attributes.Add("onclick", "window.parent.document.getElementById('" + Request["id"] + "').value='" + oVal.UrlBase() + DataBinder.Eval(e.Row.DataItem, "Url") + "'; parent.jQuery.fancybox.close();");

            var lblImagem = (Literal)e.Row.FindControl("lblImagem");
            lblImagem.Text = "<div class=\"imagem-selecionado\" style=\"background-image: url('" + oVal.UrlBase() + DataBinder.Eval(e.Row.DataItem, "Url") + "'); \"></div>";

            var lblDataInclusao = (Literal)e.Row.FindControl("lblDataInclusao");
            if (DataBinder.Eval(e.Row.DataItem, "DataInclusao") != null)
                lblDataInclusao.Text = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DataInclusao")).ToString("dd/MM/yy HH:mm");
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            var txtPaginaAtual = (TextBox)e.Row.FindControl("txtPaginaAtual");
            txtPaginaAtual.Text = (Convert.ToInt32(ViewState["paginaAtual"]) + 1).ToString();

            var lblTotalDePaginas = (Literal)e.Row.FindControl("lblTotalDePaginas");
            lblTotalDePaginas.Text = ViewState["totalPaginas"].ToString();

            var lblTotalDeIRegistros = (Literal)e.Row.FindControl("lblTotalDeIRegistros");
            lblTotalDeIRegistros.Text = ViewState["totalDeRegistros"].ToString();
        }
    }



    protected void grdwListar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        var image = new Image();
        var separador = new Image();

        if (e.Row != null && e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                if (cell.HasControls())
                {
                    var button = cell.Controls[0] as LinkButton;
                    if (button != null)
                    {
                        if (ViewState["coluna"].ToString().Trim() == button.CommandArgument)
                        {
                            if (Convert.ToBoolean(ViewState["ascendente"]))
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_cima.png";
                            else
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_baixo.png";

                            image.AlternateText = "Middle";
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }
    }

    protected void btnTodos_Click(object sender, EventArgs e)
    {
        txtPesquisar.Text = "";
        ViewState["filtro"] = "";
        ViewState["paginaAtual"] = 0;
        IniciarGridView();
    }

    protected void btnLocalizar_Click(object sender, ImageClickEventArgs e)
    {
        Localizar();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        Localizar();
    }

    private void Localizar()
    {
        var valor = txtPesquisar.Text;
        ViewState["filtro"] = valor;
        ViewState["paginaAtual"] = 0;
        IniciarGridView();
    }

    protected void txtPesquisar_TextChanged(object sender, EventArgs e)
    {
        Localizar();
    }



    protected void lnkUpload_Click(object sender, EventArgs e)
    {
        if (!Directory.Exists(caminho))
            Directory.CreateDirectory(caminho);

        if (uplImagem.HasFile)
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var arquivo = (HttpPostedFile)Request.Files[i];

                Upload(arquivo);
            }

            IniciarGridView();
        }
    }



    private void Upload(HttpPostedFile arquivo)
    {
        var vIma = new VO.Imagem();

        var nome = Path.GetFileName(arquivo.FileName);
        var extensao = Path.GetExtension(arquivo.FileName).ToLower();

        if (oVal.Imagem(extensao))
        {
            nome = nome.Replace(extensao, "");
            vIma.Titulo = nome;
            nome = oVal.CaracteresEspeciais(nome);
            nome = oVal.Acentuacao(nome).Replace(" ", "-");

            var caminhoAux = caminho + nome + ".png";

            if (File.Exists(caminhoAux))
            {
                var i = 0;
                for (; ; )
                {
                    i++;
                    caminhoAux = caminho + nome + i + ".png";
                    if (!File.Exists(caminhoAux))
                    {
                        nome = nome + i;
                        break;
                    }
                }
            }

            arquivo.SaveAs(caminho + nome + "-o" + extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminho + nome + "-o" + extensao, caminho + nome + ".png", 1920, 1080, UTIL.EdicaoDeImagem.Formato.Png);

            vIma.Url = @"arquivos/imagens/" + nome + ".png";

            try
            {
                File.Delete(caminho + nome + "-o" + extensao);
            }
            catch { }

            vIma = bIma.Salvar(vIma);
        }
    }
}