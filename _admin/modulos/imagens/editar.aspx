﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="editar.aspx.cs" Inherits="_admin_modulos_imagens_editar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../layout/css/geral.css" rel="stylesheet" type="text/css" />
    <link href="../../layout/css/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../../layout/js/jquery.js" type="text/javascript"></script>
    <script src="../../layout/js/jquery.Jcrop.min.js" type="text/javascript"></script>
    <script src="../../layout/js/jquery.cookie.js" type="text/javascript"></script>
    <script language="Javascript">
        $(function () {
            var bg = $(<% Response.Write("'#" + Request["i"] + "'"); %>, window.parent.document).css("background-image");
            bg = bg.replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');

            $('#imagem').attr('src', bg);

            var jcrop_api;
            $('#imagem').Jcrop({ /*boxWidth: 800, boxHeight: 600, */allowSelect: false }, function () {
                jcrop_api = this;
            });
            storeCoords = function (c) {
                $('#X').val(c.x);
                $('#Y').val(c.y);
                $('#W').val(c.w);
                $('#H').val(c.h);
                //$('#' + objId, window.parent.document);
            }
            jcrop_api.setOptions({ onSelect: storeCoords, setSelect: [0, 0, 300, 100], bgOpacity: .4, bgColor: '#fff', allowResize: false });

            $('.cortar').click(function () {
                $(<% Response.Write("'#" + Request["X"] + "'"); %>, window.parent.document).val($('#X').val());
                $(<% Response.Write("'#" + Request["Y"] + "'"); %>, window.parent.document).val($('#Y').val());
                $(<% Response.Write("'#" + Request["W"] + "'"); %>, window.parent.document).val($('#W').val());
                $(<% Response.Write("'#" + Request["H"] + "'"); %>, window.parent.document).val($('#H').val());
                parent.jQuery.fancybox.close();
            });
            $('.modelo1').click(function () {
                jcrop_api.setOptions({ setSelect: [0, 0, 600, 300], allowResize: false });
                return false;
            });
            $('.modelo2').click(function () {
                jcrop_api.setOptions({ setSelect: [0, 0, 300, 300], allowResize: false });
                return false;
            });
            $('.modelo3').click(function () {
                jcrop_api.setOptions({ setSelect: [0, 0, 300, 100], allowResize: false });
                return false;
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <header>
            <div class="menu-topo">
                <ul>
                    <li><a class="cortar"><span><img src="../../layout/imagens/ico-crop.png" />Cortar</span></a></li><li><a class="modelo1"><span><img src="../../layout/imagens/ico-marquee.png" />Modelo 1</span></a></li><li><a class="modelo2"><span><img src="../../layout/imagens/ico-marquee.png" />Modelo 2</span></a></li><li><a class="modelo3"><span><img src="../../layout/imagens/ico-marquee.png" />Modelo 3</span></a></li>
                </ul>
            </div>
        </header>
    </div>
    <div class="margem">
        <img id="imagem" src="" />
        <%--<img id="imagem" src="" width="800" />--%>
        <asp:HiddenField ID="X" runat="server" />
        <asp:HiddenField ID="Y" runat="server" />
        <asp:HiddenField ID="W" runat="server" />
        <asp:HiddenField ID="H" runat="server" />
        <%--<asp:Button ID="btnCortar" runat="server" Text="Cortar" />
        <asp:Button ID="btnLivre" CssClass="livre" runat="server" Text="Livre" />
        <asp:Button ID="btnDestaque1" CssClass="destaque1" runat="server" Text="Destaque 1" />
        <asp:Button ID="btnDestaque2" CssClass="destaque2" runat="server" Text="Destaque 2" />
        <asp:Button ID="btnDestaque3" CssClass="destaque3" runat="server" Text="Destaque 3" />
        <asp:Button ID="btnDiminuir" CssClass="diminuir" runat="server" Text="Diminuir" />
        <asp:Button ID="btnAumentar" CssClass="aumentar" runat="server" Text="Aumentar" />--%>
    </div>
    </form>
</body>
</html>
