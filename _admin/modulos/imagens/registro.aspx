﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_imagens_registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        $(function () {
            $('.imagem').click(function () {
                var url = $(this).attr('data-url');
                window.open(url, 'imagem', '');
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li id="liUpload" runat="server"><asp:LinkButton ID="lnkUpload" runat="server" onclick="lnkUpload_Click"><span><asp:Image ID="Image2" ImageUrl="~/_admin/layout/imagens/ico-upload.png" runat="server" />Upload</span></asp:LinkButton></li>
        <li id="liSalvar" runat="server"><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <table id="tbUpload" runat="server" class="formulario">
            <tr>
                <td class="texto">Imagem:</td>
                <td>
                    <asp:FileUpload ID="uplImagem" runat="server" accept="image/*" AllowMultiple="true"  />
                    <div class="preview">
                        <img id="imgCrop" src="" class="imgCrop" />
                        <asp:HiddenField ID="X" runat="server" />
                        <asp:HiddenField ID="Y" runat="server" />
                        <asp:HiddenField ID="W" runat="server" />
                        <asp:HiddenField ID="H" runat="server" />
                        <asp:Button ID="btnCortar" runat="server" Text="Cortar" />
                        <asp:Button ID="btnDestaque1" CssClass="destaque1" runat="server" Text="Destaque 1" />
                        <asp:Button ID="btnDestaque2" CssClass="destaque2" runat="server" Text="Destaque 2" />
                        <asp:Button ID="btnDestaque3" CssClass="destaque3" runat="server" Text="Destaque 3" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="texto"></td>
                <td>
                    <asp:Label ID="lblErro" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table id="tbSalvar" runat="server" class="formulario">
            <tr>
                <td colspan="2">
                    <table style="margin:0 0 20px 0;">
                        <tr>
                            <td class="texto">
                                <asp:Literal ID="lblImagem" runat="server"></asp:Literal><br />
<%--                                <asp:LinkButton ID="lnkSalvarImagem" class="botao" runat="server" onclick="lnkSalvar_Click"><span><img src="../../layout/imagens/ico-edit.png" />Editar</span></asp:LinkButton>--%>
                            </td>
                            <td style=" vertical-align:text-top;">
                                <asp:Literal ID="lblInformacoes" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Titulo: 
                </td>
                <td>
                    <asp:TextBox ID="txtTitulo" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
               <td class="texto">
                    Url parcial:</td>
                <td>
                    <asp:TextBox ID="txtUrlParcial" runat="server" CssClass="campo" MaxLength="80" ReadOnly="True" BackColor="#f1f1f1"></asp:TextBox>
                </td>
            </tr>
            <tr>
               <td class="texto">
                    Url completa:</td>
                <td>
                    <asp:TextBox ID="txtUrl" runat="server" CssClass="campo" MaxLength="80" ReadOnly="True" BackColor="#f1f1f1"></asp:TextBox>
                </td>
            </tr>
            </table>
    </div>
</asp:Content>

