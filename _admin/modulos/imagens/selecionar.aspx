﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="selecionar.aspx.cs" Inherits="_admin_modulos_imagens_selecionar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../layout/css/geral.css" rel="stylesheet" type="text/css" />
    <link href="../../layout/css/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../../layout/js/jquery.js" type="text/javascript"></script>
    <script src="../../layout/js/jquery.cookie.js" type="text/javascript"></script>
    <script src="../../layout/js/geral.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <header>
            <div class="menu-topo">
                <ul>
                    <li>
                        <asp:FileUpload ID="uplImagem" CssClass="upload-file" runat="server" accept="image/*" onchange="javascript:__doPostBack('lnkUpload','')"  />
                        <asp:LinkButton ID="lnkUpload" CssClass="upload-button" runat="server" onclick="lnkUpload_Click"><span><asp:Image ID="Image4" ImageUrl="~/_admin/layout/imagens/ico-upload.png" runat="server" />Upload</span></asp:LinkButton>
                        <%--<span class="upload"><asp:LinkButton ID="lnkUpload" runat="server" onclick="lnkUpload_Click"><span><asp:Image ID="Image3" ImageUrl="~/_admin/layout/imagens/ico-upload.png" runat="server" />Upload</span></asp:LinkButton><asp:FileUpload ID="uplImagem" runat="server" accept="image/*" onchange="javascript:__doPostBack('lnkUpload','')"  /></span>--%>
                    </li>
                    <li><asp:LinkButton ID="lnkTodos" runat="server" onclick="btnTodos_Click"><span><asp:Image ID="Image2" ImageUrl="~/_admin/layout/imagens/ico-all.png" runat="server" />Todos</span></asp:LinkButton></li>
                </ul>
            </div>
            <div class="menu-topo-direito">
                <ul>
                    <li><asp:TextBox ID="txtPesquisar" runat="server" AutoPostBack="True" 
                            ontextchanged="txtPesquisar_TextChanged" Width="136px"></asp:TextBox>
                        <asp:LinkButton ID="btnPesquisar" runat="server" onclick="btnPesquisar_Click"><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-search.png" runat="server" />Pesquisar</asp:LinkButton></li>
                </ul>
            </div>
        </header>
    </div>
    <div class="margem">
        <!-- Grid Inicio -->
        <asp:GridView ID="grdwListar" runat="server" Width="100%" PagerSettings-Visible="True"
            AutoGenerateColumns="false" AllowPaging="True" 
            CssClass="listagem" OnRowDataBound="grdwListar_RowDataBound"
            GridLines="None" onprerender="grdwListar_PreRender" AllowSorting="True" 
            onsorting="grdwListar_Sorting" onrowcreated="grdwListar_RowCreated">
            <Columns>
                <asp:TemplateField HeaderText="Imagem">
                    <ItemTemplate>
                        <asp:Literal ID="lblImagem" runat="server"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                    <HeaderStyle HorizontalAlign="Center" Width="60px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Titulo" SortExpression="Titulo">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Titulo")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Dt. Inclusão" SortExpression="DataInclusao">
                    <ItemTemplate>
                        <asp:Literal ID="lblDataInclusao" runat="server"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" Width="90px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Op&#231;&#245;es">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgSelecionar" runat="server" ImageUrl="~/_admin/layout/imagens/ico-select-dark.png" ImageAlign="absbottom" CssClass="opcoes" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                    <HeaderStyle HorizontalAlign="Center" Width="90px" />
                </asp:TemplateField>
            </Columns>
            <PagerTemplate>
                <div class="total-de-registros" >
                    <asp:Literal ID="lblTotalDeIRegistros" runat="server"></asp:Literal>
                    registros
                </div>
                <div class="paginacao-setas">
                    <asp:ImageButton ID="btnPrimeiro" runat="server" AlternateText="Primeira página"
                        ImageUrl="~/_admin/layout/imagens/ico-first-dark.png" 
                        onclick="btnPrimeiro_Click" />
                    <asp:ImageButton ID="btnAnterior" runat="server" AlternateText="Página anterior"
                        ImageUrl="~/_admin/layout/imagens/ico-back-dark.png" 
                        onclick="btnAnterior_Click" />

                    <asp:TextBox ID="txtPaginaAtual" runat="server" Width="25" MaxLength="3"></asp:TextBox>
                    de
                    <asp:Literal ID="lblTotalDePaginas" runat="server"></asp:Literal>
                    <asp:Button ID="btnIr" runat="server" Text=" Ir " onclick="btnIr_Click" />

                    <asp:ImageButton ID="btnProximo" runat="server" AlternateText="Próxima página"
                        ImageUrl="~/_admin/layout/imagens/ico-next-dark.png" 
                        onclick="btnProximo_Click" />
                    <asp:ImageButton ID="btnUltimo" runat="server" AlternateText="Ultima página"
                        ImageUrl="~/_admin/layout/imagens/ico-last-dark.png" 
                        onclick="btnUltimo_Click" />
                </div>
            </PagerTemplate>
            <HeaderStyle CssClass="cabecalho" />
            <AlternatingRowStyle CssClass="registros" />
            <RowStyle CssClass="registros" />
            <PagerStyle CssClass="paginacao" />
        </asp:GridView>
        <!-- Grid Final -->
    </div>
    <%--<div class="preview">
        <img id="imgCrop" src="" class="imgCrop" />
        <asp:HiddenField ID="X" runat="server" />
        <asp:HiddenField ID="Y" runat="server" />
        <asp:HiddenField ID="W" runat="server" />
        <asp:HiddenField ID="H" runat="server" />
        <asp:Button ID="btnCortar" runat="server" Text="Cortar" />
        <asp:Button ID="btnLivre" CssClass="livre" runat="server" Text="Livre" />
        <asp:Button ID="btnDestaque1" CssClass="destaque1" runat="server" Text="Destaque 1" />
        <asp:Button ID="btnDestaque2" CssClass="destaque2" runat="server" Text="Destaque 2" />
        <asp:Button ID="btnDestaque3" CssClass="destaque3" runat="server" Text="Destaque 3" />
        <asp:Button ID="btnDiminuir" CssClass="diminuir" runat="server" Text="Diminuir" />
        <asp:Button ID="btnAumentar" CssClass="aumentar" runat="server" Text="Aumentar" />
    </div>--%>
    </form>
</body>
</html>
