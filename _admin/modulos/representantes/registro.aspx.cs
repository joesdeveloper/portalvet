﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _admin_modulos_representantes_registro : System.Web.UI.Page
{
    BLL.Representantes bRep = new BLL.Representantes();
    VO.Representante vRep = new VO.Representante();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\Representantes\";

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }


    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vRep.IdRepresentante = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vRep = bRep.Consultar(vRep.IdRepresentante);

        if (vRep != null)
        {
            LimparDados();
            txtNome.Text = vRep.Nome;
            txtEmail1.Text = vRep.Email1;
            txtEmail2.Text = vRep.Email2;
            txtTelefone1.Text = vRep.Telefone1;
            txtTelefone2.Text = vRep.Telefone2;
            txtTelefone3.Text = vRep.Telefone3;
            txtTelefone4.Text = vRep.Telefone4;
            txtCelular.Text = vRep.Celular;
            txtFax.Text = vRep.Fax;
            txtNextelId.Text = vRep.NextelId;
            txtEndereco.Text = vRep.Endereco;
            txtNumero.Text = vRep.Numero;
            txtComplemento.Text = vRep.Complemento;
            txtBairro.Text = vRep.Bairro;
            txtCidade.Text = vRep.Cidade;
            ddlEstado.Items.FindByValue(vRep.Estado).Selected = true;
            txtCep.Text = vRep.Cep;
            cbxAtivo.Checked = vRep.Ativo;
        }
    }


    private void LimparDados()
    {
        txtNome.Text = "";
        txtEmail1.Text = "";
        txtEmail2.Text = "";
        txtTelefone1.Text = "";
        txtTelefone2.Text = "";
        txtTelefone3.Text = "";
        txtTelefone4.Text = "";
        txtCelular.Text = "";
        txtFax.Text = "";
        txtNextelId.Text = "";
        txtEndereco.Text = "";
        txtNumero.Text = "";
        txtComplemento.Text = "";
        txtBairro.Text = "";
        txtCidade.Text = "";
        ddlEstado.SelectedIndex = -1;
        txtCep.Text = "";
        cbxAtivo.Checked = false;
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
            vRep.IdRepresentante = Convert.ToInt32(Request["id"]);

        vRep.Nome = txtNome.Text;
        vRep.Email1 = txtEmail1.Text;
        vRep.Email2 = txtEmail2.Text;
        vRep.Telefone1 = txtTelefone1.Text;
        vRep.Telefone2 = txtTelefone2.Text;
        vRep.Telefone3 = txtTelefone3.Text;
        vRep.Telefone4 = txtTelefone4.Text;
        vRep.Celular = txtCelular.Text;
        vRep.Fax = txtFax.Text;
        vRep.NextelId = txtNextelId.Text;
        vRep.Endereco = txtEndereco.Text;
        vRep.Numero = txtNumero.Text;
        vRep.Complemento = txtComplemento.Text;
        vRep.Bairro = txtBairro.Text;
        vRep.Cidade = txtCidade.Text;
        vRep.Estado = ddlEstado.SelectedValue;
        vRep.Cep = txtCep.Text;
        vRep.Ativo = cbxAtivo.Checked;
    }


    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    protected void lnkSalvar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        vRep = bRep.Salvar(vRep);

        if (!Notificacao.Erro)
        {
            if (Request["id"] == null)
                Response.Redirect("registro.aspx?id=" + vRep.IdRepresentante);

            IniciarPagina();
        }

        ExibirMensagem();
    }
}