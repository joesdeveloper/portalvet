﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_representantes_registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script>
    $(document).ready(function () {
        $(".sombra").fancybox({
            maxWidth: 800,
            maxHeight: 600,
            fitToView: false,
            width: '70%',
            height: '70%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'
        });
        $("#conteudo_txtTelefone1").mask("(99)9999-9999");
        $("#conteudo_txtTelefone2").mask("(99)9999-9999");
        $("#conteudo_txtTelefone3").mask("(99)9999-9999");
        $("#conteudo_txtTelefone4").mask("(99)9999-9999");
        $("#conteudo_txtCelular").mask("(99)99999-9999");
        $("#conteudo_txtFax").mask("(99)9999-9999");
    });
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
        <table class="formulario">
            <tr>
                <td class="texto">
                    Nome: 
                </td>
                <td>
                    <asp:TextBox ID="txtNome" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Endereço:
                </td>
                <td>
                    <asp:TextBox ID="txtEndereco" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Numero:
                </td>
                <td>
                    <asp:TextBox ID="txtNumero" runat="server" CssClass="campo" MaxLength="10" Width="100px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Complemento:
                </td>
                <td>
                    <asp:TextBox ID="txtComplemento" runat="server" CssClass="campo" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Bairro:
                </td>
                <td>
                    <asp:TextBox ID="txtBairro" runat="server" CssClass="campo" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Cidade:
                </td>
                <td>
                    <asp:TextBox ID="txtCidade" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Estado:
                </td>
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server">
                        <asp:ListItem>AC</asp:ListItem>  
                        <asp:ListItem>AL</asp:ListItem>  
                        <asp:ListItem>AM</asp:ListItem>  
                        <asp:ListItem>AP</asp:ListItem>  
                        <asp:ListItem>BA</asp:ListItem>  
                        <asp:ListItem>CE</asp:ListItem>  
                        <asp:ListItem>DF</asp:ListItem>  
                        <asp:ListItem>ES</asp:ListItem>  
                        <asp:ListItem>GO</asp:ListItem>  
                        <asp:ListItem>MA</asp:ListItem>  
                        <asp:ListItem>MG</asp:ListItem>  
                        <asp:ListItem>MS</asp:ListItem>  
                        <asp:ListItem>MT</asp:ListItem>  
                        <asp:ListItem>PA</asp:ListItem>  
                        <asp:ListItem>PB</asp:ListItem>  
                        <asp:ListItem>PE</asp:ListItem>  
                        <asp:ListItem>PI</asp:ListItem>  
                        <asp:ListItem>PR</asp:ListItem>  
                        <asp:ListItem>RJ</asp:ListItem>  
                        <asp:ListItem>RN</asp:ListItem>  
                        <asp:ListItem>RO</asp:ListItem>  
                        <asp:ListItem>RR</asp:ListItem>  
                        <asp:ListItem>RS</asp:ListItem>  
                        <asp:ListItem>SC</asp:ListItem>  
                        <asp:ListItem>SE</asp:ListItem>  
                        <asp:ListItem>SP</asp:ListItem>  
                        <asp:ListItem>TO</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Cep:
                </td>
                <td>
                    <asp:TextBox ID="txtCep" runat="server" CssClass="campo" MaxLength="8" Width="100px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">E-mail 1:</td>
                <td>
                    <asp:TextBox ID="txtEmail1" runat="server" CssClass="campo" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">E-mail 2:</td>
                <td>
                    <asp:TextBox ID="txtEmail2" runat="server" CssClass="campo" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Telefone 1:
                </td>
                <td>
                    <asp:TextBox ID="txtTelefone1" runat="server" CssClass="campo" MaxLength="14"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Telefone 2:
                </td>
                <td>
                    <asp:TextBox ID="txtTelefone2" runat="server" CssClass="campo" MaxLength="14"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Telefone 3:
                </td>
                <td>
                    <asp:TextBox ID="txtTelefone3" runat="server" CssClass="campo" MaxLength="14"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Telefone 4:
                </td>
                <td>
                    <asp:TextBox ID="txtTelefone4" runat="server" CssClass="campo" MaxLength="14"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Celular:
                </td>
                <td>
                    <asp:TextBox ID="txtCelular" runat="server" CssClass="campo" MaxLength="14"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Fax:
                </td>
                <td>
                    <asp:TextBox ID="txtFax" runat="server" CssClass="campo" MaxLength="14"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    ID Nextel:
                </td>
                <td>
                    <asp:TextBox ID="txtNextelId" runat="server" CssClass="campo" MaxLength="14"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Ativo:
                </td>
                <td>
                    <asp:CheckBox ID="cbxAtivo" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

