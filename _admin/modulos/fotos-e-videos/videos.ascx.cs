﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class _admin_modulos_fotos_e_videos_videos : System.Web.UI.UserControl
{
    BLL.Galerias bGal = new BLL.Galerias();
    VO.VideoPorGaleria vGalVid = new VO.VideoPorGaleria();

    const int _registrosPorPagina = 1000;

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarGridView();
    }

    private void LimparDados()
    {
        txtTitulo.Text = "";
        txtYoutube.Text = "";
    }


    #region Lista
    private void InicarSessao()
    {
        if (ViewState["coluna"] == null)
        {
            ViewState["coluna"] = "DataInclusao";
            ViewState["ascendente"] = false;
        }
    }

    private void IniciarGridView()
    {
        InicarSessao();

        if (Request["id"] != null)
        {
            var IdVideo = Convert.ToInt32(Request["id"]);

            bool ascendente = Convert.ToBoolean(ViewState["ascendente"]);
            string coluna = ViewState["coluna"].ToString();

            var lista = bGal.ListarVideos(IdVideo, coluna, ascendente);

            grdwListar.PageSize = _registrosPorPagina;
            grdwListar.PageIndex = 0;
            grdwListar.DataSource = lista;
            grdwListar.DataBind();
        }
    }


    protected void grdwListar_Sorting(object sender, GridViewSortEventArgs e)
    {
        string coluna = e.SortExpression;

        if (coluna == ViewState["coluna"].ToString())
        {
            if (Convert.ToBoolean(ViewState["ascendente"]))
                ViewState["ascendente"] = false;
            else
                ViewState["ascendente"] = true;
        }
        else
            ViewState["coluna"] = coluna;

        IniciarGridView();
    }


    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var btnExcluir = (ImageButton)e.Row.FindControl("btnExcluir");
            btnExcluir.Attributes.Add("idRegistro", DataBinder.Eval(e.Row.DataItem, "IdVideo").ToString());
            btnExcluir.Attributes.Add("data-url", DataBinder.Eval(e.Row.DataItem, "Url").ToString());

            var btnVisualizar = (ImageButton)e.Row.FindControl("btnVisualizar");
            btnVisualizar.Attributes.Add("onclick", "window.open('" + DataBinder.Eval(e.Row.DataItem, "Url") + "', '_blank'); return false;");

            var lblDataInclusao = (Literal)e.Row.FindControl("lblDataInclusao");
            if (DataBinder.Eval(e.Row.DataItem, "DataInclusao") != null)
                lblDataInclusao.Text = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DataInclusao")).ToString("dd/MM/yy HH:mm");
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            var txtPaginaAtual = (TextBox)e.Row.FindControl("txtPaginaAtual");
            txtPaginaAtual.Text = (Convert.ToInt32(ViewState["paginaAtual"]) + 1).ToString();

            var lblTotalDePaginas = (Literal)e.Row.FindControl("lblTotalDePaginas");
            lblTotalDePaginas.Text = ViewState["totalPaginas"].ToString();

            var lblTotalDeIRegistros = (Literal)e.Row.FindControl("lblTotalDeIRegistros");
            lblTotalDeIRegistros.Text = ViewState["totalDeRegistros"].ToString();
        }
    }


    protected void grdwListar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        var image = new Image();
        var separador = new Image();

        if (e.Row != null && e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                if (cell.HasControls())
                {
                    var button = cell.Controls[0] as LinkButton;
                    if (button != null)
                    {
                        if (ViewState["coluna"].ToString().Trim() == button.CommandArgument)
                        {
                            if (Convert.ToBoolean(ViewState["ascendente"]))
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_cima.png";
                            else
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_baixo.png";

                            image.AlternateText = "Middle";
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }
    }


    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        var botao = (ImageButton)sender;

        var id = Convert.ToInt32(botao.Attributes["idRegistro"]);

        bGal.ExcluirVideo(id);

        IniciarGridView();
    }

    #endregion



    public void Salvar(int IdGaleria)
    {
        vGalVid.IdGaleria = IdGaleria;
        vGalVid.Titulo = txtTitulo.Text;
        vGalVid.Url = txtYoutube.Text.TrimStart().TrimEnd();

        bGal.Salvar(vGalVid);

        LimparDados();
        IniciarGridView();
    }
}