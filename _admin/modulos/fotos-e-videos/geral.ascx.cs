﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Drawing;

public partial class _admin_modulos_fotos_e_videos_geral : System.Web.UI.UserControl
{
    BLL.Galerias bGal = new BLL.Galerias();
    VO.Galeria vGal = new VO.Galeria();
    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\fotos-e-videos\";

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarPagina();
        }
    }

    private void IniciarPagina()
    {
        LimparDados();

        if (Request["id"] != null)
        {
            vGal.IdGaleria = Convert.ToInt32(Request["id"]);
            IniciarFormulario();
        }
    }


    private void IniciarFormulario()
    {
        vGal = bGal.Consultar(vGal.IdGaleria);

        if (vGal != null)
        {
            LimparDados();


            lblImagem.Text = "<div class=\"imagem\" style=\"background-image:url('" + oVal.UrlBase() + "arquivos/fotos-e-videos/" + vGal.IdGaleria + "/imagem.png');\" data-url=\"" + oVal.UrlBase() + "arquivos/fotos-e-videos/" + vGal.IdGaleria + "/imagem.png\"></div>";
            var bmp = new Bitmap(Server.MapPath("~/arquivos/fotos-e-videos/" + vGal.IdGaleria + "/imagem.png"));
                lblInformacoes.Text =
                    "<div class=\"imagens-info\"><strong>Nome do arquivo:</strong> " + Path.GetFileName("~/arquivos/fotos-e-videos/" + vGal.IdGaleria + "/imagem.png") + " </div>" +
                    "<div class=\"imagens-info\"><strong>Tipo do arquivo:</strong> " + Path.GetExtension("~/arquivos/fotos-e-videos/" + vGal.IdGaleria + "/imagem.png") + " </div>" +
                    "<div class=\"imagens-info\"><strong>Data de upload:</strong> " + vGal.DataInclusao.ToString("dd/MM/yyyy HH:mm:ss") + "</div>" +
                    "<div class=\"imagens-info\"><strong>Dimensões:</strong> " + bmp.Width + " x " + bmp.Height + "</div>" +
                    "<div class=\"imagens-info\"><strong>Url parcial:</strong> arquivos/fotos-e-videos/" + vGal.IdGaleria + "/imagem.png" + "</div>" +
                    "<div class=\"imagens-info\"><strong>Url completa:</strong> " + oVal.UrlBase() + "arquivos/fotos-e-videos/" + vGal.IdGaleria + "/imagem.png" + "</div>";
        
            txtTitulo.Text = vGal.Titulo;
            txtDescricao.Text = vGal.Descricao;
            cbxAtivo.Checked = vGal.Ativo;
        }
    }


    private void LimparDados()
    {
        txtTitulo.Text = "";
        txtDescricao.Text = "";
        cbxAtivo.Checked = false;
    }


    private void CaptarDados()
    {
        if (Request["id"] != null)
            vGal.IdGaleria = Convert.ToInt32(Request["id"]);

        vGal.Titulo = txtTitulo.Text;
        vGal.Descricao = txtDescricao.Text;
        vGal.Ativo = cbxAtivo.Checked;
    }


    private bool ValidarDados()
    {
        if (Notificacao.Erro)
            return false;

        if (vGal.IdGaleria == 0 && !uplImagem.HasFile)
            _erro += @"- Imagem não pode ser nula.<br />";

        if (vGal.Titulo.Trim().Length == 0)
            _erro += @"- Titulo não pode ser nulo.<br />";

        if (_erro.Length > 0)
        {
            Notificacao.Erro = true;
            Notificacao.Status = Notificacao.Tipo.Atencao;
            Notificacao.Texto = _erro;

            return false;
        }

        return true;
    }

    private void ExibirMensagem()
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "notificar", Notificacao.Exibir(), true);
    }


    public VO.Galeria Salvar()
    {
        CaptarDados();

        if (ValidarDados())
        {
            vGal = bGal.Salvar(vGal);

            if (Notificacao.Erro == false)
                Upload();

            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            //if (!Notificacao.Erro)
            //{
            //    if (Request["id"] == null)
            //        Response.Redirect("registro.aspx?id=" + vGal.IdGaleria);
            //}

            //IniciarFormulario();

            //_erro = @"PARABÉNS:<br />O registro foi salvo com sucesso.<br />";
        }

        return vGal;
    }

    private void Upload()
    {
        var _nome = "";
        var _extensao = "";
        var caminhoAux = caminho + vGal.IdGaleria + @"\";

        if (!Directory.Exists(caminhoAux))
            Directory.CreateDirectory(caminhoAux);


        if (uplImagem.HasFile)
        {
            _nome = uplImagem.FileName;
            _extensao = Path.GetExtension(_nome).ToLower();

            uplImagem.PostedFile.SaveAs(caminhoAux + "t-imagem" + _extensao);

            new UTIL.EdicaoDeImagem().Redimencionar(caminhoAux + "t-imagem" + _extensao, caminhoAux + "imagem.png", 700, 700, UTIL.EdicaoDeImagem.Formato.Png);

            try
            {
                File.Delete(caminhoAux + "t-imagem" + _extensao);
            }
            catch { }
        }

        IniciarPagina();
    }
}