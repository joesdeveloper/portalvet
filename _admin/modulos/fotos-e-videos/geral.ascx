﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="geral.ascx.cs" Inherits="_admin_modulos_fotos_e_videos_geral" %>

        <table class="formulario">
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="texto">
                                <asp:Literal ID="lblImagem" runat="server"></asp:Literal><br />
                            </td>
                            <td style=" vertical-align:text-top;">
                                <asp:Literal ID="lblInformacoes" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="texto">Imagem:</td>
                <td>
                    <asp:FileUpload ID="uplImagem" runat="server" />
                    <br />(Tamanho: 480x360px)
                </td>
            </tr>
            <tr>
                <td class="texto">Titulo:</td>
                <td><asp:TextBox ID="txtTitulo" runat="server" CssClass="campo" MaxLength="80"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="texto">
                    Descrição:
                </td>
                <td>
                    <asp:TextBox ID="txtDescricao" runat="server" CssClass="campo" TextMode="MultiLine" Rows="4"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">
                    Ativo:
                </td>
                <td>
                    <asp:CheckBox ID="cbxAtivo" runat="server" />
                </td>
            </tr>
        </table>