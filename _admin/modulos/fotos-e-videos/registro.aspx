﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_admin/administracao.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="_admin_modulos_fotos_e_videos_registro" %>

<%@ Register src="geral.ascx" tagname="geral" tagprefix="uc1" %>


<%@ Register src="fotos.ascx" tagname="fotos" tagprefix="uc2" %>


<%@ Register src="videos.ascx" tagname="videos" tagprefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        $(function () {
            $('.imagem').click(function () {
                var url = $(this).attr('data-url');
                window.open(url, 'imagem', '');
            });
        });
        </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_topo" Runat="Server">
    <ul>
        <li><asp:LinkButton ID="lnkSalvar" runat="server" onclick="lnkSalvar_Click"><span><asp:Image ID="Image1" ImageUrl="~/_admin/layout/imagens/ico-save.png" runat="server" />Salvar</span></asp:LinkButton></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_topo_direito" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" Runat="Server">
    <div class="titulo">
        Registro
        <!-- breadcrumb -->
        <asp:SiteMapPath ID="breadcrumb" runat="server" CssClass="breadcrumb">
        </asp:SiteMapPath>
    </div>
    <div class="margem">
         <div class="aba-painel">
            <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" 
                onmenuitemclick="Menu1_MenuItemClick" CssClass="aba">
                <Items>
                    <asp:MenuItem Text="Geral" Value="Geral" Selected="true"></asp:MenuItem>
                    <asp:MenuItem Text="Fotos" Value="Fotos"></asp:MenuItem>
                    <asp:MenuItem Text="Vídeos" Value="Videos"></asp:MenuItem>
                </Items>
            </asp:Menu>
        </div>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="Geral" runat="server">
                <uc1:geral ID="geral1" runat="server" />
            </asp:View>
            <asp:View ID="Fotos" runat="server">
                <uc2:fotos ID="fotos1" runat="server" />
            </asp:View>
            <asp:View ID="Video" runat="server">
                <uc3:videos ID="videos1" runat="server" />
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
