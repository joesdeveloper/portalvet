﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class _admin_modulos_fotos_e_videos_fotos : System.Web.UI.UserControl
{
    BLL.Galerias bGal = new BLL.Galerias();
    VO.ImagemPorGaleria vGalImg = new VO.ImagemPorGaleria();

    const int _registrosPorPagina = 1000;
    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\galerias\";

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            IniciarGridView();
    }


    #region Lista
    private void InicarSessao()
    {
        if (ViewState["coluna"] == null)
        {
            ViewState["coluna"] = "DataInclusao";
            ViewState["ascendente"] = false;
        }
    }

    private void IniciarGridView()
    {
        InicarSessao();

        if (Request["id"] != null)
        {
            var IdImagem = Convert.ToInt32(Request["id"]);

            bool ascendente = Convert.ToBoolean(ViewState["ascendente"]);
            string coluna = ViewState["coluna"].ToString();

            var lista = bGal.ListarImagens(IdImagem, coluna, ascendente);

            grdwListar.PageSize = _registrosPorPagina;
            grdwListar.PageIndex = 0;
            grdwListar.DataSource = lista;
            grdwListar.DataBind();
        }
    }


    protected void grdwListar_Sorting(object sender, GridViewSortEventArgs e)
    {
        string coluna = e.SortExpression;

        if (coluna == ViewState["coluna"].ToString())
        {
            if (Convert.ToBoolean(ViewState["ascendente"]))
                ViewState["ascendente"] = false;
            else
                ViewState["ascendente"] = true;
        }
        else
            ViewState["coluna"] = coluna;

        IniciarGridView();
    }


    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var btnExcluir = (ImageButton)e.Row.FindControl("btnExcluir");
            btnExcluir.Attributes.Add("idRegistro", DataBinder.Eval(e.Row.DataItem, "IdImagem").ToString());
            btnExcluir.Attributes.Add("data-url", DataBinder.Eval(e.Row.DataItem, "Url").ToString());

            var btnVisualizar = (ImageButton)e.Row.FindControl("btnVisualizar");
            btnVisualizar.Attributes.Add("onclick", "window.open('../../../" + DataBinder.Eval(e.Row.DataItem, "Url") + "', '_blank'); return false;");

            var lblDataInclusao = (Literal)e.Row.FindControl("lblDataInclusao");
            if (DataBinder.Eval(e.Row.DataItem, "DataInclusao") != null)
                lblDataInclusao.Text = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DataInclusao")).ToString("dd/MM/yy HH:mm");
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            var txtPaginaAtual = (TextBox)e.Row.FindControl("txtPaginaAtual");
            txtPaginaAtual.Text = (Convert.ToInt32(ViewState["paginaAtual"]) + 1).ToString();

            var lblTotalDePaginas = (Literal)e.Row.FindControl("lblTotalDePaginas");
            lblTotalDePaginas.Text = ViewState["totalPaginas"].ToString();

            var lblTotalDeIRegistros = (Literal)e.Row.FindControl("lblTotalDeIRegistros");
            lblTotalDeIRegistros.Text = ViewState["totalDeRegistros"].ToString();
        }
    }

    protected void grdwListar_RowCreated(object sender, GridViewRowEventArgs e)
    {
        var image = new Image();
        var separador = new Image();

        if (e.Row != null && e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                if (cell.HasControls())
                {
                    var button = cell.Controls[0] as LinkButton;
                    if (button != null)
                    {
                        if (ViewState["coluna"].ToString().Trim() == button.CommandArgument)
                        {
                            if (Convert.ToBoolean(ViewState["ascendente"]))
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_cima.png";
                            else
                                image.ImageUrl = "~/_admin/layout/imagens/ico_seta_baixo.png";

                            image.AlternateText = "Middle";
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }
    }


    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        var botao = (ImageButton)sender;

        var id = Convert.ToInt32(botao.Attributes["idRegistro"]);
        var caminho = Server.MapPath("~/" + botao.Attributes["data-url"]);

        bGal.ExcluirImagem(id);

        if (File.Exists(caminho))
            File.Delete(caminho);

        IniciarGridView();
    }

    #endregion



    private bool ValidarDados()
    {
        if (Notificacao.Erro)
            return false;

        return true;
    }

    //protected void lnkUpload_Click(object sender, EventArgs e)

    public void Salvar(int IdGaleria)
    {
        vGalImg.IdGaleria = IdGaleria;
        caminho = caminho + @"\" + vGalImg.IdGaleria + @"\";

        if (!Directory.Exists(caminho))
            Directory.CreateDirectory(caminho);

        if (ValidarDados())
        {
            if (uplImagem.HasFile)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var arquivo = (HttpPostedFile)Request.Files[i];

                    Upload(arquivo);
                }

                IniciarGridView();
            }
        }
    }



    private void Upload(HttpPostedFile arquivo)
    {
        var nome = Path.GetFileName(arquivo.FileName);
        var extensao = Path.GetExtension(arquivo.FileName).ToLower();

        nome = nome.Replace(extensao, "");
        vGalImg.Titulo = nome;
        nome = oVal.CaracteresEspeciais(nome);
        nome = oVal.Acentuacao(nome).Replace(" ", "-");

        var caminhoAux = caminho + nome + extensao;

        if (File.Exists(caminhoAux))
        {
            var i = 0;
            for (; ; )
            {
                i++;
                caminhoAux = caminho + nome + i + extensao;
                if (!File.Exists(caminhoAux))
                {
                    nome = nome + i;
                    break;
                }
            }
        }

        arquivo.SaveAs(caminho + nome + extensao);

        vGalImg.Url = @"arquivos/galerias/" + vGalImg.IdGaleria + @"/" + nome + extensao;

        bGal.Salvar(vGalImg);
    }

}