﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="videos.ascx.cs" Inherits="_admin_modulos_fotos_e_videos_videos" %>
        <table class="formulario">
            <tr>
                <td class="texto">Titulo:</td>
                <td>
                    <asp:TextBox ID="txtTitulo" runat="server" MaxLength="80"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Url Youtube:</td>
                <td>
                    <asp:TextBox ID="txtYoutube" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="texto">Víd. Enviados:</td>
                <td>
                    <div class="margem">
                        <!-- Grid Inicio -->
                        <asp:GridView ID="grdwListar" runat="server" Width="100%" AutoGenerateColumns="false" 
                            CssClass="listagem" OnRowDataBound="grdwListar_RowDataBound" GridLines="None" 
                            AllowSorting="True" onsorting="grdwListar_Sorting" 
                            onrowcreated="grdwListar_RowCreated" >
                            <Columns>
                                <asp:TemplateField HeaderText="Titulo" SortExpression="Titulo">
                                    <ItemTemplate>
                                        <%# DataBinder.Eval(Container.DataItem, "Titulo")%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dt. Inclusão" SortExpression="DataInclusao">
                                    <ItemTemplate>
                                        <asp:Literal ID="lblDataInclusao" runat="server"></asp:Literal>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    <HeaderStyle HorizontalAlign="Center" Width="90px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Op&#231;&#245;es">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnVisualizar" runat="server" ImageUrl="~/_admin/layout/imagens/ico-video-dark.png" ImageAlign="absbottom" CssClass="opcoes" />
                                        <asp:ImageButton ID="btnExcluir" OnClick="btnExcluir_Click" runat="server" ImageUrl="~/_admin/layout/imagens/ico-delete-dark.png" ImageAlign="absbottom" CssClass="opcoes" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    <HeaderStyle HorizontalAlign="Center" Width="90px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecalho" />
                            <AlternatingRowStyle CssClass="registros" />
                            <RowStyle CssClass="registros" />
                        </asp:GridView>
                        <!-- Grid Final -->
                    </div>
                </td>
            </tr>
        </table>