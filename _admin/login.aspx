﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="_admin_login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <!--[if IE]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div class="login">
        <div class="autenticacao">
            <div class="topo"><img src="layout/imagens/img-logo.png" /></div>
            <div class="formulario-autenticacao">
                <table>
                    <tr>
                        <td class="descricao" colspan="2">Preencha os campos "Email" e "Senha" depois clique em "Entrar".</td>
                    </tr>
                    <tr>
                        <td class="texto">E-mail: </td>
                        <td><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="texto">Senha: </td>
                        <td><asp:TextBox ID="txtSenha" runat="server" TextMode="Password"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="texto"></td>
                        <td class="botoes"><asp:LinkButton ID="btnEntrar" CssClass="botao" runat="server" 
                                onclick="btnEntrar_Click">Entrar</asp:LinkButton></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
