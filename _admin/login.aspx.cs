﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Security;

public partial class _admin_login : System.Web.UI.Page
{
    BLL.Administradores bAdm = new BLL.Administradores();
    BLL.Permissoes bPer = new BLL.Permissoes();
    BLL.PermissoesPorAdministradores bPerAdm = new BLL.PermissoesPorAdministradores();

    VO.Administrador vAdm = new VO.Administrador();

    UTIL.Validacao uVal = new UTIL.Validacao();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            FormsAuthentication.SignOut();
    }


    private void CaptarDados()
    {
        vAdm.Email = txtEmail.Text;
        vAdm.Senha = txtSenha.Text;
    }


    private bool ValidarDados()
    {
        _erro = "";

        if (vAdm.Email.Trim().Length == 0)
            _erro += @"- O email não pode ser nulo.\n";
        else
        {
            if (!uVal.Email(vAdm.Email))
                _erro += @"- O email é inválido.\n";
        }

        if (vAdm.Senha.Trim().Length == 0)
            _erro += @"- A senha não pode ser nula.\n";
        else
        {
            if (!uVal.Senha(vAdm.Senha, 6, 20))
                _erro += @"- A senha é inválida.\n";
        }

        if (_erro.Length > 0)
        {
            _erro = @"ATENÇÃO:\n\n" + _erro;
            return false;
        }

        return true;
    }


    private void ExibirMensagem()
    {
        if (_erro.Trim().Length > 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", "alert('" + _erro + "');", true);
    }


    protected void btnEntrar_Click(object sender, EventArgs e)
    {
        Entrar();
    }


    private void Entrar()
    {
        CaptarDados();

        if (ValidarDados())
        {
            var vAdmAux = bAdm.Autenticar(vAdm.Email, vAdm.Senha);

            if (vAdmAux == null)
                _erro = @"ATENÇÃO:\n\n- Email ou senha inválidos.";
            else
            {
                vAdm = vAdmAux;

                //vPer.IdPermissao


                Autenticar();

                bAdm.RegistrarAcesso(vAdm.IdAdministrador);

                Response.Redirect("default.aspx");
            }
        }

        ExibirMensagem();
    }

    private void Autenticar()
    {
        DateTime expiracao = DateTime.Now.AddMinutes(720);
        bool persistente = true;

        string permissoes = "";

        var lPer = bPer.Listar();

        var lPerAdm = bPerAdm.ListarPorAdministrador(vAdm.IdAdministrador);

        foreach (var registro in lPerAdm)
        {
            var vPer = (from aux in lPer
                        where aux.IdPermissao == registro.IdPermissao
                        select aux).Single();

            permissoes += vPer.Nome + ",";
        }

        //Inicializamos o processo de autenticação
        FormsAuthentication.Initialize();

        //Criamos um ticket com os dados do usuário
        //reparem que passamos inclusive os perfis carregados
        //o ticket será a base da nossa autenticação
        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, vAdm.IdAdministrador.ToString(), DateTime.Now, expiracao, persistente, permissoes, FormsAuthentication.FormsCookiePath);

        //Criptografamos o ticket por questão se segurança
        string hash = FormsAuthentication.Encrypt(ticket);

        //Criamos um cookie(será usado para validação do asp.net)
        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

        //Setamos a persistencia do cookie
        if (ticket.IsPersistent)
            cookie.Expires = ticket.Expiration;

        //Em fim adicionamos o cookie a nossa aplicação.
        Response.Cookies.Add(cookie);

        HttpCookie oCookie = new HttpCookie("ADMINISTRADORES");
        oCookie["nome"] = vAdm.Nome;
        oCookie["email"] = vAdm.Email;

        if (vAdm.DataUltimoAcesso != null)
            oCookie["dataUltimoAcesso"] = Convert.ToDateTime(vAdm.DataUltimoAcesso).ToString("dd/MM/yyyy HH:mm");
        else
            oCookie["dataUltimoAcesso"] = "";

        oCookie.Expires = expiracao;
        Response.Cookies.Add(oCookie);
    }
}