﻿$(document).ready(function () {
    SetarThumbnail = function (objId, objUrl, objImg, id, url) {
        $('#' + objId, window.parent.document).val(id);
        $('#' + objUrl, window.parent.document).val(url);
        $('#' + objImg, window.parent.document).css({ 'background-image': 'url("' + url + '")' });
    }

//    var jcrop_api;
//    EditarImagem = function (objId, objImg) {
//        $('.preview').show();
//        var url = $(objId).val();
//        $(objImg).attr('src', url);

//        $(objImg).Jcrop({}, function () {
//            jcrop_api = this;
//        });

//        jcrop_api.setImage(url);
//        jcrop_api.setOptions({ bgOpacity: .2, bgColor: '#fff', allowResize: true, boxWidth: 800, boxHeight: 600 });
//    }

//    storeCoords = function (c) {
//        $('#X').val(c.x);
//        $('#Y').val(c.y);
//        $('#W').val(c.w);
//        $('#H').val(c.h);
//    }

//    $('.livre').click(function () {
//        jcrop_api.setOptions({ allowResize: true });
//        return false;
//    });
//    $('.modelo1').click(function () {
//        jcrop_api.setOptions({ setSelect: [0, 0, 600, 300], allowResize: false });
//        return false;
//    });
//    $('.modelo2').click(function () {
//        jcrop_api.setOptions({ setSelect: [0, 0, 300, 300], allowResize: false });
//        return false;
//    });
//    $('.modelo3').click(function () {
//        jcrop_api.setOptions({ setSelect: [0, 0, 300, 100], allowResize: false });
//        return false;
//    });

    ///////////////////////////////////////////////////////////////
    // JCROP Funcoes
    ///////////////////////////////////////////////////////////////
    //    var jcrop_api;
    //    SetarImagem = function (objId, objUrl, objImg, id, url) {
    //        $('.preview').show();
    //        $('.imgCrop').attr('src', url);

    //        $('#imgCrop').Jcrop({}, function () {
    //            jcrop_api = this;
    //        });

    //        jcrop_api.setImage(url);
    //        jcrop_api.setOptions({ bgOpacity: .4, bgColor: '#fff', boxWidth: 795, boxHeight: 600 });
    //    }
    //    storeCoords = function (c) {
    //        $('#X').val(c.x);
    //        $('#Y').val(c.y);
    //        $('#W').val(c.w);
    //        $('#H').val(c.h);
    //    }
    //    $('.livre').click(function () {
    //        jcrop_api.setOptions({ allowResize: true });
    //        return false;
    //    });
    //    $('.destaque1').click(function () {
    //        jcrop_api.setOptions({ setSelect: [0, 0, 600, 300], allowResize: false });
    //        return false;
    //    });
    //    $('.destaque2').click(function () {
    //        jcrop_api.setOptions({ setSelect: [0, 0, 300, 300], allowResize: false });
    //        return false;
    //    });
    //    $('.destaque3').click(function () {
    //        jcrop_api.setOptions({ setSelect: [0, 0, 300, 100], allowResize: false });
    //        return false;
    //    });
    //        $('.diminuir').click(function () {

    //            var width = $('.imgCrop').width();
    //            var height = $('.imgCrop').height();
    //            var maxWidth = width-10;
    //            var maxHeight = maxHeight - 10;

    //            if (width > maxWidth) {
    //                height = Math.floor(maxWidth * height / width);
    //                width = maxWidth
    //            }

    //            if (height > maxHeight) {
    //                width = Math.floor(maxHeight * width / height);
    //                height = maxHeight;
    //            }
    //            
    //            $('#iW').val(width);
    //            $('#iH').val(height);

    //            $('.imgCrop').width(width);
    //            $('.imgCrop').height(height);

    //            return false;
    //        });
    //        $('.aumentar').click(function () {
    //            var width = $('.imgCrop').width();
    //            var height = $('.imgCrop').height();
    //            var maxWidth = width + 10;
    //            var maxHeight = maxHeight + 10;

    //            if (width < maxWidth) {
    //                height = Math.floor(maxWidth * height / width);
    //                width = maxWidth
    //            }

    //            if (height < maxHeight) {
    //                width = Math.floor(maxHeight * width / height);
    //                height = maxHeight;
    //            }

    //            $('#iW').val(width);
    //            $('#iH').val(height);

    //            $('.imgCrop').width(width);
    //            $('.imgCrop').height(height);


    //            return false;
    //        });

    //        resizeImg = function () {

    //        }


    ///////////////////////////////////////////////////////////////
    // LISTA selecao
    ///////////////////////////////////////////////////////////////
    $("input[id$='chkTodos']").click(function () {
        var selecionado = $(this).is(":checked");
        var listagem = $(this).closest("table");
        $("input[type=checkbox]", listagem).each(function () {
            $(this).prop("checked", selecionado);
            if (selecionado)
                $("td", $(this).closest("tr")).addClass("selecionado");
            else
                $("td", $(this).closest("tr")).removeClass("selecionado");
        });
    });
    $("[id*='chkID']").click(function () {
        var selecionado = $(this).is(":checked");
        if (selecionado)
            $("td", $(this).closest("tr")).addClass("selecionado");
        else
            $("td", $(this).closest("tr")).removeClass("selecionado");
    });
});