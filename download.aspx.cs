﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class download : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var valor = Request["q"].ToString();
        Iniciar(valor);
    }

    private void Iniciar(string arquivo)
    {
        string path = Server.MapPath(arquivo);
        string name = Path.GetFileName(path);
        string ext = Path.GetExtension(path);
        string type = "";

        if (ext != null)
        {
            switch (ext.ToLower())
            {
                case ".htm":
                case ".html":
                    type = "text/HTML";
                    break;

                case ".txt":
                    type = "text/plain";
                    break;

                case ".xml":
                    type = "application/xml";
                    break;

                case ".js":
                    type = "application/javascript";
                    break;

                case ".gif":
                    type = "image/gif";
                    break;

                case ".jpg":
                case ".jpeg":
                    type = "image/jpeg";
                    break;

                case ".doc":
                case ".docx":
                case ".rtf":
                    type = "application/msword";
                    break;

                case ".xls":
                    type = "application/vnd.ms-excel";
                    break;

                case ".mdb":
                    type = "application/x-msaccess";
                    break;

                case ".swf":
                    type = "application/x-shockwave-flash";
                    break;

                case ".zip":
                    type = "application/zip";
                    break;

                case ".avi":
                    type = "video/avi";
                    break;

                case ".wav":
                    type = "audio/wav";
                    break;

                case ".rar":
                    type = "application/x-rar-compressed";
                    break;

                case ".mpeg3":
                    type = "audio/mpeg3";
                    break;

                default:
                    type = "application/octet-stream";
                    break;
            }
        }


        Response.AppendHeader("content-disposition", "attachment; filename=" + name.Replace(" ", "-"));

        if (type != "")
            Response.ContentType = type;

        Response.WriteFile(path);
        Response.End();
    }
}