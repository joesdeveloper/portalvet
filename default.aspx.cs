﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.IO;

public partial class _default : System.Web.UI.Page
{
    BLL.Artigos bArt = new BLL.Artigos();
    BLL.Vitrines bVit = new BLL.Vitrines();
    BLL.Busca bBus = new BLL.Busca();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\vitrines\";

    UTIL.Validacao oVal = new UTIL.Validacao();

    StringBuilder html = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
            InicarSessao();
        }
    }


    public string Vitrine()
    {
        html.Clear();

        var lista = bVit.ListarVitrinesHome();

        html.AppendLine(
                        "<!-- Vitrine -->\n" +
                        "<div id=\"vitrine\">\n" +
                        "    <div id=\"slider1\">\n" +
                        "	        <div class=\"viewport\">\n" +
                        "		        <ul class=\"overview\">\n"
                        );

        for (var i = 0; i < lista.Count; i++)
        {
            var registro = lista[i];

            html.AppendLine("			        <li>");
            
            
            if(registro.Url.Trim().Length > 0)
                html.AppendLine("<a href=\"" + registro.Url + "\" target=\"" + registro.Target + "\" >");

            //html.AppendLine("			            <img src=\"" + registro.VitrineImagem.Url + "\" />");

            if (File.Exists(caminho + registro.IdVitrine + ".png"))
                html.AppendLine("			            <img src=\"arquivos/vitrines/" + registro.IdVitrine + ".png\" />");

            if (registro.Url.Trim().Length > 0)
                html.AppendLine("</a>");

            html.AppendLine("</li>\n");
        }

        html.AppendLine(
                        "		        </ul>\n" +
                        "	        </div>\n" +
                        "        <ul class=\"bullets\">\n"
                        );

        for (var i = 0; i < lista.Count; i++)
        {
            if (i == 0)
                html.AppendLine("            <li><a data-slide=\"" + i + "\" class=\"bullet active\" href=\"#\"></a></li>\n");
            else
                html.AppendLine("            <li><a data-slide=\"" + i + "\" class=\"bullet\" href=\"#\"></a></li>\n");
        }

        html.AppendLine(
                        "        </ul>\n" +
                        "        </div>\n" +
                        "</div>\n"
                        );

        return html.ToString();
    }

    /*
    public string UltimasArtigos()
    {
        html.Clear();

        var lista = bArt.Listar_Artigos_Home();
     
        var destaques = (from aux in lista
                            where aux.Destaque == true
                            orderby aux.DataInclusao descending
                            select aux).ToList();

        var final = "";

        var listaAux = (from aux in lista
                            where aux.Destaque == false
                            orderby aux.DataInclusao descending
                            select aux).ToList();

        for (var i = 0; i < listaAux.Count; i++)
        {
            var registro = listaAux[i];

            final = "";
            if ((i % 2) != 0)
                final = "final";

            html.AppendLine(
                "<!-- Ultimas Artigos -->\n" +
                "<a href=\"artigo.aspx?id=" + registro.IdArtigo + "\" class=\"item " + registro.Tipo.Css + " " + final + "\">\n" +
                "    <div class=\"over " + registro.Tipo.Css + "-over\"><span>" + registro.Tipo.Nome + "</span></div>\n" +
                "    <div class=\"texto\">\n" +
                "        <span>" + oVal.Substring(registro.Titulo, 60) + "</span>\n" +
                "        <p>" + oVal.Substring(oVal.Html(registro.Texto), 250) + "</p>\n" +
                "    </div>\n" +
                "    <label class=\"leia-mais " + registro.Tipo.Css + "\">leia mais</label>\n" +
                "</a>\n"
                );
        }

        return html.ToString();
    }*/



    

    #region Ultimas Noticias
    
    const int registrosPorPagina = 7;

    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;
    }

    private void IniciarGridView()
    {
        InicarSessao();

        int paginaAtual = Convert.ToInt32(Session["paginaAtual"]);

        var lista = bBus.UltimasNoticias(registrosPorPagina);

        grdwListar.PageSize = registrosPorPagina;
        grdwListar.PageIndex = paginaAtual;
        grdwListar.DataSource = lista;
        grdwListar.DataBind();

    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");
            lblRegistro.Text =
                "<a href=\"" + oVal.Pagina(DataBinder.Eval(e.Row.DataItem, "Tipo").ToString()) + "?id=" + DataBinder.Eval(e.Row.DataItem, "Id") + "\">" +
                "    <span class=\"titulo-do-item\">" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Titulo").ToString(), 70) + "</span>" +
                "    <p class=\"texto-do-item\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Texto").ToString()), 190) + "</p>" +
                "</a>";
        }
    }
    #endregion
}