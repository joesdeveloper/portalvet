﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Text.RegularExpressions;
using System.IO;

public partial class produto : System.Web.UI.Page
{
    BLL.Artigos bArt = new BLL.Artigos();
    BLL.Produtos bPro = new BLL.Produtos();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\produtos\";

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public string Produto()
    {
        var registro = bPro.Consultar(Convert.ToInt32(Request["id"]));

        var html = new StringBuilder();

        html.Append(
            "<style>" +
            "   thead { background:#" + registro.Cor + " }" +
            "   .titulo-produto { background:#" + registro.Cor + " }" +
            "   .voltar { color:#" + registro.Cor + "; font-size:12px; }" +
            "</style>" +
            "<div class=\"topo\">" +
            "   <a href=\"produtos.aspx?id=" + registro.Especie.IdEspecie + "\" class=\"voltar\">◄ voltar</a>" +
            "   <div class=\"barra\" style=\"background:#" + registro.Cor + "\">" +
            "       <div class=\"categoria\">"
            );

        for (var i = 0; i < registro.Categorias.Count; i++)
        {
            html.Append(registro.Categorias[i].Nome);

            if (i < (registro.Categorias.Count-1))
            {
                html.Append(" / ");

            }
        }
        
        
        html.Append(
            "</div>" +
            "       <h1>" + registro.Nome + "</h1>" +
            "   </div>" +
            "   <div class=\"" + registro.Especie.Mascara + "\"></div>" +
            "</div>" +
            "<div class=\"produto\">" +
            "<table><tr><td>" +
            "   <div class=\"imagem-do-produto\"><img src=\"arquivos/produtos/" + registro.IdProduto + "/foto.png\" />"
            );

        if (File.Exists(caminho + registro.IdProduto + @"\petisco.png"))
            html.Append("       <div class=\"petisco\"><div class=\"borda\" style=\"border:1px solid #" + registro.Cor + ";\"><div style=\"width:50px; height: 50px; background: transparent url('_servicos/gerar_imagem.aspx?i=arquivos/produtos/" + registro.IdProduto + "/petisco.png&w=55&h=55') no-repeat center center; background-size:contain;\"></div></div></div>");


        //if (registro.Embalagem.Trim().Length > 0)
        //    html.Append("   <span style=\"background:#" + registro.Cor + ";\">" + registro.Embalagem + "</span>");

        /* Selo Seco e Umido */

        if (registro.IdOutraVersao > 0)
        {
            var vProAux = bPro.Consultar(registro.IdOutraVersao);

            if (vProAux.Ativo == true)
            {
                var classeTipo = "";
                switch (registro.Tipo.IdTipo)
                {
                    case "S":
                        html.Append("   <span style=\"background:#" + registro.Cor + ";\">" + registro.Embalagem + "<br/><a href=\"produto.aspx?id=" + registro.IdOutraVersao + "\">Clique e conheça a versão úmida</a></span>");
                        //classeTipo = "conheca-versao-umida";
                        break;
                    case "U":
                        html.Append("   <span style=\"background:#" + registro.Cor + ";\">" + registro.Embalagem + "<br/><a href=\"produto.aspx?id=" + registro.IdOutraVersao + "\">Clique e conheça a versão seca</a></span>");
                        //classeTipo = "conheca-versao-seca";
                        break;
                }
                //html.Append("   <a href=\"produto.aspx?id=" + registro.IdOutraVersao + "\" class=\"" + classeTipo + "\" style=\"background-color:#" + registro.Cor + ";\"></a>");
            }
        }
        else
        {
            if (registro.Embalagem.Trim() != "")
                html.Append("   <span style=\"background:#" + registro.Cor + ";\">" + registro.Embalagem + "<br/></span>");
        }
    

        html.Append("</div></td><td>");

        
        /* Icones */
        html.Append("   <div class=\"icones\">");

        if (registro.Icones.Trim().Length > 0)
            html.Append(registro.Icones.Replace("<p>", "").Replace("</p>",""));

        var tipo = "o";
        if (!registro.Obesidade)
            tipo = "p";

        html.Append("<a href=\"aplicativo.aspx?t=" + tipo + "&e=" + registro.Especie.IdEspecie + "#inicio\" class=\"prescrever\"><div class=\"nome1\">indique</div><div class=\"nome2\">este produto</div></a>");

        html.Append("   </div></td></tr></table>");


        html.Append(
            "   <div class=\"texto\">" + registro.Informacao + "</div>" +
            "   <div class=\"addthis\">" +
            "	    <div class=\"addthis_sharing_toolbox\"></div>" +
            "	    <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53c98c261f693605\"></script>" +
            "   </div>" +
            "</div>"
            );

        return html.ToString();
    }

    public string ProdutosRelacionados()
    {
        var html = new StringBuilder();

        var registro = bPro.Consultar(Convert.ToInt32(Request["id"]));

        var lista = bPro.Listar();

        if (lista == null)
            return "";

        var listaAux = (from aux in lista
                        where aux.Ativo == true && aux.Especie.IdEspecie == registro.Especie.IdEspecie && aux.IdProduto != registro.IdProduto && aux.Categorias.Any(c => c.IdCategoria == registro.Categorias[0].IdCategoria)
                        select aux).ToList();

        if (listaAux == null || listaAux.Count < 1)
            return "";

        html.Append("<span class=\"titulo-produto-relacionado\">Produtos relacionados</span>");

        for (var i = 0; i < listaAux.Count; i++)
        {
            var produto = listaAux[i];

            html.Append(
                    "<a href=\"produto.aspx?id=" + produto.IdProduto + "\" class=\"produtos-relacionados\">" +
                    "    <div class=\"borda\">" +
                    //"        <div class=\"imagem\" style=\"background-image:url('arquivos/produtos/" + produto.IdProduto + "/foto.png');\"></div>" +
                    "        <div class=\"imagem\" style=\"background-image:url('_servicos/gerar_imagem.aspx?i=arquivos/produtos/" + produto.IdProduto + "/foto.png&w=95&h=95');\"></div>" +
                    "    </div>" +
                    "    <div class=\"texto\">" +
                    "        <span>" + oVal.Substring(produto.Nome, 30) + "</span>" +
                    "        <p>" + oVal.Substring(oVal.Html(produto.Descricao), 120) + "</p>" +
                    "    </div>" +
                    "</a>"
                );

            if (i == 1)
                break;
        }
        return html.ToString();
    }


    public string ListarArtigoRelacionados()
    {
        var html = new StringBuilder();

        var idProduto = Convert.ToInt32(Request["id"]);

        var lista = bArt.ListarArtigoRelacionados(idProduto);

        if (lista == null)
            return "";

        html.Append(
                "<div class=\"borda-relacionados\">" +
                "<span class=\"titulo\">Artigos relacionados</span>"
                );

        for (var i = 0; i < lista.Count; i++)
        {
            var registro = lista[i];

            html.Append(
                    "<a href=\"artigo.aspx?id=" + registro.IdArtigo + "\" class=\"artigos-relacionados\">" +
                    "    <div class=\"texto\">" +
                    "        <span>" + oVal.Substring(registro.Titulo, 30) + "</span>" +
                    "        <p>" + oVal.Substring(oVal.Html(registro.Texto), 120) + "</p>" +
                    "    </div>" +
                    "</a>"
                );
        }

        html.Append(
            "<a href=\"pesquisar.aspx?q=" + lista[0].Relacionamento.Nome + "\" class=\"veja-mais\">veja mais +</a>" +
            "</div>"
            );

        return html.ToString();
    }


    public string ListarDownloadsRelacionados()
    {
        var html = new StringBuilder();

        var idProduto = Convert.ToInt32(Request["id"]);

        var lista = bArt.ListarDownloadsRelacionados(idProduto);

        if (lista == null)
            return "";

        html.Append(
                "<div class=\"borda-relacionados\">" +
                "<span class=\"titulo\">Downloads relacionados</span>"
                );

        for (var i = 0; i < lista.Count; i++)
        {
            var registro = lista[i];

            html.Append(
                    "<a href=\"artigo.aspx?id=" + registro.IdArtigo + "\" class=\"artigos-relacionados\">" +
                    "    <div class=\"texto\">" +
                    "        <span>" + oVal.Substring(registro.Titulo, 30) + "</span>" +
                    "        <p>" + oVal.Substring(oVal.Html(registro.Texto), 120) + "</p>" +
                    "    </div>" +
                    "</a>"
                );
        }

        html.Append(
            "<a href=\"pesquisar.aspx?q=" + lista[0].Relacionamento.Nome + "\" class=\"veja-mais\">veja mais +</a>" +
            "</div>"
            );

        return html.ToString();
    }
}