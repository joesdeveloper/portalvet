﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using System.Globalization;
using System.IO;

public partial class congressos_e_seminarios : System.Web.UI.Page
{
    BLL.Eventos bEve = new BLL.Eventos();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\eventos\";

    const int registrosPorPagina = 10;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
            InicarSessao();
        }
    }

    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;
    }

    private void IniciarGridView()
    {
        InicarSessao();

        int paginaAtual = Convert.ToInt32(Session["paginaAtual"]);

        var lista = bEve.Pesquisar(0, 15000, "", "DataPublicacao", false);

        try
        {
            var listaAux = (from aux in lista
                            where aux.Ativo == true && aux.DataPublicacao < DateTime.Now
                            select aux).ToList();

            grdwListar.PageSize = registrosPorPagina;
            grdwListar.PageIndex = paginaAtual;
            grdwListar.DataSource = listaAux;
            grdwListar.DataBind();
        }
        catch { }
    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");

            lblRegistro.Text = "";

            lblRegistro.Text +=  "<a class=\"item\" href=\"congresso-e-seminario.aspx?id=" + DataBinder.Eval(e.Row.DataItem, "IdEvento") + "\">\n";

            if (File.Exists(caminho + DataBinder.Eval(e.Row.DataItem, "IdEvento") + @"\imagem.png"))
                lblRegistro.Text += "<div class=\"imagem\" style=\"background: transparent url('_servicos/gerar_imagem.aspx?i=arquivos/eventos/" + DataBinder.Eval(e.Row.DataItem, "IdEvento") + "/imagem.png&w=300&h=300') no-repeat center center; background-size:cover;\"></div>\n";
            else
                lblRegistro.Text += "<div class=\"imagem\">\n<img src=\"layout/imagens/ico-congressos-e-seminarios-lateral.png\" /></div>\n";

            lblRegistro.Text += 
                "    <div class=\"dados\">\n" +
                "        <span>" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Nome").ToString(), 55) + "</span>\n" +
                "        <p>\n" +
                "            <span><strong>" + DataBinder.Eval(e.Row.DataItem, "Local") + "</strong></span>\n" +
                "            <span class=\"menor\">" + DataBinder.Eval(e.Row.DataItem, "Endereco") + ", " + DataBinder.Eval(e.Row.DataItem, "Numero") + " - " + DataBinder.Eval(e.Row.DataItem, "Cidade") + " - " + DataBinder.Eval(e.Row.DataItem, "Estado") + "</span>\n" +
                "            <span class=\"menor\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Descricao").ToString()), 160) + "</span>\n" +
                "        </p>\n" +
                "    </div>\n" +
                "</a>"
                ;
        }
    }
}