﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.IO;

public partial class artigo : System.Web.UI.Page
{
    BLL.Artigos bArt = new BLL.Artigos();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\artigos\";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string Artigo()
    {
        var registro = bArt.Consultar(Convert.ToInt32(Request["id"]));

        var html = new StringBuilder();

        html.Append(
                    "<h1>" + registro.Titulo +"</h1>"+
                    "<span class=\"data\">publicado em: " + registro.DataPublicacao.ToString("dd/MM/yyyy") + "</span>"
                    );

        //if (registro.ArtigoImagem != null)
        if (File.Exists(caminho + registro.IdArtigo + @"/imagem.png"))
            html.Append("<img src=\"arquivos/artigos/"+ registro.IdArtigo + "/imagem.png\" />");

        html.Append("<div class=\"texto\">" + registro.Texto);



        html.Append("</div>");

        if (registro.Autor != null)
            html.Append(
                "<div class=\"texto\">" + 
                "   <span class=\"titulo-artigo\">Sobre o Autor</span>" +
                "   <div>" +
                    registro.Autor.Assinatura + "<br />" +
                    registro.Autor.Curriculo +
                "   </div>" +
                "</div>"
                );        

        if (registro.ArtigoAquivos != null)
        {
            html.Append("<div class=\"download\">");

            var classe = "";
            var j = 1;

            for (var i = 0; i < registro.ArtigoAquivos.Count; i++)
            {
                classe = "";
                if (j == 5)
                {
                    classe = "final";
                    j = 0;
                }

                html.Append(
                            "<a href=\"download.aspx?q=" + registro.ArtigoAquivos[i].Url + "\" target=\"_blank\" class=\"" + classe + "\">" +
                            "    <img src=\"layout/imagens/ico-arquivos-para-download.png\" />" +
                            "    <span>" + oVal.Substring(registro.ArtigoAquivos[i].Titulo, 16) + "</span>" +
                            "</a>"
                            );
                j++;
            }

            html.Append("</div>");
        }

        return html.ToString();
    }



    public string ListarArtigoRelacionados()
    {
        var html = new StringBuilder();

        var idProduto = Convert.ToInt32(Request["id"]);

        var lista = bArt.ListarArtigoRelacionados(idProduto);

        if (lista == null)
            return "";

        html.Append(
                "<div class=\"borda-relacionados\">" +
                "<span class=\"titulo\">Artigos relacionados</span>"
                );

        for (var i = 0; i < lista.Count; i++)
        {
            var registro = lista[i];

            html.Append(
                    "<a href=\"artigo.aspx?id=" + registro.IdArtigo + "\" class=\"artigos-relacionados\">" +
                    "    <div class=\"texto\">" +
                    "        <span>" + oVal.Substring(registro.Titulo, 30) + "</span>" +
                    "        <p>" + oVal.Substring(oVal.Html(registro.Texto), 120) + "</p>" +
                    "    </div>" +
                    "</a>"
                );
        }

        html.Append(
            "<a href=\"pesquisar.aspx?q=" + lista[0].Relacionamento.Nome + "\" class=\"veja-mais\">veja mais +</a>" +
            "</div>"
            );

        return html.ToString();
    }
}