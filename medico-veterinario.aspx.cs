﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class medico_veterinario : System.Web.UI.Page
{
    System.Security.Principal.IPrincipal usuario = System.Web.HttpContext.Current.User;

    IntegracaoWab oWab = new IntegracaoWab();

    //VO.MedicoVeterinario vMed = new VO.MedicoVeterinario();

    protected void Page_Load(object sender, EventArgs e)
    {
        IniciariFrame();
    }

    private void IniciariFrame()
    {
        lblDescricao.Text = "Se você deseja se cadastrar em nosso site para receber os boletins informativos e utilizar os serviços oferecidos, cadastre-se preenchendo o formulário abaixo.";

        var url = ConfigurationManager.AppSettings["UrliFrameIncluir"].ToString();

        if (usuario.Identity.IsAuthenticated && usuario.IsInRole("MedicoVeterinario"))
        {
            url = ConfigurationManager.AppSettings["UrliFrameAlterar"].Replace("#tokenUsuario#", usuario.Identity.Name);

            // Recuperar dos dados do MV
            var vMed = oWab.RecuperarDados();

            // Verificar se os dados do MV estão completos
            if (!oWab.Validar(vMed))
            {
                var mensagem = "Você precisa atualizar seu cadastro, antes de ter acesso a nossos conteúdos.";
                lblDescricao.Text = mensagem;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\n" + mensagem + "');", true);
            }
        }
        else
        {
            lblTag.Text = 
                "<!-- Google Code for Convers&atilde;o de Leads Conversion Page -->\n" +
                "<script type=\"text/javascript\">\n" +
                "/* <![CDATA[ */\n" +
                "var google_conversion_id = 951064559;\n" +
                "var google_conversion_language = \"en\";\n" +
                "var google_conversion_format = \"3\";\n" +
                "var google_conversion_color = \"ffffff\";\n" +
                "var google_conversion_label = \"AFckCI_tiV0Q76_AxQM\";\n" +
                "var google_remarketing_only = false;\n" +
                "/* ]]> */\n" +
                "</script>\n" +
                "<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\">\n" +
                "</script>\n" +
                "<noscript>\n" +
                "<div style=\"display:inline;\">\n" +
                "<img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//www.googleadservices.com/pagead/conversion/951064559/?label=AFckCI_tiV0Q76_AxQM&amp;guid=ON&amp;script=0\"/>\n" +
                "</div>\n" +
                "</noscript>\n";
        }

        pnlCadastro.Attributes.Add("src", url);
    }

    //private void CaptarDados()
    //{
    //    vMed.Nome = txtNome.Text;
    //    vMed.Sobrenome = txtSobrenome.Text;
    //    vMed.Email = txtEmail.Text;
    //    vMed.Senha = txtSenha1.Text;
    //    vMed.Cpf = txtCpf.Text;
    //    vMed.Crmv = txtCrmv.Text;
    //    vMed.Sexo = ddlSexo.SelectedValue;
    //    vMed.Celular = txtCelular.Text;
    //    vMed.Nome_Clinica = txtNomeDaClinica.Text;
    //    vMed.Especialidade_Area_Atuacao = txtEspecialidadeAreaAtuacao.Text;
    //    vMed.Endereco = txtEndereco.Text;
    //    vMed.Numero = txtNumero.Text;
    //    vMed.Bairro = txtBairro.Text;
    //    vMed.Cidade = txtCidade.Text;
    //    vMed.Estado = ddlEstado.SelectedValue;
    //    vMed.Cep = txtCep.Text;
    //    vMed.Telefone_Comercial = txtTelefoneComercial.Text;
    //}

    //protected void ddlEstado_Init(object sender, EventArgs e)
    //{
    //    var lista = new BLL.Estados().Listar();

    //    ddlEstado.Items.Clear();
    //    foreach (var registro in lista)
    //    {
    //        ddlEstado.Items.Add(new ListItem(registro.Nome, registro.Sigla));
    //    }
    //}

    //protected void btnEnviar_Click(object sender, EventArgs e)
    //{
    //    CaptarDados();

    //    var retorno = new IntegracaoWab().Incluir(vMed);
    //    Response.Write("Mensagem: " + retorno.error + " - " + retorno.message);
    //}
}