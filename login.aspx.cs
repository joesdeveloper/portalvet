﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    IntegracaoWab oWab = new IntegracaoWab();

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnEntrar_Click1(object sender, EventArgs e)
    {
        try
        {
            var retorno = oWab.Login(txtEmail.Text, txtSenha.Text);

            if (retorno)
            {
                if (Request["ReturnUrl"] != null)
                    Response.Redirect(Request["ReturnUrl"]);
                else
                    Response.Redirect("~/");
            }
            else
            {
                if (Session["MedicoVeterinarioRetorno"] == null)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nE-MAIL e/ou SENHA inválido(s).');", true);
                else
                {
                    var mensagem = (VO.MensagemDeRetorno)Session["MedicoVeterinarioRetorno"];

                    if (mensagem.code == 4)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nVocê não está cadastrado como Veterinário. Se quiser ter acesso ao Portal Vet acesse o link abaixo e mude seu perfil para Veterinário.\n\nhttp://cadastro.royalcanin.com.br');", true);
                    else
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nE-MAIL e/ou SENHA inválido(s).');", true);
                }
            }
        }
        catch
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nOcorreu um erro na autenticação, por favor, entre em contato pelo e-mail: consumidor@royalcanin.com');", true);
        }


        /*var retorno = new IntegracaoWab().Login(txtEmail.Text, txtSenha.Text);

        if (retorno)
        {
            if (Request["ReturnUrl"] != null)
                Response.Redirect(Request["ReturnUrl"]);
            else
                Response.Redirect("~/");
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nE-MAIL e/ou SENHA inválido(s).');", true);*/
    }


    protected void btnRecuperarSenha_Click(object sender, EventArgs e)
    {
        var email = txtEmail.Text.Trim();

        if (email.Length == 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nO campo E-MAIL não pode ser branco.');", true);
        else
        {
            if (!new UTIL.Validacao().Email(email))
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nO campo E-MAIL é inválido.');", true);
            else
            {
                var retorno = new IntegracaoWab().RecuperarSenha(email);

                if (!retorno.error)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('Verifique seu e-mail, pois em alguns minutos você receberá as informações necessárias para reiniciar sua senha.');", true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nO e-mail informado não foi localizado, por favor, verificar se o mesmo está correto.');", true);
            }
        }
    }
}