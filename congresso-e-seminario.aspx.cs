﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;

using System.IO;

public partial class congresso_e_seminario : System.Web.UI.Page
{
    BLL.Eventos bEve = new BLL.Eventos();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\eventos\";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string Evento()
    {
        var registro = bEve.Consultar(Convert.ToInt32(Request["id"]));

        var html = new StringBuilder();


        html.Append(
                    "<h1>" + registro.Nome + "</h1>" +
                    "<span class=\"data\">publicado em: " + registro.DataInclusao.ToString("dd/MM/yyyy") + "</span>"
                    );

        //if (registro.EventoImagem != null)
        //    html.Append("<img src=\"" + registro.EventoImagem.Url + "\" />");

        if (File.Exists(caminho + registro.IdEvento + @"\imagem.png"))
            html.Append("<img src=\"arquivos/eventos/" + registro.IdEvento + "/imagem.png\" />");

        html.Append(
            "<div class=\"texto\">" + 
            "   <div class=\"local\">" +
            "       <span class=\"nome\">" + registro.Local + "</span><br />"
            );

        if (registro.DataEventoInicial == registro.DataEventoFinal)
            html.Append("       <span class=\"data\">" + oVal.Data(registro.DataEventoInicial, new CultureInfo("pt-BR")) + "</span><br/>");
        else
            html.Append("       <span class=\"data\">" + oVal.Data(registro.DataEventoInicial, new CultureInfo("pt-BR")) + " à " + oVal.Data(registro.DataEventoFinal, new CultureInfo("pt-BR")) + "</span><br/>");

        html.Append(
            "       " + registro.Endereco + ", " + registro.Numero
            );
        
        if (registro.Complemento.Trim().Length > 0)
            html.Append(" - " + registro.Complemento + "<br/>");
        else
            html.Append("<br/>");

        html.Append(
            "       " + registro.Bairro + " - " + registro.Cidade + ", " + registro.Estado + "<br />" +
            "       " + String.Format("{0:#####-###}", Convert.ToDecimal(registro.Cep)) + 
            "   </div>" +
            registro.Descricao +
            "</div>"
            );

        return html.ToString();
    }
}