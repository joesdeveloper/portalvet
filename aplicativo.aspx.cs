﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class aplicativo : System.Web.UI.Page
{
    string tipo = "";
    string especie = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        CaptarVariaveis();

        IniciarFormulario();
    }

    private void CaptarVariaveis()
    {

        if (Request["t"] != null)
            tipo = Request["t"];

        if (Request["e"] != null)
            especie = Request["e"];

    }

    private void IniciarFormulario()
    {
        CaptarVariaveis();

        lnkCao.NavigateUrl = "aplicativo.aspx?t=" + tipo + "&e=1#inicio";
        lnkGato.NavigateUrl = "aplicativo.aspx?t=" + tipo + "&e=2#inicio";

        // Exibe painel obesidade
        if (tipo == "o")
        {
            pnlEspecies.Visible = true;

            switch(especie)
            {
                case "1":
                    pnlObesidadeCao.Visible = true;
                    break;
                case "2":
                    pnlObesidadeGato.Visible = true;
                    break;
            }
        }

        // Exibe painel enfermidade
        if (tipo == "p")
        {
            pnlEspecies.Visible = true;

            switch (especie)
            {
                case "1":
                    pnlPatologiaCao.Visible = true;
                    break;
                case "2":
                    pnlPatologiaGato.Visible = true;
                    break;
            }
        }


        // Marca tipo
        switch (tipo)
        {
            case "o":
                lnkObesidade.CssClass += " marcado";
                break;
            case "p":
                lnkEnfermidades.CssClass += " marcado";
                break;
        }

        // Marcar especie
        switch (especie)
        {
            case "1":
                lnkCao.CssClass += " marcado";
                break;
            case "2":
                lnkGato.CssClass += " marcado";
                break;
        }
    }
}