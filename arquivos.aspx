﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="arquivos.aspx.cs" Inherits="arquivos" %>

<%@ Register Src="modulos/rodape.ascx" TagName="rodape" TagPrefix="uc1" %>
<%@ Register Src="modulos/cabecalho.ascx" TagName="cabecalho" TagPrefix="uc2" %>
<%@ Register Src="modulos/aside-app-prescricao.ascx" TagName="prescricao" TagPrefix="uc3" %>
<%@ Register Src="modulos/aside-pesquisar.ascx" TagName="pesquisar" TagPrefix="uc4" %>
<%@ Register Src="modulos/aside-menu.ascx" TagName="menu" TagPrefix="uc5" %>
<%--<%@ Register src="modulos/aside-produtos-relacionados.ascx" tagname="aside" tagprefix="uc6" %>--%>
<%@ Register Src="modulos/section-destaque.ascx" TagName="destaque" TagPrefix="uc6" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Portal Vet - Arquivos para Download</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#slider1').tinycarousel({
                interval: true,
                intervalTime: 6000,
                bullets: true,
                buttons: false
            });
        });
    </script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="principal">
        <uc2:cabecalho ID="cabecalho1" runat="server" />
        <div id="conteudo">
            <section>
                <%--<uc6:destaque ID="destaque1" runat="server" />--%>
                <div class="arquivos-para-download">
                    <h1>
                        <div>
                            <span>Arquivos para</span> Download
                        </div>
                    </h1>
                    <!-- Grid -->
                    <asp:GridView ID="grdwListar" runat="server" PagerSettings-Visible="True" AutoGenerateColumns="false"
                        AllowPaging="True" OnRowDataBound="grdwListar_RowDataBound" GridLines="None"
                        CssClass="lista" OnPageIndexChanging="grdwListar_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Literal ID="lblRegistro" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings PageButtonCount="20" />
                        <PagerStyle CssClass="paginacao" HorizontalAlign="Right" />
                    </asp:GridView>
                </div>
            </section>
            <aside>
                <uc3:prescricao ID="prescricao1" runat="server" />
                <uc4:pesquisar ID="pesquisar1" runat="server" />

                <div class="borda-relacionados" style="display:block;">
                <span class="titulo">Filtro por categoria</span>
                <asp:Repeater ID="pnlCategorias" runat="server" oninit="pnlCategorias_Init">
                    <ItemTemplate>
                    <a class="arquivos-filtro" href='arquivos.aspx?id=<%#Eval("IdCategoria") %>'>
                        <div class="texto">
                            <span><%#Eval("Nome") %></span>
                        </div>
                    </a>
                    </ItemTemplate>
                </asp:Repeater>
                    <a class="arquivos-filtro" href="arquivos.aspx">
                        <div class="texto">
                            <span>Todos</span>
                        </div>
                    </a>
                </div>

                <uc5:menu ID="menu1" runat="server" />
                <%--<uc6:produtos_relacionados ID="produtos_relacionados1" runat="server" />--%>
            </aside>
        </div>
        <uc1:rodape ID="rodape1" runat="server" />
    </div>
    </form>
</body>
</html>
