﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Globalization;

public partial class foruns : System.Web.UI.Page
{
    BLL.Foruns bFor = new BLL.Foruns();

    UTIL.Validacao oVal = new UTIL.Validacao();

    const int registrosPorPagina = 15;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
            InicarSessao();
        }
    }

    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;
    }

    private void IniciarGridView()
    {
        InicarSessao();

        int paginaAtual = Convert.ToInt32(Session["paginaAtual"]);

        var lista = bFor.Pesquisar(0, 1000, "", "Nome", true);

        
            var listaAux = (from aux in lista
                            where aux.Ativo == true
                            select aux).ToList();

            grdwListar.PageSize = registrosPorPagina;
            grdwListar.PageIndex = paginaAtual;
            grdwListar.DataSource = listaAux;
            grdwListar.DataBind();
        
    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");
            lblRegistro.Text =
                "<a href=\"foruns-perguntas.aspx?id=" + DataBinder.Eval(e.Row.DataItem, "IdForum") + "\">" +
                "    <span class=\"titulo-do-item\">" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Nome").ToString(), 70) + "</span>" +
                "    <p class=\"texto-do-item\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Descricao").ToString()), 200) + "</p>" +
                "</a>";

            var lblDataAtualizacao = (Literal)e.Row.FindControl("lblDataAtualizacao");
            lblDataAtualizacao.Text =
                "<strong>" + String.Format("{0:N0}", Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "TotalPerguntas")), new CultureInfo("pt-BR")) + "</strong> tópicos<br />" +
                "<strong>" + String.Format("{0:N0}", Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "TotalRespostas")), new CultureInfo("pt-BR")) + "</strong> respostas<br />" +
                oVal.Data(Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DataAtualizacao")), DateTime.Now, new CultureInfo("pt-BR"));
        }
    }
}