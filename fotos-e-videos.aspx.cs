﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class fotos_e_videos : System.Web.UI.Page
{
    BLL.Galerias bGal = new BLL.Galerias();

    UTIL.Validacao oVal = new UTIL.Validacao();

    const int registrosPorPagina = 8;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
            InicarSessao();
        }
    }

    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;
    }

    private void IniciarGridView()
    {
        InicarSessao();

        int paginaAtual = Convert.ToInt32(Session["paginaAtual"]);

        var lista = bGal.Pesquisar(0, 15000, "", "DataInclusao", false);

        try
        {
            var listaAux = (from aux in lista
                            where aux.Ativo == true
                            select aux).ToList();

            grdwListar.PageSize = registrosPorPagina;
            grdwListar.PageIndex = paginaAtual;
            grdwListar.DataSource = listaAux;
            grdwListar.DataBind();
        }
        catch { }
        
    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");
            lblRegistro.Text = 
                "<a class=\"item\" href=\"foto-e-video.aspx?id=" + DataBinder.Eval(e.Row.DataItem, "IdGaleria") + "\">\n" +
                "<div class=\"imagem\" style=\"background: transparent url('_servicos/gerar_imagem.aspx?i=arquivos/fotos-e-videos/" + DataBinder.Eval(e.Row.DataItem, "IdGaleria") + "/imagem.png&w=300&h=300') no-repeat center center; background-size:cover;\"></div>\n" +
                "    <div class=\"dados\">\n" +
                "       <span class=\"titulo-do-item\">" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Titulo").ToString(), 70) + "</span>" +
                "       <p class=\"texto-do-item\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Descricao").ToString()), 200) + "</p>" +
                "    </div>\n" +
                "</a>"
                ;
        }
    }
}