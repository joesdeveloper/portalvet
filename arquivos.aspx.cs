﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Globalization;
using System.IO;

public partial class arquivos : System.Web.UI.Page
{
    BLL.Artigos bArt = new BLL.Artigos();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\artigos\";

    const int registrosPorPagina = 8;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
            InicarSessao();
        }
    }

    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;
    }

    private void IniciarGridView()
    {
        InicarSessao();

        int paginaAtual = Convert.ToInt32(Session["paginaAtual"]);


        var lista = bArt.ListarArquivos(0, 25000, "", "DataPublicacao", false);

        try
        {
            if (Request["id"] == null)
            {
                lista = (from aux in lista
                         where aux.Ativo == true && aux.DataPublicacao < DateTime.Now
                         select aux).ToList();
            }
            else
            {
                lista = (from aux in lista
                         where aux.Ativo == true && aux.DataPublicacao < DateTime.Now && aux.IdCategoria == Convert.ToInt32(Request["id"])
                         select aux).ToList();
            }

            grdwListar.PageSize = registrosPorPagina;
            grdwListar.PageIndex = paginaAtual;
            grdwListar.DataSource = lista;
            grdwListar.DataBind();
        }
        catch { }
    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");

            lblRegistro.Text = "";

            lblRegistro.Text += "<a class=\"item\" href=\"artigo.aspx?id=" + DataBinder.Eval(e.Row.DataItem, "IdArtigo") + "\">\n";

            if (File.Exists(caminho + DataBinder.Eval(e.Row.DataItem, "IdArtigo") + @"\imagem.png"))
                lblRegistro.Text += "<div class=\"imagem\" style=\"background: transparent url('_servicos/gerar_imagem.aspx?i=arquivos/artigos/" + DataBinder.Eval(e.Row.DataItem, "IdArtigo") + "/imagem.png&w=180&h=180') no-repeat center center; background-size:cover;\"></div>\n";
                //lblRegistro.Text += "<div class=\"imagem\" style=\"background: transparent url('arquivos/artigos/" + DataBinder.Eval(e.Row.DataItem, "IdArtigo") + "/imagem.png') no-repeat center center; background-size:cover;\"></div>\n";
            else
                lblRegistro.Text += "<div class=\"imagem\">\n<img   src=\"layout/imagens/ico-congressos-e-seminarios-lateral.png\" /></div>\n";

            lblRegistro.Text +=
                "    <div class=\"dados\">\n" +
                "       <span class=\"titulo-do-item\">" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Titulo").ToString(), 55) + "</span>" +
                "       <div class=\"autor\">" + DataBinder.Eval(e.Row.DataItem, "Autor.Assinatura") + "</div>\n" +
                "       <p class=\"texto-do-item\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Texto").ToString()), 200) + "</p>" +
                "    </div>\n" +
                "</a>"
                ;

            //lblRegistro.Text =
            //    "<a href=\"artigo.aspx?id=" + DataBinder.Eval(e.Row.DataItem, "IdArtigo") + "\">" +
            //    "    <span class=\"titulo-do-item\">" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Titulo").ToString(), 70) + "</span>" +
            //    "    <p class=\"texto-do-item\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Texto").ToString()), 200) + "</p>" +
            //    "</a>";
        }
    }

    protected void pnlCategorias_Init(object sender, EventArgs e)
    {
        var lista = (from aux in bArt.ListarCategorias()
                     orderby aux.Nome
                     select aux);

        pnlCategorias.DataSource = lista;
        pnlCategorias.DataBind();
    }
}