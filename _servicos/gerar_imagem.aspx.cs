﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class _servicos_gerar_imagem : System.Web.UI.Page
{
    string imagem = "";
    int altura = 0;
    int largura = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        Exibir();
    }


    private void CaptarDados()
    {
        imagem = Request["i"];
        altura = Convert.ToInt32(Request["h"]);
        largura = Convert.ToInt32(Request["w"]);
    }


    private void Exibir()
    {
        CaptarDados();

        var oIma = new UTIL.EdicaoDeImagem();
        System.Drawing.Image image = null;

        if (File.Exists(Server.MapPath("~/" + imagem)))
            image = oIma.Redimencionar(Server.MapPath("~/" + imagem), largura, altura);

        var ms = new MemoryStream();
        image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

        byte[] array = ms.ToArray();
        Response.ContentType = "image/png";
        Response.BinaryWrite(array);

    }
}