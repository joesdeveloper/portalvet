﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

using System.Text;

public partial class _servicos_servicos : System.Web.UI.Page
{
    

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string Representantes(string estado)
    {
        UTIL.Validacao oVal = new UTIL.Validacao();

        var bRep = new BLL.Representantes();
        var lista = bRep.Listar(estado);

        var listaAux = (from aux in lista
                        where aux.Ativo == true
                        select aux).ToList();

        var json = new StringBuilder();

        json.Append("[");

        foreach (var registro in lista)
        {
            json.Append("{");

            json.Append("\"nome\":\"" + registro.Nome + "\",");
            json.Append("\"endereco\":\"" + registro.Endereco + "\",");
            json.Append("\"numero\":\"" + registro.Numero + "\",");
            json.Append("\"complemento\":\"" + registro.Complemento + "\",");
            json.Append("\"cidade\":\"" + registro.Cidade + "\",");
            json.Append("\"estado\":\"" + registro.Estado + "\",");
            json.Append("\"cep\":\"" + oVal.CEP_Formatar(registro.Cep) + "\",");
            json.Append("\"telefone1\":\"" + oVal.Telefone_Formatar(registro.Telefone1) + "\",");
            json.Append("\"telefone2\":\"" + oVal.Telefone_Formatar(registro.Telefone2) + "\",");
            json.Append("\"telefone3\":\"" + oVal.Telefone_Formatar(registro.Telefone3) + "\",");
            json.Append("\"telefone4\":\"" + oVal.Telefone_Formatar(registro.Telefone4) + "\",");
            json.Append("\"fax\":\"" + oVal.Telefone_Formatar(registro.Fax) + "\",");
            json.Append("\"celular\":\"" + oVal.Celular_Formatar(registro.Celular) + "\",");
            json.Append("\"nextelid\":\"" + registro.NextelId + "\",");
            json.Append("\"email1\":\"" + registro.Email1 + "\",");
            json.Append("\"email2\":\"" + registro.Email2 + "\"");

            json.Append("},");
        }

        json.Append("]");

        var retorno = json.ToString().Substring(0, json.ToString().Length - 3);
        retorno = retorno + "}]";

        return retorno;
    }
}
