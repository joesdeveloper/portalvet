﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="produtos.aspx.cs" Inherits="produtos" %>
<%@ Register src="modulos/rodape.ascx" tagname="rodape" tagprefix="uc1" %>
<%@ Register src="modulos/cabecalho.ascx" tagname="cabecalho" tagprefix="uc2" %>
<%@ Register src="modulos/aside-app-prescricao.ascx" tagname="prescricao" tagprefix="uc3" %>
<%@ Register src="modulos/aside-pesquisar.ascx" tagname="pesquisar" tagprefix="uc4" %>
<%@ Register src="modulos/aside-menu.ascx" tagname="menu" tagprefix="uc5" %>
<%--<%@ Register src="modulos/aside-produtos-relacionados.ascx" tagname="aside" tagprefix="uc6" %>--%>
<%@ Register src="modulos/section-destaque.ascx" tagname="destaque" tagprefix="uc6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Portal Vet - Produtos</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.categoria').click(function () {
                var id = $(this).attr('href');
                $(id).toggle();
            });

            $('.selo-do-produto').click(function () {
                var id = $(this).attr('href');
                $(id).show();
            });

            $('.quadro-over').mouseover(function () {
                var id = $(this).attr('data-id');
                $('.quadro').hide();
                $(id).show();
            });

            $('.quadro-over').click(function () {
                var id = $(this).attr('data-id');
                $('.quadro').hide();
                $(id).show();
            });

            

            /*function myRepeat() {
                $('.obesidade img').delay(1000).fadeOut(1200).delay(150).fadeIn(400);
                $('.coracao img').delay(2000).fadeOut(1200).delay(150).fadeIn(400);
                $('.renal img').delay(3000).fadeOut(1200).delay(150).fadeIn(400);
                $('.gastrointestinal img').delay(13000).fadeOut(1200).delay(150).fadeIn(400);
                $('.dentario img').delay(4000).fadeOut(1200).delay(150).fadeIn(400);
                $('.emocional img').delay(5000).fadeOut(1200).delay(150).fadeIn(400);
                $('.diabetes img').delay(6000).fadeOut(1200).delay(150).fadeIn(400);
                $('.doencas-hepaticas img').delay(7000).fadeOut(1200).delay(150).fadeIn(400);
                $('.adversas img').delay(8000).fadeOut(1200).delay(150).fadeIn(400);
                $('.outros img').delay(9000).fadeOut(1200).delay(150).fadeIn(400);
                $('.convalescenca img').delay(1000).fadeOut(1200).delay(150).fadeIn(400);
                $('.articulacao img').delay(11000).fadeOut(1200).delay(150).fadeIn(400);
                $('.trato-urinario-inferior img').delay(12000).fadeOut(1200).delay(150).fadeIn(400);
                
                $('.obesidade img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.coracao img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.renal img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.gastrointestinal img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.dentario img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.emocional img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.diabetes img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.doencas-hepaticas img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.adversas img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.outros img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.convalescenca img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.articulacao img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.trato-urinario-inferior img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.obesidade img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.obesidade img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                $('.obesidade img').delay(1200).fadeOut(1200).delay(150).fadeIn(400);
                //$('.coracao img').delay(150).animate({ width: "0", height: "auto", top: '17', left: '17' }, 1000).delay(150).animate({ width: "35", height: "auto", top: '0', left: '0' }, 0)
                //$('.obesidade img').delay(150).animate({ width: "0", height: "auto", top: '17', left: '17' }, 1000).delay(150).animate({ width: "35", height: "auto", top: '0', left: '0' }, 0)
            }
            setInterval(myRepeat, 500);*/
        });
	</script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="principal">
        <uc2:cabecalho ID="cabecalho1" runat="server" />

        <div id="conteudo">
            <section>
                
                <div class="produtos">
                    <h1>
                        <div>
                            <span>Info de</span> Produtos <%=Icone() %>
                        </div>
                        
                    </h1>
                   <%=Especies() %>
                   <%=Produtos() %>
                </div>
            </section>

            <aside>
                <uc3:prescricao ID="prescricao1" runat="server" />
                <uc4:pesquisar ID="pesquisar1" runat="server" />
                <uc5:menu ID="menu1" runat="server" />
                <%--<uc6:produtos_relacionados ID="produtos_relacionados1" runat="server" />--%>
            </aside>
        </div>

        <uc1:rodape ID="rodape1" runat="server" />
        
    </div>
    </form>
</body>
</html>
