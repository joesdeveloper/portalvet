﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="foruns-perguntas.aspx.cs" Inherits="foruns_perguntas" %>
<%@ Register src="modulos/rodape.ascx" tagname="rodape" tagprefix="uc1" %>
<%@ Register src="modulos/cabecalho.ascx" tagname="cabecalho" tagprefix="uc2" %>
<%@ Register src="modulos/aside-app-prescricao.ascx" tagname="prescricao" tagprefix="uc3" %>
<%@ Register src="modulos/aside-pesquisar.ascx" tagname="pesquisar" tagprefix="uc4" %>
<%@ Register src="modulos/aside-menu.ascx" tagname="menu" tagprefix="uc5" %>
<%--<%@ Register src="modulos/aside-produtos-relacionados.ascx" tagname="aside" tagprefix="uc6" %>--%>
<%@ Register src="modulos/section-destaque.ascx" tagname="destaque" tagprefix="uc6" %>
<%@ Register src="modulos/banner-faq.ascx" tagname="banner" tagprefix="uc7" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Portal Vet - Forum - Tópicos</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#slider1').tinycarousel({
                interval: true,
                intervalTime: 6000,
                bullets: true,
                buttons: false
            });
        });
	</script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body> 
    <form id="form1" runat="server">
    <div id="principal">
        <uc2:cabecalho ID="cabecalho1" runat="server" />

        <div id="conteudo">
            <section>
                
                <%--<uc6:destaque ID="destaque1" runat="server" />--%>

                <div class="foruns">
                    <h1>
                        <div>
                            Forum
                            <asp:Button ID="btnCriarTopico" CssClass="botao" runat="server" Text="Criar novo tópico" OnClientClick="window.location.hash='#topico';return false;" />
                        </div>

                    </h1>

                    <!-- Grid -->
                    <asp:GridView ID="grdwListar" runat="server" PagerSettings-Visible="True"
                        AutoGenerateColumns="false" AllowPaging="True"  
                        OnRowDataBound="grdwListar_RowDataBound" GridLines="None" CssClass="lista" 
                        onpageindexchanging="grdwListar_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Tópicos" >
                                <ItemTemplate>
                                    <asp:Literal ID="lblRegistro" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <ItemStyle Width="550px" HorizontalAlign="Left" />
                                <HeaderStyle Width="550px" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField >
                                <ItemTemplate>
                                    <p class="texto-do-data">
                                        <asp:Literal ID="lblDataAtualizacao" runat="server"></asp:Literal>
                                    </p>
                                </ItemTemplate>
                                <ItemStyle Width="130px" HorizontalAlign="Left" />
                                <HeaderStyle Width="130px" HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings PageButtonCount="20" />
                        <HeaderStyle CssClass="cabecalho" />
                        <PagerStyle CssClass="paginacao" HorizontalAlign="Right" />
                    </asp:GridView>

                    <p id="topico" style="margin:30px 0 0 0; display:inline-block; font-size:13px; text-align:justify; color:#545556;">Se você tiver uma pergunta sobre o tema que ainda não foi respondida. Crie um novo tópico e aguarde a colaboração dos participantes.</p>
                    <div class="mensagem">
                        <span class="titulo-mensagem">Novo tópico</span>
                        <asp:TextBox ID="txtTitulo" runat="server" placeholder="Escreva o titulo do seu tópico." MaxLength="80" Width="100%"></asp:TextBox>
                        <div>
                            <asp:TextBox ID="txtTexto" runat="server" TextMode="MultiLine" placeholder="Escreva aqui sua mensagem." Width="100%"></asp:TextBox>
                        </div>
                        <input type="button" class="botao-esquerdo" onclick="window.location='foruns.aspx';" value="Voltar">
                        <asp:Button ID="btnEnviar" CssClass="botao" runat="server" Text="Enviar" onclick="btnEnviar_Click" />
                    </div>
                </div>

                <uc7:banner ID="banner1" runat="server" />
            </section>

            <aside>
                <uc3:prescricao ID="prescricao1" runat="server" />
                <uc4:pesquisar ID="pesquisar1" runat="server" />
                <uc5:menu ID="menu1" runat="server" />
                <%--<uc6:produtos_relacionados ID="produtos_relacionados1" runat="server" />--%>
            </aside>
        </div>

        <uc1:rodape ID="rodape1" runat="server" />
        
    </div>
    </form>
</body>
</html>

