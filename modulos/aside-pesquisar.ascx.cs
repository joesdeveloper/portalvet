﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modulos_aside_pesquisar : System.Web.UI.UserControl
{
    string valor = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["q"] != null)
            {
                valor = Request["q"].TrimStart().TrimEnd();

                txtPesquisar.Text = valor;
            }
        }
    }

    protected void btnPesquisar_Click(object sender, ImageClickEventArgs e)
    {
        valor = txtPesquisar.Text;

        Response.Redirect("pesquisar.aspx?q=" + valor);
    }
}