﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modulos_aside_menu : System.Web.UI.UserControl
{
   System.Security.Principal.IPrincipal usuario = System.Web.HttpContext.Current.User;

    IntegracaoWab oWab = new IntegracaoWab();

    protected void Page_Load(object sender, EventArgs e)
    {
        IniciariFrame();
    }

    private void IniciariFrame()
    {
        if (usuario.Identity.IsAuthenticated && usuario.IsInRole("MedicoVeterinario"))
        {
            // Recuperar dos dados do MV
            var vMed = oWab.RecuperarDados();

            // Verificar se o MV tem acesso a Coroa Premiada
            if (vMed.Url_Coroa_Premiada.Trim().Length > 0)
                pnlCoroaPremiada.Visible = true;
        }
    }

    
}