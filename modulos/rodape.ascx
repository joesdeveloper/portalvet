﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="rodape.ascx.cs" Inherits="modulos_rodape" %>
        <footer>
            <nav>
                <ul>
                    <li><a href="default.aspx">Home</a></li>
                    <li><a href="faq.aspx">FAQ</a></li>
                    <li><a href="artigos.aspx">Conteúdo</a></li>
                    <li><a href="aplicativo.aspx">App NutriVET</a></li>
                    <li><a href="produtos.aspx">Produtos</a></li>
                    <li><a href="contato.aspx">Contato & Representantes</a></li>
                    <li><a href="medico-veterinario.aspx">Cadastro</a></li>
                </ul>
            </nav>
            <img class="logo-royal-canin" src="layout/imagens/img-logo-royal.png" />
            <img class="logo-veterinary-exclusive" src="layout/imagens/img-veterinary-exclusive.png" />
        </footer>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-26524227-5', 'auto');
    ga('send', 'pageview');

</script>
<!-- Código do Google para tag de remarketing -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 951064559;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/951064559/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: portalvet_ione
URL of the webpage where the tag is expected to be placed: http://portalvet.royalcanin.com.br
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 05/26/2015
-->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="https://4896523.fls.doubleclick.net/activityi;src=4896523;type=invmedia;cat=gfdgdzhd;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://4896523.fls.doubleclick.net/activityi;src=4896523;type=invmedia;cat=gfdgdzhd;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->