﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="aplicativo-obesidade-gato.ascx.cs" Inherits="modulos_aplicativo_obesidade_gato" %>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                var numero = '6';
                                var rodape = '&bull; Costelas não visíveis, porém palpáveis.<br />&bull; Cintura não claramente definida na visão dorsoventral.';
                                var detalhe = '10% acima do peso';
                                var indice = '';

                                $(window).load(function () {
                                    SetarScoreCorporal();
                                });

                                //Inicializarndo na após a carga da página
                                Inicializar();

                                //função com o código JQuery
                                function Inicializar() {
                                    $('#<%= txtRecomendacoesAdicionais.ClientID%>').limit('250', '#charsLeft');

                                    SetarScoreCorporal();
                                    SetarAlimento();

                                    $('#<%= txtPesoAtual.ClientID%>').maskMoney({ showSymbol: false, decimal: ",", thousands: "." });

                                    $('.saiba-mais').click(function () {
                                        SaibaMais($(this));
                                    });

                                    $('.porte-do-animal').change(function () {
                                        SetarScoreCorporal();
                                    });

                                    $('.score-corporal-obesidade > ul > li > a').click(function () {
                                        SelecionarScoreCorporal($(this));
                                        return false;
                                    });

                                    $('.produto').click(function () {
                                        SelecionarProduto($(this));
                                    });
                                }


                                function SaibaMais(objeto) {
                                    window.open($(objeto).attr('data-url'));
                                    return false;
                                }

                                function SelecionarScoreCorporal(objeto) {
                                    numero = $(objeto).attr('data-score');
                                    detalhe = $(objeto).attr('data-detalhe');
                                    rodape = $(objeto).attr('data-rodape');
                                    indice = $(objeto).attr('data-indice')

                                    SetarScoreCorporal();

                                    return false;
                                }


                                $('.porte-do-animal').change(function () {
                                    SetarScoreCorporal();
                                });


                                function SelecionarProduto(objeto) {
                                    //alert($(objeto).attr("id"));

                                    // obtem os id válidos
                                    var secaNome = '#<%= hdnAlimentoSecoNome.ClientID%>';
                                    var umidaNome = '#<%= hdnAlimentoUmidoNome.ClientID%>';

                                    var seca = '#<%= hdnAlimentoSecoEM.ClientID%>';
                                    var umida = '#<%= hdnAlimentoUmidoEM.ClientID%>';

                                    var secaId = '#<%= hdnAlimentoSecoId.ClientID%>';
                                    var umidaId = '#<%= hdnAlimentoUmidoId.ClientID%>';

                                    var idProduto = $(objeto).attr('id');
                                    var idOutro = $(objeto).attr('data-outro');

                                    // verificar se ja é marcado
                                    if ($('#' + idProduto).hasClass('marcado')) {
                                        // verificar se ja é marcado
                                        if (!$('#' + idProduto).hasClass('habilitado')) {
                                            Limpar();

                                            $(umida).val('');
                                            $(seca).val('');
                                            $(umidaId).val('0');
                                            $(secaId).val('0');
                                            $(secaNome).val('');
                                            $(umidaNome).val('');
                                        }
                                    }
                                    else {

                                        // verificar se ja é marcado
                                        if ($('#' + idProduto).hasClass('habilitado')) {
                                            Limpar();
                                            $('#' + idProduto).addClass('marcado');
                                            $('#' + idOutro).addClass('marcado');

                                            if ($('#' + idProduto).attr('data-tipo') == 'S') {
                                                $(seca).val($('#' + idProduto).attr('data-em'));
                                                $(umida).val($('#' + idOutro).attr('data-em'));

                                                $(secaId).val($('#' + idProduto).attr('id'));
                                                $(umidaId).val($('#' + idOutro).attr('id'));

                                                $(secaNome).val($('#' + idProduto).attr('data-nome'));
                                                $(umidaNome).val($('#' + idOutro).attr('data-nome'));
                                            }
                                            else {

                                                $(seca).val($('#' + idOutro).attr('data-em'));
                                                $(umida).val($('#' + idProduto).attr('data-em'));

                                                $(secaId).val($('#' + idOutro).attr('id'));
                                                $(umidaId).val($('#' + idProduto).attr('id'));

                                                $(secaNome).val($('#' + idOutro).attr('data-nome'));
                                                $(umidaNome).val($('#' + idProduto).attr('data-nome'));
                                            }
                                        } else {
                                            Limpar();

                                            $('#' + idProduto).addClass('marcado');

                                            if ($('#' + idProduto).attr('data-tipo') == 'S') {
                                                $(seca).val($('#' + idProduto).attr('data-em'));
                                                $(umida).val('');

                                                $(secaId).val($('#' + idProduto).attr('id'));
                                                $(umidaId).val('');

                                                $(secaNome).val($('#' + idProduto).attr('data-nome'));
                                                $(umidaNome).val('');
                                            }
                                            else {
                                                $(seca).val('');
                                                $(umida).val($('#' + idProduto).attr('data-em'));

                                                $(secaId).val('');
                                                $(umidaId).val($('#' + idProduto).attr('id'));

                                                $(secaNome).val('');
                                                $(umidaNome).val($('#' + idProduto).attr('data-nome'));
                                            }

                                            if (idOutro != '') {
                                                $('#' + idOutro).addClass('habilitado');

                                                //$(umida).val($('#' + idOutro).attr('data-em'));
                                                //$(seca).val('');
                                            }
                                        }
                                    }

                                    return false;
                                }

                                function Limpar() {
                                    $('.produto').removeClass('marcado');
                                    $('.produto').removeClass('habilitado');
                                }

                                function SetarAlimento() {
                                    var secaId = $('#<%= hdnAlimentoSecoId.ClientID%>').val();
                                    var umidaId = $('#<%= hdnAlimentoUmidoId.ClientID%>').val();

                                    if (secaId != '')
                                        SelecionarProduto($('#' + secaId));

                                    if (umidaId != '')
                                        SelecionarProduto($('#' + umidaId));
                                }

                                function SetarScoreCorporal() {
                                    porte = $('.porte-do-animal').val();

                                    $('#<%= hdnScoreCorporalIndice.ClientID%>').val(indice);
                                    $('#<%= hdnScoreCorporalNumero.ClientID%>').val(numero);
                                    $('#<%= hdnScoreCorporalDetalhe.ClientID%>').val(detalhe);
                                    $('#<%= hdnScoreCorporalRodape.ClientID%>').val(rodape);
                                    $('#<%= hdnScoreCorporalPorte.ClientID%>').val(porte);

                                    $('.numero').html(numero + '<br /><span>' + detalhe + '</span>');

                                    var posicaoAux = (numero - 1) * 350;

                                    $('.lateral').css({ 'background-image': 'url("layout/imagens/score-corporal/gato-lateral.png")', 'background-position': '0px -' + posicaoAux + 'px' });

                                    $('.topo').css({ 'background-image': 'url("layout/imagens/score-corporal/gato-topo.png")', 'background-position': '0px -' + posicaoAux + 'px' });

                                    $('.rodape').html(rodape);
                                }

                                //Recuperando a instância ativa da classe PageRequestManager.
                                var prm = Sys.WebForms.PageRequestManager.getInstance();
                                if (prm != null) {
                                    //Registrando uma chamada a função Inicializar() após o fim da renderização parcial da página.
                                    prm.add_endRequest(function () {
                                        Inicializar();
                                    });
                                }
                            });
                        </script>
                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="Server">
                            <ContentTemplate>
                                <asp:HiddenField ID="hdnAlimentoSecoNome" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoUmidoNome" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoSecoEM" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoUmidoEM" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoSecoId" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoUmidoId" runat="server" />
                                <asp:HiddenField ID="hdnScoreCorporalIndice" runat="server" />
                                <asp:HiddenField ID="hdnScoreCorporalPorte" runat="server" Value="gato" />
                                <asp:HiddenField ID="hdnScoreCorporalNumero" runat="server" Value="6" />
                                <asp:HiddenField ID="hdnScoreCorporalDetalhe" runat="server" Value="10% acima do peso" />
                                <asp:HiddenField ID="hdnScoreCorporalRodape" runat="server" Value="&bull; Costelas não visíveis, porém palpáveis.<br />&bull; Cintura não claramente definida na visão dorsoventral." />

                                <span id="inicio" name="inicio" class="titulo">Programa de perda de peso</span>
                                <p class="passo-a-passo"><strong>1.</strong> Preencha os campos abaixo iniciar o programa de orientação nutricional.</p>

                                <div class="completo">
                                    <span>
                                        Nome do proprietário:<asp:TextBox ID="txtNomeDoProprietario" CssClass="nome-do-proprietario" runat="server" MaxLength="55"></asp:TextBox>
                                    </span>
                                </div>
                                <div class="metade">
                                    <span>
                                        Nome do animal:<asp:TextBox ID="txtNomeDoAnimal" CssClass="nome-do-animal" runat="server" MaxLength="20"></asp:TextBox>
                                    </span>
                                </div>
                                <div class="metade direito">
                                    <div class="campos">
                                        <div class="seletor">
                                            <asp:DropDownList ID="ddlRaca" runat="server" CssClass="raca" 
                                            oninit="ddlRaca_Init">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                        


                                <%--<span class="titulo">Condições atuais do animal</span>--%>
                                <p class="passo-a-passo"><strong>2.</strong> Preencha as informações da condição corporal do paciente.</p>
                                <div class="metade">
                                    <span>
                                        Peso atual:<asp:TextBox ID="txtPesoAtual" CssClass="peso-atual" runat="server" MaxLength="5"></asp:TextBox>(kg)
                                    </span>
                                </div>

                        
                                <%--<span class="titulo">Selecione o score corporal do animal</span>--%>
                                <p class="passo-a-passo"><strong>3.</strong> Selecione o índice de Score Corporal do animal.</p>
                                <div class="score-corporal-obesidade">
                                    <ul>
                                        <li><a href="#" id="score6" data-score="6" data-indice="1,1" data-detalhe="10% acima do peso" data-rodape="&bull; Costelas não visíveis, porém palpáveis.<br />&bull; Cintura não claramente definida na visão dorsoventral.">6<span>10% acima do peso</span></a></li>
                                        <li><a href="#" id="score7" data-score="7" data-indice="1,2" data-detalhe="20% acima do peso" data-rodape="&bull; Costelas de difícil palpação sob a gordura.<br />&bull; Cintura quase imperceptível.<br />&bull; Arredondamento do abdome com presença de gordura abdominal moderada.">7<span>20% acima do peso</span></a></li>
                                        <li><a href="#" id="score8" data-score="8" data-indice="1,3" data-detalhe="30% acima do peso" data-rodape="&bull; Costelas não palpáveis sob a gordura.<br />&bull; Cintura não visível.<br />&bull; Leve distensão abdominal.">8<span>30% acima do peso</span></a></li>
                                        <li><a href="#" id="score9" data-score="9" data-indice="1,4" data-detalhe="40% acima do peso" data-rodape="&bull; Costelas não palpáveis sob uma camada espessa de gordura.<br />&bull; Cintura ausente.<br />&bull; Distensão abdominal evidente.<br />&bull; Depósitos abdominais extensos de gordura.">9<span>40% acima do peso</span></a></li>
                                    </ul>

                                    <div class="detalhe"> 
                                        <div class="numero">
                                            <asp:Literal ID="lblNumero" runat="server">6</asp:Literal>
                                            <span><asp:Literal ID="lblDetalhe" runat="server">10% acima do peso</asp:Literal></span>
                                        </div>
                                        <div id="pnlVistaLateral" runat="server" class="lateral" style="background: #f5f5f6 url('layout/imagens/score-corporal/6-gato-lateral.png') no-repeat 0 -1750px;">
                                            <span>Vista lateral</span>
                                        </div>
                                        <div id="pnlVistaTopo" runat="server" class="topo" style="background: #f5f5f6 url('layout/imagens/score-corporal/6-gato-topo.png') no-repeat 0 -1750px;">
                                            <span>Vista superior</span>
                                        </div>
                                        <div class="rodape">
                                            <asp:Literal ID="lblRodape" runat="server"></asp:Literal>
                                            Costelas palpáveis com leve excesso de camada de gordura.<br />Cintura discernível na visão dorsal, mas não proeminente.
                                        </div>
                                    </div>
                                </div>
                                <p class="passo-a-passo"><strong>4.</strong> Clique sobre o(s) alimento(s) de sua escolha para fazer a indicação. Você pode associar o alimento seco com o alimento úmido correspondente*.</p>

                                <%=Exibir_Produtos()%>


                                <p style="font-size:12px; margin:10px 0 0 0;">*Por padrão, ao associar alimentação úmida e seca, o cálculo correspondente será realizado considerando-se 2/3 das calorias provenientes de alimentação seca e 1/3 proveniente de alimentação úmida.</p>
                                
                                <p class="passo-a-passo"><strong>5.</strong> Recomendações adicionais</p>
                                <div class="completo">
                                    <asp:TextBox ID="txtRecomendacoesAdicionais" CssClass="observacoes" runat="server" TextMode="MultiLine" placeholder="Inclua aqui as recomendações adicionais para ao seu programa de orientação nutricional" Rows="6"></asp:TextBox>
                                    <div class="limite">Limite de caracteres <span id="charsLeft">250</span>.</div>
                                </div>

                                <p class="passo-a-passo"><strong>6.</strong> Clique no botão abaixo para concluir e gerar sua orientação nutricional personalizada.</p>
                                <asp:Button ID="btnGerarRecomendacao" CssClass="botao" runat="server" 
                                    Text="Gerar Recomendação" onclick="btnGerarRecomendacao_Click"/>

                                <p style="margin:20px 0 0 0; font-size:12px; line-height:120%; text-align:left;">Antes de gerar sua recomendação, gostaria de atualizar os dados da sua clínica? <br />
                                    <a href="medico-veterinario.aspx" target="_blank" style="color:#6b6c6e; font-size:12px; background:#22b14c; color:#fff; padding:5px 10px; margin:7px 0 0 0; display:inline-block;">CLIQUE PARA ATUALIZAR</a>
                                </p>

                            <p style="margin:30px 0 0 0; font-size:12px;text-align:justify; line-height:120%;">
                                <strong>OBSERVAÇÕES AO MÉDICO-VETERINÁRIO:</strong><br/>
                                A definição da quantidade diária de alimento leva em consideração as seguintes informações:<br />
                                1) Energia metabolizável do alimento: para estimativa deste valor a Royal Canin utiliza a equação preconizada no NRC (2006).<br />
                                2) Necessidade energética de manutenção: este dado se refere a quantidade de energia que o animal necessita para realizar suas atividades diárias. Diante disso, alguns fatores devem ser considerados para se estimar esta necessidade, como o nível de atividade física, escore de condição corporal, sexo, entre outros. Toda a base de cálculo é feita com o peso metabólico do animal, utilizando-se um fator de correção.<br />
                                3) Gatos devem perder cerca de 0,5% a 1% do seu peso atual por semana a fim de garantir uma perda de peso saudável.<br />
                                4) Caso o animal não esteja perdendo essa quantidade de peso por semana a quantidade de alimento deverá ser ajustada (aumentada ou diminuída).<br />
                                5) Para aumentar as chances de sucesso o peso do animal deverá ser monitorado quinzenalmente pelo Médico-Veterinário.<br />
                            </p>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <div class="atualizando">
                                    <img src="layout/imagens/ico-atualizando.gif" /><span>Atualizando produtos...</span>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>