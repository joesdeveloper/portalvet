﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cabecalho.ascx.cs" Inherits="modulos_cabecalho" %>
        <header>
            <div id="topo">
                <a href="default.aspx"><img src="layout/imagens/img-logo.png" /></a>
                <a href="#" class="logo-royal-canin"><img src="layout/imagens/img-logo-royal.png" /></a>
                <img class="logo-veterinary-exclusive" src="layout/imagens/img-veterinary-exclusive.png" />
            </div>
            <nav>
                <ul>
                    <li><a href="default.aspx">Home</a></li>
                    <li><a href="artigos.aspx">Artigos Técnicos</a></li>
                    <li><a href="produtos.aspx">Produtos</a></li>
                    <li runat="server" id="pnlCoroaPremiada" visible="false"><a href="coroa-premiada.aspx">Coroa Premiada</a></li>
                    <li runat="server" id="pnlForum" visible="true"><a href="foruns.aspx">Fórum</a></li>
                    <li><a href="aplicativo.aspx">App NutriVET</a></li>
                    <li><a href="medico-veterinario.aspx">Cadastre-se</a></li>
                    <li><a href="contato.aspx">Contato</a></li>
                </ul>
            </nav>
        </header>