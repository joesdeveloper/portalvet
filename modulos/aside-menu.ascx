﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="aside-menu.ascx.cs" Inherits="modulos_aside_menu" %>
                <a href="artigos.aspx" class="itens artigos-tecnicos">
                    <div class="nome1">Artigos</div>
                    <div class="nome2">Técnicos</div>
                </a>
                <a href="ultimas-noticias.aspx" class="itens ultimas-noticias">
                    <div class="nome1">Últimas</div>
                    <div class="nome2">Notícias</div>
                </a>
                <a href="congressos-e-seminarios.aspx" class="itens congressos-e-seminarios">
                    <div class="nome1">Congressos</div>
                    <div class="nome2">& Seminários</div>
                </a>
                <a href="aplicativo.aspx" class="itens app-indicacao">
                    <div class="nome1">APP</div>
                    <div class="nome2">NutriVET</div>
                </a>
                <a href="../coroa-premiada.aspx" class="itens coroa-premiada" runat="server" id="pnlCoroaPremiada" visible="false">
                    <div class="nome1">coroa</div>
                    <div class="nome2">premiada</div>
                </a>
                <a href="foruns.aspx" class="itens forum">
                    <div class="nome1">Fórum</div>
                    <div class="nome2">& Dúvidas</div>
                </a>
                <a href="produtos.aspx" class="itens produtos">
                    <div class="nome1">Info de</div>
                    <div class="nome2">Produtos</div>
                </a>
                <a href="fotos-e-videos.aspx" class="itens videos-e-fotos">
                    <div class="nome1">Vídeos</div>
                    <div class="nome2">& Fotos</div>
                </a>
                <a href="arquivos.aspx" class="itens downloads">
                    <div class="nome1">arquivos para</div>
                    <div class="nome2">download</div>
                </a>
                <a href="#" class="banner-chat-online" style=" padding:0; margin:11px 0 0 0;">
                    <img src="layout/imagens/btn-banner.png" />
                </a>
                <div class="redes-sociais">
                    <a href="https://www.facebook.com/royalcanindobrasil" target="_blank" class="redes-social facebook"></a>
                    <a href="https://twitter.com/royal_canin" target="_blank" class="redes-social twitter"></a>
                    <a href="http://instagram.com/royalcaninbrasil" target="_blank" class="redes-social instagram"></a>
                </div>