﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using System.IO;

public partial class modulos_section_destaque : System.Web.UI.UserControl
{
    BLL.Artigos bArt = new BLL.Artigos();

    StringBuilder html = new StringBuilder();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\artigos\";

    UTIL.Validacao oVal = new UTIL.Validacao();

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public string Destaques()
    {
        var lista = bArt.ListarArtigosHome();

        var html = new StringBuilder();


        var listaAux = (from aux in lista
                        where aux.Destaque == true
                        orderby aux.DataInclusao descending
                        select aux).ToList();

        var final = "";

        for (var i = 0; i < listaAux.Count; i++)
        {
            var registro = listaAux[i];

            //final = "";
            //if ((i % 2) != 0)
            //    final = "final";

            final = "";
            if (i == 2)
                final = "final";

            //if (registro.ArtigoImagem == null)
            if (!File.Exists(caminho + registro.IdArtigo + @"/imagem.png"))
                html.AppendLine("<a href=\"artigo.aspx?id=" + registro.IdArtigo + "\" class=\"destaque " + registro.Tipo.Css + " " + final + "\">\n");
            else
                html.AppendLine("<a href=\"artigo.aspx?id=" + registro.IdArtigo + "\" class=\"destaque " + registro.Tipo.Css + " " + final + "\" style=\"background:url('_servicos/gerar_imagem.aspx?i=arquivos/artigos/" + registro.IdArtigo + "/imagem.png&w=350&h=350') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;\">\n");
                //html.AppendLine("<a href=\"artigo.aspx?id=" + registro.IdArtigo + "\" class=\"destaque " + registro.Tipo.Css + " " + final + "\" style=\"background:url('arquivos/artigos/" + registro.IdArtigo + "/imagem.png') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;\">\n");
            
            html.AppendLine(
                "    <div class=\"over " + registro.Tipo.Css + "-over\"><span>" + registro.Tipo.Nome + "</span></div>\n" +
                "    <div class=\"texto\">\n" +
                "        <span>" + oVal.Substring(registro.Titulo, 21) + "</span>\n" +
                "        <p>" + oVal.Substring(oVal.Html(registro.Texto), 55) + "</p>\n" +
                "    </div>\n" +
                "    <label class=\"leia-mais\"></label>\n" +
                "</a>\n"
                );
        }

        return html.ToString();
    }
}