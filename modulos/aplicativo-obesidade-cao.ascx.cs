﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using System.Globalization;

public partial class modulos_aplicativo_obesidade_cao : System.Web.UI.UserControl
{
    BLL.Racas bRac = new BLL.Racas();
    BLL.Especies bEsp = new BLL.Especies();
    VO.Receita vRec = new VO.Receita();
    VO.MedicoVeterinario vMed = new VO.MedicoVeterinario();

    IntegracaoWab oWab = new IntegracaoWab();

    CultureInfo cultura = new CultureInfo("pt-br");

    StringBuilder htmlImpressao = new StringBuilder();
    StringBuilder htmlEmail = new StringBuilder();

    string _erro = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }


    private void CaptarDados()
    {
        vMed = oWab.RecuperarDados();

        if (vMed != null)
        {
            vRec.IdVeterinario = Convert.ToInt32(vMed.Id);
            vRec.userToken = vMed.UserToken;
            vRec.Logo = vMed.Logo;
            vRec.NomeClinica = vMed.Nome_Clinica;
            vRec.Cpf = vMed.Cpf;

            vRec.NomeDoVeterinario = vMed.Nome;
            vRec.SobrenomeDoVeterinario = vMed.Sobrenome;
            vRec.Crmv = vMed.Crmv;
            vRec.CrmvUf = vMed.Crmv_Uf;
            vRec.EmailDoVeterinario = vMed.Email;

            vRec.EspecialidadeAreaAtuacao = vMed.Especialidade_Area_Atuacao;
            vRec.Endereco = vMed.Endereco;
            vRec.Numero = vMed.Numero;
            vRec.Complemento = vMed.Complemento;
            vRec.Bairro = vMed.Bairro;
            vRec.Cidade = vMed.Cidade;
            vRec.Cep = vMed.Cep;
            vRec.Estado = vMed.Estado;
            vRec.TelefoneComercial = vMed.Telefone_Comercial;

            vRec.NomeDoProprietario = txtNomeDoProprietario.Text;
            vRec.NomeDoAnimal = txtNomeDoAnimal.Text;
            vRec.RecomendacoesAdicionais = txtRecomendacoesAdicionais.Text;
        }
        else
        {
            oWab.Logout();
            Response.Redirect("~/login.aspx");
        }

        vRec.TipoDeRelatorio = "Obesidade";

        try
        {
            vRec.IdRaca = Convert.ToInt32(ddlRaca.SelectedValue);

            if (vRec.IdRaca > 0)
                vRec.Raca = bRac.Consultar(vRec.IdRaca).Nome;
        }
        catch { }

        try
        {
            vRec.IdEspecie = Convert.ToInt32(Request["e"]);

            if (vRec.IdEspecie > 0)
                vRec.Especie = bEsp.Consultar(vRec.IdEspecie).Nome;
        }
        catch { }

        try
        {
            vRec.PesoAtual = Convert.ToDouble(txtPesoAtual.Text, cultura);
        }
        catch { }


        vRec.AlimentoSecoNome = hdnAlimentoSecoNome.Value;
        vRec.AlimentoUmidoNome = hdnAlimentoUmidoNome.Value;
        try
        {
            vRec.IdAlimentoSeco = Convert.ToInt32(hdnAlimentoSecoId.Value);
        }
        catch { }
        try
        {
            vRec.IdAlimentoUmido = Convert.ToInt32(hdnAlimentoUmidoId.Value);
        }
        catch { }
        try
        {
            vRec.AlimentoSecoEM = Convert.ToDouble(hdnAlimentoSecoEM.Value);
        }
        catch { }

        try
        {
            vRec.AlimentoUmidoEM = Convert.ToDouble(hdnAlimentoUmidoEM.Value);
        }
        catch { }

        try
        {
            vRec.ScoreCorporal = Convert.ToDouble(hdnScoreCorporalNumero.Value);
        }
        catch { }

        vRec.Sexo = ddlSexoCondicao.SelectedItem.Text;
        try
        {
            vRec.SexoIndice = Convert.ToInt32(ddlSexoCondicao.SelectedItem.Attributes["data-indice"]);
        }
        catch { }

        vRec.Porte = hdnScoreCorporalPorte.Value;

        lblNumero.Text = hdnScoreCorporalNumero.Value;
        lblDetalhe.Text = hdnScoreCorporalDetalhe.Value;
        lblRodape.Text = hdnScoreCorporalRodape.Value;

        pnlVistaLateral.Attributes.Add("style", "background: #f5f5f6 url('layout/imagens/score-corporal/" + hdnScoreCorporalNumero.Value + "-" + vRec.Porte + "-lateral.png') no-repeat center center;");
        pnlVistaTopo.Attributes.Add("style", "background: #f5f5f6 url('layout/imagens/score-corporal/" + hdnScoreCorporalNumero.Value + "-" + vRec.Porte + "-topo.png') no-repeat center center;");
    }


    private bool ValidarDados()
    {
        _erro = "";

        if (vRec.NomeDoProprietario.Trim().Length == 0)
            _erro += @"- Nome do proprietário não pode nulo.\n";

        if (vRec.NomeDoAnimal.Trim().Length == 0)
            _erro += @"- Nome do proprietário não pode nulo.\n";

        if (vRec.IdRaca == 0)
            _erro += @"- Raça não pode nula.\n";

        if (vRec.PesoAtual == 0)
            _erro += @"- Peso Atual não pode nulo.\n";

        if (vRec.AlimentoSecoEM == 0 && vRec.AlimentoUmidoEM == 0)
            _erro += @"- Você deve selecionar um alimento.\n";

        if (vRec.ScoreCorporal == 0)
            _erro += @"- Score Corporal não pode nulo.\n";

        if (vRec.SexoIndice == 0)
            _erro += @"- Sexo / Condição não pode nulo.\n";

        if (_erro.Length > 0)
        {
            _erro = @"ATENÇÃO:\n\n" + _erro;
            return false;
        }

        return true;
    }


    private void ExibirMensagem()
    {
        if (_erro.Trim().Length > 0)
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(string), "Alerta", "alert('" + _erro + "');", true);
    }


    protected void btnGerarRecomendacao_Click(object sender, EventArgs e)
    {
        lblNumero.Text = hdnScoreCorporalIndice.Value;

        CaptarDados();

        if (ValidarDados())
        {
            var ScoreCorporalIndice = Convert.ToDouble(hdnScoreCorporalIndice.Value);

            if (vRec.AlimentoSecoEM > 0)
                vRec.AlimentoSecoQuantidade = CaoQuantidadeDiaria(vRec.PesoAtual, ScoreCorporalIndice, vRec.SexoIndice, vRec.AlimentoSecoEM);

            if (vRec.AlimentoUmidoEM > 0)
                vRec.AlimentoUmidoQuantidade = CaoQuantidadeDiaria(vRec.PesoAtual, ScoreCorporalIndice, vRec.SexoIndice, vRec.AlimentoUmidoEM);

            if (vRec.AlimentoSecoEM > 0 && vRec.AlimentoUmidoEM > 0)
            {
                var porcentagemUmida = 33.33;
                
                var porcentagemSeca = (100 - porcentagemUmida) * 0.01;
                porcentagemUmida = porcentagemUmida * 0.01;

                vRec.AlimentoSecoQuantidade = vRec.AlimentoSecoQuantidade * porcentagemSeca;
                vRec.AlimentoUmidoQuantidade = vRec.AlimentoUmidoQuantidade * porcentagemUmida;
            }

            vRec.AlimentoSecoQuantidade = Convert.ToInt32(vRec.AlimentoSecoQuantidade);
            vRec.AlimentoUmidoQuantidade = Convert.ToInt32(vRec.AlimentoUmidoQuantidade);

            // Montar Logo
            var logo = "";
            if (vRec.Logo != null && vRec.Logo.Length > 0)
                logo += "                           <img src=\"" + vRec.Logo + "\" height=\"70\" style=\"margin-right:10px;\">";

            // Montar endereço
            var endereco = "";

            if (vRec.NomeClinica.Length > 0)
                endereco += "                           " + vRec.NomeClinica + "<br />";

            if (vRec.Endereco.Length > 0)
                endereco += "                           " + vRec.Endereco + ", " + vRec.Numero + "<br />";

            if (vRec.Cep.Length > 0)
                endereco += "                           Cep: " + vRec.Cep;

            if (vRec.Cidade.Length > 0)
                endereco += " - " + vRec.Cidade;

            if (vRec.Estado.Length > 0)
                endereco += " - " + vRec.Estado + "<br />";

            if (vRec.TelefoneComercial.Length > 0)
                endereco += "Fone: " + vRec.TelefoneComercial;

            // Montar Assinatura
            var assinatura = "";
            if (vRec.NomeDoVeterinario.Trim().Length > 0)
                assinatura += vRec.NomeDoVeterinario + " " + vRec.SobrenomeDoVeterinario + "<br />";

            if (vMed.Sexo.Trim().ToUpper() == "F")
                assinatura += "Médica Veterinária";
            else
                assinatura += "Médico Veterinário";

            if (vRec.CrmvUf.Length > 0)
                assinatura += "<br />CRMV - " + vRec.CrmvUf + vRec.Crmv;

            // Cabecalho Impressao
            htmlImpressao.Append(
                "   <table style=\"width:100%; font-size: 12px; line-height:130%;\">" +
                "       <tr>" +
                "           <td>" +
                "               <table style=\"font-size: 12px; line-height:130%;\">" +
                "                   <tr>" +
                "                       <td>" +
                logo +
                "                       </td>" +
                "                       <td>" +
                endereco +
                "                       </td>" +
                "                   </tr>" +
                "               </table>" +
                "           </td>" +
                "           <td style=\"text-align: right;\">" + vRec.DataInclusao.ToString("dd/MM/yyyy") + "</td>" +
                "       </tr>" +
                "   </table>" +
                "   <table style=\"width:100%; color:#e91d25; margin:10px 0 10px 0;\">" +
                "       <tr>" +
                "           <td style=\"width:35px;\"><img src=\"" + Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/layout/imagens/ico-aplicativo-impressao-titulo.jpg\" /></td>" +
                "           <td style=\"width:auto; font-family:'DIN', 'Arial'; font-size:22px; font-weight:700; text-transform:uppercase; color:#e91d25; padding:0 0 0 5px;\">" +
                "               <span style=\"width:auto; font-family:'DIN', 'Arial'; font-weight:300; font-size:22px; text-transform:uppercase;color:#e91d25;\">PROGRAMA DE</span> ORIENTAÇÃO NUTRICIONAL" +
                "           </td>" +
                "       </tr>" +
                "   </table>"
                );


            // Cabecalho Email
            htmlEmail.Append(
                "               <table style=\"font-size: 12px; line-height:130%;\">" +
                "                   <tr>" +
                "                       <td>" +
                logo +
                "                       </td>" +
                "                       <td>" +
                endereco +
                "<br />Data: " + vRec.DataInclusao.ToString("dd/MM/yyyy") + 
                "                       </td>" +
                "                   </tr>" +
                "               </table>" +
                "<br />" +
                "<img src=\"" + Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/layout/imagens/ico-aplicativo-email-titulo.png\" /><br />"
                );

            // Dados do proprietario Impressao
            htmlImpressao.Append(
                "   <table style=\"width:100%; font-size:12px; float:left;\">" +
                "       <tr>" +
                "           <td>Nome do proprietário: <span>" + vRec.NomeDoProprietario + "</span></td>" +
                "           <td>Peso Pretendido: <span>" + vRec.PesoPretendido + "kg</span></td>" +
                "       </tr>" +
                "      <tr>" +
                "           <td>Nome do animal: <span>" + vRec.NomeDoAnimal + "</span></td>" +
                "           <td>"
                );
            if (vRec.AlimentoSecoQuantidade > 0)
                htmlImpressao.Append("Qtd de energia diária recomendada: <span>" + vRec.QuantidadeEnergetica + "kcal</span>");
            htmlImpressao.Append(
                "           </td>" +
                "      </tr>" +
                "       <tr>" +
                "           <td>Raça: <span>" + vRec.Raca + "</span></td>" +
                "           <td>Perda de peso por semana: <span>" + vRec.PerdaPorSemana + "g</span></td>" +
                "      </tr>" +
                "      <tr>" +
                "           <td>Peso Atual: <span>" + vRec.PesoAtual + "kg</span></td>" +
                "           <td>Duração recomendada da dieta: <span>" + vRec.QuantidadeDeSemanas + " semanas</span></td>" +
                "      </tr>" +
                "   </table>"
                );

            // Dados do proprietario Email
            htmlEmail.Append(
                "<br />" +
                "Nome do proprietário: <span>" + vRec.NomeDoProprietario + "</span><br />" +
                "Nome do animal: <span>" + vRec.NomeDoAnimal + "</span><br />" +
                "Raça: <span>" + vRec.Raca + "</span><br />" +
                "Peso Atual: <span>" + vRec.PesoAtual + "kg</span><br />" +
                "Peso Pretendido: <span>" + vRec.PesoPretendido + "kg</span><br />" +
                "Qtd de energia diária recomendada: <span>" + vRec.QuantidadeEnergetica + "kcal</span><br />" +
                "Perda de peso por semana: <span>" + vRec.PerdaPorSemana + "g</span><br />" +
                "Duração recomendada da dieta: <span>" + vRec.QuantidadeDeSemanas + " semanas</span><br />"
                );


            // Cabecalho Recomendacao Impressao
            htmlImpressao.Append(
                "<div style=\"width:100%; font-size:12px; float:left;\">" +
                "    <h2 style=\"color:#333; float:left; font-family:'DIN', 'Arial'; font-size:16px; width:100%; margin:20px 0 0 0; text-transform:uppercase;\">RECOMENDAÇÃO:</h2>" +

                "    <div style=\"width:100%; font-size:14px; margin:3px 0; line-height:130%;\">" +
                "        Como auxiliar ao tratamento utilizar alimentação seguindo as quantidades e especificações descritas abaixo:<br />" +
                "        <br />"
                );

            // Cabecalho Recomendacao Email
            htmlEmail.Append(
                "<h2 style=\"color:#333; float:left; font-family:'DIN', 'Arial'; font-size:16px; width:100%; margin:30px 0 0 0; text-transform:uppercase;\">RECOMENDAÇÃO:</h2><br />" +
                "Como auxiliar ao tratamento utilizar alimentação seguindo as quantidades e especificações descritas abaixo:<br />" +
                "<br />"
                );

            // Recomendacao Sexo Impressao e Email
            if (vRec.AlimentoSecoQuantidade > 0)
            {
                htmlImpressao.Append(
                    "        ALIMENTO SECO<br />" +
                    "        Royal Canin " + vRec.AlimentoSecoNome + "<br />" +
                    "        Quantidade diária: " + vRec.AlimentoSecoQuantidade + "g<br />"
                    );
                htmlEmail.Append(
                    "        ALIMENTO SECO<br />" +
                    "        Royal Canin " + vRec.AlimentoSecoNome + "<br />" +
                    "        Quantidade diária: " + vRec.AlimentoSecoQuantidade + "g<br />"
                    );
            }

            // Recomendacao Umida Impressao e Email
            if (vRec.AlimentoUmidoQuantidade > 0)
            {
                if (vRec.AlimentoSecoQuantidade > 0)
                {
                    htmlImpressao.Append("<br />");
                    htmlEmail.Append("<br />");
                }

                htmlImpressao.Append(
                    "        ALIMENTO ÚMIDO<br />" +
                    "        Royal Canin " + vRec.AlimentoUmidoNome + "<br />" +
                    "        Quantidade diária: " + vRec.AlimentoUmidoQuantidade + "g<br />"
                    );
                htmlEmail.Append(
                    "        ALIMENTO ÚMIDO<br />" +
                    "        Royal Canin " + vRec.AlimentoUmidoNome + "<br />" +
                    "        Quantidade diária: " + vRec.AlimentoUmidoQuantidade + "g<br />"
                    );
            }
            htmlImpressao.Append("    </div>");


            if (vRec.RecomendacoesAdicionais.Trim().Length > 0)
            {
                // Observações Impressao
                htmlImpressao.Append(
                    "    <h2 style=\"color:#333; float:left; font-family:'DIN', 'Arial'; margin:20px 0 0 0; font-size:16px; width:100%; text-transform:uppercase;\">Recomendações adicionais:</h2>" +
                    "    <div class=\"width:100%; margin:3px 0; line-height:160%;\">" + vRec.RecomendacoesAdicionais.Replace("\n", "<br />") + "</div>"
                    );

                // Observações Email
                htmlEmail.Append(
                    "<h2 style=\"color:#333; float:left; font-family:'DIN', 'Arial'; font-size:16px; width:100%; margin:30px 0 0 0; text-transform:uppercase;\">Recomendações adicionais:</h2><br />" +
                    vRec.RecomendacoesAdicionais.Replace("\n", "<br />") + "<br />"
                    );
            }
            htmlImpressao.Append("</div>");

            var RecomendacoesAdicionaisGerais =
                "        1) Dividir a quantidade de alimento em, pelo menos, 3 refeições diárias ou conforme orientação do médico veterinário.<br />" +
                "        2) Não forneça nenhum outro tipo de alimento, inclusive petiscos, sem consultar o médico veterinário.<br />" +
                "        3) Brinque e caminhe com seu cão durante o processo de emagrecimento, aumentando a distância e o ritmo gradativamente.<br />" +
                "        4) Mantenha o alimento sempre em sua embalagem original bem fechada.<br />" +
                "        5) Deixar sempre água fresca e limpa à disposição. Caso seu animal não esteja habituado a este alimento, respeite um período de transição de 7 dias, durante o qual deve-se mudar o alimento gradativamente.<br />" +
                "        6) É normal e esperado que as quantidades de alimentos tenham que ser periodicamente ajustadas e por isso é fundamental o acompanhamento do médico veterinário durante todo o processo de perda de peso para maximizar as chance de sucesso.<br />" +
                "        7) Tenha paciência, um programa de emagrecimento efetivo, pode demorar; e o seu comprometimento é muito importante.<br />"
                ;

            // Observações Gerais Impressao
            htmlImpressao.Append(
                "<div style=\"position:absolute; bottom:0; left:0;\">" +
                "    <h2 style=\"color:#333; float:left; font-family:'DIN', 'Arial'; font-size:16px; width:100%; margin:30px 0 0 0; text-transform:uppercase;\">OBSERVAÇÕES GERAIS:</h2>" +
                "    <div style=\"width:100%; display:block; font-size:12px; text-align:justify;\">" +
                RecomendacoesAdicionaisGerais +
                "    </div>" +

                "   <table style=\"width:100%;\">" +
                "       <tr>" +
                "           <td>" +
                "               <div style=\"float:right;display:block; width: 350px; border-top: 1px solid #333; text-align:center; font-size:11px; margin:60px 0 0 0; text-transform:uppercase; line-height:140%; \">" + assinatura + "</div>" +
                "           </td>" +
                "       </tr>" +
                "   </table>" +
                "</div>"
                );

            // Observações Gerais Email
            htmlEmail.Append(
                "<h2 style=\"color:#333; float:left; font-family:'DIN', 'Arial'; font-size:16px; width:100%; margin:30px 0 0 0; text-transform:uppercase;\">OBSERVAÇÕES GERAIS:</h2><br />" +
                RecomendacoesAdicionaisGerais +
                "<h2 style=\"color:#333; float:left; font-family:'DIN', 'Arial'; font-size:16px; width:100%; margin:30px 0 0 0; text-transform:uppercase;\">MÉDICO VETERINÁRIO:</h2><br />" + assinatura + "<br />"
                );


            Session["htmlImpressao"] = htmlImpressao.ToString();
            Session["htmlEmail"] = htmlEmail.ToString();


            new BLL.Receitas().Salvar(vRec);

            //var userAgent = Request.UserAgent.ToLower();

            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            var bw = browser.Browser.ToLower();

            if (bw.Contains("safari"))
                Response.Redirect("~/aplicativo-imprimir.aspx");
            else
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(string), "Alerta", "var w = window.open('aplicativo-imprimir.aspx','impressao', 'width:800px,height:500px,toolbar=0,titlebar=0,menubar=0,resizable=1,scrollbars=1,status=0'); w.focus(); if(navigator.userAgent.indexOf('Chrome/') > 0) { if (w.opener) { w.close(); } window.open('aplicativo-imprimir.aspx','impressao', 'width:800px,height:500px,toolbar=0,titlebar=0,menubar=0,resizable=1,scrollbars=1,status=0'); }", true);
            
        }

        ExibirMensagem();
    }


    #region Produtos
    public string Exibir_Produtos()
    {
        var html = new StringBuilder();

        var lista = new BLL.Produtos().Listar();

        if (lista == null)
            return "";

        var lSeco = (from pro in lista
                     where pro.Ativo == true && pro.Obesidade == true && pro.Tipo.IdTipo == "S" && pro.Especie.IdEspecie == Convert.ToInt32(Request["e"]) && (pro.LocalExibicao == "t" || pro.LocalExibicao == "a")
                     select pro).ToList();

        var i = 0;
        var classFinal = "";

        if (lSeco.Count > 0)
        {
            html.Append("<span class=\"titulo\">Alimento seco</span>");
            
            foreach (var registro in lSeco)
            {
                if (i == 1)
                {
                    classFinal = "final";
                    i = 0;
                }
                else
                {
                    classFinal = "";
                    i++;
                }

                html.Append(
                    "<div class=\"produtos\">" +
                    "    <div id=\"" + registro.IdProduto + "\" class=\"produto " + classFinal + "\" data-outro=\"" + registro.IdOutraVersao + "\" data-tipo=\"S\" data-em=\"" + registro.EnergiaMetabolizavel + "\" data-nome=\"" + registro.Nome + "\">" +
                    "        <div class=\"borda\">" +
                    "            <div style=\"background-image:url('_servicos/gerar_imagem.aspx?i=arquivos/produtos/" + registro.IdProduto + "/foto.png&w=112&h=112');\" class=\"imagem\"></div>" +
                    "        </div>" +
                    "        <div class=\"texto\">" +
                    "            <span>" + registro.Nome + "</span>" +
                    "            <p>" + registro.Descricao + "</p>" +
                    "            <a class=\"saiba-mais\" data-url=\"produto.aspx?id=" + registro.IdProduto + "\">Saiba mais</a>" +
                    "        </div>" +
                    "    </div>" +
                    "</div>"
                    );
                
            }
        }


        var lUmido = (from pro in lista
                      where pro.Ativo == true && pro.Obesidade == true && pro.Tipo.IdTipo == "U" && pro.Especie.IdEspecie == Convert.ToInt32(Request["e"]) && (pro.LocalExibicao == "t" || pro.LocalExibicao == "a")
                      select pro).ToList();

        i = 0;
        classFinal = "";


        if (lUmido.Count > 0)
        {
            html.Append("<span class=\"titulo\">Alimento úmido</span>");

            foreach (var registro in lUmido)
            {
                if (i == 1)
                {
                    classFinal = "final";
                    i = 0;
                }
                else
                {
                    classFinal = "";
                    i++;
                }

                html.Append(
                    "<div class=\"produtos\">" +
                    "    <div id=\"" + registro.IdProduto + "\" class=\"produto " + classFinal + "\" data-outro=\"" + registro.IdOutraVersao + "\" data-tipo=\"" + registro.Tipo.IdTipo.ToUpper() + "\" data-em=\"" + registro.EnergiaMetabolizavel + "\" data-nome=\"" + registro.Nome + "\">" +
                    "        <div class=\"borda\">" +
                    "            <div style=\"background-image:url('_servicos/gerar_imagem.aspx?i=arquivos/produtos/" + registro.IdProduto + "/foto.png&w=112&h=112');\" class=\"imagem\"></div>" +
                    "        </div>" +
                    "        <div class=\"texto\">" +
                    "            <span>" + registro.Nome + "</span>" +
                    "            <p>" + registro.Descricao + "</p>" +
                    "            <a class=\"saiba-mais\" data-url=\"produto.aspx?id=" + registro.IdProduto + "\">Saiba mais</a>" +
                    "        </div>" +
                    "    </div>" +
                    "</div>"
                    );
            }
        }
        

        return html.ToString();
    }
    #endregion


    #region Funcoes de Calculo
    private double CaoQuantidadeDiaria(double pesoAtual, double score, double sexo, double racao)
    {
        //var scoreCorporal = score * 0.01;
        //var pesoPretendido = pesoAtual - (pesoAtual * scoreCorporal);
        var pesoPretendido = (pesoAtual / score);
        var quantidadeEnergetica = sexo * (Math.Pow(pesoPretendido, 0.75));
        var perdaPorSemana = pesoAtual * 0.015 * 1000;
        var quantidadeDiaria = (quantidadeEnergetica / racao) * 1000;
        var semanas = Math.Ceiling(((pesoAtual - pesoPretendido) / perdaPorSemana) * 1000);

        vRec.QuantidadeEnergetica = Convert.ToInt32(quantidadeEnergetica);
        vRec.PesoPretendido = Math.Round(pesoPretendido, 3);
        vRec.PerdaPorSemana = perdaPorSemana;
        vRec.QuantidadeDeSemanas = Convert.ToInt32(semanas);

        return quantidadeDiaria;
    }
    #endregion


    protected void ddlRaca_Init(object sender, EventArgs e)
    {
        var bRac = new BLL.Racas();

        if (Request["e"] != null)
        {
            var idEspecie = Convert.ToInt32(Request["e"]);

            var lista = bRac.Listar(idEspecie);

            ddlRaca.Items.Add(new ListItem("Raças", "0"));

            if (lista != null && lista.Count > 0)
            {
                var listaAux = (from aux in lista
                                orderby aux.Ordenacao ascending, aux.Nome ascending
                                select aux).ToList();

                foreach (var registro in listaAux)
                {
                    ddlRaca.Items.Add(new ListItem(registro.Nome, registro.IdRaca.ToString()));
                }
            }
        }
    }
    
}