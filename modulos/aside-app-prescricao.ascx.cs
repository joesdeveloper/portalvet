﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class modulos_aside_app_prescricao : System.Web.UI.UserControl
{
    System.Security.Principal.IPrincipal usuario = System.Web.HttpContext.Current.User;

     IntegracaoWab oWab = new IntegracaoWab();

     protected void Page_Load(object sender, EventArgs e)
     {
         Iniciar();
     }

    protected void btnLogin_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            var retorno = oWab.Login(txtEmail.Text, txtSenha.Text);

            if (retorno)
            {
                if (Request["ReturnUrl"] != null)
                    Response.Redirect(Request["ReturnUrl"]);
                else
                    Response.Redirect("~/");
            }
            else
            {
                if (Session["MedicoVeterinarioRetorno"] == null)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nE-MAIL e/ou SENHA inválido(s).');", true);
                else
                {
                    var mensagem = (VO.MensagemDeRetorno)Session["MedicoVeterinarioRetorno"];

                    if (mensagem.code == 4)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nVocê não está cadastrado como Veterinário. Se quiser ter acesso ao Portal Vet acesse o link abaixo e mude seu perfil para Veterinário.\n\nhttp://cadastro.royalcanin.com.br');", true);
                    else
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nE-MAIL e/ou SENHA inválido(s).');", true);
                }
            }
        }
        catch {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nOcorreu um erro na autenticação, por favor, entre em contato pelo e-mail: consumidor@royalcanin.com');", true);
        }

        

        /*if (Session["MedicoVeterinarioRetorno"] != null)
        {
            var mensagem = (MensagemDeRetorno)Session["MedicoVeterinarioRetorno"];

            Response.Write(mensagem.code + " - " + mensagem.status + " - " + mensagem.data);
        }
        else
            Response.Write("nulo");*/
    }


    protected void btnLogout_Click(object sender, ImageClickEventArgs e)
    {
        new IntegracaoWab().Logout();

        Response.Redirect("~/");
    }


    protected void btnRecuperarSenha_Click(object sender, EventArgs e)
    {
        var email = txtEmail.Text.Trim();

        if (email.Length == 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nO campo E-MAIL não pode ser branco.');", true);
        else
        {
            if (!new UTIL.Validacao().Email(email))
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nO campo E-MAIL é inválido.');", true);
            else
            {
                var retorno = new IntegracaoWab().RecuperarSenha(email);

                if (!retorno.error)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('Verifique seu e-mail, pois em alguns minutos você receberá as informações necessárias para reiniciar sua senha.');", true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", @"alert('ATENÇÃO\n\nO e-mail informado não foi localizado, por favor, verificar se o mesmo está correto.');", true);
            }
        }
    }


    private void Iniciar()
    {
        login.Visible = false;
        logout.Visible = false;

        // Verificar se o MV está autenticado
        if (usuario.Identity.IsAuthenticated && usuario.IsInRole("MedicoVeterinario"))
        {
            logout.Visible = true;

            // Recuperar dos dados do MV
            var vMed = oWab.RecuperarDados();

            lblNome.Text = vMed.Nome + " " + vMed.Sobrenome;
            lblCrmv.Text = vMed.Crmv_Uf + vMed.Crmv;
            lblData.Text = Convert.ToDateTime(Session["Data"]).ToString("dd/MM/yyyy HH:mm");

            // Verificar se os dados do MV estão completos
            if (!oWab.Validar(vMed))
            {
                // Verificar se o usuário não esta na página de alteração de dados
                // caso ele não esteja, rediciona o usuario para página de alteração de dados
                if (Request.Url.ToString().IndexOf("medico-veterinario.aspx") == -1)
                    Response.Redirect("~/medico-veterinario.aspx");
            }

            Session["MedicoVeterinarioInfo"] = null;
        }
        else
            login.Visible = true;
        
        //try
        //{
        //    if (Session["MedicoVeterinarioInfo"] != null)
        //    {
        //        var vMed = (VO.MedicoVeterinario)Session["MedicoVeterinarioInfo"];

        //        lblNome.Text = vMed.Nome + " " + vMed.Sobrenome;
        //        lblCrmv.Text = vMed.Crmv_Uf + vMed.Crmv;
        //        lblData.Text = Convert.ToDateTime(Session["Data"]).ToString("dd/MM/yyyy HH:mm");
        //    }
        //    else
        //    {
        //        if (Request.Cookies["MedicoVeterinarioInfo"] != null)
        //        {
        //            var cookie = Request.Cookies["MedicoVeterinarioInfo"];

        //            lblNome.Text = cookie["Nome"] + " " + cookie["Sobrenome"];
        //            lblCrmv.Text = cookie["Crmv_Uf"] + cookie["Crmv"];
        //            lblData.Text = cookie["Data"];
        //        }
        //        else
        //        {
        //            new IntegracaoWab().Logout();
        //            //Response.Redirect("~/login.aspx");
        //        }
        //    }

        //    //Response.Write("t:" + UTIL.Criptografia.Descriptografar(cookie["UserToken"]) + "<br />S: " + UTIL.Criptografia.Descriptografar(cookie["UserTokenAux"]) + "<br />");
        //}
        //catch { }
    }
}