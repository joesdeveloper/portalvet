﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="aplicativo-patologia-cao.ascx.cs" Inherits="modulos_aplicativo_patologia_cao" %>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                var numero = '4';
                                var score = '';

                                //Inicializarndo na após a carga da página
                                Inicializar();

                                //função com o código JQuery
                                function Inicializar() {
                                    $('#<%= txtRecomendacoesAdicionais.ClientID%>').limit('250', '#charsLeft');

                                    SetarScoreCorporal();
                                    SetarAlimento();
                                    SetarCategoria();
                                    SetarSubcategoria();
                                    SetarFiltro();
                                    SetarPorNome();

                                    Ancora();

                                    $('#<%= txtPesoAtual.ClientID%>').maskMoney({ showSymbol: false, decimal: ",", thousands: "." });

                                    $('.saiba-mais').click(function () {
                                        SaibaMais($(this));
                                    });

                                    $('.score-corporal-patologia > ul > li > a').click(function () {
                                        SelecionarScoreCorporal($(this));
                                        return false;
                                    });

                                    $('.filtro > ul > li > a').click(function () {
                                        SelecionarFiltro($(this));
                                    });

                                    $('.categorias-enfermidades > ul > li > a').click(function () {
                                        SelecionarCategorias($(this));
                                    });

                                    $('.categorias-produtos > ul > li > a').click(function () {
                                        SelecionarPorNome($(this));
                                    });

                                    $('.doencas-patologia-subcategoria > ul > li > a').click(function () {
                                        SelecionarSubcategorias($(this));
                                    });

                                    $('.produto').click(function () {
                                        SelecionarProduto($(this));
                                    });

                                }

                                /* Funções de Seleção */
                                function SelecionarScoreCorporal(objeto) {
                                    numero = $(objeto).attr('data-score');
                                    score = $(objeto).attr('data-value')

                                    SetarScoreCorporal();
                                }


                                function SelecionarFiltro(objeto) {
                                    var id = $(objeto).attr('id');

                                    $('#<%= hdnSelecaoId.ClientID%>').val(id);

                                    SetarFiltro();
                                }


                                function SelecionarCategorias(objeto) {
                                    var id = $(objeto).attr('id');
                                    var valor = $(objeto).attr('data-value');

                                    $('#<%= hdnCategoriaId.ClientID%>').val(id);
                                    $('#<%= hdnCategoriaValue.ClientID%>').val(valor);

                                    SetarCategoria();
                                }


                                function SelecionarSubcategorias(objeto) {
                                    var id = $(objeto).attr('id');

                                    $('#<%= hdnSubcategoriaId.ClientID%>').val(id);

                                    SetarSubcategoria();
                                }

                                function SelecionarPorNome(objeto) {
                                    var id = $(objeto).attr('id');

                                    $('#<%= hdnPorNomeId.ClientID%>').val(id);

                                    SetarPorNome();
                                }

                                function SelecionarProduto(objeto) {
                                    //alert($(objeto).attr("id"));

                                    // obtem os id válidos
                                    var secaNome = '#<%= hdnAlimentoSecoNome.ClientID%>';
                                    var umidaNome = '#<%= hdnAlimentoUmidoNome.ClientID%>';

                                    var seca = '#<%= hdnAlimentoSecoEM.ClientID%>';
                                    var umida = '#<%= hdnAlimentoUmidoEM.ClientID%>';

                                    var secaId = '#<%= hdnAlimentoSecoId.ClientID%>';
                                    var umidaId = '#<%= hdnAlimentoUmidoId.ClientID%>';

                                    var idProduto = $(objeto).attr('id');
                                    var idOutro = $(objeto).attr('data-outro');

                                    // verificar se ja é marcado
                                    if ($('#' + idProduto).hasClass('marcado')) {
                                        // verificar se ja é marcado
                                        if (!$('#' + idProduto).hasClass('habilitado')) {
                                            Limpar();

                                            $(umida).val('');
                                            $(seca).val('');
                                            $(umidaId).val('0');
                                            $(secaId).val('0');
                                            $(secaNome).val('');
                                            $(umidaNome).val('');
                                        }
                                    }
                                    else {

                                        // verificar se ja é marcado
                                        if ($('#' + idProduto).hasClass('habilitado')) {
                                            Limpar();
                                            $('#' + idProduto).addClass('marcado');
                                            $('#' + idOutro).addClass('marcado');

                                            if ($('#' + idProduto).attr('data-tipo') == 'S') {
                                                $(seca).val($('#' + idProduto).attr('data-em'));
                                                $(umida).val($('#' + idOutro).attr('data-em'));

                                                $(secaId).val($('#' + idProduto).attr('id'));
                                                $(umidaId).val($('#' + idOutro).attr('id'));

                                                $(secaNome).val($('#' + idProduto).attr('data-nome'));
                                                $(umidaNome).val($('#' + idOutro).attr('data-nome'));
                                            }
                                            else {

                                                $(seca).val($('#' + idOutro).attr('data-em'));
                                                $(umida).val($('#' + idProduto).attr('data-em'));

                                                $(secaId).val($('#' + idOutro).attr('id'));
                                                $(umidaId).val($('#' + idProduto).attr('id'));

                                                $(secaNome).val($('#' + idOutro).attr('data-nome'));
                                                $(umidaNome).val($('#' + idProduto).attr('data-nome'));
                                            }
                                        } else {
                                            Limpar();

                                            $('#' + idProduto).addClass('marcado');

                                            if ($('#' + idProduto).attr('data-tipo') == 'S') {
                                                $(seca).val($('#' + idProduto).attr('data-em'));
                                                $(umida).val('');

                                                $(secaId).val($('#' + idProduto).attr('id'));
                                                $(umidaId).val('');

                                                $(secaNome).val($('#' + idProduto).attr('data-nome'));
                                                $(umidaNome).val('');
                                            }
                                            else {
                                                $(seca).val('');
                                                $(umida).val($('#' + idProduto).attr('data-em'));

                                                $(secaId).val('');
                                                $(umidaId).val($('#' + idProduto).attr('id'));

                                                $(secaNome).val('');
                                                $(umidaNome).val($('#' + idProduto).attr('data-nome'));
                                            }

                                            if (idOutro != '') {
                                                $('#' + idOutro).addClass('habilitado');

                                                //$(umida).val($('#' + idOutro).attr('data-em'));
                                                //$(seca).val('');
                                            }
                                        }
                                    }

                                    return false;
                                }

                                function SaibaMais(objeto) {
                                    window.open($(objeto).attr('data-url'));
                                    return false;
                                }


                                /* Funções para Marcar Itens */
                                function Limpar() {
                                    $('.produto').removeClass('marcado');
                                    $('.produto').removeClass('habilitado');
                                }

                                function SetarAlimento() {
                                    var secaId = $('#<%= hdnAlimentoSecoId.ClientID%>').val();
                                    var umidaId = $('#<%= hdnAlimentoUmidoId.ClientID%>').val();

                                    if (secaId != '')
                                        SelecionarProduto($('#' + secaId));

                                    if (umidaId != '')
                                        SelecionarProduto($('#' + umidaId));
                                }

                                function SetarScoreCorporal() {
                                    $('.score-corporal-selecao').removeClass('score-marcado');

                                    $('#<%= hdnScoreCorporalNumero.ClientID%>').val(numero);

                                    var id = $('#<%= hdnScoreCorporalNumero.ClientID%>').val();

                                    $('#score' + id).addClass('score-marcado');
                                }


                                function SetarFiltro() {
                                    $('.filtro-selecao').removeClass('categoria-marcado');

                                    var id = $('#<%= hdnSelecaoId.ClientID%>').val();

                                    $('#' + id).addClass('categoria-marcado');
                                }

                                function SetarCategoria() {
                                    $('.categoria-selecao').removeClass('categoria-marcado');

                                    var Id = $('#<%= hdnCategoriaId.ClientID%>').val();

                                    $('#' + Id).addClass('categoria-marcado');
                                }

                                function SetarSubcategoria() {
                                    $('.subcategoria-selecao').removeClass('subcategoria-marcado');

                                    var id = $('#<%= hdnSubcategoriaId.ClientID%>').val();

                                    $('#' + id).addClass('subcategoria-marcado');
                                }

                                function SetarPorNome() {
                                    //$('.filtro-selecao').removeClass('categoria-marcado');
                                    
                                    var id = $('#<%= hdnPorNomeId.ClientID%>').val();

                                    $('#' + id).addClass('categoria-marcado');
                                }


                                function Ancora() {
                                    var tag = $('#<%= hdnAncora.ClientID%>').val();

                                    if (tag != '')
                                        window.location.hash = tag;
                                }

                                //Recuperando a instância ativa da classe PageRequestManager.
                                var prm = Sys.WebForms.PageRequestManager.getInstance();
                                if (prm != null) {
                                    //Registrando uma chamada a função Inicializar() após o fim da renderização parcial da página.
                                    prm.add_endRequest(function () {
                                        Inicializar();
                                    });
                                }
                            });
                        </script>
                        
                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="Server">
                            <ContentTemplate>

                                <asp:HiddenField ID="hdnAlimentoSecoNome" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoUmidoNome" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoSecoEM" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoUmidoEM" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoSecoId" runat="server" />
                                <asp:HiddenField ID="hdnAlimentoUmidoId" runat="server" />

                                <asp:HiddenField ID="hdnScoreCorporalNumero" runat="server" Value="4"  /> 
                                <asp:HiddenField ID="hdnCategoriaId" runat="server" />
                                <asp:HiddenField ID="hdnCategoriaValue" runat="server" />
                                <asp:HiddenField ID="hdnSubcategoriaId" runat="server" />
                                <asp:HiddenField ID="hdnSelecaoId" runat="server" Value=""/>
                                <asp:HiddenField ID="hdnPorNomeId" runat="server" Value=""/>
                                <asp:HiddenField ID="hdnAncora" runat="server" Value=""/>
                                <asp:HiddenField ID="hdnIdDoenca" runat="server" Value=""/>
                               

                                <span id="inicio" name="inicio" class="titulo">Programa de recomendação alimentar</span>
                                <p class="passo-a-passo"><strong>1.</strong> Preencha os campos abaixo iniciar o programa de orientação nutricional.</p>

                                <div class="completo">
                                    <span>
                                        Nome do proprietário:<asp:TextBox ID="txtNomeDoProprietario" CssClass="nome-do-proprietario" runat="server" MaxLength="55"></asp:TextBox>
                                    </span>
                                </div>
                        
                                <div class="metade">
                                    <span>
                                        Nome do animal:<asp:TextBox ID="txtNomeDoAnimal" CssClass="nome-do-animal" runat="server" MaxLength="20"></asp:TextBox>
                                    </span>
                                </div>
                                <div class="metade direito">
                                    <div class="campos">
                                        <div class="seletor">
                                            <asp:DropDownList ID="ddlRaca" runat="server" CssClass="raca" 
                                            oninit="ddlRaca_Init">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>


                                <p class="passo-a-passo"><strong>2.</strong> Insira o peso atual do paciente</p>
                                <div class="metade">
                                    <span>
                                        Peso atual:<asp:TextBox ID="txtPesoAtual" CssClass="peso-atual" runat="server" MaxLength="5"></asp:TextBox>(kg)
                                    </span>
                                </div>
                        
                                <p class="passo-a-passo"><strong>3.</strong> Selecione o nível de atividade ou condição corporal do animal</p>
                                <div class="score-corporal-patologia">
                                    <ul>
                                        <li><a href="#" id="score4" class="score-corporal-selecao" data-score="4"><img src="layout/imagens/ico-cao-peso-magro.png" /><span>Alta atividade<br />Magro</span></a></li>
                                        <li><a href="#" id="score5" class="score-corporal-selecao" data-score="5"><img src="layout/imagens/ico-cao-peso-ideal.png" /><span>Atividade moderada<br />Peso ideal</span></a></li>
                                        <li><a href="#" id="score6" class="score-corporal-selecao" data-score="6"><img src="layout/imagens/ico-cao-peso-sobrepeso.png" /><span>Baixa atividade<br />Sobrepeso</span></a></li>
                                    </ul>
                                </div>

                                <p class="passo-a-passo"><strong>4.</strong> Selecione o produto a ser indicado. Você pode fazer uma busca por enfermidade ou por produto.</p>
                                <div class="filtro">
                                    <ul>
                                        <li>
                                            <asp:LinkButton ID="btnFiltroEnfermidade" CssClass="filtro-selecao" runat="server" onclick="btnFiltroEnfermidade_Click"><div>1</div><span>Por enfermidades</span></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnFiltroProduto" CssClass="filtro-selecao" runat="server" onclick="btnFiltroProduto_Click"><div>2</div><span>Por nome</span></asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>

                                <div id="pnlEnfermidade" class="filtro-produto" runat="server" visible="false">
                                    <div id="categoriasenfermidades" class="categorias-enfermidades">
                                        <span class="categorias-enfermidades-titulo">Categorias</span>
                                        <ul>
                                            <asp:Repeater ID="rptCategoria" runat="server">
                                                <ItemTemplate>
                                                    <li>
                                                        <asp:LinkButton ID="lnkCategoria" CssClass="categoria-selecao" runat="server" ClientIDMode="AutoID" OnClick="lnkCategoria_Click" data-value='<%#Eval("IdCategoria") %>'><div><%# Container.ItemIndex+1 %></div><span><%#Eval("Nome") %></span></asp:LinkButton>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>

                                    <div id="pnlSubcategoria" class="doencas-patologia-subcategoria" runat="server" visible="false">
                                        <span class="doencas-patologia-titulo">Enfermidades</span>
                                        <ul>
                                            <asp:Repeater ID="rptSubcategoria" runat="server">
                                                <ItemTemplate>
                                                    <li>
                                                        <asp:LinkButton ID="lnkSubcategoria" CssClass="subcategoria-selecao" runat="server" ClientIDMode="AutoID" OnClick="lnkSubcategoria_Click" data-value='<%#Eval("IdDoenca") %>'><div><%# Container.ItemIndex+1 %></div><span><%#Eval("Nome") %></span></asp:LinkButton>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>

                                <div id="pnlProduto" class="filtro-produto" runat="server" visible="false">
                                    <div id="Div1" class="categorias-produtos">
                                        <span class="categorias-enfermidades-titulo">Painel de produtos</span>
                                        <ul>
                                            <asp:Repeater ID="rptPorNomeProdutos" runat="server">
                                                <ItemTemplate>
                                                    <li id="linha" runat="server">
                                                        <asp:LinkButton ID="lnkCategoria" CssClass="categoria-selecao" runat="server" ClientIDMode="AutoID" OnClick="lnkPorNomeProdutos_Click" data-value='<%#Eval("IdProduto") %>'><div><%# Container.ItemIndex+1 %></div><span><%#Eval("Nome") %></span></asp:LinkButton>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>

                                <div id="produtos" style=" width:100%; float:left; height:0px;"></div>
                                <asp:Literal ID="lblProdutos" runat="server"></asp:Literal>

                                <p class="passo-a-passo"><strong>6.</strong> Recomendações adicionais</p>
                                <div class="completo">
                                    <asp:TextBox ID="txtRecomendacoesAdicionais" CssClass="observacoes" runat="server" TextMode="MultiLine" placeholder="Inclua aqui as recomendações adicionais para ao seu programa de orientação nutricional." Rows="6"></asp:TextBox>
                                    <div class="limite">Limite de caracteres <span id="charsLeft">250</span>.</div>
                                </div>
                        
                                
                                <p class="passo-a-passo"><strong>7.</strong> Clique no botão abaixo para concluir e gerar sua orientação nutricional personalizada.</p>
                                <asp:Button ID="btnGerarRecomendacao" CssClass="botao" runat="server" 
                                    Text="Gerar Recomendação" onclick="btnGerarRecomendacao_Click"/>

                                <p style="margin:20px 0 0 0; font-size:12px; line-height:120%; text-align:left;">Antes de gerar sua recomendação, gostaria de atualizar os dados da sua clínica? <br />
                                    <a href="medico-veterinario.aspx" target="_blank" style="color:#6b6c6e; font-size:12px; background:#22b14c; color:#fff; padding:5px 10px; margin:7px 0 0 0; display:inline-block;">CLIQUE PARA ATUALIZAR</a>
                                </p>

                                <p style="margin:30px 0 0 0; font-size:12px;text-align:justify; line-height:120%;">
                                    <strong>OBSERVAÇÕES AO MÉDICO-VETERINÁRIO:</strong><br/>
                                    A definição da quantidade diária de alimento leva em consideração as seguintes informações:<br/>
                                    1) Energia metabolizável do alimento: para estimativa deste valor a Royal Canin utiliza a equação preconizada no NRC (2006).<br/>
                                    2) Necessidade energética de manutenção: este dado se refere a quantidade de energia que o animal necessidade para realizar suas atividades diárias. Diante disso, alguns fatores devem ser considerados para se estimar esta necessidade, como o nível de atividade física, escore de condição corporal, sexo, entre outros. Toda a base de cálculo é feita com o peso metabólico do animal, utilizando-se um fator de correção.<br/>
                                    3) Os valores gerados pelos cálculos são de referência, podendo ocorrer variação individual caso necessário.
                                </p>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <div class="atualizando">
                                    <img src="layout/imagens/ico-atualizando.gif" /><span>Atualizando produtos...</span>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        
