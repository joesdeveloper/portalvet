﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="aside-app-prescricao.ascx.cs" Inherits="modulos_aside_app_prescricao" %>
                <div id="login" runat="server" class="login" visible="false">
                    <div class="nome">login <span>área exclusiva</span></div>
                    <div class="campos">
                        <asp:LinkButton ID="btnRecuperarSenha" runat="server" CssClass="esqueci-minha-senha" OnClick="btnRecuperarSenha_Click">Esqueci minha senha</asp:LinkButton>
                        <div CssClass="esqueci-minha-senha">&nbsp;</div>
                        <asp:TextBox ID="txtEmail" placeholder="NOME/E-MAIL" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtSenha" placeholder="SENHA" runat="server" TextMode="Password"></asp:TextBox>
                        <div class="botoes">
                            <asp:HyperLink ID="HyperLink2" NavigateUrl="~/medico-veterinario.aspx" runat="server">Ainda não é cadastrado? Clique aqui</asp:HyperLink>
                            <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/layout/imagens/btn-login.png" OnClick="btnLogin_Click" />
                        </div>
                    </div>
                </div> 
                <div id="logout" runat="server" class="logout" visible="false">
                    <div class="nome">login <span>área exclusiva</span></div>
                    <div class="campos">
                        <div class="botoes">
                            <span class="dados"><strong><asp:Literal ID="lblNome" runat="server"></asp:Literal></strong></span>
                            <span class="dados">CRMV: <asp:Literal ID="lblCrmv" runat="server"></asp:Literal></span>
                            <span class="dados">Data: <asp:Literal ID="lblData" runat="server"></asp:Literal></span>
                            <asp:HyperLink ID="HyperLink1" NavigateUrl="~/medico-veterinario.aspx" runat="server">Alterar dados cadastrais? Clique aqui</asp:HyperLink>
                            <asp:ImageButton ID="btnLogout" runat="server" ImageUrl="~/layout/imagens/btn-logout.png" OnClick="btnLogout_Click" />
                        </div>
                    </div>
                </div>