﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="aplicativo.aspx.cs" Inherits="aplicativo" validateRequest="false" %>
<%@ Register src="modulos/rodape.ascx" tagname="rodape" tagprefix="uc1" %>
<%@ Register src="modulos/cabecalho.ascx" tagname="cabecalho" tagprefix="uc2" %>
<%@ Register src="modulos/aside-app-prescricao.ascx" tagname="prescricao" tagprefix="uc3" %>
<%@ Register src="modulos/aside-pesquisar.ascx" tagname="pesquisar" tagprefix="uc4" %>
<%@ Register src="modulos/aside-menu.ascx" tagname="menu" tagprefix="uc5" %>
<%@ Register src="modulos/aplicativo-obesidade-cao.ascx" tagname="aplicativo" tagprefix="uc7" %>
<%@ Register src="modulos/aplicativo-obesidade-gato.ascx" tagname="aplicativo" tagprefix="uc8" %>
<%@ Register src="modulos/aplicativo-patologia-cao.ascx" tagname="aplicativo" tagprefix="uc9" %>
<%@ Register src="modulos/aplicativo-patologia-gato.ascx" tagname="aplicativo" tagprefix="uc10" %>
<%--<%@ Register src="modulos/aside-produtos-relacionados.ascx" tagname="aside" tagprefix="uc6" %>--%>
<%@ Register src="modulos/section-destaque.ascx" tagname="destaque" tagprefix="uc6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Portal Vet - App NutriVET</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.maskMoney.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/jquery.limit-1.2.source.js" type="text/javascript"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <%--<script type="text/javascript">

        var tipo = '';
        var especie = '';

        var PorteDoAnimal = 'mini';
        var numero = '6';
        var detalhe = '10% acima do peso';
        var rodape = 'Costelas palpáveis com leve excesso de camada de gordura.<br />Cintura discernível na visão dorsal, mas não proeminente.';

        $(document).ready(function () {

            /*** INICIAL FILTROS ***/
            $('.tipo-de-tratamento > ul > li > a').click(function () {
                $('.tipo-de-tratamento > ul > li > a').removeClass('habilitado');

                tipo = $(this).attr('data-tipo');

                $(this).addClass('habilitado');

                $('.especies').show();

                ExibirFormulario();

                return false;
            });

            $('.especies > ul > li > a').click(function () {
                $('.especies > ul > li > a').removeClass('habilitado');

                especie = $(this).attr('data-especie');

                $(this).addClass('habilitado');

                ExibirFormulario();

                return false;
            });

            ExibirFormulario = function () {
                $('.formulario').hide();

                $('#' + tipo + '-' + especie).show();
            }
            /*** FINAL FILTROS ***/


            /*** INICIO SELECAO ALIMENTO ***/
            $('.produto').click(function () {
                // obtem os id válidos
                var idProduto = $(this).attr('id');
                var idOutro = $(this).attr('data-outro');

                // verificar se ja é marcado
                if ($('#' + idProduto).hasClass('marcado')) {
                    // verificar se ja é marcado
                    if (!$('#' + idProduto).hasClass('habilitado')) {
                        limpar();

                        $('#hdnUmido').val('0');
                        $('#hdnSeco').val('0');
                    }
                }
                else {
                    // verificar se ja é marcado
                    if ($('#' + idProduto).hasClass('habilitado')) {
                        limpar();
                        $('#' + idProduto).addClass('marcado');
                        $('#' + idOutro).addClass('marcado');

                        if ($('#' + idProduto).attr('data-tipo') == 'S') {
                            $('#hdnSeco').val($('#' + idProduto).attr('data-em'));
                            $('#hdnUmido').val($('#' + idOutro).attr('data-em'));
                        }
                        else {

                            $('#hdnSeco').val($('#' + idOutro).attr('data-em'));
                            $('#hdnUmido').val($('#' + idProduto).attr('data-em'));
                        }
                    } else {
                        limpar();

                        $('#' + idProduto).addClass('marcado');

                        if ($('#' + idProduto).attr('data-tipo') == 'S') {
                            $('#hdnSeco').val($('#' + idProduto).attr('data-em'));
                            $('#hdnUmido').val('0');
                        }
                        else {
                            $('#hdnSeco').val('0');
                            $('#hdnUmido').val($('#' + idProduto).attr('data-em'));
                        }

                        if (idOutro != '') {
                            $('#' + idOutro).addClass('habilitado');

                            //$('#hdnUmido').val($('#' + idOutro).attr('data-em'));
                            //$('#hdnSeco').val('');
                        }
                    }
                }

                return false;
            });

            limpar = function () {
                $('.produto').removeClass('marcado');
                $('.produto').removeClass('habilitado');
            }
            /*** FINAL SELECAO ALIMENTO ***/


            $('.porte-do-animal').change(function () {
                PorteDoAnimal = $(this).val();

                setarScoreCorporal();
            })

            $('.score-corporal > ul > li > a').click(function () {
                numero = $(this).attr('data-score');
                detalhe = $(this).attr('data-detalhe');
                rodape = $(this).attr('data-rodape');

                $('#hdnScoreCorporal').val($(this).attr('data-value'));

                setarScoreCorporal();

                return false;
            });

            setarScoreCorporal = function () {
                $('.score-corporal > ul > li > a').removeClass('habilitado');
                $('#score' + numero).addClass('habilitado');

                $('.numero').html(numero + '<br /><span>' + detalhe + '</span>');

                $('.lateral').css({ 'background-image': 'url("layout/imagens/score-corporal/' + numero + '-' + PorteDoAnimal + '-lateral.png")' });

                $('.topo').css({ 'background-image': 'url("layout/imagens/score-corporal/' + numero + '-' + PorteDoAnimal + '-topo.png")' });

                $('.rodape').html(rodape);
            }

            /********************************************************************************
            Ajax para criar sessão com as variaveis
            ********************************************************************************/

            $('.botao').click(function () {
                window.open('aplicativo-imprimir.aspx' +
                    '?pa=' + $('#txtPesoAtual').val() +
                    '&rs=' + $('#hdnSeco').val() +
                    '&ru=' + $('#hdnUmido').val() +
                    '&sc=' + $('#hdnScoreCorporal').val() +
                    '&sx=' + $('#ddlSexoCondicao').val() +
                    '&np=' + $('#txtNomeDoProprietario').val() +
                    '&na=' + $('#txtNomeDoAnimal').val() +
                    '&rc=' + $('#txtRaca').val() +
                    '&ob=' + $('#txtObservacoes').val()
                    );

                return false;
            });
        });
	</script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="principal">
        <uc2:cabecalho ID="cabecalho1" runat="server" />

        <div id="conteudo">
            <section>
                
                <div class="aplicativo">
                    <h1>
                        <div>
                            <span>App</span> NutriVET
                        </div>
                    </h1>
                    <p>
                        Prezado Médico-Veterinário<br />
                        <br />
                        Animais acometidos por algumas doenças ou condições que possam alterar sua fisiologia e metabolismo normais, apresentam necessidades nutricionais específicas e podem beneficiar-se do uso de alimentos coadjuvantes, adequados ao seu estado.<br />
                        <br />
                        Com o intuito de lhe auxiliar na importante missão de levar qualidade de vida e saúde aos pacientes que lhes buscam todos os dias, oferecendo uma prescrição nutricional precisa e completa, desenvolvemos essa ferramenta.<br />
                        <br />
                        Através dela, é possível determinar o alimento mais adequado ao paciente, de acordo com seu quadro clínico, bem como a quantidade correta de alimento diário, com base em suas necessidades e características individuais.<br />
                        <br />
                        Para iniciar selecione uma das opções abaixo. <a href="passo-a-passo.aspx" target="_blank">VEJA AQUI UM PASSO A PASSO</a>.<br />
                        <br />
                    </p>
                    <div id="pnlTipo" runat="server" class="tipo-de-tratamento">
                        <ul>
                            <li><asp:HyperLink ID="lnkObesidade" runat="server" NavigateUrl="aplicativo.aspx?t=o" data-tipo="obesidade">obesidade</asp:HyperLink>
                            <li><asp:HyperLink ID="lnkEnfermidades" runat="server" NavigateUrl="aplicativo.aspx?t=p" data-tipo="obesidade">outras enfermidades</asp:HyperLink>
                        </ul>
                    </div>

                    <div id="pnlEspecies" runat="server" class="especies" visible="false">
                        <ul>
                            <li>
                                <asp:HyperLink ID="lnkCao" runat="server" data-especie="cao" CssClass="cao"><img src="layout/imagens/img-cao-aplicativo.png" /><span>Cão</span></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="lnkGato" runat="server" data-especie="gato" CssClass="gato"><img src="layout/imagens/img-gato-aplicativo.png" /><span>Gato</span></asp:HyperLink>
                            </li>
                        </ul>
                    </div>
                    
                    <div id="pnlObesidadeCao" runat="server" class="formulario" visible="false">
                        <uc7:aplicativo ID="aplicativo1" runat="server" />
                    </div>
                    <div id="pnlObesidadeGato" runat="server" class="formulario" visible="false">
                        <uc8:aplicativo ID="aplicativo2" runat="server" />
                    </div>
                    <div id="pnlPatologiaCao" runat="server" class="formulario" visible="false">
                        <uc9:aplicativo ID="aplicativo3" runat="server" />
                    </div>
                    <div id="pnlPatologiaGato" runat="server" class="formulario" visible="false">
                        <uc10:aplicativo ID="aplicativo4" runat="server" />
                    </div>
                   <%-- <div id="obesidade-cao" class="formulario" style="display:none;">
                        <span class="titulo">Programa de perda de peso</span>
                        <p>Preencha os campos abaixo para efetuar o calculo da quantidade diária de alimento.</p>

                        <div class="completo">
                            <span>
                                Nome do proprietário:<asp:TextBox ID="txtNomeDoProprietario" CssClass="nome-do-proprietario" runat="server"></asp:TextBox>
                            </span>
                        </div>
                        <div class="metade">
                            <span>
                                Nome do animal:<asp:TextBox ID="txtNomeDoAnimal" CssClass="nome-do-animal" runat="server"></asp:TextBox>
                            </span>
                        </div>
                        <div class="metade direito">
                            <span>
                                Raça:<asp:TextBox ID="txtRaca" CssClass="raca" runat="server"></asp:TextBox>
                            </span>
                        </div>
                        


                        <span class="titulo">Condições atuais do animal</span>
                        <div class="metade">
                            <span>
                                Peso atual:<asp:TextBox ID="txtPesoAtual" CssClass="peso-atual" runat="server"></asp:TextBox>
                            </span>
                        </div>
                        <div class="metade direito">
                            <div class="campos">
                                <div class="seletor">
                                    <asp:DropDownList ID="ddlSexoCondicao" runat="server" CssClass="sexo-e-condicao">
                                        <asp:ListItem Text="Sexo / Condição" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Macho intacto" Value="80"></asp:ListItem>
                                        <asp:ListItem Text="Macho castrado" Value="70"></asp:ListItem>
                                        <asp:ListItem Text="Femêa intacta" Value="70"></asp:ListItem>
                                        <asp:ListItem Text="Femêa castrada" Value="60"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="completo">
                            <div class="campos">
                                <div class="seletor">
                                    <asp:DropDownList ID="ddlPorteDoAnimal" runat="server" CssClass="porte-do-animal">
                                        <asp:ListItem Text="Cão de porte mini ( até 4kg)" Value="mini"></asp:ListItem>
                                        <asp:ListItem Text="Cão de pequeno porte ( até 10kg)" Value="pequeno"></asp:ListItem>
                                        <asp:ListItem Text="Cão de porte medio (de 11 a 25kg)" Value="medio"></asp:ListItem>
                                        <asp:ListItem Text="Cão de grande porte (de 26 a 44kg)" Value="grande"></asp:ListItem>
                                        <asp:ListItem Text="Cão de porte gigante (acima de 45kg)" Value="gigante"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        

                        
                        <span class="titulo">Selecione o score corporal do animal</span>
                        
                        <div class="score-corporal">
                            <ul>
                                <li><a href="#" id="score6" data-score="6" data-value="10" data-detalhe="10% acima do peso" data-rodape="Costelas palpáveis com leve excesso de camada de gordura.<br />Cintura discernível na visão dorsal, mas não proeminente.">6<span>10% acima do peso</span></a></li>
                                <li><a href="#" id="score7" data-score="7" data-value="20" data-detalhe="20% acima do peso" data-rodape="Costelas palpáveis com dificuldade pela presença excessiva de gordura.<br />Depósitos notáveis de grodura sobre a região das vértebras lombares e a base da cauda.<br />Cintura ausente ou quase imperceptível.">7<span>20% acima do peso</span></a></li>
                                <li><a href="#" id="score8" data-score="8" data-value="30" data-detalhe="30% acima do peso" data-rodape="Costelas não palpáveis sob camada muito exessiva de gordura ou palpáveis apenas com compressão significativa<br />Depósitos maciços de gordura sobre a região das vértebras lombares e a base da cauda<br />Cintura ausente<br />Possível presença de distensão abdominal evidente">8<span>30% acima do peso</span></a></li>
                                <li><a href="#" id="score9" data-score="9" data-value="40" data-detalhe="40% acima do peso" data-rodape="Depósitos maciços de gordura sobre o tórax, a coluna vertebral e a base da cauda<br />Cintura ausente<br />Depósitos de gordura no pescoço e nos membros<br />Distensão abdominal evidente">9<span>40% acima do peso</span></a></li>
                            </ul>

                            <div class="detalhe"> 
                                <div class="numero">
                                    6
                                    <span>10% acima do peso</span>
                                </div>
                                <div class="lateral" style="background: #f5f5f6 url('layout/imagens/score-corporal/6-mini-lateral.png') no-repeat center center;">
                                    <span>Vista lateral</span>
                                </div>
                                <div class="topo" style="background: #f5f5f6 url('layout/imagens/score-corporal/6-mini-topo.png') no-repeat center center;">
                                    <span>Vista superior</span>
                                </div>
                                <div class="rodape">
                                    Costelas palpáveis com leve excesso de camada de gordura.<br />Cintura discernível na visão dorsal, mas não proeminente.
                                </div>
                                </div>
                        </div>
                        


                        <span class="titulo">Alimento seco</span>
                        <div class="produtos">
                            <a id="6" class="produto" href="produto.aspx?id=6" data-outro="7" data-tipo="S" data-em="1400">
                                <div class="borda">
                                    <div style="background-image:url('arquivos/produtos/6/foto.png');" class="imagem"></div>
                                </div>
                                <div class="texto">
                                    <span>Urinary Feline S/O - High Dilution</span>
                                    <p>Auxilia o tratamento da insuficiência cardíaca e hipertensão</p>
                                </div>
                            </a>

                            <a id="11" class="produto final" href="produto.aspx?id=11" data-outro="" data-tipo="S" data-em="1000">
                                <div class="borda">
                                    <div style="background-image:url('arquivos/produtos/11/foto.png');" class="imagem"></div>
                                </div>
                                <div class="texto">
                                    <span>Cardiac Canine</span>
                                    <p>Auxilia o tratamento da insuficiência cardíaca e hipertensão</p>
                                </div>
                            </a>
                        </div>



                        <span class="titulo">Alimento úmido</span>
                        <div class="produtos">
                            <a id="7" class="produto" href="produto.aspx?id=7" data-outro="6" data-tipo="U" data-em="400">
                                <div class="borda">
                                    <div style="background-image:url('arquivos/produtos/7/foto.png');" class="imagem"></div>
                                </div>
                                <div class="texto">
                                    <span>Urinary Feline S/O - High Dilution</span>
                                    <p>Auxilia o tratamento da insuficiência cardíaca e hipertensão</p>
                                </div>
                            </a>
                        </div>



                        <span class="titulo vermelho">Observações gerais</span>
                        <div class="completo">
                            <asp:TextBox ID="txtObservacoes" CssClass="observacoes" runat="server" TextMode="MultiLine" Rows="6"></asp:TextBox>
                        </div>

                        <asp:Button ID="btnGerarRecomendacao" CssClass="botao" runat="server" 
                            Text="Gerar Recomendação"/>
                    </div>--%>

                </div>
            </section>

            <aside>
                <uc3:prescricao ID="prescricao1" runat="server" />
                <uc4:pesquisar ID="pesquisar1" runat="server" />
                <uc5:menu ID="menu1" runat="server" />
                <%--<uc6:produtos_relacionados ID="produtos_relacionados1" runat="server" />--%>
            </aside>
        </div>

        <uc1:rodape ID="rodape1" runat="server" />
        
    </div>
    </form>
</body>
</html>
