﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="_default" %>

<%@ Register src="modulos/rodape.ascx" tagname="rodape" tagprefix="uc1" %>
<%@ Register src="modulos/cabecalho.ascx" tagname="cabecalho" tagprefix="uc2" %>
<%@ Register src="modulos/aside-app-prescricao.ascx" tagname="prescricao" tagprefix="uc3" %>
<%@ Register src="modulos/aside-pesquisar.ascx" tagname="pesquisar" tagprefix="uc4" %>
<%@ Register src="modulos/aside-menu.ascx" tagname="menu" tagprefix="uc5" %>
<%@ Register src="modulos/section-destaque.ascx" tagname="destaque" tagprefix="uc6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Portal Vet - Home</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#slider1').tinycarousel({
                interval: true,
                intervalTime: 6000,
                bullets  : true,
                buttons: false
            });
        });
	</script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="principal">
       <uc2:cabecalho ID="cabecalho1" runat="server" />
            <%=Vitrine()%>
<%--        <div id="vitrine">
            <div id="slider1">
		        <div class="viewport">
			        <ul class="overview">
				        <li><img src="arquivos/vitrine/vitrine1.jpg" /><a href="#" class="saiba-mais"><img src="layout/imagens/btn-saiba-mais.png" /></a></li>
				        <li><img src="arquivos/vitrine/vitrine2.jpg" /><a href="#" class="saiba-mais"><img src="layout/imagens/btn-saiba-mais.png" /></a></li>
			        </ul>
		        </div>
                <ul class="bullets">
                    <li><a data-slide="0" class="bullet active" href="#"></a></li>
                    <li><a data-slide="1" class="bullet" href="#"></a></li>
                </ul>
	        </div>
        </div>--%>

        <div id="conteudo">
            <section>
                <uc6:destaque ID="destaque1" runat="server" />
                <%--<%=UltimasArtigos()%>--%>

                <div class="ultimas-noticias">
                    <h1>
                        <div>
                            <span>Últimas</span> notícias
                        </div>
                    </h1>
                    <!-- Grid -->
                    <asp:GridView ID="grdwListar" runat="server" ShowHeader="false" PagerSettings-Visible="True" 
                        AutoGenerateColumns="false" AllowPaging="True"  
                        OnRowDataBound="grdwListar_RowDataBound" GridLines="None" CssClass="lista" 
                        onpageindexchanging="grdwListar_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Literal ID="lblRegistro" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings PageButtonCount="20" />
                        <PagerStyle CssClass="paginacao" HorizontalAlign="Right" />
                    </asp:GridView>
                </div>
            </section>
                <%--<!-- Destaques -->
                <div class="destaque artigos-tecnicos" style="background:url('arquivos/destaques/foto.jpg') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                    <div class="over artigos-tecnicos-over"><span>Artigos técnicos</span></div>
                    <div class="texto">
                        <span>CURVA DE CRESCIMENTO</span>
                        <p>De modo geral, o ganho de peso diário (GPD) do cachorro cresce desde o nascimento, estabiliza-se...</p>
                    </div>
                    <a href="#" class="leia-mais"></a>
                </div>
                <div class="destaque ultimas-novidades final" style="background:url('arquivos/destaques/foto.jpg') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                    <div class="over ultimas-novidades-over"><span>Últimas novidades</span></div>
                    <div class="texto">
                        <span>CURVA DE CRESCIMENTO</span>
                        <p>De modo geral, o ganho de peso diário (GPD) do cachorro cresce desde o nascimento, estabiliza-se...</p>
                    </div>
                    <a href="#" class="leia-mais"></a>
                </div>

                <!-- Ultimas noticias -->
                <div class="item artigos-tecnicos">
                    <div class="over artigos-tecnicos-over"><span>Artigos técnicos</span></div>
                    <div class="texto">
                        <span>CURVA DE CRESCIMENTO</span>
                        <p>De modo geral, o ganho de peso diário (GPD) do cachorro cresce desde o nascimento, estabiliza-se e depois diminui à medida que o cão vai se tornando adulto.</p>
                    </div>
                    <a href="#" class="leia-mais artigos-tecnicos">leia mais</a>
                </div>
                <div class="item ultimas-novidades final">
                    <div class="over ultimas-novidades-over"><span>Últimas novidades</span></div>
                    <div class="texto">
                        <span>CURVA DE CRESCIMENTO</span>
                        <p>De modo geral, o ganho de peso diário (GPD) do cachorro cresce desde o nascimento, estabiliza-se e depois diminui à medida que o cão vai se tornando adulto.</p>
                    </div>
                    <a href="#" class="leia-mais ultimas-novidades">leia mais</a>
                </div>
                <div class="item congressos-e-seminarios">
                    <div class="over congressos-e-seminarios-over"><span>Congressos e Seminarios</span></div>
                    <div class="texto">
                        <span>CURVA DE CRESCIMENTO</span>
                        <p>De modo geral, o ganho de peso diário (GPD) do cachorro cresce desde o nascimento, estabiliza-se e depois diminui à medida que o cão vai se tornando adulto.</p>
                    </div>
                    <a href="#" class="leia-mais congressos-e-seminarios">leia mais</a>
                </div>
                <div class="item final videos-e-fotos">
                    <div class="over videos-e-fotos-over"><span>Vídeos e Fotos</span></div>
                    <div class="texto">
                        <span>CURVA DE CRESCIMENTO</span>
                        <p>De modo geral, o ganho de peso diário (GPD) do cachorro cresce desde o nascimento, estabiliza-se e depois diminui à medida que o cão vai se tornando adulto.</p>
                    </div>
                    <a href="#" class="leia-mais videos-e-fotos">leia mais</a>
                </div>--%>
            <aside>
                <uc3:prescricao ID="prescricao1" runat="server" />
                <uc4:pesquisar ID="pesquisar1" runat="server" />
                <uc5:menu ID="menu1" runat="server" />
            </aside>
            
        </div>
        
        <uc1:rodape ID="rodape1" runat="server" />
    </div>
    </form>
</body>
</html>
