﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pesquisar.aspx.cs" Inherits="pesquisar" %>
<%@ Register src="modulos/rodape.ascx" tagname="rodape" tagprefix="uc1" %>
<%@ Register src="modulos/cabecalho.ascx" tagname="cabecalho" tagprefix="uc2" %>
<%@ Register src="modulos/aside-app-prescricao.ascx" tagname="prescricao" tagprefix="uc3" %>
<%@ Register src="modulos/aside-pesquisar.ascx" tagname="pesquisar" tagprefix="uc4" %>
<%@ Register src="modulos/aside-menu.ascx" tagname="menu" tagprefix="uc5" %>
<%--<%@ Register src="modulos/aside-produtos-relacionados.ascx" tagname="aside" tagprefix="uc6" %>--%>
<%@ Register src="modulos/section-destaque.ascx" tagname="destaque" tagprefix="uc6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Portal Vet - Resultado da Busca</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#slider1').tinycarousel({
                interval: true,
                intervalTime: 6000,
                bullets: true,
                buttons: false
            });
        });
	</script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="principal">
        <uc2:cabecalho ID="cabecalho1" runat="server" />

        <div id="conteudo">
            <section>
                
                <%--<uc6:destaque ID="destaque1" runat="server" />--%>

                <div class="pesquisar">
                    <h1>
                        <div>
                            <span>Resultado </span> da busca
                        </div>
                    </h1>
                    
                    <asp:Literal ID="lblMensagem" runat="server"></asp:Literal>

                    <!-- Grid -->
                    <asp:GridView ID="grdwListar" runat="server" ShowHeader="false" PagerSettings-Visible="True" 
                        AutoGenerateColumns="false" AllowPaging="True"  
                        OnRowDataBound="grdwListar_RowDataBound" GridLines="None" CssClass="lista" 
                        onpageindexchanging="grdwListar_PageIndexChanging" Width="98%">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Literal ID="lblRegistro" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings PageButtonCount="20" />
                        <PagerStyle CssClass="paginacao" HorizontalAlign="Right" />
                    </asp:GridView>
                </div>
            </section>

            <aside>
                <uc3:prescricao ID="prescricao1" runat="server" />
                <uc4:pesquisar ID="pesquisar1" runat="server" />
                <uc5:menu ID="menu1" runat="server" />
                <%--<uc6:produtos_relacionados ID="produtos_relacionados1" runat="server" />--%>
            </aside>
        </div>

        <uc1:rodape ID="rodape1" runat="server" />
        
    </div>
    </form>
</body>
</html>
