﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>
<%@ Register src="modulos/rodape.ascx" tagname="rodape" tagprefix="uc1" %>
<%@ Register src="modulos/cabecalho.ascx" tagname="cabecalho" tagprefix="uc2" %>
<%@ Register src="modulos/aside-app-prescricao.ascx" tagname="prescricao" tagprefix="uc3" %>
<%@ Register src="modulos/aside-pesquisar.ascx" tagname="pesquisar" tagprefix="uc4" %>
<%@ Register src="modulos/aside-menu.ascx" tagname="menu" tagprefix="uc5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Portal Vet - Autenticação</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#slider1').tinycarousel({
                interval: true,
                intervalTime: 6000,
                bullets: true,
                buttons: false
            });
        });
	</script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="principal">
        <uc2:cabecalho ID="cabecalho1" runat="server" />
        <div id="conteudo">
            <section style="width:100%;">
                <div class="medico-veterinario" style="padding-right: 0px;">
                    <h1>
                        <div>
                            <span>Médico</span> Veterinário
                        </div>
                    </h1>
                    <div class="fundo">
                        <div class="coluna barra">
                            <div class="campos">
                                <h2>Já é cadastrado</h2>  
                                <span>E-mail:</span> 
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                <br /><br />
                                <span>Senha:</span>  
                                <asp:TextBox ID="txtSenha" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:LinkButton ID="btnRecuperarSenha" runat="server" CssClass="esqueci-minha-senha" OnClick="btnRecuperarSenha_Click">Esqueci minha senha</asp:LinkButton>
                                <asp:LinkButton ID="btnEntrar" runat="server" CssClass="botao" OnClick="btnEntrar_Click1">Acessar os conteúdos</asp:LinkButton>
                            </div>
                        </div>
                        <div class="coluna">
                            <div class="campos">
                                <h2>Não tenho cadastro</h2>
                                Você é um Médico-Veterinário e ainda não possui seu cadastro?<br />
                                <br />
                                Faça agora mesmo e tenha acesso aos nossos conteúdos, além do exclusivo aplicativo para facilitar a prescrição nutricional aos seus pacientes.
                                <asp:HyperLink ID="lnkCadastro" NavigateUrl="~/medico-veterinario.aspx" runat="server" CssClass="botao">Fazer meu cadastro</asp:HyperLink>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <uc1:rodape ID="rodape1" runat="server" />
        
    </div>
    </form>
</body>
</html>

