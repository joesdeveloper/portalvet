﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;

public partial class foto_e_video : System.Web.UI.Page
{
    BLL.Galerias bGal = new BLL.Galerias();

    UTIL.Validacao oVal = new UTIL.Validacao();


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string Galeria()
    {
        var registro = bGal.Consultar(Convert.ToInt32(Request["id"]));

        var html = new StringBuilder();

        html.Append(
                    "<h1>" + registro.Titulo + "</h1>" +
                    "<span class=\"data\">publicado em: " + registro.DataInclusao.ToString("dd/MM/yyyy") + "</span>"
                    );

        html.Append(
                    "<div id=\"vitrine\">" +
                    "	<div id=\"fotos-e-videos-carrossel\">" +
                    "		<div class=\"viewport\">" +
                    "			<ul class=\"overview\">"
                    );

        var i = 0;

        if (registro.Videos != null)
        {
            foreach (var vo in registro.Videos)
            {
                html.Append("				<li><iframe width=\"669\" height=\"373\" src=\"//www.youtube.com/embed/" + oVal.IdVideoYoutube(vo.Url) + "\" frameborder=\"0\" allowfullscreen></iframe></li>");
                i++;
            }
        }

        if (registro.Imagens != null)
        {
            foreach (var vo in registro.Imagens)
            {
                html.Append("				<li><a href=\"\" style=\"background-image:url('" + vo.Url + "');\"></a></li>");
                i++;
            }
        }
        

        html.Append(
                    "			</ul>" +
                    "		</div>" +
                    "		    <ul class=\"bullets\">" 
                    );

        for (var j = 0; j < i; j++)
        {
            if (j == 0)
                html.Append("			    <li><a data-slide=\"0\" class=\"bullet active\" href=\"#\"></a></li>");
            else
                html.Append("			    <li><a data-slide=\"" + j + "\" class=\"bullet\" href=\"#\"></a></li>");
        }

        html.Append(

                    "		    </ul>" +
                    "   </div>" +
                    "</div>"
                    );
        //if (registro.ArtigoImagem != null)
        //    html.Append("<img src=\"" + registro.ArtigoImagem.Url + "\" />");

        html.Append("<div class=\"texto\">" + registro.Descricao + "</div>");


        return html.ToString();
    }
}