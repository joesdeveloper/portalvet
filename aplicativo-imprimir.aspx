﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="aplicativo-imprimir.aspx.cs" Inherits="aplicativo_imprimir" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Portal Vet - App NutriVET - Impressão</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <style>
        @font-face {
          font-family: 'DIN';
          font-style: normal;
          font-weight: 300;
          src: local('DIN Condensed Regular'), local('FF_DIN_Condensed_Regular'), url(layout/fontes/FF_DIN_Condensed_Regular.otf) format('opentype');
        }

        @font-face {
          font-family: 'DIN';
          font-style: normal;
          font-weight: 700;
          src: local('DIN Condensed Bold'), local('FF_DIN_Condensed_Bold'), url(layout/fontes/FF_DIN_Condensed_Bold.otf) format('opentype');
        }
        * { box-sizing: border-box; -moz-box-sizing: border-box; text-align:justify; }
        body { margin: 0; padding: 0; font-family:Sans-Serif; }
        
        .page { position:relative; width: 21cm; min-height:27cm; padding: 1.0cm 1.0cm 0.5cm 1.0cm; margin: 0.5cm auto; border: 1px #D3D3D3 solid; background: #fff; box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); }
        .subpage { position:relative; height: 250mm; outline: 0.5cm #fff solid; }
       
        .opcoes { position:relative; width:100%; margin:10px 0 0 0; text-transform:uppercase;}

        .imprimir { display:block; background:#58a8f3; color:#fff; padding:5px; border:1px solid #417cb3; float:left; }
        .email { display:block; background:#35b672; color:#fff; padding:5px; margin:0 0 0 10px; border:1px solid #2b955d; float:left; }
        
        @page { size:A4; margin: 0; }
        @media print 
        {
            .page { margin: 0; border: initial; border-radius: initial; width: initial; min-height: initial; box-shadow: initial; background: initial; page-break-after: always; }
            .opcoes { display:none; }
        }
    </style>
    <script>
        $(document).ready(function () {

            $('#btnEmail').click(function () {
                var email = prompt("Por favor, digite o endereço de e-mail:", "");
                if (email != null) {
                    $('#hdnEmail').val(email);
                }

                return true;
            });
        });
</script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnEmail" runat="server" />
    <div class="aplicativo-impressao" runat="server" id="impressao">
        <div class="page">
            <div class="subpage" id="pnlImpressao" runat="server">
                <%--<%Exibir();%>--%>
                <%--<div class="informacao">
                    <div class="completo">Nome do proprietário:<span>Samir Maioral Silverio</span></div>
                    <div class="meio">Nome do animal:<span>Thor</span></div>
                    <div class="meio direito">Raça:<span>Labrador</span></div>
                    <div class="meio">Peso atual:<span>80kg</span></div>
                    <div class="meio direito">Peso pretendido:<span>30kg</span></div>

                    <div class="meio">Qtd de energia recomendada:<span>2000kcal</span></div>
                    <div class="meio direito">Perda de peso por semana:<span>3kg</span></div>
                </div>

                <div class="informacao">
                    <h2>Prescrição:</h2>

                    <div class="completo">
                        Para o tratamento utilizar a alimentação seguindo as especificações descritas abaixo:<br />
                        <br />
                        Royal Canin Obsesity Canine<br />
                        - Quantidade diária: 191g<br />
                        <br />
                        Royal Canin Obesity Management Wet<br />
                        - Quantidade diária: 497g <br />
                    </div>

                    <h2>Observações:</h2>
                    <div class="completo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consectetur odio a sem feugiat sollicitudin. Vivamus non leo ligula. Praesent sit amet tortor sed tortor aliquet efficitur quis et arcu. Maecenas iaculis ultrices lacinia. Duis quis porttitor massa, vitae volutpat dolor. Pellentesque placerat nisi nulla. Nullam dictum commodo feugiat. Integer suscipit dignissim bibendum. <br />
                        <br />
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consectetur odio a sem feugiat sollicitudin. Vivamus non leo ligula. Praesent sit amet tortor sed tortor aliquet efficitur quis et arcu. Maecenas iaculis ultrices lacinia. Duis quis porttitor massa, vitae volutpat dolor. Pellentesque placerat nisi nulla. Nullam dictum commodo feugiat. Integer suscipit dignissim bibendum. 
                    </div>
                </div>
                <div class="rodape">
                    <h2>Observações gerais:</h2>
                    <div class="nota">
                        1) Dividir a quantidade diária em pelo menos 3 refeições<br />
                        2) O Cão ou Gato em um regime para perda de peso deverá ser pesado semanalmente pelo proprietário para a fim de monitorar a perda de peso do animal. <br />
                        3) Cães devem perder cerca de 1 % a 2% do seu peso atual por semana a fim de garantir o sucesso do tratamento.<br />
                        4) Caso o animal não esteja perdendo essa quantidade de peso por semana a quantidade de alimento deverá ser ajustada (Aumentada ou Reduzida).<br />
                        5) É normal e esperado que as quantidades de alimentos tenham que ser períodicamente ajustadas e por isso é imperativo o acompanhamento<br />
                        do Médico-Veterinário durante todo o processo de perda de peso para maximizar as chances de sucesso.
                    </div>

                    <div class="meio">
                        <div class="endereco">
                            Consultório XYZ<br />
                            Rua John Sample, 333 - Bairro Sample<br />
                            CEP 03333-333 - São Paulo - SP<br />
                            (11)4444-4444 / consultorio@site.com.br
                        </div>
                    </div>
                    <div class="meio direito"><div class="nome">Nome do veterinário</div></div>
                  

                </div>--%>
            </div>

            <div class="opcoes">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnImprimir" CssClass="imprimir" runat="server" 
                                Text="IMPRIMIR" OnClientClick="window.print();" OnClick="btnImprimir_Click" />
                            <asp:Button ID="btnEmail" CssClass="email" runat="server" 
                                Text="ENVIAR POR E-MAIL" onclick="btnEmail_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>

<%--        <div class="page">
            <div class="subpage">Page 2/2</div>    
        </div>--%>
    </div>
    </form>
</body>
</html>
