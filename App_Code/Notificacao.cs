﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class Notificacao
{
    private static int tipo;
    private static bool erro;
    private static string texto;

    public enum Tipo : byte { Sucesso, Informacao, Atencao, Erro }

    public static bool Erro
    {
        get { return erro; }
        set { erro = value; }
    }

    public static Tipo Status
    {
        get { return (Tipo)tipo; }
        set { tipo = (int)value; }
    }

    public static string Texto
    {
        get { return texto; }
        set { texto = value; }
    }

    public static string Exibir()
    {
        var retorno = "";

        var titulo = "";
        var cor = "";

        switch (tipo)
        {
            case 0:
                titulo = "Sucesso";
                cor = "#60c05f";
                break;
            case 1:
                titulo = "Informação";
                cor = "#5bc0de";
                break;
            case 2:
                titulo = "Atenção";
                cor = "#ff9900";
                break;
            case 3:
                titulo = "Erro";
                cor = "erro";
                break;
            default:
                titulo = "Informação";
                cor = "#e04b33";
                break;
        }

        retorno =
            "       $(function() {\n" +
            "           $('#notificacao').html('<span>" + titulo + "<a href=\"#\">&times;</a></span>" + texto + "');\n" +
            "        	$('#notificacao').css('background', '" + cor + "');" +
            "        	$('#notificacao').show();" +
            "        	window.setTimeout('notificar()', 8000);" +
            "        	$('#notificacao > span > a').click(function() {" +
            "               $('#notificacao').fadeOut();" +
            "           });" +
            "       });" +
            "       function notificar() { " +
            "           $('#notificacao').fadeOut(); " +
            "       }\n"
            ;


        return retorno;
    }
}

