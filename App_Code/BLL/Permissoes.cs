﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Permissoes
    {
        DAL.Permissoes dal = new DAL.Permissoes();

        #region Consultas
        
        public List<VO.Permissao> Listar()
        {
            return dal.Listar();
        }

        #endregion

    }
}
