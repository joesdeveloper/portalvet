﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Proprietarios
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Proprietarios dal = new DAL.Proprietarios();

        #region Validação

        private bool ValidarDados(VO.Proprietario vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome não pode ser nulo.<br />";

            if (vo.Email.Trim().Length == 0)
                erro += @"- Email não pode ser nulo.<br />";
            else
            {
                if (!oVal.Email(vo.Email))
                    erro += @"- Email inválido.<br />";
            }

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.Proprietario Consultar(System.Guid id)
        {
            return dal.Consultar(id);
        }

        public VO.Proprietario Consultar(string email)
        {
            return dal.Consultar(email);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }        
        public List<VO.Proprietario> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Proprietario Salvar(VO.Proprietario vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdProprietario == Guid.Empty)
            {
                vo.IdProprietario = Guid.NewGuid();

                // Verifica se email já é cadastrado
                var voAux = dal.Consultar(vo.Email);

                if (voAux == null)
                    dal.Incluir(vo);
                else
                {
                    Notificacao.Erro = true;
                    Notificacao.Status = Notificacao.Tipo.Atencao;
                    Notificacao.Texto = "- Este e-mail já cadastrado";
                }
            }
            else
            {
                var voAux = dal.Consultar(vo.IdProprietario);
                if (voAux == null)
                {
                    Notificacao.Erro = true;
                    Notificacao.Status = Notificacao.Tipo.Atencao;
                    Notificacao.Texto = "- Usuário não cadastrado.";
                }
                else
                {
                    // Verifica se o email do administração não foi alterado
                    if (voAux.Email.Equals(vo.Email))
                        dal.Alterar(vo);
                    else
                    {
                        // Verfica se o email novo já é cadastrado
                        voAux = dal.Consultar(vo.Email);
                        if (voAux == null)
                            dal.Alterar(vo);
                        else
                        {
                            Notificacao.Erro = true;
                            Notificacao.Status = Notificacao.Tipo.Atencao;
                            Notificacao.Texto = "- Este e-mail já cadastrado";
                        }
                    }
                }
            }

            return vo;
        }


        public void Excluir(System.Guid id)
        {
            dal.Excluir(id);
        }

        #endregion
    }
}
