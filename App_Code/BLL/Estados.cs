﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Estados
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Estados dal = new DAL.Estados();

        #region Consultas


        public List<VO.Estado> Listar()
        {
            return dal.Listar();
        }

       
        #endregion

    }
}
