﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Doencas
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Doencas dal = new DAL.Doencas();
        DAL.ProdutosCategorias dProCat = new DAL.ProdutosCategorias();

        #region Validação

        private bool ValidarDados(VO.Doenca vo, List<int> idCategoria)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome não pode ser nulo.<br />";

            if (idCategoria.Count == 0)
                erro += @"- Selecione pelo menos uma categoria.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.Doenca Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public VO.Doenca Consultar(string nome)
        {
            return dal.Consultar(nome);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }
        
        public List<VO.Doenca> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        public List<VO.Doenca> ListarPorProduto(int idProduto)
        {
            return dal.ListarPorProduto(idProduto);
        }

        public List<VO.Doenca> ListarPorProdutosExistentes(int idEspecie)
        {
            return dal.ListarPorProdutosExistentes(idEspecie);
        }


        public List<VO.Doenca> ListarPorCategoria(int idCategoria)
        {
            return dal.ListarPorCategoria(idCategoria);
        }
        
        #endregion



        #region Inclusao, Alteração e Exclusão

        public VO.Doenca Salvar(VO.Doenca vo, List<int> idCategorias, List<int> idProdutos)
        {
            if (!ValidarDados(vo, idCategorias))
                return null;

            if (vo.IdDoenca == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            // Salva as categorias relacionadas
            SalvarCategorias(vo.IdDoenca, idCategorias);

            // Salva as produtos relacionadas
            SalvarPorProduto(vo.IdDoenca, idProdutos);

            return vo;
        }

        public void Excluir(int id)
        {
            // Exclui Categoria Relaciona a Doenca
            ExcluirPorDoenca(id);

            // Exclui Doenca
            dal.Excluir(id);
        }

        #endregion



        #region Doencas Por Produtos
        public void Salvar(int idProduto, List<int> idDoencas)
        {
            dal.ExcluirPorProduto(idProduto);

            for (var i = 0; i < idDoencas.Count; i++)
            {
                dal.IncluirPorProduto(idProduto, idDoencas[i]);
            }
        }

        private void SalvarPorProduto(int idDoenca, List<int> idProdutos)
        {
            dal.ExcluirPorDoenca(idDoenca);
            
            for (var i = 0; i < idProdutos.Count; i++)
            {
                dal.IncluirPorProduto(idProdutos[i], idDoenca);
            }
        }
        
        public List<VO.Produto> ListarProdutosPorDoenca(int idDoenca)
        {
            var lista = new BLL.Produtos().Listar();

            lista = (from aux in lista
                            where aux.Ativo == true && aux.Doenca != null
                            orderby aux.Nome ascending
                            select aux).ToList();

            var listaAux = lista.Where(x => x.Doenca.Any(d => d.IdDoenca == idDoenca)).ToList();

            return listaAux;

        }
        #endregion


        #region Doencas Por Categoria
        private void SalvarCategorias(int idDoenca, List<int> idCategoria)
        {
            ExcluirPorDoenca(idDoenca);

            for (var i = 0; i < idCategoria.Count; i++)
            {
                dProCat.IncluirPorDoenca(idDoenca, idCategoria[i]);
            }
        }


        public List<VO.ProdutoCategoria> ListarPorDoenca(int idDoenca)
        {
            return dProCat.ListarPorDoenca(idDoenca);
        }


        private void ExcluirPorDoenca(int idDoenca)
        {
            dProCat.ExcluirPorDoenca(idDoenca);
        }
        #endregion
    }
}
