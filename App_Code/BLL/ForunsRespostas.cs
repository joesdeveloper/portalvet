﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class ForunsRespostas
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.ForunsRespostas dal = new DAL.ForunsRespostas();

        #region Validação

        private bool ValidarDados(VO.ForumResposta vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Texto.Trim().Length == 0)
                erro += @"- Texto não pode ser nulo.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.ForumResposta Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa, int idForum)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa, idForum);

            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa, int idForum)
        {
            return dal.TotalDeRegistros(pesquisa, idForum);
        }

        public List<VO.ForumResposta> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente, int idPergunta)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente, idPergunta);
        }

        #endregion

        #region Inclusao, Alteração e Exclusão

        public VO.ForumResposta Incluir(VO.ForumResposta vo)
        {
            if (!ValidarDados(vo))
                return null;

            vo = dal.Incluir(vo);

            return vo;
        }

        public VO.ForumResposta Salvar(VO.ForumResposta vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdResposta == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }


        public void Excluir(int id)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            dal.Excluir(id);
        }

        #endregion

    }
}
