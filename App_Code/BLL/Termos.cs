﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Termos
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Termos dal = new DAL.Termos();

        #region Validação

        private bool ValidarDados(VO.Termo vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome não pode ser nulo.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.Termo Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public VO.Termo Consultar(string nome)
        {
            return dal.Consultar(nome);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }
        
        public List<VO.Termo> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion



        #region Inclusao, Alteração e Exclusão

        public VO.Termo Salvar(VO.Termo vo)
        {
            if (!ValidarDados(vo))
                return null;

            var voAux = Consultar(vo.Nome);

            if (vo.IdTermo == 0)
            {
                if (voAux == null)
                    vo = dal.Incluir(vo);
                else
                {
                    Notificacao.Erro = true;
                    Notificacao.Status = Notificacao.Tipo.Atencao;
                    Notificacao.Texto = "- Este termo já cadastrado";
                }
            }
            else
            {
                if (voAux == null)
                    dal.Alterar(vo);
                else
                {
                    if (voAux.IdTermo == vo.IdTermo)
                        dal.Alterar(vo);
                    else
                    {
                        Notificacao.Erro = true;
                        Notificacao.Status = Notificacao.Tipo.Atencao;
                        Notificacao.Texto = "- Este termo já cadastrado";
                    }
                }
            }

            return vo;
        }

        public void AlterarTotal(int id)
        {
            dal.AlterarTotal(id);
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion


        #region Termos Por Artigo
        
        public List<VO.Termo> ListarPorArtigo(int idArtigo)
        {
            return dal.ListarPorArtigo(idArtigo);
        }


        public void SalvarPorArtigo(int idArtigo, List<int> idTermos)
        {
            ExcluirPorArtigo(idArtigo);

            for (var i = 0; i < idTermos.Count; i++)
            {
                dal.IncluirPorArtigo(idArtigo, idTermos[i]);
            }
        }

        public void ExcluirPorArtigo(int idArtigo)
        {
            dal.ExcluirPorArtigo(idArtigo);
        }

        #endregion





        #region Termos Por Produto

        public List<VO.Termo> ListarPorProduto(int idProduto)
        {
            return dal.ListarPorProduto(idProduto);
        }

        public void SalvarPorProduto(int idProduto, List<int> idTermos)
        {
            ExcluirPorProduto(idProduto);

            for (var i = 0; i < idTermos.Count; i++)
            {
                dal.IncluirPorProduto(idProduto, idTermos[i]);
            }
        }

        public void ExcluirPorProduto(int idProduto)
        {
            dal.ExcluirPorProduto(idProduto);
        }

        #endregion
    }
}
