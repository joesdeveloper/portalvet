﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Busca
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Busca dal = new DAL.Busca();

        #region Consultas

        //public int TotalDePaginas(int totalPorPagina, string pesquisa)
        //{
        //    int totaldeRegistros = TotalDeRegistros(pesquisa);

        //    decimal totalPaginas = totaldeRegistros / totalPorPagina;

        //    if ((totaldeRegistros % totalPorPagina) == 0)
        //        return Convert.ToInt32(totalPaginas);
        //    else
        //    {
        //        string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
        //        return Convert.ToInt32(totalPaginasAux[0]) + 1;
        //    }
        //}

        //public int TotalDeRegistros(string pesquisa)
        //{
        //    return dal.TotalDeRegistros(pesquisa);
        //}

        public List<VO.Busca> Pesquisar(int pagina, int totalPorPagina, string pesquisa)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa);
        }

        public List<VO.Busca> UltimasNoticias(int totalPorPagina)
        {
            return dal.UltimasNoticias(totalPorPagina);
        }

        #endregion
    }
}
