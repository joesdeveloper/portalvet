﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Vitrines
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Vitrines dal = new DAL.Vitrines();

        #region Validação

        private bool ValidarDados(VO.Vitrine vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Titulo.Trim().Length == 0)
                erro += @"- Titulo não pode ser nulo.<br />";

            if (vo.DataPublicacao.ToString() == DateTime.MaxValue.ToString())
                erro += @"- Data Publicação é inválida.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion
        
        #region Consultas

        public VO.Vitrine Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }

        public List<VO.Vitrine> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        public List<VO.Vitrine> ListarVitrinesHome()
        {
            return dal.Listar_Vitrines_Home();
        }
        

        #endregion

        

        #region Inclusao, Alteração e Exclusão

        public VO.Vitrine Salvar(VO.Vitrine vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdVitrine == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion

    }
}
