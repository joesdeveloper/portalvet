﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Especies
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Especies dal = new DAL.Especies();

        #region Consultas

        public VO.Especie Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public VO.Especie Consultar(string nome)
        {
            return dal.Consultar(nome);
        }


        public List<VO.Especie> Listar()
        {
            return dal.Listar();
        }

        #endregion

    }
}
