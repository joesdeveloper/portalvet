﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Autores
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Autores dal = new DAL.Autores();

        #region Validação

        private bool ValidarDados(VO.Autor vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";


            if (vo.Assinatura.Trim().Length == 0)
                erro += @"- Assinatura não pode ser nulo.<br />";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome não pode ser nulo.<br />";

            if (vo.Email.Trim().Length > 0)
            {
                if (!oVal.Email(vo.Email))
                    erro += @"- E-mail inválido.<br />";
            }

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.Autor Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }
        
        public List<VO.Autor> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion

        #region Inclusao, Alteração e Exclusão

        public VO.Autor Salvar(VO.Autor vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdAutor == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion

    }
}
