﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Produtos
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Produtos dal = new DAL.Produtos();
        DAL.ProdutosCategorias dProCat = new DAL.ProdutosCategorias();

        #region Validação

        private bool ValidarDados(VO.Produto vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome não pode ser nulo.<br />";

            /*  if (vo.Embalagem.Trim().Length == 0)
                erro += @"- Tamanho da embalagem não pode ser nula.<br />";*/

            if (vo.Cor.Trim().Length == 0)
                erro += @"- Cor Padrão não pode ser nula.<br />";

            //if (vo.FotoDoProduto.IdImagem == 0)
            //    erro += @"- Foto do Produto não pode ser nula.<br />";

            if (vo.EnergiaMetabolizavel == 0)
                erro += @"- Energia Metabolizável não pode ser nula.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.Produto Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public VO.Produto Consultar(string nome)
        {
            return dal.Consultar(nome);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);

            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }

        public List<VO.Produto> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        public List<VO.Produto> Listar()
        {
            return dal.Listar();
        }

        public List<VO.Produto> Listar(int idEspecie)
        {
            return dal.Listar(idEspecie);
        }

        #endregion



        #region Inclusao, Alteração e Exclusão

        public VO.Produto Salvar(VO.Produto vo)
        {
            if (!ValidarDados(vo))
                return null;

            var voAux = Consultar(vo.Nome);

            if (vo.IdProduto == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            //SalvarCategorias(vo.IdProduto, vo.Categorias);

            return vo;
        }


        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion


        #region Produtos Tipos

        public List<VO.ProdutoTipo> ListarTipo()
        {
            return new DAL.ProdutosTipos().Listar();
        }

        #endregion




        #region Categorias

        #region Validacao

        private bool ValidarDados(VO.ProdutoCategoria vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";
            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome não pode ser nulo.<br />";

            if (vo.Classe.Trim().Length == 0)
                erro += @"- Classe não pode ser nula.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        public VO.ProdutoCategoria ConsultarCategoria(int IdCategoria)
        {
            return dProCat.Consultar(IdCategoria);
        }

        public int TotalDePaginasCategorias(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);

            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistrosCategorias(string pesquisa)
        {
            return dProCat.TotalDeRegistros(pesquisa);
        }

        public List<VO.ProdutoCategoria> PesquisarCategorias(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dProCat.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        public List<VO.ProdutoCategoria> ListarCategorias()
        {
            return dProCat.Listar();
        }


        public VO.ProdutoCategoria SalvarCategorias(VO.ProdutoCategoria vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdCategoria == 0)
                vo = dProCat.Incluir(vo);
            else
                dProCat.Alterar(vo);

            return vo;
            
        }

        public void SalvarCategorias(int idProduto, List<int> idCategoria)
        {
            dProCat.ExcluirPorProduto(idProduto);

            for (var i = 0; i < idCategoria.Count; i++)
            {
                dProCat.IncluirPorProduto(idProduto, idCategoria[i]);
            }
        }


        public void ExcluirCategoria(int id)
        {
            dProCat.Excluir(id);
        }

        #endregion




    }
}
