﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Representantes
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Representantes dal = new DAL.Representantes();


        #region Validação

        private bool ValidarDados(VO.Representante vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome do Representante não pode ser nulo.<br />";

            if (vo.Email1.Trim().Length != 0)
            {
                if (!oVal.Email(vo.Email1))
                    erro += @"- E-mail 1 inválido.<br />";
            }

            if (vo.Email2.Trim().Length != 0)
            {
                if (!oVal.Email(vo.Email2))
                    erro += @"- E-mail 2 inválido.<br />";
            }

            if (vo.Endereco.Trim().Length == 0)
                erro += @"- Endereço não pode ser nulo.<br />";

            if (vo.Numero.Trim().Length == 0)
                erro += @"- Número não pode ser nulo.<br />";

            if (vo.Bairro.Trim().Length == 0)
                erro += @"- Bairro não pode ser nulo.<br />";

            if (vo.Cidade.Trim().Length == 0)
                erro += @"- Cidade não pode ser nula.<br />";

            if (vo.Estado.Trim().Length == 0)
                erro += @"- Estado não pode ser nulo.<br />";

            if (vo.Cep.Trim().Length == 0)
                erro += @"- Cep não pode ser nulo.<br />";
            else
            {
                if (!oVal.CEP(vo.Cep))
                    erro += @"- Cep inválido.<br />";
            }

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion


        #region Consultas

        public VO.Representante Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }

        public List<VO.Representante> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        public List<VO.Representante> Listar(string Estado)
        {
            return dal.Listar(Estado);
        }

        public List<VO.Estado> Estados()
        {
            return new DAL.Estados().ListarRepresentates();
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Representante Salvar(VO.Representante vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdRepresentante == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }


        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion

    }
}
