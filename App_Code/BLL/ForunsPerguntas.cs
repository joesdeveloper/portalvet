﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class ForunsPerguntas
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.ForunsPerguntas dal = new DAL.ForunsPerguntas();

        #region Validação

        private bool ValidarDados(VO.ForumPergunta vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Titulo.Trim().Length == 0)
                erro += @"- Titulo não pode ser nulo.<br />";

            if (vo.Texto.Trim().Length == 0)
                erro += @"- Texto não pode ser nulo.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.ForumPergunta Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa, int idForum)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa, idForum);

            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa, int idForum)
        {
            return dal.TotalDeRegistros(pesquisa, idForum);
        }

        public List<VO.ForumPergunta> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente, int idForum)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente, idForum);
        }

        #endregion

        #region Inclusao, Alteração e Exclusão

        public VO.ForumPergunta Incluir(VO.ForumPergunta vo)
        {
            if (!ValidarDados(vo))
                return null;

            vo = dal.Incluir(vo);

            return vo;
        }

        public VO.ForumPergunta Salvar(VO.ForumPergunta vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdPergunta == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }


        public void Excluir(int id)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            new DAL.ForunsRespostas().ExcluirPorPergunta(id);
            dal.Excluir(id);
        }

        #endregion

    }
}
