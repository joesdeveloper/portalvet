﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Administradores
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Administradores dal = new DAL.Administradores();

        #region Validação

        private bool ValidarIdAdministrador(Guid valor)
        {
            if (valor.ToString() == "00000000-0000-0000-0000-000000000000")
                throw new Exception("Id do administrador inválido.");

            return true;
        }

        private void ValidarNome(string valor)
        {
            if (valor == null || valor.Equals(""))
                throw new Exception("Nome não pode ser nulo.");
        }

        private void ValidarEmail(string valor)
        {
            if (valor == null || valor.Equals(""))
                throw new Exception("Email não pode ser nulo.");
            else
            {
                if (!oVal.Email(valor))
                    throw new Exception("Email inválido.");
            }
        }

        private void ValidarSenha(string valor)
        {
            if (valor == null || valor.Equals(""))
                throw new Exception("Senha não pode ser nula.");
            else
            {
                if (!oVal.Senha(valor, 6, 20))
                    throw new Exception("A senha deve ter em 6 a 20 caracteres.");
            }
        }

        #endregion


        #region Consultas

        public VO.Administrador Consultar(System.Guid id)
        {
            return dal.Consultar(id);
        }

        public VO.Administrador Consultar(string email)
        {
            return dal.Consultar(email);
        }

        public VO.Administrador Autenticar(string email, string senha)
        {
            ValidarEmail(email);
            ValidarSenha(senha);

            VO.Administrador vo = dal.Consultar(email);

            if (vo == null)
                return null;

            if (!vo.Senha.Equals(senha))
                return null;

            if(!vo.Ativo)
                return null;

            return vo;
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }
        
        public List<VO.Administrador> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public Guid Incluir(VO.Administrador vo)
        {
            ValidarNome(vo.Nome);
            ValidarEmail(vo.Email);
            ValidarSenha(vo.Senha);

            // Gera novo id para o administrador
            vo.IdAdministrador = Guid.NewGuid();

            // Verifica se email já é cadastrado
            var voAux = dal.Consultar(vo.Email);

            if (voAux == null)
            {
                dal.Incluir(vo);
                return vo.IdAdministrador;
            }
            else
                throw new Exception("Email já cadastrado.");
        }

        public void Alterar(VO.Administrador vo)
        {
            // Verifica se id do administrador é cadastrado
            var voAux = dal.Consultar(vo.IdAdministrador);
            if (voAux == null)
                throw new Exception("Usuário não cadastrado.");
            else
            {
                // Verifica se o email do administração não foi alterado
                if (voAux.Email.Equals(vo.Email))
                    dal.Alterar(vo);
                else
                {
                    // Verfica se o email novo já é cadastrado
                    voAux = dal.Consultar(vo.Email);
                    if (voAux == null)
                        dal.Alterar(vo);
                    else
                        throw new Exception("Email já cadastrado.");
                }
            }
        }

        public void Excluir(System.Guid id)
        {
            new DAL.PermissoesPorAdministradores().Excluir(id);
            dal.Excluir(id);
        }

        public bool AlterarSenha(VO.Administrador vo)
        {
            ValidarSenha(vo.Senha);
            ValidarIdAdministrador(vo.IdAdministrador);

            // Verfica se o administrador existe
            var oAdm = new DAL.Administradores();
            var voAux = oAdm.Consultar(vo.IdAdministrador);

            if (voAux == null)
                throw new Exception("Administrador não localizado.");
            else
            {
                oAdm.AlterarSenha(vo);
                return true;
            }

            return false;
        }

        public void RegistrarAcesso(Guid id)
        {
            dal.RegistrarAcesso(id);
        }

        #endregion
    }
}
