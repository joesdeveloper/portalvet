﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Eventos
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Eventos dal = new DAL.Eventos();


        #region Validação

        private bool ValidarDados(VO.Evento vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome do Evento não pode ser nulo.<br />";

            if (vo.Descricao.Trim().Length == 0)
                erro += @"- Descrição não pode ser nulo.<br />";

            if (vo.Local.Trim().Length == 0)
                erro += @"- Local não pode ser nulo.<br />";

            if (vo.Endereco.Trim().Length == 0)
                erro += @"- Endereço não pode ser nulo.<br />";

            if (vo.Numero.Trim().Length == 0)
                erro += @"- Número não pode ser nulo.<br />";

            if (vo.Bairro.Trim().Length == 0)
                erro += @"- Bairro não pode ser nulo.<br />";

            if (vo.Cidade.Trim().Length == 0)
                erro += @"- Cidade não pode ser nula.<br />";

            if (vo.Estado.Trim().Length == 0)
                erro += @"- Estado não pode ser nulo.<br />";

            if (vo.Cep.Trim().Length == 0)
                erro += @"- Cep não pode ser nulo.<br />";
            else
            {
                if (!oVal.CEP(vo.Cep))
                    erro += @"- Cep inválido.<br />";
            }

            if (vo.DataEventoInicial == DateTime.MinValue)
                erro += @"- Data Inicial Evento não pode ser nulo.<br />";

            if (vo.DataEventoFinal == DateTime.MinValue)
                erro += @"- Data Final do Evento não pode ser nulo.<br />";

            if (vo.DataEventoInicial > vo.DataEventoFinal)
                erro += @"- Data Inicial do Evento não pode ser maior que a Data Final.<br />";

            if (vo.DataPublicacao.ToString() == DateTime.MaxValue.ToString())
                erro += @"- Data Publicação é inválida.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion


        #region Consultas

        public VO.Evento Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }

        public List<VO.Evento> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Evento Salvar(VO.Evento vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdEvento == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }


        public void RemoverImagem(int id)
        {
            dal.RemoverImagem(id);
        }


        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion

    }
}
