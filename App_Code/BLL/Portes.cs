﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Portes
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Portes dal = new DAL.Portes();

        #region Consultas

        public VO.Porte Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public List<VO.Porte> Listar()
        {
            return dal.Listar();
        }

        #endregion

    }
}
