﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Animais
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Animais dal = new DAL.Animais();
        DAL.Racas dRac = new DAL.Racas();
        DAL.Especies dEsp = new DAL.Especies();

        #region Animais

        #region Validação

        private bool ValidarDados(VO.Animal vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome não pode ser nulo.<br />";

            if (vo.Genero.Trim().Length == 0)
                erro += @"- Gênero não pode ser nulo.<br />";

            //if (vo.Raca.Especie.IdEspecie == 0)
            //    erro += @"- Especies não pode ser nulo.<br />";

            if (vo.Raca == null)
                erro += @"- Raça não pode ser nulo.<br />";
            else
            {
                if (vo.Raca.IdRaca == 0)
                    erro += @"- Raça não pode ser nulo.<br />";
            }


            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.Animal Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public List<VO.Animal> Listar(int idProprietario)
        {
            return dal.Listar(idProprietario);
        }


        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);

            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }
        public List<VO.Animal> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion

        #region Inclusao, Alteração e Exclusão

        public VO.Animal Salvar(VO.Animal vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdAnimal == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion

        #endregion


        #region Racas

        public VO.Raca ConsultarRacas(int id)
        {
            return dRac.Consultar(id);
        }

        //public VO.Raca ConsultarRacas(string nome)
        //{
        //    return dRac.Consultar(nome);
        //}

        public List<VO.Raca> ListarRacas()
        {
            return dRac.Listar();
        }

        #endregion

        #region Especies

        public VO.Especie ConsultarEspecie(int id)
        {
            return dEsp.Consultar(id);
        }

        public VO.Especie ConsultarEspecie(string nome)
        {
            return dEsp.Consultar(nome);
        }

        public List<VO.Especie> ListarEspecie()
        {
            return dEsp.Listar();
        }

        #endregion
    }
}
