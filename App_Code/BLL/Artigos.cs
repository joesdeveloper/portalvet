﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Artigos
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Artigos dal = new DAL.Artigos();
        DAL.DownloadsCategorias dDowCat = new DAL.DownloadsCategorias();

        #region Artigos
        #region Consultas

        public VO.Artigo Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }

        public List<VO.Artigo> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        public List<VO.Artigo> ListarArtigos(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar_Artigos(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }
        

        public List<VO.Artigo> ListarArquivos(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar_Arquivos(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        public List<VO.Artigo> ListarArtigosHome()
        {
            return dal.Listar_Artigos_Home();
        }

        public List<VO.Artigo> ListarArtigoRelacionados(int idProduto)
        {
            return dal.ListarArtigoRelacionados(idProduto);
        }

        public List<VO.Artigo> ListarDownloadsRelacionados(int idProduto)
        {
            return dal.ListarDownloadsRelacionados(idProduto);
        }
        
        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Artigo Salvar(VO.Artigo vo)
        {
            if (vo.IdArtigo == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }

        public void RemoverImagem(int id)
        {
            dal.RemoverImagem(id);
        }

        public void Excluir(int id)
        {
            new DAL.ArquivosPorArtigos().ExcluirPorArtigo(id);
            new Termos().ExcluirPorArtigo(id);

            dal.Excluir(id);
        }

        #endregion
        #endregion



        #region Produtos Tipos

        public List<VO.ArtigoTipo> ListarTipo()
        {
            return new DAL.ArtigosTipos().Listar();
        }

        #endregion



        #region Arquivos Por Artigo

        public void Salvar(VO.ArquivoPorArtigo vo)
        {
            new DAL.ArquivosPorArtigos().Incluir(vo);
        }

        public void ExcluirArquivo(int IdArquivo)
        {
            new DAL.ArquivosPorArtigos().Excluir(IdArquivo);
        }

        public List<VO.ArquivoPorArtigo> ListarArquivos(int IdArtigo, string colunaOrdenacao, bool ascendente)
        {
            return new DAL.ArquivosPorArtigos().ListarPorArtigo(IdArtigo, colunaOrdenacao, ascendente);
        }

        #endregion



        #region Arquivos Por Downloads

        public List<VO.DownloadCategoria> ListarCategorias()
        {
            return dDowCat.Listar();
        }

        #endregion
    }
}
