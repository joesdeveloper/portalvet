﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Galerias
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Galerias dal = new DAL.Galerias();

        #region Galerias
        #region Validação

        private bool ValidarDados(VO.Galeria vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Titulo.Trim().Length == 0)
                erro += @"- Titulo não pode ser nulo.<br />";

            if (vo.Descricao.Trim().Length == 0)
                erro += @"- Descrição não pode ser nulo.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion


        #region Consultas

        public VO.Galeria Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }

        public List<VO.Galeria> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Galeria Salvar(VO.Galeria vo)
        {
            if (vo.IdGaleria == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion
        #endregion



        #region Imagens Por Galeria

        public void Salvar(VO.ImagemPorGaleria vo)
        {
            new DAL.ImagensPorGalerias().Incluir(vo);
        }

        public void ExcluirImagem(int IdImagem)
        {
            new DAL.ImagensPorGalerias().Excluir(IdImagem);
        }

        public List<VO.ImagemPorGaleria> ListarImagens(int IdGaleria, string colunaOrdenacao, bool ascendente)
        {
            return new DAL.ImagensPorGalerias().ListarPorGaleria(IdGaleria, colunaOrdenacao, ascendente);
        }

        #endregion




        #region Videos Por Galeria

        private bool ValidarDados(VO.VideoPorGaleria vo)
        {
            var erro = "";

            if (vo.Titulo.Trim().Length == 0)
                erro += @"- Titulo não pode ser nulo.<br />";

            if (!oVal.Youtube(vo.Url))
                erro += @"- Url do Youtube inválida.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        public void Salvar(VO.VideoPorGaleria vo)
        {
            if (vo.Url.Trim().Length > 0)
            {
                if (ValidarDados(vo))
                    new DAL.VideosPorGalerias().Incluir(vo);
            }
        }

        public void ExcluirVideo(int IdVideo)
        {
            new DAL.VideosPorGalerias().Excluir(IdVideo);
        }

        public List<VO.VideoPorGaleria> ListarVideos(int IdGaleria, string colunaOrdenacao, bool ascendente)
        {
            return new DAL.VideosPorGalerias().ListarPorGaleria(IdGaleria, colunaOrdenacao, ascendente);
        }

        #endregion

    }
}
