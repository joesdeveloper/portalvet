﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class PerguntasRespostas
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.PerguntasRespostas dal = new DAL.PerguntasRespostas();

        #region Perguntas e Respostas
        #region Validacao

        private bool ValidarDados(VO.PerguntaResposta vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";
            if (vo.Pergunta.Trim().Length == 0)
                erro += @"- Pergunta não pode ser nula.<br />";

            if (vo.Resposta.Trim().Length == 0)
                erro += @"- Resposta não pode ser nula.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.PerguntaResposta Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);
            
            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[]{'.'});
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }
        
        public List<VO.PerguntaResposta> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion

        #region Inclusao, Alteração e Exclusão

        public VO.PerguntaResposta Salvar(VO.PerguntaResposta vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdPerguntaResposta == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }


        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion
        #endregion



        #region Categorias

        public List<VO.PerguntaRespostaCategoria> ListarCategorias()
        {
            return new DAL.PerguntasRespostasCategorias().Listar();
        }

        #endregion
    }
}
