﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Racas
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Racas dal = new DAL.Racas();

        #region Consultas

        public VO.Raca Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public List<VO.Raca> Listar()
        {
            return dal.Listar();
        }

        public List<VO.Raca> Listar(int IdEspecie)
        {
            return dal.Listar(IdEspecie);
        }


        #endregion

    }
}
