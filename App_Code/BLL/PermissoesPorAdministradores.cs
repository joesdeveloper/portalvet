﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class PermissoesPorAdministradores
    {
        DAL.PermissoesPorAdministradores dal = new DAL.PermissoesPorAdministradores();

        #region Consultas

        public VO.PermissaoPorAdministrador Consultar(System.Guid id)
        {
            return dal.Consultar(id);
        }


        public List<VO.PermissaoPorAdministrador> ListarPorAdministrador(Guid IdAdministrador)
        {
            return dal.ListarPorAdministrador(IdAdministrador);
        }


        public List<VO.PermissaoPorAdministrador> Listar()
        {
            return dal.Listar();
        }

        #endregion



        #region Inclusao, Alteração e Exclusão

        public void Salvar(VO.PermissaoPorAdministrador vo)
        {
            // Verifica se email já é cadastrado
            var voAux = dal.Consultar(vo.IdAdministrador);

            if (voAux == null)
                dal.Incluir(vo);
            else
                dal.Alterar(vo);
        }

        #endregion

    }
}
