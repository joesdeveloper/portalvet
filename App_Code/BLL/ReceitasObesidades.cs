﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;

using System.Web;

namespace BLL
{
    public class ReceitasObesidades
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.ReceitasObesidades dal = new DAL.ReceitasObesidades();

        #region Validação

        private bool ValidarDados(VO.ReceitaObesidade vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.userToken.Trim().Length == 0)
                erro += @"- Usuário não autenticado.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion


        #region Consultas

        public VO.ReceitaObesidade Consultar(int id)
        {
            return dal.Consultar(id);
        }


        public int TotalDePaginas(int totalPorPagina, string pesquisa, string idTipoDeRelatorio, int idEspecie, int idProduto, DateTime dataIncial, DateTime dataFinal)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa, idTipoDeRelatorio, idEspecie, idProduto, dataIncial, dataFinal);

            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa, string idTipoDeRelatorio, int idEspecie, int idProduto, DateTime dataIncial, DateTime dataFinal)
        {
            return dal.TotalDeRegistros(pesquisa, idTipoDeRelatorio, idEspecie, idProduto, dataIncial, dataFinal);
        }

        public List<VO.ReceitaObesidade> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente, string idTipoDeRelatorio, int idEspecie, int idProduto, DateTime dataIncial, DateTime dataFinal)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente, idTipoDeRelatorio, idEspecie, idProduto, dataIncial, dataFinal);
        }


        public DataTable Exportacao()
        {
            return dal.Exportacao();
        }
        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.ReceitaObesidade Salvar(VO.ReceitaObesidade vo)
        {
            var voAux = dal.Incluir(vo);

            var cookieInfo = HttpContext.Current.Response.Cookies["MedicoVeterinarioReceita"];
            cookieInfo["id"] = voAux.IdReceita.ToString();
            HttpContext.Current.Response.Cookies.Add(cookieInfo);

            return voAux;
        }

        public void SalvarImpresso(int idReceita, bool status)
        {
            dal.AlterarImpresso(idReceita, status);
        }

        public void SalvarEmail(int idReceita, bool status, string email)
        {
            dal.AlterarEmail(idReceita, status, email);
        }
        

        #endregion

    }
}
