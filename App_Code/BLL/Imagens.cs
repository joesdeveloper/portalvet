﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Imagens
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Imagens dal = new DAL.Imagens();

        #region Validação

        private bool ValidarDados(VO.Imagem vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Url.Trim().Length == 0)
                erro += @"- Url não pode ser nula.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.Imagem Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public VO.Imagem Consultar(string titulo)
        {
            return dal.Consultar(titulo);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);

            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }

        public List<VO.Imagem> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        public List<VO.Imagem> Listar()
        {
            return dal.Listar();
        }

       

        #endregion



        #region Inclusao, Alteração e Exclusão

        public VO.Imagem Salvar(VO.Imagem vo)
        {
            if (!ValidarDados(vo))
                return null;

            if (vo.IdImagem == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);


            return vo;
        }


        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        #endregion

    }
}
