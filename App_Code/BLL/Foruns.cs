﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Foruns
    {
        UTIL.Validacao oVal = new UTIL.Validacao();
        DAL.Foruns dal = new DAL.Foruns();

        #region Validação

        private bool ValidarDados(VO.Forum vo)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            var erro = "";

            if (vo.Nome.Trim().Length == 0)
                erro += @"- Nome não pode ser nulo.<br />";

            if (vo.Descricao.Trim().Length == 0)
                erro += @"- Descrição não pode ser nula.<br />";

            if (erro.Length > 0)
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = erro;

                return false;
            }

            return true;
        }

        #endregion

        #region Consultas

        public VO.Forum Consultar(int id)
        {
            return dal.Consultar(id);
        }

        public VO.Forum Consultar(string nome)
        {
            return dal.Consultar(nome);
        }

        public int TotalDePaginas(int totalPorPagina, string pesquisa)
        {
            int totaldeRegistros = TotalDeRegistros(pesquisa);

            decimal totalPaginas = totaldeRegistros / totalPorPagina;

            if ((totaldeRegistros % totalPorPagina) == 0)
                return Convert.ToInt32(totalPaginas);
            else
            {
                string[] totalPaginasAux = totalPaginas.ToString().Split(new char[] { '.' });
                return Convert.ToInt32(totalPaginasAux[0]) + 1;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            return dal.TotalDeRegistros(pesquisa);
        }

        public List<VO.Forum> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            return dal.Pesquisar(pagina, totalPorPagina, pesquisa, colunaOrdenacao, ascendente);
        }

        #endregion

        #region Inclusao, Alteração e Exclusão

        public VO.Forum Salvar(VO.Forum vo)
        {
            if (!ValidarDados(vo))
                return null;

            var voAux = Consultar(vo.Nome);

            vo.UrlAmigavel = oVal.UrlAmigavel(vo.Nome);

            if (vo.IdForum == 0)
                vo = dal.Incluir(vo);
            else
                dal.Alterar(vo);

            return vo;
        }

        public void Excluir(int id)
        {
            Notificacao.Erro = false;
            Notificacao.Status = Notificacao.Tipo.Sucesso;
            Notificacao.Texto = "Registro salvo com sucesso";

            try
            {
                dal.Excluir(id);
            }
            catch
            {
                Notificacao.Erro = true;
                Notificacao.Status = Notificacao.Tipo.Atencao;
                Notificacao.Texto = "O Forum não pode ser EXCLUIDO, pois existem Temas relacionados.";
            }
        }

        #endregion
    }
}
