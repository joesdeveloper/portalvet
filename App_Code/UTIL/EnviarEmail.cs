using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Net.Mail;
using System.IO;

namespace UTIL
{
    public class EnviarEmail
    {
        public EnviarEmail(string Para, string Assunto, string Corpo, bool Html)
        {
            MailMessage oMail = new MailMessage();

            oMail.Subject = Assunto;
            oMail.To.Add(Para);
            oMail.IsBodyHtml = Html;
            oMail.Body = Corpo;
            oMail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
            oMail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

            System.Net.Mail.SmtpClient oSmtp = new System.Net.Mail.SmtpClient();
            oSmtp.Send(oMail);

            oSmtp = null;
            oMail.Dispose();
            oMail = null;
        }


        public EnviarEmail(string Para, string De, string Assunto, string Corpo, bool Html)
        {
            MailMessage oMail = new MailMessage();

            oMail.Subject = Assunto;
            oMail.From = new MailAddress(De);
            oMail.Sender = new MailAddress(De);
            oMail.To.Add(Para);
            oMail.IsBodyHtml = Html;
            oMail.Body = Corpo;
            oMail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
            oMail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

            var oSmtp = new System.Net.Mail.SmtpClient("localhost");
            oSmtp.Send(oMail);

            oSmtp = null;
            oMail.Dispose();
            oMail = null;
        }
    }
}