﻿using System;
using System.Data;
using System.Configuration;

using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace UTIL
{
    public class EdicaoDeImagem
    {
        // declares the enum
        public enum Formato : byte
        {
            Jpeg,
            Bmp,
            Png,
            Gif,
            Tiff
        }

        public EdicaoDeImagem() { }

        #region Resize

        public System.Drawing.Image Redimencionar(string caminhoOrigem, int largura, int altura)
        {
            int widthOriginal = largura;
            int heightOriginal = altura;

            using (System.Drawing.Image imgToResize = System.Drawing.Image.FromFile(caminhoOrigem))
            {
                decimal x = Convert.ToDecimal(imgToResize.Height) / Convert.ToDecimal(imgToResize.Width);
                decimal y = Convert.ToDecimal(1);

                if (x > y)
                {
                    if (imgToResize.Height > altura)
                        largura = altura * imgToResize.Size.Width / imgToResize.Size.Height;
                    else
                    {
                        largura = imgToResize.Width;
                        altura = imgToResize.Height;
                    }

                    if (largura > widthOriginal)
                    {
                        largura = widthOriginal;
                        altura = largura * imgToResize.Size.Height / imgToResize.Size.Width;
                    }
                }
                else
                {
                    if (imgToResize.Width > largura)
                        altura = largura * imgToResize.Size.Height / imgToResize.Size.Width;
                    else
                    {
                        largura = imgToResize.Width;
                        altura = imgToResize.Height;
                    }

                    if (altura > heightOriginal)
                    {
                        altura = heightOriginal;
                        largura = altura * imgToResize.Size.Width / imgToResize.Size.Height;
                    }
                }


                Bitmap bmp = new Bitmap(largura, altura);
                Graphics g = Graphics.FromImage(bmp);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                g.DrawImage(imgToResize, 0, 0, largura, altura);

                imgToResize.Dispose();
                return (System.Drawing.Image)(bmp);
            }
        }

        public void Redimencionar(string caminhoOrigem, string caminhoDestino, int largura, int altura, Formato formato)
        {
            int widthOriginal = largura;
            int heightOriginal = altura;

            using (System.Drawing.Image imgToResize = System.Drawing.Image.FromFile(caminhoOrigem))
            {

                decimal x = Convert.ToDecimal(imgToResize.Height) / Convert.ToDecimal(imgToResize.Width);
                decimal y = Convert.ToDecimal(1);

                if (x > y)
                {
                    if (imgToResize.Height > altura)
                        largura = altura * imgToResize.Size.Width / imgToResize.Size.Height;
                    else
                    {
                        largura = imgToResize.Width;
                        altura = imgToResize.Height;
                    }

                    if (largura > widthOriginal)
                    {
                        largura = widthOriginal;
                        altura = largura * imgToResize.Size.Height / imgToResize.Size.Width;
                    }

                }
                else
                {
                    if (imgToResize.Width > largura)
                        altura = largura * imgToResize.Size.Height / imgToResize.Size.Width;
                    else
                    {
                        largura = imgToResize.Width;
                        altura = imgToResize.Height;
                    }

                    if (altura > heightOriginal)
                    {
                        altura = heightOriginal;
                        largura = altura * imgToResize.Size.Width / imgToResize.Size.Height;
                    }
                }

                Bitmap bmp = new Bitmap(largura, altura);
                Graphics g = Graphics.FromImage(bmp);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                g.DrawImage(imgToResize, 0, 0, largura, altura);
                bmp.Save(caminhoDestino, MapeamentoFormato(formato));

                bmp.Dispose();
                imgToResize.Dispose();
            }
        }

        public System.Drawing.Image Redimencionar(System.Drawing.Image imagem, int largura, int altura)
        {
            Bitmap bmp = new Bitmap(largura, altura);
            Graphics g = Graphics.FromImage(bmp);
            g.DrawImage(imagem, 0, 0, largura, altura);
            return (System.Drawing.Image)(bmp);
        }

        #endregion


        #region Convert

        public void Converter(string caminhoOrigem, string caminhoDestino, Formato formato)
        {
            using (System.Drawing.Image imgToResize = System.Drawing.Image.FromFile(caminhoOrigem))
            {
                int widthOriginal = imgToResize.Width;
                int heightOriginal = imgToResize.Height;

                Bitmap bmp = new Bitmap(widthOriginal, heightOriginal);
                Graphics g = Graphics.FromImage(bmp);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                g.DrawImage(imgToResize, 0, 0, widthOriginal, heightOriginal);
                bmp.Save(caminhoDestino, MapeamentoFormato(formato));

                bmp.Dispose();
                imgToResize.Dispose();
            }
        }

        #endregion


        #region Crop

        public System.Drawing.Image Cortar(System.Drawing.Image imagem, Rectangle area)
        {
            using (Bitmap bmp = new Bitmap(imagem))
            {
                Bitmap bmpCrop = bmp.Clone(area, bmp.PixelFormat);
                return (System.Drawing.Image)(bmpCrop);
            }
        }

        public System.Drawing.Image Cortar(string caminhoOrigem, Rectangle area)
        {
            using(System.Drawing.Image imgToCrop = System.Drawing.Image.FromFile(caminhoOrigem)){
                Bitmap bmp = new Bitmap(imgToCrop);
                Bitmap bmpCrop = bmp.Clone(area, bmp.PixelFormat);
                return (System.Drawing.Image)(bmpCrop);
            }
        }

        public void Cortar(string caminhoOrigem, string caminhoDestino, Rectangle area, Formato formato)
        {
            using (System.Drawing.Image imgToCrop = System.Drawing.Image.FromFile(caminhoOrigem))
            {
                Bitmap bmp = new Bitmap(imgToCrop);
                Bitmap bmpCrop = bmp.Clone(area, bmp.PixelFormat);
                bmpCrop.Save(caminhoDestino, MapeamentoFormato(formato));
            }
        }

        #endregion



        #region Draw

        public void Desenhar(string caminhoOrigem, string caminhoDestino, string caminhoMarcaDagua, int x, int y, Formato formato)
        {
            using (System.Drawing.Image imgToDraw = System.Drawing.Image.FromFile(caminhoOrigem))
            {
                using (System.Drawing.Image imgLayer = System.Drawing.Image.FromFile(caminhoMarcaDagua))
                {
                    Bitmap bmp = new Bitmap(imgToDraw.Width, imgToDraw.Height);
                    Graphics g = Graphics.FromImage(bmp);
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

                    g.DrawImage(imgLayer, x, y, imgLayer.Width, imgLayer.Height);

                    bmp.Save(caminhoDestino, MapeamentoFormato(formato));

                    g.Dispose();
                    bmp.Dispose();
                    imgToDraw.Dispose();
                    imgLayer.Dispose();
                }
            }
        }

        public void Desenhar(System.Drawing.Image imagemOriginal, string caminhoDestino, string caminhoMarcaDagua, int x, int y, Formato formato)
        {
            using (System.Drawing.Image imgToDraw = imagemOriginal)
            {
                using (System.Drawing.Image imgLayer = System.Drawing.Image.FromFile(caminhoMarcaDagua))
                {
                    Bitmap bmp = new Bitmap(imgToDraw.Width, imgToDraw.Height);
                    Graphics g = Graphics.FromImage(bmp);

                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

                    g.DrawImage(imgToDraw, 0, 0, imgToDraw.Width, imgToDraw.Height);
                    g.DrawImage(imgLayer, x, y, imgLayer.Width, imgLayer.Height);

                    bmp.Save(caminhoDestino, MapeamentoFormato(formato));

                    g.Dispose();
                    bmp.Dispose();
                    imgToDraw.Dispose();
                    imgLayer.Dispose();
                }
            }
        }

        #endregion


        #region MapeamentoEnum
        private System.Drawing.Imaging.ImageFormat MapeamentoFormato(Formato item)
        {
            System.Drawing.Imaging.ImageFormat formato = null;

            switch (item)
            {
                case Formato.Bmp:
                    formato = System.Drawing.Imaging.ImageFormat.Bmp;
                    break;

                case Formato.Gif:
                    formato = System.Drawing.Imaging.ImageFormat.Gif;
                    break;

                case Formato.Jpeg:
                    formato = System.Drawing.Imaging.ImageFormat.Jpeg;
                    break;

                case Formato.Png:
                    formato = System.Drawing.Imaging.ImageFormat.Png;
                    break;

                case Formato.Tiff:
                    formato = System.Drawing.Imaging.ImageFormat.Tiff;
                    break;
            }

            return formato;
        }
        #endregion
    }
}
