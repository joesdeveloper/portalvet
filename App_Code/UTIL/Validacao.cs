 using System;
using System.Web;
 using System.Globalization;
 using System.Text.RegularExpressions;

namespace UTIL
{
    public class Validacao
    {
        public Validacao() { }

        public string Pagina(string valor)
        {
            var pagina = "";

            switch (valor.ToLower())
            {
                case "artigos":
                    pagina = "artigo.aspx";
                    break;

                case "eventos":
                    pagina = "congresso-e-seminario.aspx";
                    break;

                case "arquivos":
                    pagina = "artigo.aspx";
                    break;

                case "galerias":
                    pagina = "foto-e-video.aspx";
                    break;
            }

            return pagina;
        }

        public bool Imagem(string valor)
        {
            Regex oRegex = new Regex(@"(.*?)\.(jpg|jpeg|png|gif|bmp|tiff|tif)$");
            return oRegex.IsMatch(valor.ToLower());
        }

        public bool Email(string valor)
        {
            Regex oRegex = new Regex(@"^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Z a-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-z A-Z]{2,6}$");
            return oRegex.IsMatch(valor);
        }

        public string Html(string valor)
        {
            Regex oRegex = new Regex("<(.|\n)+?>");
            valor = oRegex.Replace(valor, String.Empty).TrimStart().TrimEnd();

            valor = RemoverEspaco(valor);

            return valor;
        }

        public string RemoverEspaco(string valor)
        {
            Regex oRegex = new Regex(@"\s{2,}");
            return oRegex.Replace(valor, " ").TrimStart().TrimEnd();
        }
        
        public string Substring(string valor, int total)
        {
            if (valor.Length > total)
                valor = valor.Substring(0, total) + "...";

            valor = RemoverEspaco(valor);

            return valor;
        }

        public string UrlBase()
        {
            return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath + "/";
        }

        public string UrlAmigavel(string valor)
        {
            valor = CaracteresEspeciais(valor);
            valor = Acentuacao(valor);
            valor = valor.Replace(" ", "-").ToLower();

            return valor;
        }

        public bool Data(string valor)
        {
            try
            {
                Convert.ToDateTime(valor);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public string Data(DateTime valor, CultureInfo cultura)
        {
            DateTimeFormatInfo dtfi = cultura.DateTimeFormat;

            int dia = valor.Day;
            int ano = valor.Year;
            string mes = cultura.TextInfo.ToTitleCase(dtfi.GetMonthName(valor.Month));
            string diasemana = cultura.TextInfo.ToTitleCase(dtfi.GetDayName(valor.DayOfWeek));
            string data = diasemana + ", " + dia + " de " + mes + " de " + ano;

            return data;
        }

        public string Data(DateTime dataInicio, DateTime dataAtual, CultureInfo cultura)
        {
            var retorno = "";
            DateTimeFormatInfo dtfi = cultura.DateTimeFormat;

            TimeSpan dif = dataAtual.Subtract(dataInicio);

            if (dif.TotalDays > 1)
                retorno = dataAtual.Day + " " + cultura.TextInfo.ToTitleCase(dtfi.GetMonthName(dataAtual.Month)) + " " + dataAtual.Year;
            else
            {
                if (dif.Hours >= 2)
                    retorno = dif.Hours + " horas atras";
                else
                {
                    if (dif.Hours == 1)
                        retorno = dif.Hours + " hora atra";
                    else
                        retorno = dif.Minutes + " minutos atras";
                }
            }
            

            return retorno;
        }

        public bool Senha(string valor, int minimo, int maximo)
        {
            Regex oRegex = new Regex("^([a-zA-Z0-9]{" + minimo + "," + maximo + "})$");
            return oRegex.IsMatch(valor);
        }

        public bool Numero(string valor, int minimo, int maximo)
        {
            Regex oRegex = new Regex("^([0-9]{" + minimo + "," + maximo + "})$");
            return oRegex.IsMatch(valor);
        }

        public bool CEP(string valor)
        {
            Regex oRegex = new Regex("^([0-9]{8,8})$");
            return oRegex.IsMatch(valor);
        }

        public bool Estado(string valor)
        {
            if (valor == "AC" || valor == "AL" || valor == "AP" || valor == "AM" || valor == "BA" || valor == "CE" || valor == "DF" || valor == "ES" || valor == "GO" || valor == "MT" || valor == "MS" || valor == "MA" || valor == "MG" || valor == "PB" || valor == "PA" || valor == "PR" || valor == "PE" || valor == "PI" || valor == "RJ" || valor == "RN" || valor == "RS" || valor == "RO" || valor == "RR" || valor == "SC" || valor == "SP" || valor == "SE" || valor == "TO")
                return true;

            return false;
        }

        public string Acentuacao(string valor)
        {
           valor = System.Web.HttpUtility.UrlEncode(valor, System.Text.Encoding.GetEncoding(28597)).Replace("+", " ");
           valor = MultiplosEspacos(valor);
            return valor;
        }

        public string MultiplosEspacos(string valor)
        {
            var regex = new Regex(@"\s{2,}");
            valor = regex.Replace(valor, " ");
            return valor;
        }

        public string CaracteresEspeciais(string valor)
        {
            string[] acentos = new string[] { "!", "@", "#", "$", "%", "�", "&", "*", "(", ")", "�", "�", "`", "{", "[", "�", "]", "}", "�", "^", "~", ";", ":", "?", @"/", "<", ">", "�", "�", "�", "�", "�", "�", "'", "\"", "|" };
            for (int i = 0; i < acentos.Length; i++)
            {
                valor = valor.Replace(acentos[i], "");
            }
            valor = MultiplosEspacos(valor);
            return valor;
        }


        public bool Pontos(string pontos)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[12] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3 };
            int[] multiplicador3 = new int[12] { 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5, 6 };

            decimal soma1 = 0;
            decimal soma2 = 0;
            decimal soma3 = 0;
            decimal digito1 = 0;
            decimal digito2 = 0;
            decimal digito3 = 0;
            string pontosAux = pontos.Substring(0, pontos.Length - 3);
            string digito = pontos.Substring(pontos.Length - 4, 4);

            for (int i = 0; i < pontosAux.Length; i++)
                soma1 += int.Parse(pontosAux[i].ToString()) * multiplicador1[i];
            digito1 = (soma1 % 11m);
            if (digito1 > 9)
                digito1 = Convert.ToInt32(digito1.ToString().Substring(0, 1));

            for (int i = 0; i < pontosAux.Length; i++)
                soma2 += int.Parse(pontosAux[i].ToString()) * multiplicador2[i];
            digito2 = (soma2 % 11);
            if (digito2 > 9)
                digito2 = Convert.ToInt32(digito2.ToString().Substring(0, 1));


            for (int i = 0; i < pontosAux.Length; i++)
                soma3 += int.Parse(pontosAux[i].ToString()) * multiplicador3[i];
            digito3 = (soma3 % 11);
            if (digito3 > 9)
                digito3 = Convert.ToInt32(digito3.ToString().Substring(0, 1));

            return pontos.EndsWith(digito1 + "" + digito2 + "" + digito3);
        }


        public string GetPontos(string pontos)
        {
            // Remove digitos iniciais
            string pontosAux = pontos.Remove(0, 4);

            // Remove digitos finais
            pontosAux = pontosAux.Remove(pontosAux.Length - 4, 4);

            return pontosAux;
        }


        public bool CPF(string cpf)
        {
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");

            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
                return false;

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            if (cpf.Length != 11)
                return false;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;
            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCpf = tempCpf + digito;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }


        




        public bool CNPJ(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;

            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");

            if (cnpj.Length != 14)
                return false;

            tempCnpj = cnpj.Substring(0, 12);

            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cnpj.EndsWith(digito);
        }



        /*public string (string url)
        {
            string[] aux1 = url.Split(new char[] {'v='});
            string aux2 = "";
            aux2 = aux1[0].Replace("https", "");
            aux2 = aux2.Replace("http", "");
            aux2 = aux2.Replace(":", "");
            aux2 = aux2.Replace(@"/", "");
            aux2 = aux2.Replace(".", "");
            aux2 = aux2.Replace("www", "");
            aux2 = aux2.Replace("youtube","");
            aux2 = aux2.Replace("com", "");
            aux2 = aux2.Replace("watch", "");
            aux2 = aux2.Replace("?v=", "");

            return aux2;
        }*/

        public bool Youtube(string valor)
        {
            if (valor.ToLower().LastIndexOf("youtube") > 0)
                return true;

            return false;
        }

        public string IdVideoYoutube(string url)
        {
            if (url.Trim().Length > 0)
            {
                int divisor = url.LastIndexOf("v=");
                string aux = url.Remove(0, divisor + 2);

                divisor = aux.IndexOf("&");

                if (divisor > 0)
                    aux = aux.Substring(0, divisor);

                aux = aux.Replace("watch?v=", "");
                return aux;
            }
            else
                return "";
        }


        public bool Vimeo(string valor)
        {
            if (valor.ToLower().LastIndexOf("vimeo") > 0)
                return true;

            return false;

        }

        public string IdVideoVimeo(string url)
        {
                return url.Remove(0, url.LastIndexOf("/") + 1);
        }

        public string Telefone_Formatar(string valor)
        {
            if (valor.Trim().Length > 0)
                return Convert.ToInt64(valor).ToString("(##) ####-####");
            
            return "";
        }

        public string Celular_Formatar(string valor)
        {
            if (valor.Trim().Length > 0)
                return Convert.ToInt64(valor).ToString("(##) #####-####");

            return "";
        }

        public string CEP_Formatar(string valor)
        {
            if (valor.Trim().Length > 0)
                return Convert.ToInt64(valor).ToString("#####-###");

            return "";
        }

    }
}
