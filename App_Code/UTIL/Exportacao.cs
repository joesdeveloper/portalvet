﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Reflection;
using System.Collections.Generic;
using System.Globalization;

namespace UTIL
{
    public class Exportacao
    {
        public Exportacao() { }

        CultureInfo oCultura = new CultureInfo("pt-BR");


        public void CSV<T>(List<T> lista, string nomeDoArquivo)
        {
            var data = ToDataTable(lista);
            
            CSV(data, nomeDoArquivo);
        }


        public void CSV(DataTable oDt, string nomeDoArquivo)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + nomeDoArquivo);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

            for (int i = 0; i < oDt.Columns.Count; i++)
            {
                HttpContext.Current.Response.Write("\"" + oDt.Columns[i] + "\";");
            }

            HttpContext.Current.Response.Write("\r");

            foreach (DataRow oDw in oDt.Rows)
            {
                for (int i = 0; i < oDt.Columns.Count; i++)
                {
                    switch (oDw[i].GetType().ToString())
                    {
                        case "System.Int32":
                            HttpContext.Current.Response.Write("\"" + Convert.ToInt32(oDw[i]) + "\";");
                            break;

                        case "System.String":
                            HttpContext.Current.Response.Write("\"" + oDw[i].ToString().Replace("\n", "").Replace("\r", "").Replace("\t", "") + "\";");
                            break;

                        case "System.DateTime":
                            HttpContext.Current.Response.Write("\"" + Convert.ToDateTime(oDw[i], oCultura).ToString("yyyy-MM-dd HH:mm:ss") + "\";");
                            break;

                        case "System.DBNull":
                            HttpContext.Current.Response.Write("\"" + "NULL\";");
                            break;

                        case "System.Boolean":
                            HttpContext.Current.Response.Write("\"" + Convert.ToBoolean(oDw[i]) + "\";");
                            break;

                        default:
                            HttpContext.Current.Response.Write("\"" + oDw[i] + "\";");
                            break;

                    }
                }

                HttpContext.Current.Response.Write("\r");
            }

            HttpContext.Current.Response.End();
        }



        private DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            
            return dataTable;
        }
    }
}