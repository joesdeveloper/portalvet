﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Imagens
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Imagem Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Imagem vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Imagens] " +
                    "   WHERE " +
                    "       IdImagem=@IdImagem"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdImagem", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public VO.Imagem Consultar(string titulo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Imagem vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Imagens] " +
                    "   WHERE " +
                    "       titulo=@titulo"
                    , oConn);

                oComm.Parameters.AddWithValue("@titulo", titulo);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Imagens] " +
                    "   WHERE " +
                    "                       Titulo like @pesquisa OR " +
                    "                       Url like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Imagem> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Imagem> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM[Imagens] " +
                    "	WHERE IdImagem IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdImagem FROM[Imagens] " +
                    "		    WHERE IdImagem NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdImagem FROM[Imagens] " +
                    "				    WHERE " +
                    "                       Titulo like @pesquisa OR " +
                    "                       Url like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "                       Titulo like @pesquisa OR " +
                    "                       Url like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public List<VO.Imagem> Listar()
        {
            List<VO.Imagem> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Imagens] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Imagem Incluir(VO.Imagem vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Imagens] " +
                    "       ( " +
                    "           Titulo, " +
                    "           Url " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @Titulo, " +
                    "           @Url " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Url", vo.Url);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdImagem = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Imagem vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Imagens] " +
                    "   SET " +
                    "       Titulo=@Titulo " +
                    "   WHERE " +
                    "       IdImagem=@IdImagem"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdImagem", vo.IdImagem);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Url", vo.Url);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }


        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Imagens] " +
                    "   WHERE " +
                    "       IdImagem=@IdImagem"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdImagem", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Imagem Popular(DataRow row)
        {
            var vo = new VO.Imagem();

            vo.IdImagem = (int)row["IdImagem"];
            vo.Titulo = (string)row["Titulo"];
            vo.Url = (string)row["Url"];
            vo.DataInclusao = (DateTime)row["dataInclusao"];

            return vo;
        }

        private List<VO.Imagem> PopularLista(DataTable table)
        {
            var lista = new List<VO.Imagem>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
