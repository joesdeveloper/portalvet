﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Artigos
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Artigo Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Artigo vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Artigos] " +
                    "   WHERE " + 
                    "       IdArtigo=@IdArtigo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Artigos] " +
                    "   WHERE " +
                    "       Titulo like @pesquisa OR " +
                    "		Texto like @pesquisa OR " +
                    "		CONVERT(varchar,dataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Artigo> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Artigo> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Artigos] " +
                    "	WHERE IdArtigo IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdArtigo FROM [Artigos] " +
                    "		    WHERE IdArtigo NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdArtigo FROM [Artigos] " +
                    "				    WHERE " +

                    "					    Titulo like @pesquisa OR " +
                    "						Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "               Titulo like @pesquisa OR " +
                    "				Texto like @pesquisa OR " +
                    "				CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public List<VO.Artigo> Pesquisar_Artigos(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Artigo> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Artigos] " +
                    "	WHERE IdArtigo IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdArtigo FROM [Artigos] " +
                    "		    WHERE IdArtigo NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdArtigo FROM [Artigos] " +
                    "				    WHERE " +
                    "				        (IdTipo=1 OR IdTipo=6) AND " +
                    "				        ( " +
                    "					    Titulo like @pesquisa OR " +
                    "						Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "				        ) " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "				        (IdTipo=1 OR IdTipo=6) AND " +
                    "				        ( " +
                    "					    Titulo like @pesquisa OR " +
                    "						Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "				        ) " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public List<VO.Artigo> Pesquisar_Arquivos(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Artigo> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Artigos] " +
                    "	WHERE IdArtigo IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdArtigo FROM [Artigos] " +
                    "		    WHERE IdArtigo NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdArtigo FROM [Artigos] " +
                    "				    WHERE " +
                    "				        (IdTipo=3 OR IdTipo=6) AND " +
                    "					    IdArtigo IN (SELECT IdArtigo FROM [ArquivosPorArtigos]) AND " +
					"					    ( " +
                    "					        Titulo like @pesquisa OR " +
                    "						    Texto like @pesquisa OR " +
                    "						    CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "					    ) " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "				        (IdTipo=3 OR IdTipo=6) AND " +
                    "					    IdArtigo IN (SELECT IdArtigo FROM [ArquivosPorArtigos]) AND " +
                    "					    ( " +
                    "					        Titulo like @pesquisa OR " +
                    "						    Texto like @pesquisa OR " +
                    "						    CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "					    ) " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public List<VO.Artigo> Listar_Artigos_Home()
        {
            List<VO.Artigo> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "( " +
                    "	SELECT * FROM ( " +
                    "		SELECT TOP 3 * FROM [Artigos] " +
                    "			WHERE " +
                    "				Ativo='true' AND " +
                    "				Destaque='true' AND " +
                    "				DataPublicacao < GETDATE() " +
                    "			ORDER BY DataPublicacao DESC " +
                    "			) AS auxDestaque " +
                    ") " +
                    "UNION " +
                    "( " +
                    "	SELECT * FROM ( " +
                    "		SELECT TOP 4 * FROM [Artigos] " +
                    "			WHERE " +
                    "				Ativo='true' AND " +
                    "				Destaque='false' and " +
                    "				DataPublicacao < GETDATE() " +
                    "			ORDER BY DataPublicacao DESC " +
                    "			) AS auxArtigo " +
                    ") "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);
            }

            return lista;
        }



        public List<VO.Artigo> ListarArtigoRelacionados(int idProduto)
        {
            List<VO.Artigo> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT TOP 4 * FROM [Artigos] WHERE IdRelacionamento=(SELECT IdCategoria FROM [CategoriasPorProdutos] WHERE IdProduto=@idProduto) AND (IdTipo=1 OR IdTipo=6)"
                    //"SELECT TOP 4 * FROM Artigos " +
                    //"    WHERE " +
                    //"        IdArtigo IN ( " +
					//"                    SELECT DISTINCT IdArtigo FROM TermosPorArtigos  " +
					//"	                    WHERE  " +
                    //"		                    IdTermo in (SELECT idTermo FROM TermosPorProdutos WHERE idProduto=@idProduto) " +
                    //"                    ) " +
                    //"        AND " +
		            //"        IdArtigo NOT IN ( " +
                    //"                   SELECT IdArtigo FROM [ArquivosPorArtigos] " +
					//"                   ) " +
                    //"    ORDER BY DataInclusao DESC"
                    , oConn);

                oComm.Parameters.AddWithValue("@idProduto", idProduto);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);
            }

            return lista;
        }


        public List<VO.Artigo> ListarDownloadsRelacionados(int idProduto)
        {
            List<VO.Artigo> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT TOP 4 * FROM [Artigos] WHERE IdRelacionamento IN (SELECT IdCategoria FROM [CategoriasPorProdutos] WHERE IdProduto=@idProduto) AND (IdTipo=3)"
                    /*"SELECT TOP 4 * FROM Artigos " +
                    "    WHERE " +
                    "        IdArtigo IN ( " +
                    "                    SELECT DISTINCT IdArtigo FROM TermosPorArtigos  " +
                    "	                    WHERE  " +
                    "		                    IdTermo in (SELECT idTermo FROM TermosPorProdutos WHERE idProduto=@idProduto) " +
                    "                    ) " +
                    "        AND " +
                    "        IdArtigo IN ( " +
                    "                   SELECT IdArtigo FROM [ArquivosPorArtigos] " +
                    "                   ) " +
                    "    ORDER BY DataInclusao DESC"*/
                    , oConn);

                oComm.Parameters.AddWithValue("@idProduto", idProduto);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);
            }

            return lista;
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Artigo Incluir(VO.Artigo vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Artigos] " +
                    "       ( " +
                    //"           IdArtigoImagem, " +
                    "           IdRelacionamento, " +
                    "           IdCategoria, " +
                    "           IdTipo, " +
                    "           IdAutor, " +
                    "           Destaque, " +
                    "           Titulo, " +
                    "           Texto, " +
                    "           DataPublicacao, " +
                    "           Ativo" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    //"           @IdArtigoImagem, " +
                    "           @IdRelacionamento, " +
                    "           @IdCategoria, " +
                    "           @IdTipo, " +
                    "           @IdAutor, " +
                    "           @Destaque, " +
                    "           @Titulo, " +
                    "           @Texto, " +
                    "           @DataPublicacao, " +
                    "           @Ativo" +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                //oComm.Parameters.AddWithValue("@IdArtigoImagem", vo.ArtigoImagem.IdImagem);
                oComm.Parameters.AddWithValue("@IdRelacionamento", vo.Relacionamento.IdCategoria);
                oComm.Parameters.AddWithValue("@IdTipo", vo.Tipo.IdTipo);
                oComm.Parameters.AddWithValue("@IdAutor", vo.Autor.IdAutor);
                oComm.Parameters.AddWithValue("@IdCategoria", vo.IdCategoria);
                oComm.Parameters.AddWithValue("@Destaque", vo.Destaque);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Texto", vo.Texto);
                oComm.Parameters.AddWithValue("@DataPublicacao", vo.DataPublicacao);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdArtigo = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Artigo vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Artigos] " +
                    "   SET " +
                    //"       IdArtigoImagem=@IdArtigoImagem, " +
                    "       IdRelacionamento=@IdRelacionamento, " +
                    "       IdCategoria=@IdCategoria, " +
                    "       Destaque=@Destaque, " +
                    "       IdTipo=@IdTipo, " +
                    "       IdAutor=@IdAutor, " +
                    "       Titulo=@Titulo, " +
                    "       Texto=@Texto, " +
                    "       DataPublicacao=@DataPublicacao, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdArtigo=@IdArtigo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdRelacionamento", vo.Relacionamento.IdCategoria);
                oComm.Parameters.AddWithValue("@IdArtigo", vo.IdArtigo);
                oComm.Parameters.AddWithValue("@IdTipo", vo.Tipo.IdTipo);
                oComm.Parameters.AddWithValue("@IdAutor", vo.Autor.IdAutor);
                oComm.Parameters.AddWithValue("@IdCategoria", vo.IdCategoria);
                //oComm.Parameters.AddWithValue("@IdArtigoImagem", vo.ArtigoImagem.IdImagem);
                oComm.Parameters.AddWithValue("@Destaque", vo.Destaque);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Texto", vo.Texto);
                oComm.Parameters.AddWithValue("@DataPublicacao", vo.DataPublicacao);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void RemoverImagem(int id)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Artigos] " +
                    "   SET " +
                    "       IdArtigoImagem=0 " +
                    "   WHERE " +
                    "       IdArtigo=@IdArtigo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Artigos] " +
                    "   WHERE " +
                    "       IdArtigo=@IdArtigo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Artigo Popular(DataRow row)
        {
            var vo = new VO.Artigo();
            
            vo.IdArtigo = (int)row["IdArtigo"];
            vo.Relacionamento = new DAL.ProdutosCategorias().Consultar((int)row["IdRelacionamento"]);
            vo.IdCategoria = (int)row["IdCategoria"];
            //vo.ArtigoImagem = new DAL.Imagens().Consultar(Convert.ToInt32(row["IdArtigoImagem"]));
            vo.Autor = new DAL.Autores().Consultar(Convert.ToInt32(row["IdAutor"]));
            vo.ArtigoAquivos = new DAL.ArquivosPorArtigos().ListarPorArtigo(vo.IdArtigo, "Titulo", true);
            vo.Tipo = new DAL.ArtigosTipos().Consultar(Convert.ToInt32(row["IdTipo"]));
            vo.Destaque = (bool)row["Destaque"];
            vo.Titulo = (string)row["Titulo"];
            vo.Texto = (string)row["Texto"];
            vo.Ativo = (bool)row["Ativo"];

            vo.Termos = new DAL.Termos().ListarPorArtigo(vo.IdArtigo);

            vo.DataPublicacao = (DateTime)row["DataPublicacao"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.Artigo> PopularLista(DataTable table)
        {
            var lista = new List<VO.Artigo>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
