﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Estados
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public List<VO.Estado> ListarRepresentates()
        {
            List<VO.Estado> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM Estados WHERE Sigla IN " +
                    "   ( " +
                    "   SELECT Estado FROM [Representantes] " +
                    "       WHERE Ativo='true' " +
                    "           GROUP BY Estado " +
                    "   ) " +
                    "   ORDER BY Nome "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public List<VO.Estado> Listar()
        {
            List<VO.Estado> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM Estados " +
                    "   ORDER BY Nome "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion



        #region Popular

        private VO.Estado Popular(DataRow row)
        {
            var vo = new VO.Estado();

            vo.Nome = (string)row["Nome"];
            vo.Sigla = (string)row["Sigla"];
            
            return vo;
        }

        private List<VO.Estado> PopularLista(DataTable table)
        {
            var lista = new List<VO.Estado>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion



    }
}
