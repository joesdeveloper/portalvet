﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Proprietarios
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Proprietario Consultar(Guid id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Proprietario vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Proprietarios] " +
                    "   WHERE " + 
                    "       IdProprietario=@IdProprietario"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProprietario", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public VO.Proprietario Consultar(string email)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Proprietario vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Proprietarios] " +
                    "   WHERE " +
                    "       email=@email"
                    , oConn);

                oComm.Parameters.AddWithValue("@email", email);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Proprietarios] " +
                    "   WHERE " +
                    "					    nome like @pesquisa OR " +
                    "					    cpf like @pesquisa OR " +
                    "					    rg like @pesquisa OR " +
                    "						email like @pesquisa OR " +
                    "						comercial like @pesquisa OR " +
                    "						telefone like @pesquisa OR " +
                    "						celular like @pesquisa OR " +
                    "		                endereco like @pesquisa OR " +
                    "               		numero like @pesquisa OR " +
                    "		                complemento like @pesquisa OR " +
                    "               		bairro like @pesquisa OR " +
                    "               		cidade like @pesquisa OR " +
                    "               		estado like @pesquisa OR " +
                    "               		cep like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Proprietario> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Proprietario> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Proprietarios] " +
                    "	WHERE IdProprietario IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdProprietario FROM [Proprietarios] " +
                    "		    WHERE IdProprietario NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdProprietario FROM [Proprietarios] " +
                    "				    WHERE " +
                    "					    nome like @pesquisa OR " +
                    "					    cpf like @pesquisa OR " +
                    "					    rg like @pesquisa OR " +
                    "						email like @pesquisa OR " +
                    "						comercial like @pesquisa OR " +
                    "						telefone like @pesquisa OR " +
                    "						celular like @pesquisa OR " +
                    "		                endereco like @pesquisa OR " +
                    "               		numero like @pesquisa OR " +
                    "		                complemento like @pesquisa OR " +
                    "               		bairro like @pesquisa OR " +
                    "               		cidade like @pesquisa OR " +
                    "               		estado like @pesquisa OR " +
                    "               		cep like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "					    nome like @pesquisa OR " +
                    "					    cpf like @pesquisa OR " +
                    "					    rg like @pesquisa OR " +
                    "						email like @pesquisa OR " +
                    "						comercial like @pesquisa OR " +
                    "						telefone like @pesquisa OR " +
                    "						celular like @pesquisa OR " +
                    "		                endereco like @pesquisa OR " +
                    "               		numero like @pesquisa OR " +
                    "		                complemento like @pesquisa OR " +
                    "               		bairro like @pesquisa OR " +
                    "               		cidade like @pesquisa OR " +
                    "               		estado like @pesquisa OR " +
                    "               		cep like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public void Incluir(VO.Proprietario vo)
        {
            
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Proprietarios] " +
                    "       ( " +
                    "           IdProprietario, " +
                    "           IdMedicoVeterinario, " +
                    "           nome,  " +
                    "           cpf,  " +
                    "           rg,  " +
                    "           email,  " +
                    "           comercialDdd,  " +
                    "           comercial,  " +
                    "           telefoneDdd,  " +
                    "           telefone,  " +
                    "           celularDdd,  " +
                    "           celular,  " +
                    "           endereco,  " +
                    "           numero,  " +
                    "           complemento,  " +
                    "           bairro,  " +
                    "           cidade,  " +
                    "           estado,  " +
                    "           cep,  " +
                    "           dataNascimento,  " +
                    "           ativo " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdProprietario, " +
                    "           @IdMedicoVeterinario, " +
                    "           @nome,  " +
                    "           @cpf,  " +
                    "           @rg,  " +
                    "           @email,  " +
                    "           @comercialDdd,  " +
                    "           @comercial,  " +
                    "           @telefoneDdd,  " +
                    "           @telefone,  " +
                    "           @celularDdd,  " +
                    "           @celular,  " +
                    "           @endereco,  " +
                    "           @numero,  " +
                    "           @complemento,  " +
                    "           @bairro,  " +
                    "           @cidade,  " +
                    "           @estado,  " +
                    "           @cep,  " +
                    "           @dataNascimento,  " +
                    "           @ativo " +
                    "       ) "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProprietario", vo.IdProprietario);
                oComm.Parameters.AddWithValue("@IdMedicoVeterinario", vo.IdMedicoVeterinario);
                oComm.Parameters.AddWithValue("@nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Cpf", vo.Cpf);
                oComm.Parameters.AddWithValue("@Rg", vo.Rg);
                oComm.Parameters.AddWithValue("@email", vo.Email);
                oComm.Parameters.AddWithValue("@ComercialDdd", vo.ComercialDdd);
                oComm.Parameters.AddWithValue("@Comercial", vo.Comercial);
                oComm.Parameters.AddWithValue("@TelefoneDdd", vo.TelefoneDdd);
                oComm.Parameters.AddWithValue("@Telefone", vo.Telefone);
                oComm.Parameters.AddWithValue("@CelularDdd", vo.CelularDdd);
                oComm.Parameters.AddWithValue("@Celular", vo.Celular);
                oComm.Parameters.AddWithValue("@endereco", vo.Endereco);
                oComm.Parameters.AddWithValue("@numero", vo.Numero);
                oComm.Parameters.AddWithValue("@complemento", vo.Complemento);
                oComm.Parameters.AddWithValue("@bairro", vo.Bairro);
                oComm.Parameters.AddWithValue("@cidade", vo.Cidade);
                oComm.Parameters.AddWithValue("@estado", vo.Estado);
                oComm.Parameters.AddWithValue("@cep", vo.Cep);
                oComm.Parameters.AddWithValue("@DataNascimento", vo.DataNascimento);
                oComm.Parameters.AddWithValue("@ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Alterar(VO.Proprietario vo)
        {
            VO.Proprietario registro = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Proprietarios] " +
                    "   SET " +
                    "       IdMedicoVeterinario=@IdMedicoVeterinario, " +
                    "       nome=@nome, " +
                    "       cpf=@cpf, " +
                    "       rg=@rg, " +
                    "       email=@email, " +
                    "       comercialDdd=@comercialDdd, " +
                    "       comercial=@comercial, " +
                    "       telefoneDdd=@telefoneDdd, " +
                    "       telefone=@telefone, " +
                    "       celularDdd=@celularDdd, " +
                    "       celular=@celular, " +
                    "       endereco=@endereco, " +
                    "       numero=@numero, " +
                    "       complemento=@complemento, " +
                    "       bairro=@bairro, " +
                    "       cidade=@cidade, " +
                    "       estado=@estado, " +
                    "       cep=@cep, " +
                    "       DataNascimento=@DataNascimento, " +
                    "       ativo=@ativo " +
                    "   WHERE " +
                    "       IdProprietario=@IdProprietario"
                    , oConn);


                oComm.Parameters.AddWithValue("@IdProprietario", vo.IdProprietario);
                oComm.Parameters.AddWithValue("@IdMedicoVeterinario", vo.IdMedicoVeterinario);
                oComm.Parameters.AddWithValue("@nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Cpf", vo.Cpf);
                oComm.Parameters.AddWithValue("@Rg", vo.Rg);
                oComm.Parameters.AddWithValue("@email", vo.Email);
                oComm.Parameters.AddWithValue("@ComercialDdd", vo.ComercialDdd);
                oComm.Parameters.AddWithValue("@Comercial", vo.Comercial);
                oComm.Parameters.AddWithValue("@TelefoneDdd", vo.TelefoneDdd);
                oComm.Parameters.AddWithValue("@Telefone", vo.Telefone);
                oComm.Parameters.AddWithValue("@CelularDdd", vo.CelularDdd);
                oComm.Parameters.AddWithValue("@Celular", vo.Celular);
                oComm.Parameters.AddWithValue("@endereco", vo.Endereco);
                oComm.Parameters.AddWithValue("@numero", vo.Numero);
                oComm.Parameters.AddWithValue("@complemento", vo.Complemento);
                oComm.Parameters.AddWithValue("@bairro", vo.Bairro);
                oComm.Parameters.AddWithValue("@cidade", vo.Cidade);
                oComm.Parameters.AddWithValue("@estado", vo.Estado);
                oComm.Parameters.AddWithValue("@cep", vo.Cep);
                oComm.Parameters.AddWithValue("@DataNascimento", vo.DataNascimento);
                oComm.Parameters.AddWithValue("@ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(Guid id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Proprietarios] " +
                    "   WHERE " +
                    "       IdProprietario=@IdProprietario"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProprietario", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Proprietario Popular(DataRow row)
        {
            var vo = new VO.Proprietario();

            vo.IdProprietario = new Guid(row["IdProprietario"].ToString());
            vo.IdMedicoVeterinario = new Guid(row["IdMedicoVeterinario"].ToString());
            vo.Nome = (string)row["Nome"];
            vo.Cpf = (string)row["Cpf"];
            vo.Rg = (string)row["Rg"];
            vo.Email = (string)row["Email"];
            vo.ComercialDdd = (string)row["ComercialDdd"];
            vo.Comercial = (string)row["Comercial"];
            vo.TelefoneDdd = (string)row["TelefoneDdd"];
            vo.Telefone = (string)row["Telefone"];
            vo.CelularDdd = (string)row["CelularDdd"];
            vo.Celular = (string)row["Celular"];
            vo.Endereco = (string)row["Endereco"];
            vo.Numero = (string)row["Numero"];
            vo.Complemento = (string)row["Complemento"];
            vo.Bairro = (string)row["Bairro"];
            vo.Cidade = (string)row["Cidade"];
            vo.Estado = (string)row["Estado"];
            vo.Cep = (string)row["Cep"];
            vo.DataNascimento = (DateTime)row["DataNascimento"];
            vo.Ativo = (bool)row["Ativo"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.Proprietario> PopularLista(DataTable table)
        {
            var lista = new List<VO.Proprietario>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
