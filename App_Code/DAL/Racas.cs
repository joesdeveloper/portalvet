﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Racas
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Raca Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Raca vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Racas] " +
                    "   WHERE " +
                    "       IdRaca=@IdRaca"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdRaca", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public List<VO.Raca> Listar()
        {
            List<VO.Raca> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Racas] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public List<VO.Raca> Listar(int IdEspecie)
        {
            List<VO.Raca> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Racas] " +
                    "   WHERE " +
                    "       IdEspecie=@IdEspecie"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdEspecie", IdEspecie);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Popular

        private VO.Raca Popular(DataRow row)
        {
            var vo = new VO.Raca();

            vo.IdRaca = (int)row["IdRaca"];
            vo.IdEspecie = Convert.ToInt32(row["IdEspecie"]);
            vo.Nome = (string)row["Nome"];
            vo.Ordenacao = Convert.ToInt32(row["Ordenacao"]);

            return vo;
        }

        private List<VO.Raca> PopularLista(DataTable table)
        {
            var lista = new List<VO.Raca>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
