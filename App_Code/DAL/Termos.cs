﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Termos
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Termo Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Termo vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Termos] " +
                    "   WHERE " + 
                    "       IdTermo=@IdTermo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdTermo", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public VO.Termo Consultar(string nome)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Termo vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Termos] " +
                    "   WHERE " +
                    "       Nome=@Nome"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", nome);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Termos] " +
                    "   WHERE " +
                    "       Nome like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Termo> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Termo> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Termos] " +
                    "	WHERE IdTermo IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdTermo FROM [Termos] " +
                    "		    WHERE IdTermo NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdTermo FROM [Termos] " +
                    "				    WHERE " +
                    "					    Nome like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "               Nome like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Termo Incluir(VO.Termo vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Termos] " +
                    "       ( " +
                    "           Nome" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @Nome" +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", vo.Nome);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdTermo = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Termo vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Termos] " +
                    "   SET " +
                    "       Nome=@Nome " +
                    "   WHERE " +
                    "       IdTermo=@IdTermo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdTermo", vo.IdTermo);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void AlterarTotal(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Termos] " +
                    "   SET " +
                    "       Total=Total+1 " +
                    "   WHERE " +
                    "       IdTermo=@IdTermo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdTermo", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Termos] " +
                    "   WHERE " +
                    "       IdTermo=@IdTermo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdTermo", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Termo Popular(DataRow row)
        {
            var vo = new VO.Termo();
            
            vo.IdTermo = (int)row["IdTermo"];
            vo.Nome = (string)row["Nome"];
            vo.Total = (int)row["Total"];
            
            return vo;
        }

        private List<VO.Termo> PopularLista(DataTable table)
        {
            var lista = new List<VO.Termo>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion



        #region Termos Por Artigo

        public List<VO.Termo> ListarPorArtigo(int IdArtigo)
        {
            List<VO.Termo> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT t.* FROM [TermosPorArtigos] AS ta " +
                    "	INNER JOIN [Termos] AS t  " +
                    "		ON ta.IdTermo=t.IdTermo " +
                    "	WHERE " +
                    "		ta.IdArtigo=@IdArtigo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", IdArtigo);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public void IncluirPorArtigo(int IdArtigo, int idTermo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [TermosPorArtigos] " +
                    "       ( " +
                    "           IdTermo," +
                    "           IdArtigo" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdTermo," +
                    "           @IdArtigo" +

                    "       ); "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", IdArtigo);
                oComm.Parameters.AddWithValue("@IdTermo", idTermo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void ExcluirPorArtigo(int IdArtigo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [TermosPorArtigos] " +
                    "   WHERE " +
                    "       IdArtigo=@IdArtigo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", IdArtigo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion




        #region Termos Por Produtos

        public List<VO.Termo> ListarPorProduto(int IdProduto)
        {
            List<VO.Termo> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT t.* FROM [TermosPorProdutos] AS ta " +
                    "	INNER JOIN [Termos] AS t  " +
                    "		ON ta.IdTermo=t.IdTermo " +
                    "	WHERE " +
                    "		ta.IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public void IncluirPorProduto(int IdProduto, int idTermo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [TermosPorProdutos] " +
                    "       ( " +
                    "           IdTermo," +
                    "           IdProduto" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdTermo," +
                    "           @IdProduto" +

                    "       ); "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);
                oComm.Parameters.AddWithValue("@IdTermo", idTermo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void ExcluirPorProduto(int IdProduto)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [TermosPorProdutos] " +
                    "   WHERE " +
                    "       IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion

    }
}
