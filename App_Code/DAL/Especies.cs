﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Especies
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Especie Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Especie vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Especies] " +
                    "   WHERE " + 
                    "       IdEspecie=@IdEspecie"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdEspecie", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public VO.Especie Consultar(string nome)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Especie vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Especies] " +
                    "   WHERE " +
                    "       Nome=@Nome"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", nome);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public List<VO.Especie> Listar()
        {
            List<VO.Especie> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Especies] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion



        #region Popular

        private VO.Especie Popular(DataRow row)
        {
            var vo = new VO.Especie();

            vo.IdEspecie = (int)row["IdEspecie"];
            vo.Nome = (string)row["Nome"];
            vo.Imagem = (string)row["Imagem"];
            vo.Icone = (string)row["Icone"];
            vo.Mascara = (string)row["Mascara"];
            vo.Orgao = (string)row["Orgao"];

            return vo;
        }

        private List<VO.Especie> PopularLista(DataTable table)
        {
            var lista = new List<VO.Especie>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion



    }
}
