﻿using System;
using System.Configuration;

namespace DAL
{
    public static class Conexao
    {
        public static string BancoDeDados = ConfigurationManager.ConnectionStrings["ConexaoComBancoDeDados"].ConnectionString;
    }
}