﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Busca
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        //public int TotalDeRegistros(string pesquisa)
        //{
        //    using (SqlConnection oConn = new SqlConnection(conexao))
        //    {
        //        SqlCommand oComm = new SqlCommand(
        //            "SELECT COUNT(*) AS 'total' FROM [Noticias] " +
        //            "   WHERE " +
        //            "       Titulo like @pesquisa OR " +
        //            "		Texto like @pesquisa "
        //            , oConn);

        //        oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

        //        SqlDataAdapter oDa = new SqlDataAdapter(oComm);
        //        DataTable table = new DataTable();

        //        oConn.Open();
        //        oDa.Fill(table);

        //        return Convert.ToInt32(table.Rows[0]["total"]);
        //    }
        //}

        //public List<VO.Noticia> Pesquisar(int pagina, int totalPorPagina, string pesquisa)
        //{
        //    List<VO.Noticia> lista = null;

        //    using (SqlConnection oConn = new SqlConnection(conexao))
        //    {
        //        SqlCommand oComm = new SqlCommand(
        //            "SELECT * FROM [Noticias] " +
        //            "	WHERE Id IN " +
        //            "	( " +
        //            "		SELECT TOP (@totalPorPagina) Id FROM [Noticias] " +
        //            "		    WHERE Id NOT IN " +
        //            "		    ( " +
        //            "		        SELECT TOP(@totalPorPagina * (@pagina)) Id FROM [Noticias] " +
        //            "				    WHERE " +
        //            "					    Titulo like @pesquisa OR " +
        //            "						Texto like @pesquisa " +
        //            "                   ORDER BY DataInclusao DESC " +
        //            "			) " +
        //            "			AND " +
        //            "			( " +
        //            "               Titulo like @pesquisa OR " +
        //            "				Texto like @pesquisa " +
        //            "			) " +
        //            "		    ORDER BY DataInclusao DESC" +
        //            "	) " +
        //            "	ORDER BY DataInclusao DESC"
        //            , oConn);

        //        oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
        //        oComm.Parameters.AddWithValue("@pagina", pagina);
        //        oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


        //        SqlDataAdapter oDa = new SqlDataAdapter(oComm);
        //        DataTable table = new DataTable();

        //        oConn.Open();
        //        oDa.Fill(table);

        //        if (table.Rows.Count > 0)
        //            lista = PopularLista(table);

        //        return lista;
        //    }
        //}



        public List<VO.Busca> Pesquisar(int pagina, int totalPorPagina, string pesquisa)
        {
            List<VO.Busca> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT DISTINCT * FROM " +
                    "( " +
                    "   SELECT DISTINCT IdArtigo AS 'Id', Titulo, Texto, DataInclusao, DataPublicacao, 'Artigos' AS Tipo, Ativo FROM [Artigos] AS a " +
                    "	    WHERE " +
                    "		    a.Ativo='true' AND " +
                    "		    a.DataPublicacao < GETDATE() AND " +
                    "		    ( " +
                    "		        a.Titulo like @pesquisa COLLATE Latin1_General_CI_AI OR " +
                    "               a.Texto like @pesquisa COLLATE Latin1_General_CI_AI OR " +
                    "               a.IdRelacionamento = (SELECT IdCategoria FROM [ProdutosCategorias] WHERE Nome LIKE @pesquisa COLLATE Latin1_General_CI_AI ) " +
                    "           ) " +
                    ") AS UltimasNoticias " +
                    "ORDER BY DataPublicacao DESC"
                    , oConn);

                    /*"SELECT DISTINCT * FROM " +
                    "( " +
                    "	( " +
                    "		SELECT * FROM " +
                    "		( " +
                    "			SELECT IdArtigo AS 'Id', Titulo, Texto, DataInclusao, 'Artigos' AS Tipo, Ativo, (SELECT Nome FROM [Autores] WHERE IdAutor=a.IdAutor) AS Autor FROM [Artigos] AS a " +
                    "              WHERE " +
                    "                   Ativo='true' AND " +
                    "                   IdArtigo NOT IN (SELECT IdArtigo FROM [ArquivosPorArtigos]) AND " +
                    "                   ( " +
                    "                       Titulo like @pesquisa COLLATE Latin1_General_CI_AI OR " +
                    "           	        Texto like @pesquisa COLLATE Latin1_General_CI_AI " +
                    "                   ) " +
                    "		) AS ArtigosAux " +
                    "	) " +
                    "	UNION ALL " +
                    "	( " +
                    "		SELECT * FROM " +
                    "		( " +
                    "			SELECT IdArtigo AS 'Id', Titulo, Texto, DataInclusao, 'Arquivos' AS Tipo, Ativo, (SELECT Nome FROM [Autores] WHERE IdAutor=a.IdAutor) AS Autor FROM [Artigos] AS a " +
                    "               WHERE  " +
                    "                   Ativo='true' AND  " +
                    "                   IdArtigo IN (SELECT IdArtigo FROM [ArquivosPorArtigos])  AND " +
                    "                  ( " +
                    "                       Titulo like @pesquisa COLLATE Latin1_General_CI_AI OR " +
                    "               	    Texto like @pesquisa COLLATE Latin1_General_CI_AI " +
                    "       	        ) " +
                    "		) AS ArtigosAux " +
                    "	) " +
                    "	UNION ALL " +
                    "	( " +
                    "		SELECT * FROM " +
                    "		( " +
                    "			SELECT IdEvento AS 'Id', Nome AS 'Titulo', Descricao AS 'Texto', DataInclusao, 'Eventos' AS Tipo, Ativo, '' AS Autor FROM [Eventos] " +
                    "               WHERE  " +
                    "                   Ativo='true' AND  " +
                    "                  ( " +
                    "                       Nome like @pesquisa COLLATE Latin1_General_CI_AI OR " +
                    "               	    Descricao like @pesquisa COLLATE Latin1_General_CI_AI " +
                    "       	        ) " +
                    "		) AS EventosAux " +
                    "	) " +
                    "	UNION ALL " +
                    "	( " +
                    "		SELECT * FROM " +
                    "		( " +
                    "			SELECT IdGaleria AS 'Id', Titulo, Descricao AS 'Texto', DataInclusao, 'Galerias' AS Tipo, Ativo, '' AS Autor FROM [Galerias] " +
                    "               WHERE  " +
                    "                   Ativo='true' AND  " +
                    "                  ( " +
                    "                       Titulo like @pesquisa COLLATE Latin1_General_CI_AI OR " +
                    "               	    Descricao like @pesquisa COLLATE Latin1_General_CI_AI " +
                    "       	        ) " +
                    "		) AS GaleriasAux " +
                    "	) " +
                    ") AS UltimasNoticias " +
                    "ORDER BY DataInclusao DESC"*/

                    //"SELECT DISTINCT Id, Titulo, Texto, '' AS Tipo, Ativo, DataInclusao FROM [Noticias] " +
                    //"   WHERE " +
                    //"	    Ativo='true' AND " +
                    //"	    ( " +
                    //"	    Titulo like @pesquisa OR " +
                    //"	    Texto like @pesquisa " +
                    //"	    ) " +
                    //"   ORDER BY DataInclusao DESC "
                    //, oConn);

                //oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                //oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public List<VO.Busca> UltimasNoticias(int totalPorPagina)
        {
            List<VO.Busca> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                        //"SELECT TOP 20 * FROM " +
                        //"( " +
                        //"	( " +
                        //"		SELECT * FROM " +
                        //"		( " +
                        //"			SELECT IdArtigo AS 'Id', Titulo, Texto, DataInclusao, 'Artigos' AS Tipo FROM [Artigos] WHERE Ativo='true' " +
                        //"		) AS ArtigosAux " +
                        //"	) " +
                        //"	UNION ALL " +
                        //"	( " +
                        //"		SELECT * FROM " +
                        //"		( " +
                        //"			SELECT IdEvento AS 'Id', Nome AS 'Titulo', Descricao AS 'Texto', DataInclusao, 'Eventos' AS Tipo FROM [Eventos] WHERE Ativo='true' " +
                        //"		) AS EventosAux " +
                        //"	) " +
                        //") AS UltimasNoticias " +
                        //"ORDER BY DataInclusao DESC"
                        "SELECT TOP " + totalPorPagina + " * FROM " +
                        "                   ( "+
                        "                       SELECT DISTINCT Id, Titulo, Texto, DataInclusao, DataPublicacao, Ativo, Tipo, Autor FROM [Noticias]" +
                        "                   ) AS Aux " +
                        "   WHERE " +
                        "       Ativo = 'true' AND " +
                        "       DataPublicacao < GETDATE() " +
                        "   ORDER BY DataPublicacao DESC"
                        
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Popular

        private VO.Busca Popular(DataRow row)
        {
            var vo = new VO.Busca();
            
            vo.Id = (int)row["Id"];

            try
            {
                vo.Autor = new DAL.Autores().Consultar((int)row["IdAutor"]);
            }
            catch { }

            vo.Titulo = (string)row["Titulo"];
            vo.Texto = (string)row["Texto"];
            vo.Tipo = (string)row["Tipo"];
            vo.Ativo = (bool)row["Ativo"];
            vo.DataPublicacao = (DateTime)row["DataPublicacao"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.Busca> PopularLista(DataTable table)
        {
            var lista = new List<VO.Busca>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
