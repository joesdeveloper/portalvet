﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ProdutosTipos
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.ProdutoTipo Consultar(string id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.ProdutoTipo vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ProdutosTipos] " +
                    "   WHERE " +
                    "       IdTipo=@IdTipo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdTipo", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public List<VO.ProdutoTipo> Listar()
        {
            List<VO.ProdutoTipo> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ProdutosTipos] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Popular

        private VO.ProdutoTipo Popular(DataRow row)
        {
            var vo = new VO.ProdutoTipo();

            vo.IdTipo = (string)row["IdTipo"];
            vo.Nome = (string)row["Nome"];
            
            return vo;
        }

        private List<VO.ProdutoTipo> PopularLista(DataTable table)
        {
            var lista = new List<VO.ProdutoTipo>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
