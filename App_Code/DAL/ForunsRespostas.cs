﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ForunsRespostas
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.ForumResposta Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.ForumResposta vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ForunsRespostas] " +
                    "   WHERE " +
                    "       IdResposta=@IdResposta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdResposta", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public int TotalDeRegistros(string pesquisa, int IdPergunta)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [ForunsRespostas] " +
                    "   WHERE " +
                    "                       IdPergunta=@IdPergunta AND " +
                    "                       ( " +
                    "					    Nome like @pesquisa OR " +
                    "					    Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                       ) "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPergunta", IdPergunta);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.ForumResposta> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente, int IdPergunta)
        {
            List<VO.ForumResposta> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ForunsRespostas] " +
                    "	WHERE IdResposta IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdResposta FROM [ForunsRespostas] " +
                    "		    WHERE IdResposta NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdResposta FROM [ForunsRespostas] " +
                    "				    WHERE " +
                    "                       IdPergunta=@IdPergunta AND " +
                    "                       ( " +
                    "					    Nome like @pesquisa OR " +
                    "					    Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                       ) " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "                       IdPergunta=@IdPergunta AND " +
                    "                       ( " +
                    "					    Nome like @pesquisa OR " +
                    "					    Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                       ) " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPergunta", IdPergunta);
                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.ForumResposta Incluir(VO.ForumResposta vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [ForunsRespostas] " +
                    "       ( " +
                    "           IdPergunta, " +
                    "           IdUsuario, " +
                    "           Nome, " +
                    "           Texto,  " +
                    "           Ativo " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdPergunta, " +
                    "           @IdUsuario, " +
                    "           @Nome, " +
                    "           @Texto,  " +
                    "           @Ativo " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPergunta", vo.IdPergunta);
                oComm.Parameters.AddWithValue("@IdUsuario", vo.IdUsuario);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Texto", vo.Texto);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdResposta = Convert.ToInt32(obj);
            }

            new DAL.ForunsPerguntas().AtualizarTotais(vo.IdPergunta);

            return vo;
        }

        public void Alterar(VO.ForumResposta vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [ForunsRespostas] " +
                    "   SET " +
                    "       Nome=@Nome, " +
                    "       Texto=@Texto, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdResposta=@IdResposta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdResposta", vo.IdResposta);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Texto", vo.Texto);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }

            new DAL.ForunsPerguntas().AtualizarTotais(vo.IdPergunta);
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [ForunsRespostas] " +
                    "   WHERE " +
                    "       IdResposta=@IdResposta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdResposta", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void ExcluirPorPergunta(int idPergunta)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [ForunsRespostas] " +
                    "   WHERE " +
                    "       idPergunta=@idPergunta"
                    , oConn);

                oComm.Parameters.AddWithValue("@idPergunta", idPergunta);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }

            new DAL.ForunsPerguntas().AtualizarTotais(idPergunta);
        }

        #endregion


        #region Popular

        private VO.ForumResposta Popular(DataRow row)
        {
            var vo = new VO.ForumResposta();

            vo.IdResposta = Convert.ToInt32(row["IdResposta"]);
            vo.IdPergunta = Convert.ToInt32(row["IdPergunta"]);
            vo.IdUsuario = Convert.ToInt32(row["IdUsuario"]);
            vo.Nome = (string)row["Nome"];
            vo.Texto = (string)row["Texto"];
            vo.DataInclusao = (DateTime)row["dataInclusao"];
            vo.Ativo = (bool)row["ativo"];
            
            return vo;
        }

        private List<VO.ForumResposta> PopularLista(DataTable table)
        {
            var lista = new List<VO.ForumResposta>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
