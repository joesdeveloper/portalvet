﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class PermissoesPorAdministradores
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.PermissaoPorAdministrador Consultar(Guid id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.PermissaoPorAdministrador vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [PermissoesDosAdministradores] " +
                    "   WHERE " +
                    "       idAdministrador=@idAdministrador"
                    , oConn);

                oComm.Parameters.AddWithValue("@idAdministrador", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public List<VO.PermissaoPorAdministrador> ListarPorAdministrador(Guid IdAdministrador)
        {
            List<VO.PermissaoPorAdministrador> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [PermissoesDosAdministradores] " +
                    "WHERE " +
                    "   IdAdministrador=@IdAdministrador;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAdministrador", IdAdministrador);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public List<VO.PermissaoPorAdministrador> Listar()
        {
            List<VO.PermissaoPorAdministrador> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [PermissoesDosAdministradores] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion

        #region Inclusao, Alteração e Exclusão

        public void Incluir(VO.PermissaoPorAdministrador vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [PermissoesDosAdministradores] " +
                    "       (IdAdministrador, IdPermissao) " +
                    "   VALUES " +
                    "       (@IdAdministrador, @IdPermissao)"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAdministrador", vo.IdAdministrador);
                oComm.Parameters.AddWithValue("@IdPermissao", vo.IdPermissao);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Alterar(VO.PermissaoPorAdministrador vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [PermissoesDosAdministradores] " +
                    "   SET " +
                    "       IdPermissao=@IdPermissao " +
                    "   WHERE " +
                    "       IdAdministrador=@IdAdministrador"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAdministrador", vo.IdAdministrador);
                oComm.Parameters.AddWithValue("@IdPermissao", vo.IdPermissao);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }


        public void Excluir(VO.PermissaoPorAdministrador vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [PermissoesDosAdministradores] " +
                    "   WHERE " +
                    "       IdAdministrador=@IdAdministrador AND " +
                    "       IdPermissao=@IdPermissao "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAdministrador", vo.IdAdministrador);
                oComm.Parameters.AddWithValue("@IdPermissao", vo.IdPermissao);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(Guid idAdministrador)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [PermissoesDosAdministradores] " +
                    "   WHERE " +
                    "       IdAdministrador=@IdAdministrador "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAdministrador", idAdministrador);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion

        #region Popular

        private VO.PermissaoPorAdministrador Popular(DataRow row)
        {
            var vo = new VO.PermissaoPorAdministrador();

            vo.IdAdministrador = new Guid(row["IdAdministrador"].ToString());
            vo.IdPermissao = (int)row["IdPermissao"];
            
            return vo;
        }

        private List<VO.PermissaoPorAdministrador> PopularLista(DataTable table)
        {
            var lista = new List<VO.PermissaoPorAdministrador>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
