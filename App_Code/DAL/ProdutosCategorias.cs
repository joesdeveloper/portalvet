﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ProdutosCategorias
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.ProdutoCategoria Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.ProdutoCategoria vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ProdutosCategorias] " +
                    "   WHERE " + 
                    "       IdCategoria=@IdCategoria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdCategoria", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [ProdutosCategorias] " +
                    "   WHERE " +
                    "					    nome like @pesquisa OR " +
                    "					    Classe like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.ProdutoCategoria> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.ProdutoCategoria> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ProdutosCategorias] " +
                    "	WHERE IdCategoria IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdCategoria FROM [ProdutosCategorias] " +
                    "		    WHERE IdCategoria NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdCategoria FROM [ProdutosCategorias] " +
                    "				    WHERE " +
                    "					    nome like @pesquisa OR " +
                    "					    Classe like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "					    nome like @pesquisa OR " +
                    "					    Classe like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public List<VO.ProdutoCategoria> Listar()
        {
            List<VO.ProdutoCategoria> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ProdutosCategorias] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.ProdutoCategoria Incluir(VO.ProdutoCategoria vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [ProdutosCategorias] " +
                    "       ( " +
                    "           Nome, " +
                    "           Classe, " +
                    "           Obesidade " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @Nome, " +
                    "           @Classe, " +
                    "           @Obesidade " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Classe", vo.Classe);
                oComm.Parameters.AddWithValue("@Obesidade", vo.Obesidade);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdCategoria = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.ProdutoCategoria vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [ProdutosCategorias] " +
                    "   SET " +
                    "       Nome=@Nome, " +
                    "       Classe=@Classe, " +
                    "       Obesidade=@Obesidade " +
                    "   WHERE " +
                    "       IdCategoria=@IdCategoria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdCategoria", vo.IdCategoria);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Classe", vo.Classe);
                oComm.Parameters.AddWithValue("@Obesidade", vo.Obesidade);
                
                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }


        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [ProdutosCategorias] " +
                    "   WHERE " +
                    "       IdCategoria=@IdCategoria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdCategoria", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.ProdutoCategoria Popular(DataRow row)
        {
            var vo = new VO.ProdutoCategoria();

            vo.IdCategoria = (int)row["IdCategoria"];
            //vo.IdEspecie = (int)row["IdEspecie"];
            vo.Nome = (string)row["Nome"];
            vo.Classe = (string)row["Classe"];
            vo.Obesidade = (bool)row["Obesidade"];

            return vo;
        }

        private List<VO.ProdutoCategoria> PopularLista(DataTable table)
        {
            var lista = new List<VO.ProdutoCategoria>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion




        #region Categoria Por Produto

        public List<VO.ProdutoCategoria> ListarPorProduto(int IdProduto)
        {
            List<VO.ProdutoCategoria> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT c.* FROM [CategoriasPorProdutos] AS cp " +
                    "	INNER JOIN [ProdutosCategorias] AS c  " +
                    "		ON cp.IdCategoria=c.IdCategoria " +
                    "	WHERE " +
                    "		cp.IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public void IncluirPorProduto(int IdProduto, int IdCategoria)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [CategoriasPorProdutos] " +
                    "       ( " +
                    "           IdCategoria," +
                    "           IdProduto" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdCategoria," +
                    "           @IdProduto" +

                    "       ); "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);
                oComm.Parameters.AddWithValue("@IdCategoria", IdCategoria);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void ExcluirPorProduto(int IdProduto)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [CategoriasPorProdutos] " +
                    "   WHERE " +
                    "       IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion



        #region Categoria Por Doenca

        public List<VO.ProdutoCategoria> ListarPorDoenca(int IdDoenca)
        {
            List<VO.ProdutoCategoria> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT c.* FROM [DoencasPorCategorias] AS dc " +
                    "	INNER JOIN [ProdutosCategorias] AS c  " +
                    "		ON dc.IdCategoria=c.IdCategoria " +
                    "	WHERE " +
                    "		dc.IdDoenca=@IdDoenca"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdDoenca", IdDoenca);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public void IncluirPorDoenca(int IdDoenca, int IdCategoria)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [DoencasPorCategorias] " +
                    "       ( " +
                    "           IdDoenca," +
                    "           IdCategoria" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdDoenca," +
                    "           @IdCategoria" +
                    "       ); "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdDoenca", IdDoenca);
                oComm.Parameters.AddWithValue("@IdCategoria", IdCategoria);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void ExcluirPorDoenca(int IdDoenca)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [DoencasPorCategorias] " +
                    "   WHERE " +
                    "       IdDoenca=@IdDoenca"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdDoenca", IdDoenca);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
