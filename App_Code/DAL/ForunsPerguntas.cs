﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ForunsPerguntas
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.ForumPergunta Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.ForumPergunta vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ForunsPerguntas] " +
                    "   WHERE " +
                    "       IdPergunta=@IdPergunta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPergunta", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public int TotalDeRegistros(string pesquisa, int IdForum)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [ForunsPerguntas] " +
                    "   WHERE " +
                    "                       IdForum=@IdForum AND " +
                    "                       ( " +
                    "					    Titulo like @pesquisa OR " +
                    "					    Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                       ) "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdForum", IdForum);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.ForumPergunta> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente, int IdForum)
        {
            List<VO.ForumPergunta> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ForunsPerguntas] " +
                    "	WHERE IdPergunta IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdPergunta FROM [ForunsPerguntas] " +
                    "		    WHERE IdPergunta NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdPergunta FROM [ForunsPerguntas] " +
                    "				    WHERE " +
                    "                       IdForum=@IdForum AND " +
                    "                       ( " +
                    "					    Titulo like @pesquisa OR " +
                    "					    Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                       ) " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "                       IdForum=@IdForum AND " +
                    "                       ( " +
                    "					    Titulo like @pesquisa OR " +
                    "					    Texto like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                       ) " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@IdForum", IdForum);
                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.ForumPergunta Incluir(VO.ForumPergunta vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [ForunsPerguntas] " +
                    "       ( " +
                    "           IdForum, " +
                    "           IdUsuario, " +
                    "           Nome,  " +
                    "           Titulo,  " +
                    "           Texto,  " +
                    "           Ativo " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdForum, " +
                    "           @IdUsuario, " +
                    "           @Nome,  " +
                    "           @Titulo,  " +
                    "           @Texto,  " +
                    "           @Ativo " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdForum", vo.IdForum);
                oComm.Parameters.AddWithValue("@IdUsuario", vo.IdUsuario);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Texto", vo.Texto);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdPergunta = Convert.ToInt32(obj);
            }

            AtualizarTotais(vo.IdPergunta);

            return vo;
        }

        public void Alterar(VO.ForumPergunta vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [ForunsPerguntas] " +
                    "   SET " +
                    "       Titulo=@Titulo, " +
                    "       Texto=@Texto, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdPergunta=@IdPergunta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPergunta", vo.IdPergunta);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Texto", vo.Texto);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }

            AtualizarTotais(vo.IdPergunta);
        }


        public void AtualizarTotais(int IdPergunta)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                     "UPDATE [Foruns] " +
                    "   SET " +
                    "       TotalPerguntas=(SELECT COUNT(*) FROM [ForunsPerguntas] WHERE IdForum=(SELECT IdForum FROM [ForunsPerguntas] WHERE IdPergunta=@IdPergunta)), " +
                    "       TotalRespostas=(SELECT COUNT(*) FROM [ForunsRespostas] WHERE IdPergunta IN (SELECT IdPergunta FROM [ForunsPerguntas] WHERE IdForum=(SELECT IdForum FROM [ForunsPerguntas] WHERE IdPergunta=@IdPergunta))), " +
                    "       DataAtualizacao=( " +
                    "                       SELECT TOP 1 DataInclusao FROM " +
                    "                       ( " +
                    "                       	( " +
                    "                       		SELECT * FROM (SELECT TOP 1 DataInclusao FROM [ForunsPerguntas] WHERE IdForum=(SELECT IdForum FROM [ForunsPerguntas] WHERE IdPergunta=@IdPergunta) ORDER BY DataInclusao DESC) AS p " +
                    "                       	)  " +
                    "                       	UNION ALL " +
                    "                       	( " +
                    "                       		SELECT * FROM (SELECT TOP 1 DataInclusao FROM [ForunsRespostas] WHERE IdPergunta IN (SELECT IdPergunta FROM [ForunsPerguntas] WHERE IdForum=(SELECT IdForum FROM [ForunsPerguntas] WHERE IdPergunta=@IdPergunta)) ORDER BY DataInclusao DESC) AS r " +
                    "                       	) " +
                    "                       ) AS DataAtualizacao " +
                    "                       ORDER BY DataInclusao DESC " +
                    "                       ) " +
                    "   WHERE " +
                    "       IdForum=(SELECT IdForum FROM [ForunsPerguntas] WHERE IdPergunta=@IdPergunta); " +
                    "UPDATE [ForunsPerguntas] " +
                    "   SET " +
                    "       TotalDeRespostas=(SELECT COUNT(*) FROM [ForunsRespostas] WHERE IdPergunta=@IdPergunta) " +
                    "   WHERE " +
                    "       IdPergunta=@IdPergunta;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPergunta", IdPergunta);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [ForunsPerguntas] " +
                    "   WHERE " +
                    "       IdPergunta=@IdPergunta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPergunta", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }

            AtualizarTotais(id);
        }

        #endregion


        #region Popular

        private VO.ForumPergunta Popular(DataRow row)
        {
            var vo = new VO.ForumPergunta();

            vo.IdPergunta = Convert.ToInt32(row["IdPergunta"]);
            vo.IdForum = Convert.ToInt32(row["IdForum"]);
            vo.IdUsuario = Convert.ToInt32(row["IdUsuario"]);
            vo.Nome = (string)row["Nome"];
            vo.Titulo = (string)row["Titulo"];
            vo.Texto = (string)row["Texto"];
            vo.TotalDeRespostas = (int)row["TotalDeRespostas"];
            vo.DataInclusao = (DateTime)row["dataInclusao"];
            vo.Ativo = (bool)row["ativo"];
            
            return vo;
        }

        private List<VO.ForumPergunta> PopularLista(DataTable table)
        {
            var lista = new List<VO.ForumPergunta>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
