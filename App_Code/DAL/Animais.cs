﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Animais
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Animal Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Animal vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Animais] " +
                    "   WHERE " + 
                    "       idAnimal=@idAnimal"
                    , oConn);

                oComm.Parameters.AddWithValue("@idAnimal", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public List<VO.Animal> Listar(int idProprietario)
        {
            List<VO.Animal> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Animais] " +
                    "   WHERE " +
                    "       idProprietario=@idProprietario"
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }



        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Animais] " +
                    "   WHERE " +
                    "					    nome like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Animal> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Animal> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Animais] " +
                    "	WHERE IdAnimal IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdAnimal FROM [Animais] " +
                    "		    WHERE IdAnimal NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdAnimal FROM [Animais] " +
                    "				    WHERE " +
                    "					    nome like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "					    nome like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }
        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Animal Incluir(VO.Animal vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Animais] " +
                    "       ( " +
                    "           IdProprietario, " +
                    "           IdRaca, " +
                    "           Nome,  " +
                    "           Genero,  " +
                    "           DataNascimento " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdProprietario, " +
                    "           @IdRaca, " +
                    "           @Nome,  " +
                    "           @Genero,  " +
                    "           @DataNascimento " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProprietario", vo.IdProprietario);
                oComm.Parameters.AddWithValue("@IdRaca", vo.Raca.IdRaca);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Genero", vo.Genero);
                oComm.Parameters.AddWithValue("@DataNascimento", vo.DataNascimento);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdAnimal = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Animal vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Animais] " +
                    "   SET " +
                    "       IdProprietario=@IdProprietario, " +
                    "       IdRaca=@IdRaca, " +
                    "       Nome=@Nome, " +
                    "       Genero=@Genero, " +
                    "       DataNascimento=@DataNascimento " +
                    "   WHERE " +
                    "       IdAnimal=@IdAnimal"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProprietario", vo.IdProprietario);
                oComm.Parameters.AddWithValue("@IdAnimal", vo.IdAnimal);
                oComm.Parameters.AddWithValue("@IdRaca", vo.Raca.IdRaca);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Genero", vo.Genero);
                oComm.Parameters.AddWithValue("@DataNascimento", vo.DataNascimento);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Animais] " +
                    "   WHERE " +
                    "       IdAnimal=@IdAnimal"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAnimal", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


            

        private VO.Animal Popular(DataRow row)
        {
            var vo = new VO.Animal();

            vo.IdAnimal = (int)row["IdAnimal"];
            vo.IdProprietario = new Guid(row["IdProprietario"].ToString());
            vo.Raca = new Racas().Consultar((int)row["IdRaca"]);
            vo.Nome = (string)row["Nome"];
            vo.Genero = (string)row["Genero"];
            vo.DataNascimento = (DateTime)row["DataNascimento"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.Animal> PopularLista(DataTable table)
        {
            var lista = new List<VO.Animal>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }


    }
}
