﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Permissoes
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public List<VO.Permissao> Listar()
        {
            List<VO.Permissao> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Permissoes] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Popular

        private VO.Permissao Popular(DataRow row)
        {
            var vo = new VO.Permissao();

            vo.IdPermissao = (int)row["IdPermissao"];
            vo.Nome = (string)row["Nome"];
            
            return vo;
        }

        private List<VO.Permissao> PopularLista(DataTable table)
        {
            var lista = new List<VO.Permissao>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
