﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Eventos
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Evento Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Evento vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Eventos] " +
                    "   WHERE " + 
                    "       IdEvento=@IdEvento"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdEvento", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Eventos] " +
                    "   WHERE " +
                    "                       Nome like @pesquisa OR " +
                    "               		Descricao like @pesquisa OR " +
                    "               		Local like @pesquisa OR " +
                    "               		Endereco like @pesquisa OR " +
                    "               		Bairro like @pesquisa OR " +
                    "               		Cidade like @pesquisa OR " +
                    "               		Cep like @pesquisa OR " +
                    "               		CONVERT(varchar,DataEventoInicial,103) like @pesquisa OR " +
                    "               		CONVERT(varchar,DataEventoFinal,103) like @pesquisa OR " +
                    "               		CONVERT(varchar,DataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Evento> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Evento> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Eventos] " +
                    "	WHERE IdEvento IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdEvento FROM [Eventos] " +
                    "		    WHERE IdEvento NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdEvento FROM [Eventos] " +
                    "				    WHERE " +
                    "                       Nome like @pesquisa OR " +
                    "               		Descricao like @pesquisa OR " +
                    "               		Local like @pesquisa OR " +
                    "               		Endereco like @pesquisa OR " +
                    "               		Bairro like @pesquisa OR " +
                    "               		Cidade like @pesquisa OR " +
                    "               		Cep like @pesquisa OR " +
                    "               		CONVERT(varchar,DataEventoInicial,103) like @pesquisa OR " +
                    "               		CONVERT(varchar,DataEventoFinal,103) like @pesquisa OR " +
                    "               		CONVERT(varchar,DataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "                       Nome like @pesquisa OR " +
                    "               		Descricao like @pesquisa OR " +
                    "               		Local like @pesquisa OR " +
                    "               		Endereco like @pesquisa OR " +
                    "               		Bairro like @pesquisa OR " +
                    "               		Cidade like @pesquisa OR " +
                    "               		Cep like @pesquisa OR " +
                    "               		CONVERT(varchar,DataEventoInicial,103) like @pesquisa OR " +
                    "               		CONVERT(varchar,DataEventoFinal,103) like @pesquisa OR " +
                    "               		CONVERT(varchar,DataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Evento Incluir(VO.Evento vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Eventos] " +
                    "       ( " +
                    //"           IdEventoImagem, " +
                    "           Nome, " +
                    "           Descricao, " +
                    "           Local, " +
                    "           Endereco, " +
                    "           Numero, " +
                    "           Complemento, " +
                    "           Bairro, " +
                    "           Cidade, " +
                    "           Estado, " +
                    "           Cep, " +
                    "           DataEventoInicial, " +
                    "           DataEventoFinal, " +
                    "           DataPublicacao, " +
                    "           Ativo " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    //"           @IdEventoImagem, " +
                    "           @Nome, " +
                    "           @Descricao, " +
                    "           @Local, " +
                    "           @Endereco, " +
                    "           @Numero, " +
                    "           @Complemento, " +
                    "           @Bairro, " +
                    "           @Cidade, " +
                    "           @Estado, " +
                    "           @Cep, " +
                    "           @DataEventoInicial, " +
                    "           @DataEventoFinal, " +
                    "           @DataPublicacao, " +
                    "           @Ativo " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                //oComm.Parameters.AddWithValue("@IdEventoImagem", vo.EventoImagem.IdImagem);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@Local", vo.Local);
                oComm.Parameters.AddWithValue("@Endereco", vo.Endereco);
                oComm.Parameters.AddWithValue("@Numero", vo.Numero);
                oComm.Parameters.AddWithValue("@Complemento", vo.Complemento);
                oComm.Parameters.AddWithValue("@Bairro", vo.Bairro);
                oComm.Parameters.AddWithValue("@Cidade", vo.Cidade);
                oComm.Parameters.AddWithValue("@Estado", vo.Estado);
                oComm.Parameters.AddWithValue("@Cep", vo.Cep);
                oComm.Parameters.AddWithValue("@DataEventoInicial", vo.DataEventoInicial);
                oComm.Parameters.AddWithValue("@DataEventoFinal", vo.DataEventoFinal);
                oComm.Parameters.AddWithValue("@DataPublicacao", vo.DataPublicacao);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);


                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdEvento = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Evento vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Eventos] " +
                    "   SET " +
                    //"       IdEventoImagem=@IdEventoImagem, " +
                    "       Nome=@Nome, " +
                    "       Descricao=@Descricao, " +
                    "       Local=@Local, " +
                    "       Endereco=@Endereco, " +
                    "       Numero=@Numero, " +
                    "       Complemento=@Complemento, " +
                    "       Bairro=@Bairro, " +
                    "       Cidade=@Cidade, " +
                    "       Estado=@Estado, " +
                    "       Cep=@Cep, " +
                    "       DataEventoInicial=@DataEventoInicial, " +
                    "       DataEventoFinal=@DataEventoFinal, " +
                    "       DataPublicacao=@DataPublicacao, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdEvento=@IdEvento"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdEvento", vo.IdEvento);
                //oComm.Parameters.AddWithValue("@IdEventoImagem", vo.EventoImagem.IdImagem);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@Local", vo.Local);
                oComm.Parameters.AddWithValue("@Endereco", vo.Endereco);
                oComm.Parameters.AddWithValue("@Numero", vo.Numero);
                oComm.Parameters.AddWithValue("@Complemento", vo.Complemento);
                oComm.Parameters.AddWithValue("@Bairro", vo.Bairro);
                oComm.Parameters.AddWithValue("@Cidade", vo.Cidade);
                oComm.Parameters.AddWithValue("@Estado", vo.Estado);
                oComm.Parameters.AddWithValue("@Cep", vo.Cep);
                oComm.Parameters.AddWithValue("@DataEventoInicial", vo.DataEventoInicial);
                oComm.Parameters.AddWithValue("@DataEventoFinal", vo.DataEventoFinal);
                oComm.Parameters.AddWithValue("@DataPublicacao", vo.DataPublicacao);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }


        public void RemoverImagem(int id)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Eventos] " +
                    "   SET " +
                    "       IdEventoImagem=0 " +
                    "   WHERE " +
                    "       IdEvento=@IdEvento"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdEvento", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Eventos] " +
                    "   WHERE " +
                    "       IdEvento=@IdEvento"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdEvento", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Evento Popular(DataRow row)
        {
            var vo = new VO.Evento();
            
            vo.IdEvento = (int)row["IdEvento"];
            //vo.EventoImagem = new DAL.Imagens().Consultar(Convert.ToInt32(row["IdEventoImagem"]));
            vo.Nome = (string)row["Nome"];
            vo.Descricao = (string)row["Descricao"];
            vo.Nome = (string)row["Nome"];
            vo.Descricao = (string)row["Descricao"];
            vo.Local = (string)row["Local"];
            vo.Endereco = (string)row["Endereco"];
            vo.Numero = (string)row["Numero"];
            vo.Complemento = (string)row["Complemento"];
            vo.Bairro = (string)row["Bairro"];
            vo.Cidade = (string)row["Cidade"];
            vo.Estado = (string)row["Estado"];
            vo.Cep = (string)row["Cep"];
            vo.DataEventoInicial = (DateTime)row["DataEventoInicial"];
            vo.DataEventoFinal = (DateTime)row["DataEventoFinal"];
            vo.Ativo = (bool)row["Ativo"];

            vo.DataPublicacao = (DateTime)row["DataPublicacao"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.Evento> PopularLista(DataTable table)
        {
            var lista = new List<VO.Evento>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
