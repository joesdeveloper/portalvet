﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ImagensPorGalerias
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.ImagemPorGaleria Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.ImagemPorGaleria vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ImagensPorGalerias] " +
                    "   WHERE " + 
                    "       IdImagem=@IdImagem"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdImagem", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [ImagensPorGalerias] " +
                    "   WHERE " +
                    "					    Titulo like @pesquisa OR " +
                    "                       Descricao like @pesquisa OR " +
                    "						Url like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.ImagemPorGaleria> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.ImagemPorGaleria> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ImagensPorGalerias] " +
                    "	WHERE IdImagem IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdImagem FROM [ImagensPorGalerias] " +
                    "		    WHERE IdImagem NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdImagem FROM [ImagensPorGalerias] " +
                    "				    WHERE " +
                    "					    Titulo like @pesquisa OR " +
                    "                       Descricao like @pesquisa OR " +
                    "						Url like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "					    Titulo like @pesquisa OR " +
                    "                       Descricao like @pesquisa OR " +
                    "						Url like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.ImagemPorGaleria Incluir(VO.ImagemPorGaleria vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [ImagensPorGalerias] " +
                    "       ( " +
                    "           IdGaleria, " +
                    "           Titulo, " +
                    "           Descricao, " +
                    "           Url " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdGaleria, " +
                    "           @Titulo, " +
                    "           @Descricao, " +
                    "           @Url " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdGaleria", vo.IdGaleria);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@Url", vo.Url);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdImagem = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.ImagemPorGaleria vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [ImagensPorGalerias] " +
                    "   SET " +
                    "       Titulo=@Titulo, " +
                    "       Descricao=@Descricao, " +
                    "       Url=@Url " +
                    "   WHERE " +
                    "       IdImagem=@IdImagem"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdGaleria", vo.IdGaleria);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@Url", vo.Url);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }


        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [ImagensPorGalerias] " +
                    "   WHERE " +
                    "       IdImagem=@IdImagem"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdImagem", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.ImagemPorGaleria Popular(DataRow row)
        {
            var vo = new VO.ImagemPorGaleria();

            vo.IdImagem = (int)row["IdImagem"];
            vo.IdGaleria = (int)row["IdGaleria"];
            vo.Titulo = (string)row["Titulo"];
            vo.Descricao = (string)row["Descricao"];
            vo.Url = (string)row["Url"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.ImagemPorGaleria> PopularLista(DataTable table)
        {
            var lista = new List<VO.ImagemPorGaleria>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion




        #region Imagens Por Galeria

        public List<VO.ImagemPorGaleria> ListarPorGaleria(int IdGaleria, string colunaOrdenacao, bool ascendente)
        {
            List<VO.ImagemPorGaleria> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ImagensPorGalerias] " +
                    "	WHERE " +
                    "		IdGaleria=@IdGaleria " +
                    "   ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@IdGaleria", IdGaleria);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public void ExcluirPorGaleria(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [ImagensPorGalerias] " +
                    "   WHERE " +
                    "       IdGaleria=@IdGaleria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdGaleria", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
