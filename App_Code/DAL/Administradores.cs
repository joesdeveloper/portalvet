﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Administradores
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Administrador Consultar(Guid id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Administrador vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Administradores] " +
                    "   WHERE " + 
                    "       idAdministrador=@idAdministrador"
                    , oConn);

                oComm.Parameters.AddWithValue("@idAdministrador", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public VO.Administrador Consultar(string email)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Administrador vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Administradores] " +
                    "   WHERE " +
                    "       email=@email"
                    , oConn);

                oComm.Parameters.AddWithValue("@email", email);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Administradores] " +
                    "   WHERE " +
                    "       nome like @pesquisa OR " +
                    "		email like @pesquisa OR " +
                    "		CONVERT(varchar,dataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Administrador> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Administrador> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Administradores] " +
                    "	WHERE idAdministrador IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) idAdministrador FROM [Administradores] " +
                    "		    WHERE idAdministrador NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) idAdministrador FROM [Administradores] " +
                    "				    WHERE " +
                    "					    nome like @pesquisa OR " +
                    "						email like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "               nome like @pesquisa OR " +
                    "				email like @pesquisa OR " +
                    "				CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public void Incluir(VO.Administrador vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Administradores] " +
                    "       ( " +
                    "           idAdministrador,  " +
                    "           nome,  " +
                    "           email,  " +
                    "           senha,  " +
                    "           ativo " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @idAdministrador,  " +
                    "           @nome,  " +
                    "           @email,  " +
                    "           @senha,  " +
                    "           @ativo " +
                    "       ) "
                    , oConn);

                oComm.Parameters.AddWithValue("@idAdministrador", vo.IdAdministrador);
                oComm.Parameters.AddWithValue("@nome", vo.Nome);
                oComm.Parameters.AddWithValue("@email", vo.Email);
                oComm.Parameters.AddWithValue("@senha", vo.Senha);
                oComm.Parameters.AddWithValue("@ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Alterar(VO.Administrador vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Administradores] " +
                    "   SET " + 
                    "       nome=@nome, " +
                    "       email=@email, " +
                    "       ativo=@ativo " +
                    "   WHERE " +
                    "       idAdministrador=@idAdministrador"
                    , oConn);

                oComm.Parameters.AddWithValue("@idAdministrador", vo.IdAdministrador);
                oComm.Parameters.AddWithValue("@nome", vo.Nome);
                oComm.Parameters.AddWithValue("@email", vo.Email);
                oComm.Parameters.AddWithValue("@ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(Guid id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Administradores] " +
                    "   WHERE " +
                    "       idAdministrador=@idAdministrador"
                    , oConn);

                oComm.Parameters.AddWithValue("@idAdministrador", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void AlterarSenha(VO.Administrador vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Administradores] " +
                    "   SET " +
                    "       senha=@senha " +
                    "   WHERE " +
                    "       idAdministrador=@idAdministrador"
                    , oConn);

                oComm.Parameters.AddWithValue("@idAdministrador", vo.IdAdministrador);
                oComm.Parameters.AddWithValue("@senha", vo.Senha);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void RegistrarAcesso(Guid id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Administradores] " +
                    "   SET " +
                    "       totalAcessos=totalAcessos+1, " +
                    "       dataUltimoAcesso=GETDATE() " +
                    "   WHERE " +
                    "       idAdministrador=@idAdministrador"
                    , oConn);

                oComm.Parameters.AddWithValue("@idAdministrador", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Administrador Popular(DataRow row)
        {
            var vo = new VO.Administrador();
            
            vo.IdAdministrador = new Guid(row["IdAdministrador"].ToString());
            vo.Nome = (string)row["nome"];
            vo.Email = (string)row["email"];
            vo.Senha = (string)row["senha"];
            vo.Ativo = (bool)row["ativo"];
            vo.TotalAcessos = (int)row["totalAcessos"];
            
            if (row["dataUltimoAcesso"].ToString() != "")
                vo.DataUltimoAcesso = (DateTime)row["dataUltimoAcesso"];

            vo.DataInclusao = (DateTime)row["dataInclusao"];
            
            return vo;
        }

        private List<VO.Administrador> PopularLista(DataTable table)
        {
            var lista = new List<VO.Administrador>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
