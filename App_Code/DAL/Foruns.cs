﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Foruns
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Forum Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Forum vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Foruns] " +
                    "   WHERE " + 
                    "       IdForum=@IdForum"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdForum", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public VO.Forum Consultar(string Nome)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Forum vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Foruns] " +
                    "   WHERE " +
                    "       Nome=@Nome"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", Nome);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Foruns] " +
                    "   WHERE " +
                    "					    Nome like @pesquisa OR " +
                    "					    Descricao like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Forum> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Forum> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Foruns] " +
                    "	WHERE IdForum IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdForum FROM [Foruns] " +
                    "		    WHERE IdForum NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdForum FROM [Foruns] " +
                    "				    WHERE " +
                    "					    Nome like @pesquisa OR " +
                    "					    Descricao like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "					    Nome like @pesquisa OR " +
                    "					    Descricao like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Forum Incluir(VO.Forum vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Foruns] " +
                    "       ( " +
                    "           Nome, " +
                    "           Descricao, " +
                    "           UrlAmigavel, " +
                    "           Ativo " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @Nome, " +
                    "           @Descricao, " +
                    "           @UrlAmigavel, " +
                    "           @Ativo " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@UrlAmigavel", vo.UrlAmigavel);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdForum = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Forum vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Foruns] " +
                    "   SET " +
                    "       Nome=@Nome, " +
                    "       Descricao=@Descricao, " +
                    "       UrlAmigavel=@UrlAmigavel, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdForum=@IdForum"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdForum", vo.IdForum);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@UrlAmigavel", vo.UrlAmigavel);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Foruns] " +
                    "   WHERE " +
                    "       IdForum=@IdForum"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdForum", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }


        #endregion


        #region Popular

        private VO.Forum Popular(DataRow row)
        {
            var vo = new VO.Forum();
            
            vo.IdForum = Convert.ToInt32(row["IdForum"]);
            vo.Nome = (string)row["Nome"];
            vo.Descricao = (string)row["Descricao"];
            vo.UrlAmigavel = (string)row["UrlAmigavel"];
            vo.TotalPerguntas = (int)row["TotalPerguntas"];
            vo.TotalRespostas = (int)row["TotalRespostas"];
            vo.DataAtualizacao = (DateTime)row["DataAtualizacao"];
            vo.Ativo = (bool)row["ativo"];
            vo.DataInclusao = (DateTime)row["dataInclusao"];
            
            return vo;
        }

        private List<VO.Forum> PopularLista(DataTable table)
        {
            var lista = new List<VO.Forum>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
