﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Vitrines
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Vitrine Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Vitrine vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Vitrines] " +
                    "   WHERE " + 
                    "       IdVitrine=@IdVitrine"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdVitrine", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Vitrines] " +
                    "   WHERE " +
                    "       Titulo like @pesquisa OR " +
                    "		Url like @pesquisa OR " +
                    "		CONVERT(varchar,dataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Vitrine> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Vitrine> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Vitrines] " +
                    "	WHERE IdVitrine IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdVitrine FROM [Vitrines] " +
                    "		    WHERE IdVitrine NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdVitrine FROM [Vitrines] " +
                    "				    WHERE " +
                    "					    Titulo like @pesquisa OR " +
                    "						Url like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "               Titulo like @pesquisa OR " +
                    "				Url like @pesquisa OR " +
                    "				CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public List<VO.Vitrine> Listar_Vitrines_Home()
        {
            List<VO.Vitrine> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Vitrines] " +
                    "	WHERE " +
                    "		Ativo='true' AND " +
                    "		DataPublicacao < GETDATE() " +
                    "	ORDER BY DataPublicacao DESC "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);
            }

            return lista;
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Vitrine Incluir(VO.Vitrine vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Vitrines] " +
                    "       ( " +
                    "           Titulo, " +
                    "           Url, " +
                    "           Target, " +
                    "           DataPublicacao, " +
                    "           Ativo" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @Titulo, " +
                    "           @Url, " +
                    "           @Target, " +
                    "           @DataPublicacao, " +
                    "           @Ativo" +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Url", vo.Url);
                oComm.Parameters.AddWithValue("@Target", vo.Target);
                oComm.Parameters.AddWithValue("@DataPublicacao", vo.DataPublicacao);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdVitrine = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Vitrine vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Vitrines] " +
                    "   SET " +
                    "       Titulo=@Titulo, " +
                    "       Url=@Url, " +
                    "       Target=@Target, " +
                    "       DataPublicacao=@DataPublicacao, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdVitrine=@IdVitrine"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdVitrine", vo.IdVitrine);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Url", vo.Url);
                oComm.Parameters.AddWithValue("@Target", vo.Target);
                oComm.Parameters.AddWithValue("@DataPublicacao", vo.DataPublicacao);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Vitrines] " +
                    "   WHERE " +
                    "       IdVitrine=@IdVitrine"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdVitrine", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Vitrine Popular(DataRow row)
        {
            var vo = new VO.Vitrine();
            
            vo.IdVitrine = (int)row["IdVitrine"];
            vo.Titulo = (string)row["Titulo"];
            vo.Url = (string)row["Url"];
            vo.Target = (string)row["Target"];
            vo.Ativo = (bool)row["Ativo"];
            vo.DataPublicacao = (DateTime)row["DataPublicacao"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.Vitrine> PopularLista(DataTable table)
        {
            var lista = new List<VO.Vitrine>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
