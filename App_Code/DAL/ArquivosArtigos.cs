﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ArquivosPorArtigos
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.ArquivoPorArtigo Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.ArquivoPorArtigo vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ArquivosPorArtigos] " +
                    "   WHERE " + 
                    "       IdArquivo=@IdArquivo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArquivo", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [ArquivosPorArtigos] " +
                    "   WHERE " +
                    "       Titulo like @pesquisa OR " +
                    "		Url like @pesquisa OR " +
                    "		CONVERT(varchar,dataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.ArquivoPorArtigo> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.ArquivoPorArtigo> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ArquivosPorArtigos] " +
                    "	WHERE IdArquivo IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdArquivo FROM [ArquivosPorArtigos] " +
                    "		    WHERE IdArquivo NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdArquivo FROM [ArquivosPorArtigos] " +
                    "				    WHERE " +
                    "					    Titulo like @pesquisa OR " +
                    "						Url like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "               Titulo like @pesquisa OR " +
                    "				Url like @pesquisa OR " +
                    "				CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.ArquivoPorArtigo Incluir(VO.ArquivoPorArtigo vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [ArquivosPorArtigos] " +
                    "       ( " +
                    "           IdArtigo, " +
                    "           Titulo, " +
                    "           Url " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdArtigo, " +
                    "           @Titulo, " +
                    "           @Url " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", vo.IdArtigo);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Url", vo.Url);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdArquivo = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.ArquivoPorArtigo vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [ArquivosPorArtigos] " +
                    "   SET " +
                    "       Titulo=@Titulo, " +
                    "       Url=@Url " +
                    "   WHERE " +
                    "       IdArquivo=@IdArquivo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", vo.IdArtigo);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Url", vo.Url);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }


        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [ArquivosPorArtigos] " +
                    "   WHERE " +
                    "       IdArquivo=@IdArquivo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArquivo", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.ArquivoPorArtigo Popular(DataRow row)
        {
            var vo = new VO.ArquivoPorArtigo();

            vo.IdArquivo = (int)row["IdArquivo"];
            vo.IdArtigo = (int)row["IdArtigo"];
            vo.Titulo = (string)row["Titulo"];
            vo.Url = (string)row["Url"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.ArquivoPorArtigo> PopularLista(DataTable table)
        {
            var lista = new List<VO.ArquivoPorArtigo>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion




        #region Arquivos Por Artigo

        public List<VO.ArquivoPorArtigo> ListarPorArtigo(int IdArtigo, string colunaOrdenacao, bool ascendente)
        {
            List<VO.ArquivoPorArtigo> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ArquivosPorArtigos] " +
                    "	WHERE " +
                    "		IdArtigo=@IdArtigo " +
                    "   ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", IdArtigo);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        public void ExcluirPorArtigo(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [ArquivosPorArtigos] " +
                    "   WHERE " +
                    "       IdArtigo=@IdArtigo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdArtigo", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
