﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Representantes
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Representante Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Representante vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Representantes] " +
                    "   WHERE " + 
                    "       IdRepresentante=@IdRepresentante"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdRepresentante", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Representantes] " +
                    "   WHERE " +
                    "                       Nome like @pesquisa OR " +
                    "                       Email1 like @pesquisa OR " +
                    "                       Email2 like @pesquisa OR " +
                    "                       Telefone1 like @pesquisa OR " +
                    "                       Telefone2 like @pesquisa OR " +
                    "                       Telefone3 like @pesquisa OR " +
                    "                       Telefone4 like @pesquisa OR " +
                    "                       Celular like @pesquisa OR " +
                    "                       NextelId like @pesquisa OR " +
                    "               		Endereco like @pesquisa OR " +
                    "               		Bairro like @pesquisa OR " +
                    "               		Cidade like @pesquisa OR " +
                    "               		Estado like @pesquisa OR " +
                    "               		Cep like @pesquisa OR " +
                    "               		CONVERT(varchar,DataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Representante> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Representante> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Representantes] " +
                    "	WHERE IdRepresentante IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdRepresentante FROM [Representantes] " +
                    "		    WHERE IdRepresentante NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdRepresentante FROM [Representantes] " +
                    "				    WHERE " +
                    "                       Nome like @pesquisa OR " +
                    "                       Email1 like @pesquisa OR " +
                    "                       Email2 like @pesquisa OR " +
                    "                       Telefone1 like @pesquisa OR " +
                    "                       Telefone2 like @pesquisa OR " +
                    "                       Telefone3 like @pesquisa OR " +
                    "                       Telefone4 like @pesquisa OR " +
                    "                       Celular like @pesquisa OR " +
                    "                       NextelId like @pesquisa OR " +
                    "               		Endereco like @pesquisa OR " +
                    "               		Bairro like @pesquisa OR " +
                    "               		Cidade like @pesquisa OR " +
                    "               		Estado like @pesquisa OR " +
                    "               		Cep like @pesquisa OR " +
                    "               		CONVERT(varchar,DataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "                       Nome like @pesquisa OR " +
                    "                       Email1 like @pesquisa OR " +
                    "                       Email2 like @pesquisa OR " +
                    "                       Telefone1 like @pesquisa OR " +
                    "                       Telefone2 like @pesquisa OR " +
                    "                       Telefone3 like @pesquisa OR " +
                    "                       Telefone4 like @pesquisa OR " +
                    "                       Celular like @pesquisa OR " +
                    "                       NextelId like @pesquisa OR " +
                    "               		Endereco like @pesquisa OR " +
                    "               		Bairro like @pesquisa OR " +
                    "               		Cidade like @pesquisa OR " +
                    "               		Estado like @pesquisa OR " +
                    "               		Cep like @pesquisa OR " +
                    "               		CONVERT(varchar,DataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }



        public List<VO.Representante> Listar(string Estado)
        {
            List<VO.Representante> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Representantes] " +
                    "	WHERE Estado=@Estado " +
                    "	ORDER BY Nome ASC"
                    , oConn);

                oComm.Parameters.AddWithValue("@Estado", Estado);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Representante Incluir(VO.Representante vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Representantes] " +
                    "       ( " +
                    "           Nome, " +
                    "           Email1, " +
                    "           Email2, " +
                    "           Telefone1, " +
                    "           Telefone2, " +
                    "           Telefone3, " +
                    "           Telefone4, " +
                    "           Celular, " +
                    "           Fax, " +
                    "           NextelId, " +
                    "           Endereco, " +
                    "           Numero, " +
                    "           Complemento, " +
                    "           Bairro, " +
                    "           Cidade, " +
                    "           Estado, " +
                    "           Cep, " +
                    "           Ativo " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @Nome, " +
                    "           @Email1, " +
                    "           @Email2, " +
                    "           @Telefone1, " +
                    "           @Telefone2, " +
                    "           @Telefone3, " +
                    "           @Telefone4, " +
                    "           @Celular, " +
                    "           @Fax, " +
                    "           @NextelId, " +
                    "           @Endereco, " +
                    "           @Numero, " +
                    "           @Complemento, " +
                    "           @Bairro, " +
                    "           @Cidade, " +
                    "           @Estado, " +
                    "           @Cep, " +
                    "           @Ativo " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Email1", vo.Email1);
                oComm.Parameters.AddWithValue("@Email2", vo.Email2);
                oComm.Parameters.AddWithValue("@Telefone1", vo.Telefone1);
                oComm.Parameters.AddWithValue("@Telefone2", vo.Telefone2);
                oComm.Parameters.AddWithValue("@Telefone3", vo.Telefone3);
                oComm.Parameters.AddWithValue("@Telefone4", vo.Telefone4);
                oComm.Parameters.AddWithValue("@Celular", vo.Celular);
                oComm.Parameters.AddWithValue("@Fax", vo.Fax);
                oComm.Parameters.AddWithValue("@NextelId", vo.NextelId);
                oComm.Parameters.AddWithValue("@Endereco", vo.Endereco);
                oComm.Parameters.AddWithValue("@Numero", vo.Numero);
                oComm.Parameters.AddWithValue("@Complemento", vo.Complemento);
                oComm.Parameters.AddWithValue("@Bairro", vo.Bairro);
                oComm.Parameters.AddWithValue("@Cidade", vo.Cidade);
                oComm.Parameters.AddWithValue("@Estado", vo.Estado);
                oComm.Parameters.AddWithValue("@Cep", vo.Cep);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);


                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdRepresentante = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Representante vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Representantes] " +
                    "   SET " +
                    "       Nome=@Nome, " +
                    "       Email1=@Email1, " +
                    "       Email2=@Email2, " +
                    "       Telefone1=@Telefone1, " +
                    "       Telefone2=@Telefone2, " +
                    "       Telefone3=@Telefone3, " +
                    "       Telefone4=@Telefone4, " +
                    "       Celular=@Celular, " +
                    "       Fax=@Fax, " +
                    "       NextelId=@NextelId, " +
                    "       Endereco=@Endereco, " +
                    "       Numero=@Numero, " +
                    "       Complemento=@Complemento, " +
                    "       Bairro=@Bairro, " +
                    "       Cidade=@Cidade, " +
                    "       Estado=@Estado, " +
                    "       Cep=@Cep, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdRepresentante=@IdRepresentante"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdRepresentante", vo.IdRepresentante);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Email1", vo.Email1);
                oComm.Parameters.AddWithValue("@Email2", vo.Email2);
                oComm.Parameters.AddWithValue("@Telefone1", vo.Telefone1);
                oComm.Parameters.AddWithValue("@Telefone2", vo.Telefone2);
                oComm.Parameters.AddWithValue("@Telefone3", vo.Telefone3);
                oComm.Parameters.AddWithValue("@Telefone4", vo.Telefone4);
                oComm.Parameters.AddWithValue("@Celular", vo.Celular);
                oComm.Parameters.AddWithValue("@Fax", vo.Fax);
                oComm.Parameters.AddWithValue("@NextelId", vo.NextelId); 
                oComm.Parameters.AddWithValue("@Endereco", vo.Endereco);
                oComm.Parameters.AddWithValue("@Numero", vo.Numero);
                oComm.Parameters.AddWithValue("@Complemento", vo.Complemento);
                oComm.Parameters.AddWithValue("@Bairro", vo.Bairro);
                oComm.Parameters.AddWithValue("@Cidade", vo.Cidade);
                oComm.Parameters.AddWithValue("@Estado", vo.Estado);
                oComm.Parameters.AddWithValue("@Cep", vo.Cep);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Representantes] " +
                    "   WHERE " +
                    "       IdRepresentante=@IdRepresentante"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdRepresentante", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Representante Popular(DataRow row)
        {
            var vo = new VO.Representante();
            
            vo.IdRepresentante = (int)row["IdRepresentante"];
            vo.Nome = (string)row["Nome"];
            vo.Email1 = (string)row["Email1"];
            vo.Email2 = (string)row["Email2"];
            vo.Telefone1 = (string)row["Telefone1"];
            vo.Telefone2 = (string)row["Telefone2"];
            vo.Telefone3 = (string)row["Telefone3"];
            vo.Telefone4 = (string)row["Telefone4"];
            vo.Celular = (string)row["Celular"];
            vo.Fax = (string)row["Fax"];
            vo.NextelId = (string)row["NextelId"];
            vo.Endereco = (string)row["Endereco"];
            vo.Numero = (string)row["Numero"];
            vo.Complemento = (string)row["Complemento"];
            vo.Bairro = (string)row["Bairro"];
            vo.Cidade = (string)row["Cidade"];
            vo.Estado = (string)row["Estado"];
            vo.Cep = (string)row["Cep"];
            vo.Ativo = (bool)row["Ativo"];

            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.Representante> PopularLista(DataTable table)
        {
            var lista = new List<VO.Representante>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
