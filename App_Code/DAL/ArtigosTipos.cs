﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ArtigosTipos
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.ArtigoTipo Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.ArtigoTipo vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ArtigosTipos] " +
                    "   WHERE " + 
                    "       IdTipo=@IdTipo"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdTipo", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public List<VO.ArtigoTipo> Listar()
        {
            List<VO.ArtigoTipo> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ArtigosTipos] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        #endregion


        #region Popular

        private VO.ArtigoTipo Popular(DataRow row)
        {
            var vo = new VO.ArtigoTipo();

            vo.IdTipo = (int)row["IdTipo"];
            vo.Nome = (string)row["Nome"];
            vo.Css = (string)row["Css"];
            
            return vo;
        }

        private List<VO.ArtigoTipo> PopularLista(DataTable table)
        {
            var lista = new List<VO.ArtigoTipo>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion


    }
}
