﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Galerias
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Galeria Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Galeria vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Galerias] " +
                    "   WHERE " +
                    "       IdGaleria=@IdGaleria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdGaleria", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Galerias] " +
                    "   WHERE " +
                    "       Titulo like @pesquisa OR " +
                    "		Descricao like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Galeria> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Galeria> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Galerias] " +
                    "	WHERE IdGaleria IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdGaleria FROM [Galerias] " +
                    "		    WHERE IdGaleria NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdGaleria FROM [Galerias] " +
                    "				    WHERE " +
                    "					    Titulo like @pesquisa OR " +
                    "						Descricao like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "               Titulo like @pesquisa OR " +
                    "				Descricao like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Galeria Incluir(VO.Galeria vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Galerias] " +
                    "       ( " +
                    "           Titulo," +
                    "           Descricao," +
                    "           Ativo" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @Titulo," +
                    "           @Descricao," +
                    "           @Ativo" +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdGaleria = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Galeria vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Galerias] " +
                    "   SET " +
                    "       Titulo=@Titulo, " +
                    "       Descricao=@Descricao, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdGaleria=@IdGaleria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdGaleria", vo.IdGaleria);
                oComm.Parameters.AddWithValue("@Titulo", vo.Titulo);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Galerias] " +
                    "   WHERE " +
                    "       IdGaleria=@IdGaleria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdGaleria", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Galeria Popular(DataRow row)
        {
            var vo = new VO.Galeria();

            vo.IdGaleria = (int)row["IdGaleria"];
            vo.Titulo = (string)row["Titulo"];
            vo.Descricao = (string)row["Descricao"];
            vo.Imagens = new ImagensPorGalerias().ListarPorGaleria(vo.IdGaleria, "Titulo", true);
            vo.Videos = new VideosPorGalerias().ListarPorGaleria(vo.IdGaleria, "Titulo", true);
            vo.Ativo = (bool)row["Ativo"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.Galeria> PopularLista(DataTable table)
        {
            var lista = new List<VO.Galeria>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
