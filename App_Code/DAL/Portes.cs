﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Portes
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Porte Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Porte vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Especies] " +
                    "   WHERE " +
                    "       IdPorte=@IdPorte"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPorte", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public List<VO.Porte> Listar()
        {
            List<VO.Porte> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Especies] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion



        #region Popular

        private VO.Porte Popular(DataRow row)
        {
            var vo = new VO.Porte();

            vo.IdPorte = (int)row["IdPorte"];
            vo.IdEspecie = (int)row["IdEspecie"];
            vo.Nome = (string)row["Nome"];

            return vo;
        }

        private List<VO.Porte> PopularLista(DataTable table)
        {
            var lista = new List<VO.Porte>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion



    }
}
