﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Produtos
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Produto Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Produto vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Produtos] " +
                    "   WHERE " +
                    "       IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public VO.Produto ConsultarOutraVersao(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Produto vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Produtos] " +
                    "   WHERE " +
                    "       IdProdutoOutraVersao=@Id"
                    , oConn);

                oComm.Parameters.AddWithValue("@Id", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public VO.Produto Consultar(string nome)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Produto vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Produtos] " +
                    "   WHERE " +
                    "       Nome=@Nome"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", nome);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Produtos] " +
                    "   WHERE " +
                    "					    nome like @pesquisa OR " +
                    "					    Descricao like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Produto> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Produto> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Produtos] " +
                    "	WHERE IdProduto IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdProduto FROM [Produtos] " +
                    "		    WHERE IdProduto NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdProduto FROM [Produtos] " +
                    "				    WHERE " +
                    "					    nome like @pesquisa OR " +
                    "					    Descricao like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "					    nome like @pesquisa OR " +
                    "					    Descricao like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public List<VO.Produto> Listar()
        {
            List<VO.Produto> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Produtos] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public List<VO.Produto> Listar(int idEspecie)
        {
            List<VO.Produto> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Produtos] " +
                    "   WHERE " +
                    "        IdEspecie=@IdEspecie"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdEspecie", idEspecie);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion



        #region Inclusao, Alteração e Exclusão

        public VO.Produto Incluir(VO.Produto vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Produtos] " +
                    "       ( " +
                    //"           IdCategoria, " +
                    "           IdEspecie, " +
                    //"           IdProdutoImagem, " +
                    "           Nome, " +
                    "           Icones, " +
                    "           Descricao, " +
                    "           Embalagem, " +
                    "           Informacao, " +
                    "           EnergiaMetabolizavel, " +
                    "           FatorSobrepeso, " +
                    "           FatorPesoideial, " +
                    "           FatorMagro, " +
                    "           IndiceDeCalculo, " +
                    "           IdTipo, " +
                    "           Obesidade, " +
                    "           Cor, " +
                    "           Ativo, " +
                    "           LocalExibicao, " +
                    "           TabelaQuantidadeDiaria " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    //"           @IdCategoria, " +
                    "           @IdEspecie, " +
                    //"           @IdProdutoImagem, " +
                    "           @Nome, " +
                    "           @Icones, " +
                    "           @Descricao, " +
                    "           @Embalagem, " +
                    "           @Informacao, " +
                    "           @EnergiaMetabolizavel, " +
                    "           @FatorSobrepeso, " +
                    "           @FatorPesoideial, " +
                    "           @FatorMagro, " +
                    "           @IndiceDeCalculo, " +
                    "           @IdTipo, " +
                    "           @Obesidade, " +
                    "           @Cor, " +
                    "           @Ativo, " +
                    "           @LocalExibicao, " +
                    "           @TabelaQuantidadeDiaria " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                //oComm.Parameters.AddWithValue("@IdCategoria", vo.Categoria.IdCategoria);
                oComm.Parameters.AddWithValue("@IdEspecie", vo.Especie.IdEspecie);
                //oComm.Parameters.AddWithValue("@IdProdutoImagem", vo.FotoDoProduto.IdImagem);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Icones", vo.Icones);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@Embalagem", vo.Embalagem);
                oComm.Parameters.AddWithValue("@Informacao", vo.Informacao);
                oComm.Parameters.AddWithValue("@EnergiaMetabolizavel", vo.EnergiaMetabolizavel);
                
                oComm.Parameters.AddWithValue("@FatorSobrepeso", vo.FatorSobrepeso);
                oComm.Parameters.AddWithValue("@FatorPesoideial", vo.FatorPesoideial);
                oComm.Parameters.AddWithValue("@FatorMagro", vo.FatorMagro);
                oComm.Parameters.AddWithValue("@IndiceDeCalculo", vo.IndiceDeCalculo);

                oComm.Parameters.AddWithValue("@IdTipo", vo.Tipo.IdTipo);
                oComm.Parameters.AddWithValue("@Obesidade", vo.Obesidade);
                oComm.Parameters.AddWithValue("@Cor", vo.Cor);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);
                oComm.Parameters.AddWithValue("@LocalExibicao", vo.LocalExibicao);
                oComm.Parameters.AddWithValue("@TabelaQuantidadeDiaria", vo.TabelaQuantidadeDiaria);
                

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdProduto = Convert.ToInt32(obj);

                LimparOutrasVersoes(vo.IdProduto, vo.IdOutraVersao);
            }

            return vo;
        }

        public void Alterar(VO.Produto vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Produtos] " +
                    "   SET " +
                    "       IdEspecie=@IdEspecie, " +
                    //"       IdCategoria=@IdCategoria, " +
                    //"       IdProdutoImagem=@IdProdutoImagem, " +
                    "       Nome=@Nome, " +
                    "       Icones=@Icones, " +
                    "       Descricao=@Descricao, " +
                    "       Embalagem=@Embalagem, " +
                    "       Informacao=@Informacao, " +
                    "       EnergiaMetabolizavel=@EnergiaMetabolizavel, " +

                    "       FatorSobrepeso=@FatorSobrepeso, " +
                    "       FatorPesoideial=@FatorPesoideial, " +
                    "       FatorMagro=@FatorMagro, " +
                    "       IndiceDeCalculo=@IndiceDeCalculo, " +
                    
                    "       IdTipo=@IdTipo, " +
                    "       Obesidade=@Obesidade, " +
                    "       Cor=@Cor, " +
                    "       Ativo=@Ativo, " +
                    "       LocalExibicao=@LocalExibicao, " +
                    "       TabelaQuantidadeDiaria=@TabelaQuantidadeDiaria " +
                    "   WHERE " +
                    "       IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", vo.IdProduto);
                //oComm.Parameters.AddWithValue("@IdCategoria", vo.Categoria.IdCategoria);
                oComm.Parameters.AddWithValue("@IdEspecie", vo.Especie.IdEspecie);
                //oComm.Parameters.AddWithValue("@IdProdutoImagem", vo.FotoDoProduto.IdImagem);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Icones", vo.Icones);
                oComm.Parameters.AddWithValue("@Descricao", vo.Descricao);
                oComm.Parameters.AddWithValue("@Embalagem", vo.Embalagem);
                oComm.Parameters.AddWithValue("@Informacao", vo.Informacao);
                oComm.Parameters.AddWithValue("@EnergiaMetabolizavel", vo.EnergiaMetabolizavel);

                oComm.Parameters.AddWithValue("@FatorSobrepeso", vo.FatorSobrepeso);
                oComm.Parameters.AddWithValue("@FatorPesoideial", vo.FatorPesoideial);
                oComm.Parameters.AddWithValue("@FatorMagro", vo.FatorMagro);
                oComm.Parameters.AddWithValue("@IndiceDeCalculo", vo.IndiceDeCalculo);

                oComm.Parameters.AddWithValue("@IdTipo", vo.Tipo.IdTipo);
                oComm.Parameters.AddWithValue("@Obesidade", vo.Obesidade);
                oComm.Parameters.AddWithValue("@Cor", vo.Cor);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);
                oComm.Parameters.AddWithValue("@LocalExibicao", vo.LocalExibicao);
                oComm.Parameters.AddWithValue("@TabelaQuantidadeDiaria", vo.TabelaQuantidadeDiaria);

                oConn.Open();
                oComm.ExecuteNonQuery();

                LimparOutrasVersoes(vo.IdProduto, vo.IdOutraVersao);
            }
        }


        private void LimparOutrasVersoes(int IdProduto, int IdOutraVersao)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    
                    "UPDATE [Produtos] " +
                    "   SET " +
                    "       IdOutraVersao=0 " +
                    "   WHERE " +
                    "       IdProduto=" + IdProduto + ";" +
                    "UPDATE [Produtos] " +
                    "   SET " +
                    "       IdOutraVersao=0 " +
                    "   WHERE " +
                    "       IdProduto=" + IdOutraVersao + ";" +

                    
                    "UPDATE [Produtos] " +
                    "   SET " +
                    "       IdOutraVersao= " + IdProduto +
                    "   WHERE " +
                    "       IdProduto=" + IdOutraVersao + ";" +
                    "UPDATE [Produtos] " +
                    "   SET " +
                    "       IdOutraVersao= " + IdOutraVersao +
                    "   WHERE " +
                    "       IdProduto=" + IdProduto + ";"
                    , oConn);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Produtos] " +
                    "   WHERE " +
                    "       IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion



        #region Popular

        private VO.Produto Popular(DataRow row)
        {
            var vo = new VO.Produto();

            vo.IdProduto = (int)row["IdProduto"];
            vo.IdOutraVersao = (int)row["IdOutraVersao"];
            //vo.Categoria = new DAL.ProdutosCategorias().Consultar(Convert.ToInt32(row["IdCategoria"]));
            vo.Categorias = new DAL.ProdutosCategorias().ListarPorProduto(vo.IdProduto);
            vo.Especie = new DAL.Especies().Consultar(Convert.ToInt32(row["IdEspecie"]));
            vo.Doenca = new DAL.Doencas().ListarPorProduto(vo.IdProduto);
            //vo.FotoDoProduto = new DAL.Imagens().Consultar(Convert.ToInt32(row["IdProdutoImagem"]));
            vo.Nome = (string)row["Nome"];
            vo.Icones = (string)row["Icones"];
            vo.Descricao = (string)row["Descricao"];
            vo.Embalagem = (string)row["Embalagem"];
            vo.Informacao = (string)row["Informacao"];
            vo.EnergiaMetabolizavel = (int)row["EnergiaMetabolizavel"];

            vo.FatorSobrepeso = (decimal)row["FatorSobrepeso"];
            vo.FatorPesoideial = (decimal)row["FatorPesoideial"];
            vo.FatorMagro = (decimal)row["FatorMagro"];
            vo.IndiceDeCalculo = (decimal)row["IndiceDeCalculo"];

            vo.Tipo = new DAL.ProdutosTipos().Consultar(row["IdTipo"].ToString());
            vo.Obesidade = (bool)row["Obesidade"];
            vo.Cor = (string)row["Cor"];

            vo.Termos = new DAL.Termos().ListarPorProduto(vo.IdProduto);

            vo.Ativo = (bool)row["Ativo"];

            vo.LocalExibicao = (string)row["LocalExibicao"];
            vo.TabelaQuantidadeDiaria = (string)row["TabelaQuantidadeDiaria"];

            return vo;
        }

        private List<VO.Produto> PopularLista(DataTable table)
        {
            var lista = new List<VO.Produto>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
