﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class PerguntasRespostasCategorias
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.PerguntaRespostaCategoria Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.PerguntaRespostaCategoria vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [PerguntasRespostasCategorias] " +
                    "   WHERE " + 
                    "       IdCategoria=@IdCategoria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdCategoria", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public List<VO.PerguntaRespostaCategoria> Listar()
        {
            List<VO.PerguntaRespostaCategoria> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [PerguntasRespostasCategorias] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        #endregion


        #region Popular

        private VO.PerguntaRespostaCategoria Popular(DataRow row)
        {
            var vo = new VO.PerguntaRespostaCategoria();

            vo.IdCategoria = (int)row["IdCategoria"];
            vo.Nome = (string)row["Nome"];
            
            return vo;
        }

        private List<VO.PerguntaRespostaCategoria> PopularLista(DataTable table)
        {
            var lista = new List<VO.PerguntaRespostaCategoria>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion


    }
}
