﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Autores
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Autor Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Autor vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Autores] " +
                    "   WHERE " + 
                    "       IdAutor=@IdAutor"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAutor", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Autores] " +
                    "   WHERE " +
                    "					    Assinatura like @pesquisa OR " +
                    "					    Nome like @pesquisa OR " +
                    "					    Email like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Autor> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Autor> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Autores] " +
                    "	WHERE IdAutor IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdAutor FROM [Autores] " +
                    "		    WHERE IdAutor NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdAutor FROM [Autores] " +
                    "				    WHERE " +
                    "					    Assinatura like @pesquisa OR " +
                    "					    Nome like @pesquisa OR " +
                    "					    Email like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "					    Assinatura like @pesquisa OR " +
                    "					    Nome like @pesquisa OR " +
                    "					    Email like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Autor Incluir(VO.Autor vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Autores] " +
                    "       ( " +
                    "           Assinatura, " +
                    "           Nome, " +
                    "           Email, " +
                    "           Curriculo " +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @Assinatura, " +
                    "           @Nome, " +
                    "           @Email, " +
                    "           @Curriculo " +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@Assinatura", vo.Assinatura);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Email", vo.Email);
                oComm.Parameters.AddWithValue("@Curriculo", vo.Curriculo);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdAutor = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Autor vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Autores] " +
                    "   SET " +
                    "       Assinatura=@Assinatura, " +
                    "       Nome=@Nome, " +
                    "       Email=@Email, " +
                    "       Curriculo=@Curriculo " +
                    "   WHERE " +
                    "       IdAutor=@IdAutor"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAutor", vo.IdAutor);
                oComm.Parameters.AddWithValue("@Assinatura", vo.Assinatura);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);
                oComm.Parameters.AddWithValue("@Email", vo.Email);
                oComm.Parameters.AddWithValue("@Curriculo", vo.Curriculo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Autores] " +
                    "   WHERE " +
                    "       IdAutor=@IdAutor"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdAutor", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Autor Popular(DataRow row)
        {
            var vo = new VO.Autor();
            
            vo.IdAutor = (int)row["IdAutor"];
            vo.Assinatura = (string)row["Assinatura"];
            vo.Nome = (string)row["Nome"];
            vo.Email = (string)row["Email"];
            vo.Curriculo = (string)row["Curriculo"];
           
            return vo;
        }

        private List<VO.Autor> PopularLista(DataTable table)
        {
            var lista = new List<VO.Autor>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
