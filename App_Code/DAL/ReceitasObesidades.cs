﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ReceitasObesidades
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.ReceitaObesidade Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.ReceitaObesidade vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [ReceitasObesidades] " +
                    "   WHERE " +
                    "       IdReceita=@IdReceita"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdReceita", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }



        public int TotalDeRegistros(string pesquisa, string idTipoDeRelatorio, int idEspecie, int idProduto, DateTime dataIncial, DateTime dataFinal)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                var sql =
                    "SELECT COUNT(*) AS 'total' FROM [ReceitasObesidades] " +
                    "   WHERE ";

                if (idTipoDeRelatorio.Trim().Length > 0)
                    sql += "					    (idTipoDeRelatorio='" + idTipoDeRelatorio + "') AND";

                if (idProduto > 0)
                    sql += "					    (IdAlimentoSeco=" + idProduto + " OR IdAlimentoUmido=" + idProduto + ") AND";

                if (idEspecie > 0)
                    sql += "					    idEspecie=" + idEspecie + " AND";

                sql +=
                    "					    DataInclusao BETWEEN @DataInicial AND @DataFinal AND " +
                    "					    ( " +
                    "					    Especie like @pesquisa OR " +
                    "					    Raca like @pesquisa OR " +
                    "					    NomeClinica like @pesquisa OR " +
                    "					    NomeDoVeterinario like @pesquisa OR " +
                    "					    SobrenomeDoVeterinario like @pesquisa OR " +
                    "					    CRMV like @pesquisa OR " +
                    "					    EmailDoVeterinario like @pesquisa OR " +
                    "					    NomeDoProprietario like @pesquisa OR " +
                    "					    EmailDoProprietario like @pesquisa OR " +
                    "					    NomeDoAnimal like @pesquisa OR " +
                    "					    Sexo like @pesquisa OR " +
                    "					    Porte like @pesquisa OR " +
                    "					    AlimentoSecoNome like @pesquisa OR " +
                    "					    AlimentoUmidoNome like @pesquisa OR " +
                    "					    RecomendacoesAdicionais like @pesquisa " +
                    "					    ) ";

                SqlCommand oComm = new SqlCommand(sql, oConn);

                oComm.Parameters.AddWithValue("@DataInicial", dataIncial.ToString("yyyy-MM-dd HH:mm:ss"));
                oComm.Parameters.AddWithValue("@DataFinal", dataFinal.ToString("yyyy-MM-dd HH:mm:ss"));

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.ReceitaObesidade> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente, string idTipoDeRelatorio, int idEspecie, int idProduto, DateTime dataIncial, DateTime dataFinal)
        {
            List<VO.ReceitaObesidade> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                var sql =
                    "SELECT * FROM [ReceitasObesidades] " +
                    "	WHERE IdReceita IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdReceita FROM [ReceitasObesidades] " +
                    "		    WHERE IdReceita NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdReceita FROM [ReceitasObesidades] " +
                    "				    WHERE ";

                if (idTipoDeRelatorio.Trim().Length > 0)
                    sql += "					    (idTipoDeRelatorio='" + idTipoDeRelatorio + "') AND";

                if (idProduto > 0)
                    sql += "					    (IdAlimentoSeco=" + idProduto + " OR IdAlimentoUmido=" + idProduto + ") AND";

                if (idEspecie > 0)
                    sql += "					    idEspecie=" + idEspecie + " AND";

                sql +=
                    "					    DataInclusao BETWEEN @DataInicial AND @DataFinal AND " +
                    "					    ( " +
                    "					    Especie like @pesquisa OR " +
                    "					    Raca like @pesquisa OR " +
                    "					    NomeClinica like @pesquisa OR " +
                    "					    NomeDoVeterinario like @pesquisa OR " +
                    "					    SobrenomeDoVeterinario like @pesquisa OR " +
                    "					    CRMV like @pesquisa OR " +
                    "					    EmailDoVeterinario like @pesquisa OR " +
                    "					    NomeDoProprietario like @pesquisa OR " +
                    "					    EmailDoProprietario like @pesquisa OR " +
                    "					    NomeDoAnimal like @pesquisa OR " +
                    "					    Sexo like @pesquisa OR " +
                    "					    Porte like @pesquisa OR " +
                    "					    AlimentoSecoNome like @pesquisa OR " +
                    "					    AlimentoUmidoNome like @pesquisa OR " +
                    "					    RecomendacoesAdicionais like @pesquisa " +
                    "					    ) " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( ";

                if (idTipoDeRelatorio.Trim().Length > 0)
                    sql += "					    (idTipoDeRelatorio='" + idTipoDeRelatorio + "') AND";

                if (idProduto > 0)
                    sql += "					    (IdAlimentoSeco=" + idProduto + " OR IdAlimentoUmido=" + idProduto + ") AND";

                if (idEspecie > 0)
                    sql += "					    idEspecie=" + idEspecie + " AND";

                sql += 
                    "					    DataInclusao BETWEEN @DataInicial AND @DataFinal AND" +
                    "					    ( " +
                    "					    Especie like @pesquisa OR " +
                    "					    Raca like @pesquisa OR " +
                    "					    NomeClinica like @pesquisa OR " +
                    "					    NomeDoVeterinario like @pesquisa OR " +
                    "					    SobrenomeDoVeterinario like @pesquisa OR " +
                    "					    CRMV like @pesquisa OR " +
                    "					    EmailDoVeterinario like @pesquisa OR " +
                    "					    NomeDoProprietario like @pesquisa OR " +
                    "					    EmailDoProprietario like @pesquisa OR " +
                    "					    NomeDoAnimal like @pesquisa OR " +
                    "					    Sexo like @pesquisa OR " +
                    "					    Porte like @pesquisa OR " +
                    "					    AlimentoSecoNome like @pesquisa OR " +
                    "					    AlimentoUmidoNome like @pesquisa OR " +
                    "					    RecomendacoesAdicionais like @pesquisa " +
                    "					    ) " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao;

                SqlCommand oComm = new SqlCommand(sql, oConn);

                oComm.Parameters.AddWithValue("@DataInicial", dataIncial.ToString("yyyy-MM-dd HH:mm:ss"));
                oComm.Parameters.AddWithValue("@DataFinal", dataFinal.ToString("yyyy-MM-dd HH:mm:ss"));
                
                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }





        public DataTable Exportacao()
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                var sql =
                    "SELECT " +
                    "   TipoDeRelatorio," +
                    "   IdReceita, " +
                    "   Especie, " +
                    "   Raca, " +

                    "   NomeClinica, " +
                    "   NomeDoVeterinario, " +
                    "   SobrenomeDoVeterinario, " +
                    "   Crmv, " +
                    "   CrmvUf, " +

                    "   NomeDoProprietario, " +
                    "   EmailDoProprietario, " +

                    "   NomeDoAnimal, " +
                    "   Sexo, " +
                    "   Porte, " +
                    "   PesoAtual, " +
                    "   PesoPretendido, " +
                    "   QuantidadeDeSemanas, " +

                    "   AlimentoSecoNome, " +
                    "   AlimentoSecoQuantidade, " +
                    "   AlimentoUmidoNome, " +
                    "   AlimentoUmidoQuantidade, " +

                    "   RecomendacoesAdicionais, " +

                    "   EnviadoPorEmail, " +
                    "   Impresso, " +
                    "   DataInclusao " +

                    "FROM [ReceitasObesidades] ";


                SqlCommand oComm = new SqlCommand(sql, oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return table;
            }
        }

        #endregion



        #region Inclusao, Alteração e Exclusão

        public VO.ReceitaObesidade Incluir(VO.ReceitaObesidade vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [ReceitasObesidades] " +
                    "       ( " +
                    "           IdTipoDeRelatorio," +
                    "           TipoDeRelatorio," +
                    "           IdEspecie," +
                    "           Especie," +
                    "           IdRaca," +
                    "           Raca," +
                    "           IdVeterinario," +
                    "           userToken," +
                    "           NomeClinica," +
                    "           NomeDoVeterinario," +
                    "           SobrenomeDoVeterinario," +
                    "           Crmv," +
                    "           CrmvUf," +
                    "           EmailDoVeterinario," +
                    "           EspecialidadeAreaAtuacao," +
                    "           Endereco," +
                    "           Numero," +
                    "           Complemento," +
                    "           Bairro," +
                    "           Cidade," +
                    "           Cep," +
                    "           Estado," +
                    "           TelefoneComercial," +
                    "           NomeDoProprietario," +
                    "           EmailDoProprietario," +
                    "           NomeDoAnimal," +
                    "           PesoAtual," +
                    "           SexoIndice," +
                    "           Sexo," +
                    "           Porte," +
                    "           ScoreCorporal," +
                    "           IdDoenca," +
                    "           Doenca," +
                    "           QuantidadeEnergetica," +
                    "           PesoPretendido," +
                    "           PerdaPorSemana," +
                    "           QuantidadeDeSemanas," +
                    "           IdAlimentoSeco," +
                    "           IdAlimentoUmido," +
                    "           AlimentoSecoNome," +
                    "           AlimentoUmidoNome," +
                    "           AlimentoSecoEM," +
                    "           AlimentoUmidoEM," +
                    "           AlimentoSecoQuantidade," +
                    "           AlimentoUmidoQuantidade," +
                    "           RecomendacoesAdicionais," +
                    "           Impresso," +
                    "           EnviadoPorEmail" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdTipoDeRelatorio," +
                    "           @TipoDeRelatorio," +
                    "           @IdEspecie," +
                    "           @Especie," +
                    "           @IdRaca," +
                    "           @Raca," +
                    "           @IdVeterinario," +
                    "           @userToken," +
                    "           @NomeClinica," +
                    "           @NomeDoVeterinario," +
                    "           @SobrenomeDoVeterinario," +
                    "           @Crmv," +
                    "           @CrmvUf," +
                    "           @EmailDoVeterinario," +
                    "           @EspecialidadeAreaAtuacao," +
                    "           @Endereco," +
                    "           @Numero," +
                    "           @Complemento," +
                    "           @Bairro," +
                    "           @Cidade," +
                    "           @Cep," +
                    "           @Estado," +
                    "           @TelefoneComercial," +
                    "           @NomeDoProprietario," +
                    "           @EmailDoProprietario," +
                    "           @NomeDoAnimal," +
                    "           @PesoAtual," +
                    "           @SexoIndice," +
                    "           @Sexo," +
                    "           @Porte," +
                    "           @ScoreCorporal," +
                    "           @IdDoenca," +
                    "           @Doenca," +
                    "           @QuantidadeEnergetica," +
                    "           @PesoPretendido," +
                    "           @PerdaPorSemana," +
                    "           @QuantidadeDeSemanas," +
                    "           @IdAlimentoSeco," +
                    "           @IdAlimentoUmido," +
                    "           @AlimentoSecoNome," +
                    "           @AlimentoUmidoNome," +
                    "           @AlimentoSecoEM," +
                    "           @AlimentoUmidoEM," +
                    "           @AlimentoSecoQuantidade," +
                    "           @AlimentoUmidoQuantidade," +
                    "           @RecomendacoesAdicionais," +
                    "           @Impresso," +
                    "           @EnviadoPorEmail" +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdTipoDeRelatorio", vo.IdTipoDeRelatorio);
                oComm.Parameters.AddWithValue("@TipoDeRelatorio", vo.TipoDeRelatorio);
                oComm.Parameters.AddWithValue("@IdEspecie", vo.IdEspecie);
                oComm.Parameters.AddWithValue("@Especie", vo.Especie);
                oComm.Parameters.AddWithValue("@IdRaca", vo.IdRaca);
                oComm.Parameters.AddWithValue("@Raca", vo.Raca);
                oComm.Parameters.AddWithValue("@IdVeterinario", vo.IdVeterinario);
                oComm.Parameters.AddWithValue("@userToken", vo.userToken);
                oComm.Parameters.AddWithValue("@NomeClinica", vo.NomeClinica);
                oComm.Parameters.AddWithValue("@NomeDoVeterinario", vo.NomeDoVeterinario);
                oComm.Parameters.AddWithValue("@SobrenomeDoVeterinario", vo.SobrenomeDoVeterinario);
                oComm.Parameters.AddWithValue("@Crmv", vo.Crmv);
                oComm.Parameters.AddWithValue("@CrmvUf", vo.CrmvUf);
                oComm.Parameters.AddWithValue("@EmailDoVeterinario", vo.EmailDoVeterinario);
                oComm.Parameters.AddWithValue("@EspecialidadeAreaAtuacao", vo.EspecialidadeAreaAtuacao);
                oComm.Parameters.AddWithValue("@Endereco", vo.Endereco);
                oComm.Parameters.AddWithValue("@Numero", vo.Numero);
                oComm.Parameters.AddWithValue("@Complemento", vo.Complemento);
                oComm.Parameters.AddWithValue("@Bairro", vo.Bairro);
                oComm.Parameters.AddWithValue("@Cidade", vo.Cidade);
                oComm.Parameters.AddWithValue("@Cep", vo.Cep);
                oComm.Parameters.AddWithValue("@Estado", vo.Estado);
                oComm.Parameters.AddWithValue("@TelefoneComercial", vo.TelefoneComercial);
                oComm.Parameters.AddWithValue("@NomeDoProprietario", vo.NomeDoProprietario);
                oComm.Parameters.AddWithValue("@EmailDoProprietario", vo.EmailDoProprietario);
                oComm.Parameters.AddWithValue("@NomeDoAnimal", vo.NomeDoAnimal);
                oComm.Parameters.AddWithValue("@PesoAtual", vo.PesoAtual);
                oComm.Parameters.AddWithValue("@SexoIndice", vo.SexoIndice);
                oComm.Parameters.AddWithValue("@Sexo", vo.Sexo);
                oComm.Parameters.AddWithValue("@Porte", vo.Porte);
                oComm.Parameters.AddWithValue("@ScoreCorporal", vo.ScoreCorporal);
                oComm.Parameters.AddWithValue("@IdDoenca", vo.IdDoenca);
                oComm.Parameters.AddWithValue("@Doenca", vo.Doenca);
                oComm.Parameters.AddWithValue("@QuantidadeEnergetica", vo.QuantidadeEnergetica);
                oComm.Parameters.AddWithValue("@PesoPretendido", vo.PesoPretendido);
                oComm.Parameters.AddWithValue("@PerdaPorSemana", vo.PerdaPorSemana);
                oComm.Parameters.AddWithValue("@QuantidadeDeSemanas", vo.QuantidadeDeSemanas);
                oComm.Parameters.AddWithValue("@IdAlimentoSeco", vo.IdAlimentoSeco);
                oComm.Parameters.AddWithValue("@IdAlimentoUmido", vo.IdAlimentoUmido);
                oComm.Parameters.AddWithValue("@AlimentoSecoNome", vo.AlimentoSecoNome);
                oComm.Parameters.AddWithValue("@AlimentoUmidoNome", vo.AlimentoUmidoNome);
                oComm.Parameters.AddWithValue("@AlimentoSecoEM", vo.AlimentoSecoEM);
                oComm.Parameters.AddWithValue("@AlimentoUmidoEM", vo.AlimentoUmidoEM);
                oComm.Parameters.AddWithValue("@AlimentoSecoQuantidade", vo.AlimentoSecoQuantidade);
                oComm.Parameters.AddWithValue("@AlimentoUmidoQuantidade", vo.AlimentoUmidoQuantidade);
                oComm.Parameters.AddWithValue("@RecomendacoesAdicionais", vo.RecomendacoesAdicionais);
                oComm.Parameters.AddWithValue("@Impresso", vo.Impresso);
                oComm.Parameters.AddWithValue("@EnviadoPorEmail", vo.EnviadoPorEmail);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdReceita = Convert.ToInt32(obj);
            }

            return vo;
        }


        public void AlterarImpresso(int idReceita, bool status)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [ReceitasObesidades] " +
                    "   SET " +
                    "       Impresso=@Impresso " +
                    "   WHERE " +
                    "       IdReceita=@IdReceita"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdReceita", idReceita);
                oComm.Parameters.AddWithValue("@Impresso", status);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void AlterarEmail(int idReceita, bool status, string email)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [ReceitasObesidades] " +
                    "   SET " +
                    "       EmailDoProprietario=@EmailDoProprietario, " +
                    "       EnviadoPorEmail=@EnviadoPorEmail " +
                    "   WHERE " +
                    "       IdReceita=@IdReceita"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdReceita", idReceita);
                oComm.Parameters.AddWithValue("@EnviadoPorEmail", status);
                oComm.Parameters.AddWithValue("@EmailDoProprietario", email);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.ReceitaObesidade Popular(DataRow row)
        {
            var vo = new VO.ReceitaObesidade();

            vo.IdReceita = (int)row["IdReceita"];
            vo.IdTipoDeRelatorio = (string)row["IdTipoDeRelatorio"];
            vo.TipoDeRelatorio = (string)row["TipoDeRelatorio"];
            vo.IdEspecie = Convert.ToInt32(row["IdEspecie"]);
            vo.Especie = (string)row["Especie"];
            vo.IdRaca = Convert.ToInt32(row["IdRaca"]);
            vo.Raca = (string)row["Raca"];
            vo.IdVeterinario = (int)row["IdVeterinario"];
            vo.userToken = (string)row["userToken"];
            vo.NomeClinica = (string)row["NomeClinica"];
            vo.NomeDoVeterinario = (string)row["NomeDoVeterinario"];
            vo.SobrenomeDoVeterinario = (string)row["SobrenomeDoVeterinario"];
            vo.Crmv = (string)row["Crmv"];
            vo.CrmvUf = (string)row["CrmvUf"];
            vo.EmailDoVeterinario = (string)row["EmailDoVeterinario"];
            vo.EspecialidadeAreaAtuacao = (string)row["EspecialidadeAreaAtuacao"];
            vo.Endereco = (string)row["Endereco"];
            vo.Numero = (string)row["Numero"];
            vo.Complemento = (string)row["Complemento"];
            vo.Bairro = (string)row["Bairro"];
            vo.Cidade = (string)row["Cidade"];
            vo.Cep = (string)row["Cep"];
            vo.Estado = (string)row["Estado"];
            vo.TelefoneComercial = (string)row["TelefoneComercial"];
            vo.NomeDoProprietario = (string)row["NomeDoProprietario"];
            vo.EmailDoProprietario = (string)row["EmailDoProprietario"];
            vo.NomeDoAnimal = (string)row["NomeDoAnimal"];
            vo.PesoAtual = Convert.ToDouble(row["PesoAtual"]);
            vo.SexoIndice = (int)row["SexoIndice"];
            vo.Sexo = (string)row["Sexo"];
            vo.Porte = (string)row["Porte"];
            vo.ScoreCorporal = Convert.ToInt32(row["ScoreCorporal"]);
            vo.IdDoenca = Convert.ToInt32(row["IdDoenca"]);
            vo.Doenca = (string)row["Doenca"];
            vo.QuantidadeEnergetica = (int)row["QuantidadeEnergetica"];
            vo.PesoPretendido = Convert.ToDouble(row["PesoPretendido"]);
            vo.PerdaPorSemana = Convert.ToDouble(row["PerdaPorSemana"]);
            vo.QuantidadeDeSemanas = (int)row["QuantidadeDeSemanas"];
            vo.IdAlimentoSeco = (int)row["IdAlimentoSeco"];
            vo.IdAlimentoUmido = (int)row["IdAlimentoUmido"];
            vo.AlimentoSecoNome = (string)row["AlimentoSecoNome"];
            vo.AlimentoUmidoNome = (string)row["AlimentoUmidoNome"];
            vo.AlimentoSecoEM = (int)row["AlimentoSecoEM"];
            vo.AlimentoUmidoEM = (int)row["AlimentoUmidoEM"];
            vo.AlimentoSecoQuantidade = Convert.ToInt32(row["AlimentoSecoQuantidade"]);
            vo.AlimentoUmidoQuantidade = Convert.ToInt32(row["AlimentoUmidoQuantidade"]);
            vo.RecomendacoesAdicionais = (string)row["RecomendacoesAdicionais"];
            vo.Impresso = (bool)row["Impresso"];
            vo.EnviadoPorEmail = (bool)row["EnviadoPorEmail"];
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.ReceitaObesidade> PopularLista(DataTable table)
        {
            var lista = new List<VO.ReceitaObesidade>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
