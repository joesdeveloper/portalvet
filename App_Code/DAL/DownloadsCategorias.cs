﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class DownloadsCategorias
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.DownloadCategoria Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.DownloadCategoria vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [DownloadsCategorias] " +
                    "   WHERE " + 
                    "       IdCategoria=@IdCategoria"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdCategoria", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public List<VO.DownloadCategoria> Listar()
        {
            List<VO.DownloadCategoria> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [DownloadsCategorias] "
                    , oConn);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }


        #endregion



        #region Popular

        private VO.DownloadCategoria Popular(DataRow row)
        {
            var vo = new VO.DownloadCategoria();

            vo.IdCategoria = (int)row["IdCategoria"];
            vo.Nome = (string)row["Nome"];

            return vo;
        }

        private List<VO.DownloadCategoria> PopularLista(DataTable table)
        {
            var lista = new List<VO.DownloadCategoria>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion
    }
}
