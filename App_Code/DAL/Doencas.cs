﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class Doencas
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.Doenca Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Doenca vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Doencas] " +
                    "   WHERE " + 
                    "       IdDoenca=@IdDoenca"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdDoenca", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public VO.Doenca Consultar(string nome)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.Doenca vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Doencas] " +
                    "   WHERE " +
                    "       Nome=@Nome"
                    , oConn);

                oComm.Parameters.AddWithValue("@Nome", nome);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }

        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [Doencas] as d " +
                    "   INNER JOIN [Especies] AS e ON d.IdEspecie=e.IdEspecie " +
                    "   WHERE " +
                    "       d.Nome like @pesquisa OR " +
                    "       e.Nome like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.Doenca> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.Doenca> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT d.* FROM [Doencas] AS d " +
                    "   INNER JOIN [Especies] AS e ON d.IdEspecie=e.IdEspecie " +
                    "	WHERE d.IdDoenca IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdDoenca FROM [Doencas] AS d2 " +
                    "           INNER JOIN [Especies] AS e2 ON d2.IdEspecie=e2.IdEspecie " +
                    "		    WHERE IdDoenca NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdDoenca FROM [Doencas] AS d3 " +
                    "                   INNER JOIN [Especies] AS e3 ON d3.IdEspecie=e3.IdEspecie " +
                    "				    WHERE " +
                    "					    d3.Nome like @pesquisa OR " +
                    "					    e3.Nome like @pesquisa " +
                    "                   ORDER BY d3." + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "               d2.Nome like @pesquisa OR " +
                    "               e2.Nome like @pesquisa " +
                    "			) " +
                    "		    ORDER BY d2." + ordenacao +
                    "	) OR " +
                    "   d.Nome like @pesquisa OR " +
                    "   e.Nome like @pesquisa " +
                    "	ORDER BY d." + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }



        public List<VO.Doenca> ListarPorProdutosExistentes(int idEspecie)
        {
            List<VO.Doenca> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [Doencas] " +
                    "	WHERE " +
                    "       IdEspecie=@IdEspecie AND " +
                    "       IdDoenca IN (SELECT DISTINCT IdDoenca FROM [DoencasPorProdutos]) " +
                    "   ORDER BY Nome"
                    , oConn);

                oComm.Parameters.AddWithValue("@idEspecie", idEspecie);


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public List<VO.Doenca> ListarPorCategoria(int idCategoria)
        {
            List<VO.Doenca> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT d.* FROM [Doencas] AS d " +
                    "	INNER JOIN [DoencasPorCategorias] AS dc ON d.IdDoenca=dc.IdDoenca " +
                    "   WHERE " +
                    "       dc.IdCategoria=@idCategoria"
                    , oConn);

                oComm.Parameters.AddWithValue("@idCategoria", idCategoria);


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

	

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.Doenca Incluir(VO.Doenca vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [Doencas] " +
                    "       ( " +
                    "           IdEspecie, " +
                    "           Nome" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdEspecie, " +
                    "           @Nome" +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdEspecie", vo.IdEspecie);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdDoenca = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.Doenca vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [Doencas] " +
                    "   SET " +
                    "       IdEspecie=@IdEspecie, " +
                    "       Nome=@Nome " +
                    "   WHERE " +
                    "       IdDoenca=@IdDoenca"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdDoenca", vo.IdDoenca);
                oComm.Parameters.AddWithValue("@IdEspecie", vo.IdEspecie);
                oComm.Parameters.AddWithValue("@Nome", vo.Nome);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [Doencas] " +
                    "   WHERE " +
                    "       IdDoenca=@IdDoenca"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdDoenca", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.Doenca Popular(DataRow row)
        {
            var vo = new VO.Doenca();
            
            vo.IdDoenca = (int)row["IdDoenca"];
            vo.IdEspecie = (int)row["IdEspecie"];
            vo.Nome = (string)row["Nome"];
            
            return vo;
        }

        private List<VO.Doenca> PopularLista(DataTable table)
        {
            var lista = new List<VO.Doenca>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion



        #region Doenca Por Produto

        public List<VO.Doenca> ListarPorProduto(int IdProduto)
        {
            List<VO.Doenca> lista = null;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT t.* FROM [DoencasPorProdutos] AS ta " +
                    "	INNER JOIN [Doencas] AS t  " +
                    "		ON ta.IdDoenca=t.IdDoenca " +
                    "	WHERE " +
                    "		ta.IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        public void IncluirPorProduto(int IdProduto, int IdDoenca)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [DoencasPorProdutos] " +
                    "       ( " +
                    "           IdDoenca," +
                    "           IdProduto" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdDoenca," +
                    "           @IdProduto" +

                    "       ); "
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);
                oComm.Parameters.AddWithValue("@IdDoenca", IdDoenca);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void ExcluirPorProduto(int IdProduto)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [DoencasPorProdutos] " +
                    "   WHERE " +
                    "       IdProduto=@IdProduto"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdProduto", IdProduto);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void ExcluirPorDoenca(int IdDoenca)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [DoencasPorProdutos] " +
                    "   WHERE " +
                    "       IdDoenca=@IdDoenca"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdDoenca", IdDoenca);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion



    }
}
