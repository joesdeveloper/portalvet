﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class PerguntasRespostas
    {
        string conexao = Conexao.BancoDeDados;

        #region Consultas

        public VO.PerguntaResposta Consultar(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                VO.PerguntaResposta vo = null;

                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [PerguntasRespostas] " +
                    "   WHERE " + 
                    "       IdPerguntaResposta=@IdPerguntaResposta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPerguntaResposta", id);
                SqlDataAdapter oDa = new SqlDataAdapter(oComm);

                DataTable oDt = new DataTable();

                oConn.Open();
                oDa.Fill(oDt);

                if (oDt.Rows.Count > 0)
                    vo = Popular(oDt.Rows[0]);

                return vo;
            }
        }


        public int TotalDeRegistros(string pesquisa)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT COUNT(*) AS 'total' FROM [PerguntasRespostas] " +
                    "   WHERE " +
                    "       Pergunta like @pesquisa OR " +
                    "		Resposta like @pesquisa OR " +
                    "		CONVERT(varchar,dataInclusao,103) like @pesquisa "
                    , oConn);

                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");

                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                return Convert.ToInt32(table.Rows[0]["total"]);
            }
        }

        public List<VO.PerguntaResposta> Pesquisar(int pagina, int totalPorPagina, string pesquisa, string colunaOrdenacao, bool ascendente)
        {
            List<VO.PerguntaResposta> lista = null;

            string ordenacao = "DESC";
            if (ascendente)
                ordenacao = "ASC";

            ordenacao = colunaOrdenacao + " " + ordenacao;

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "SELECT * FROM [PerguntasRespostas] " +
                    "	WHERE IdPerguntaResposta IN " +
                    "	( " +
                    "		SELECT TOP (@totalPorPagina) IdPerguntaResposta FROM [PerguntasRespostas] " +
                    "		    WHERE IdPerguntaResposta NOT IN " +
                    "		    ( " +
                    "		        SELECT TOP(@totalPorPagina * (@pagina)) IdPerguntaResposta FROM [PerguntasRespostas] " +
                    "				    WHERE " +
                    "					    Pergunta like @pesquisa OR " +
                    "						Resposta like @pesquisa OR " +
                    "						CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "                   ORDER BY " + ordenacao +
                    "			) " +
                    "			AND " +
                    "			( " +
                    "               Pergunta like @pesquisa OR " +
                    "				Resposta like @pesquisa OR " +
                    "				CONVERT(varchar,dataInclusao,103) like @pesquisa " +
                    "			) " +
                    "		    ORDER BY " + ordenacao +
                    "	) " +
                    "	ORDER BY " + ordenacao
                    , oConn);

                oComm.Parameters.AddWithValue("@totalPorPagina", totalPorPagina);
                oComm.Parameters.AddWithValue("@pagina", pagina);
                oComm.Parameters.AddWithValue("@pesquisa", "%" + pesquisa + "%");


                SqlDataAdapter oDa = new SqlDataAdapter(oComm);
                DataTable table = new DataTable();

                oConn.Open();
                oDa.Fill(table);

                if (table.Rows.Count > 0)
                    lista = PopularLista(table);

                return lista;
            }
        }

        #endregion


        #region Inclusao, Alteração e Exclusão

        public VO.PerguntaResposta Incluir(VO.PerguntaResposta vo)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "INSERT INTO [PerguntasRespostas] " +
                    "       ( " +
                    "           IdCategoria, " +
                    "           Pergunta, " +
                    "           Resposta, " +
                    "           Ativo" +
                    "       ) " +
                    "   VALUES " +
                    "       ( " +
                    "           @IdCategoria, " +
                    "           @Pergunta, " +
                    "           @Resposta, " +
                    "           @Ativo" +
                    "       ); " +
                    "SELECT @@IDENTITY;"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdCategoria", vo.Categoria.IdCategoria);
                oComm.Parameters.AddWithValue("@Pergunta", vo.Pergunta);
                oComm.Parameters.AddWithValue("@Resposta", vo.Resposta);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                Object obj = oComm.ExecuteScalar();
                vo.IdPerguntaResposta = Convert.ToInt32(obj);
            }

            return vo;
        }

        public void Alterar(VO.PerguntaResposta vo)
        {

            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "UPDATE [PerguntasRespostas] " +
                    "   SET " +
                    "       IdCategoria=@IdCategoria, " +
                    "       Pergunta=@Pergunta, " +
                    "       Resposta=@Resposta, " +
                    "       Ativo=@Ativo " +
                    "   WHERE " +
                    "       IdPerguntaResposta=@IdPerguntaResposta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPerguntaResposta", vo.IdPerguntaResposta);
                oComm.Parameters.AddWithValue("@IdCategoria", vo.Categoria.IdCategoria);
                oComm.Parameters.AddWithValue("@Pergunta", vo.Pergunta);
                oComm.Parameters.AddWithValue("@Resposta", vo.Resposta);
                oComm.Parameters.AddWithValue("@Ativo", vo.Ativo);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        public void Excluir(int id)
        {
            using (SqlConnection oConn = new SqlConnection(conexao))
            {
                SqlCommand oComm = new SqlCommand(
                    "DELETE FROM [PerguntasRespostas] " +
                    "   WHERE " +
                    "       IdPerguntaResposta=@IdPerguntaResposta"
                    , oConn);

                oComm.Parameters.AddWithValue("@IdPerguntaResposta", id);

                oConn.Open();
                oComm.ExecuteNonQuery();
            }
        }

        #endregion


        #region Popular

        private VO.PerguntaResposta Popular(DataRow row)
        {
            var vo = new VO.PerguntaResposta();
            
            vo.IdPerguntaResposta = (int)row["IdPerguntaResposta"];
            vo.Pergunta = (string)row["Pergunta"];
            vo.Resposta = (string)row["Resposta"];
            vo.Ativo = (bool)row["Ativo"];
            vo.Categoria = new DAL.PerguntasRespostasCategorias().Consultar((int)row["IdCategoria"]);
            vo.DataInclusao = (DateTime)row["DataInclusao"];
            
            return vo;
        }

        private List<VO.PerguntaResposta> PopularLista(DataTable table)
        {
            var lista = new List<VO.PerguntaResposta>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                lista.Add(Popular(table.Rows[i]));
            }

            return lista;
        }

        #endregion

    }
}
