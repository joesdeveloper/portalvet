﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.IO;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.Web.Security;

public class IntegracaoWab
{
    public bool Login(string email, string senha)
    {
        var js = new JavaScriptSerializer();

        try
        {
            using (var wb = new WebClient())
            {
                wb.Headers["Content-Type"] = "application/x-www-form-urlencoded";

                var data = new NameValueCollection();
                data["user"] = email;
                data["password"] = senha;

                var response = wb.UploadValues(ConfigurationManager.AppSettings["UrlAutenticacao"].ToString(), "POST", data);

                var obj = js.Deserialize<dynamic>(Encoding.ASCII.GetString(response));

                var mensagem = (VO.MensagemDeRetorno)js.Deserialize<VO.MensagemDeRetorno>(js.Serialize(obj));

                // Verifica se foi autenticado com sucesso
                if (mensagem.code == 1)
                {
                    var vo = (VO.MedicoVeterinario)js.Deserialize<VO.MedicoVeterinario>(js.Serialize(obj["data"]));

                    HttpContext.Current.Session["MedicoVeterinarioRetorno"] = mensagem;

                    Autenticar(vo);

                    IniciarSessao(vo);

                    return true;
                }
            }
        }
        catch (WebException ex)
        {
            StreamReader sr = new StreamReader(ex.Response.GetResponseStream(), true);
            var json = sr.ReadToEnd();

            var obj = js.Deserialize<dynamic>(json);
            HttpContext.Current.Session["MedicoVeterinarioRetorno"] = (VO.MensagemDeRetorno)js.Deserialize<VO.MensagemDeRetorno>(js.Serialize(obj));

            //HttpContext.Current.Session["MedicoVeterinarioRetorno"] = mensagem;

            //var mensagem = (VO.IntegracaoWab)js.Deserialize<VO.IntegracaoWab>(new JavaScriptSerializer().Serialize(obj));
            //HttpContext.Current.Session["MedicoVeterinarioRetorno"] = mensagem;
        }

        return false;
    }


    public void Logout()
    {
        FormsAuthentication.SignOut();

        if (HttpContext.Current.Request.Cookies["MedicoVeterinarioInfo"] != null)
        {
            var cookieInfo = new HttpCookie("MedicoVeterinarioInfo");
            cookieInfo.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(cookieInfo);
        }
    }


    public VO.MedicoVeterinario Consultar(string token)
    {
        var js = new JavaScriptSerializer();
        VO.MedicoVeterinario vo = null;

        using (var wb = new WebClient())
        {
            wb.Headers["Content-Type"] = "application/x-www-form-urlencoded";

            var data = new NameValueCollection();

            var response = wb.UploadValues(ConfigurationManager.AppSettings["UrlConsultarDados"].ToString().Replace("#tokenUsuario#", token), "POST", data);

            var obj = js.Deserialize<dynamic>(Encoding.ASCII.GetString(response));
            vo = (VO.MedicoVeterinario)js.Deserialize<VO.MedicoVeterinario>(js.Serialize(obj["data"]));

            var mensagem = (VO.MensagemDeRetorno)js.Deserialize<VO.MensagemDeRetorno>(js.Serialize(obj));
            HttpContext.Current.Session["MedicoVeterinarioRetorno"] = mensagem;
        }

        return vo;
    }


    public VO.MensagemDeRetorno RecuperarSenha(string email)
    {
        var js = new JavaScriptSerializer();
        VO.MensagemDeRetorno retorno = null;

        try
        {
            using (var wb = new WebClient())
            {
                wb.Headers["Content-Type"] = "application/x-www-form-urlencoded";

                var data = new NameValueCollection();
                data["email"] = email;

                var response = wb.UploadValues(ConfigurationManager.AppSettings["UrlRecuperarSenha"].ToString(), "POST", data);

                var obj = js.Deserialize<dynamic>(Encoding.ASCII.GetString(response));
                retorno = (VO.MensagemDeRetorno)js.Deserialize<VO.MensagemDeRetorno>(js.Serialize(obj));
            }
        }
        catch (WebException ex)
        {
            StreamReader sr = new StreamReader(ex.Response.GetResponseStream(), true);
            var json = sr.ReadToEnd();

            var obj = js.Deserialize<dynamic>(json);
            retorno = (VO.MensagemDeRetorno)js.Deserialize<VO.MensagemDeRetorno>(js.Serialize(obj));
        }

        return retorno;
    }


    public VO.MedicoVeterinario RecuperarDados()
    {
        VO.MedicoVeterinario vo = null;

        if (HttpContext.Current.Session["MedicoVeterinarioInfo"] != null && HttpContext.Current.Session["MedicoVeterinarioInfo"] != "")
            vo = (VO.MedicoVeterinario)HttpContext.Current.Session["MedicoVeterinarioInfo"];
        else
        {
            var usuario = System.Web.HttpContext.Current.User;

            vo = Consultar(usuario.Identity.Name);

            IniciarSessao(vo);

            //throw new Exception("b: " + usuario.Identity.Name + " - " + vo.Nome);
        }

        /*else
        {
            if (HttpContext.Current.Request.Cookies["MedicoVeterinarioInfo"] != null)
            {
                var cookieInfo = HttpContext.Current.Request.Cookies["MedicoVeterinarioInfo"];

                vo = new VO.MedicoVeterinario();

                vo.Id = cookieInfo["Id"];
                vo.UserToken = UTIL.Criptografia.Descriptografar(cookieInfo["UserToken"]);
                vo.Logo = cookieInfo["Logo"];
                vo.Nome_Clinica = cookieInfo["NomeClinica"];
                vo.Cpf = UTIL.Criptografia.Descriptografar(cookieInfo["Cpf"]);
                vo.Nome = cookieInfo["Nome"];
                vo.Sobrenome = cookieInfo["Sobrenome"];
                vo.Crmv = cookieInfo["Crmv"];
                vo.Crmv_Uf = cookieInfo["Crmv_Uf"];
                vo.Email = cookieInfo["Email"];

                vo.Especialidade_Area_Atuacao = cookieInfo["EspecialidadeAreaAtuacao"];
                vo.Endereco = cookieInfo["Endereco"];
                vo.Numero = cookieInfo["Numero"];
                vo.Complemento = cookieInfo["Complemento"];
                vo.Bairro = cookieInfo["Bairro"];
                vo.Cidade = cookieInfo["Cidade"];
                vo.Cep = cookieInfo["Cep"];
                vo.Estado = cookieInfo["Estado"];
                vo.Telefone_Comercial = cookieInfo["TelefoneComercial"];

                vo.Total_Pontos = cookieInfo["TotalPontos"];
                vo.Url_Coroa_Premiada = cookieInfo["UrlCoroaPremiada"];
            }
        }*/

        return vo;
    }
    

    public void IniciarSessao(VO.MedicoVeterinario vo)
    {
        HttpContext.Current.Session["MedicoVeterinarioInfo"] = vo;
        HttpContext.Current.Session["Ip"] = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        HttpContext.Current.Session["Data"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
    }


    #region Metodos

    public bool Validar(VO.MedicoVeterinario vo)
    {
        var retorno = true;

        if (vo.Nome == null || vo.Nome.Trim().Length == 0)
            retorno = false;

        if (vo.Sobrenome == null || vo.Sobrenome.Trim().Length == 0)
            retorno = false;

        if (vo.Email == null || vo.Email.Trim().Length == 0)
            retorno = false;

        if (vo.Cpf == null || vo.Cpf.Trim().Length == 0)
            retorno = false;

        if (vo.Crmv == null || vo.Crmv.Trim().Length == 0)
            retorno = false;

        if (vo.Crmv_Uf == null || vo.Crmv_Uf.Trim().Length == 0)
            retorno = false;

        if (vo.Sexo == null || vo.Sexo.Trim().Length == 0)
            retorno = false;

        if (vo.Nome_Clinica == null || vo.Nome_Clinica.Trim().Length == 0)
            retorno = false;

        if (vo.Endereco == null || vo.Endereco.Trim().Length == 0)
            retorno = false;

        if (vo.Numero == null || vo.Numero.Trim().Length == 0)
            retorno = false;

        if (vo.Bairro == null || vo.Bairro.Trim().Length == 0)
            retorno = false;

        if (vo.Cidade == null || vo.Cidade.Trim().Length == 0)
            retorno = false;

        if (vo.Cep == null || vo.Cep.Trim().Length == 0)
            retorno = false;

        if (vo.Estado == null || vo.Estado.Trim().Length == 0)
            retorno = false;

        if (vo.Telefone_Comercial == null || vo.Telefone_Comercial.Trim().Length == 0)
            retorno = false;

        return retorno;
    }


    private void Autenticar(VO.MedicoVeterinario vo)
    {
        //Inicializamos o processo de autenticação
        FormsAuthentication.Initialize();

        //Criamos um ticket com os dados do usuário
        //reparem que passamos inclusive os perfis carregados
        //o ticket será a base da nossa autenticação
        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, vo.UserToken, DateTime.Now, DateTime.Now.AddHours(8), false, "MedicoVeterinario", FormsAuthentication.FormsCookiePath);

        //Criptografamos o ticket por questão se segurança
        string hash = FormsAuthentication.Encrypt(ticket);

        //Criamos um cookie(será usado para validação do asp.net)
        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

        //Setamos a persistencia do cookie
        if (ticket.IsPersistent)
            cookie.Expires = ticket.Expiration;

        //Em fim adicionamos o cookie a nossa aplicação.
        HttpContext.Current.Response.Cookies.Add(cookie);

        /*var cookieInfo = HttpContext.Current.Response.Cookies["MedicoVeterinarioInfo"];
        cookieInfo["Id"] = vo.Id;
        cookieInfo["UserToken"] = UTIL.Criptografia.Criptografar(vo.UserToken);
        cookieInfo["Cpf"] = UTIL.Criptografia.Criptografar(vo.Cpf);
        cookieInfo["Nome"] = vo.Nome;
        cookieInfo["Sobrenome"] = vo.Sobrenome;
        cookieInfo["Email"] = vo.Email;
        cookieInfo["Crmv"] = vo.Crmv;
        cookieInfo["Crmv_Uf"] = vo.Crmv_Uf;
        cookieInfo["Sexo"] = vo.Sexo;
        cookieInfo["EspecialidadeAreaAtuacao"] = vo.Especialidade_Area_Atuacao;
        cookieInfo["NomeClinica"] = vo.Nome_Clinica;
        cookieInfo["Endereco"] = vo.Endereco;
        cookieInfo["Numero"] = vo.Numero;
        cookieInfo["Complemento"] = vo.Complemento;
        cookieInfo["Bairro"] = vo.Bairro;
        cookieInfo["Cidade"] = vo.Cidade;
        cookieInfo["Cep"] = vo.Cep;
        cookieInfo["Estado"] = vo.Estado;
        cookieInfo["TelefoneComercial"] = vo.Telefone_Comercial;
        cookieInfo["Logo"] = vo.Logo;
        cookieInfo["TotalPontos"] = vo.Total_Pontos;
        cookieInfo["UrlCoroaPremiada"] = vo.Url_Coroa_Premiada;

        cookieInfo["Ip"] = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        cookieInfo["Data"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        HttpContext.Current.Response.Cookies.Add(cookieInfo);

        HttpContext.Current.Session["MedicoVeterinarioInfo"] = vo;
        HttpContext.Current.Session["Ip"] = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        HttpContext.Current.Session["Data"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        */
    }

    #endregion


    /*
    public MensagemDeRetorno Incluir(VO.MedicoVeterinario vo)
    {
        MensagemDeRetorno retorno = null;

        using(WebClient client = new WebClient()) { 
            System.Collections.Specialized.NameValueCollection reqparm = new System.Collections.Specialized.NameValueCollection(); 
            reqparm.Add("param1", "<any> kinds & of = ? strings"); 
            reqparm.Add("param2", "escaping is already handled"); 
            byte[] responsebytes = client.UploadValues("http://localhost", "POST", reqparm); 
            string responsebody = Encoding.UTF8.GetString(responsebytes); 
        } 



        try
        {
            using (var wb = new WebClient())
            {
                wb.Headers["Content-Type"] = "application/x-www-form-urlencoded";

                var data = new NameValueCollection();
                data["nome"] = vo.Nome;
                data["sobrenome"] = vo.Sobrenome;
                data["email"] = vo.Email;
                data["senha"] = vo.Senha;
                data["cpf"] = vo.Cpf;
                data["crmv"] = vo.Crmv;
                data["sexo"] = vo.Sexo;
                data["celular"] = vo.Celular;
                data["nome_clinica"] = "ovo";//vo.Nome_Clinica;
                data["especialidade_area_atuacao"] = vo.Especialidade_Area_Atuacao;
                data["endereco"] = vo.Endereco;
                data["numero"] = vo.Numero;
                data["bairro"] = vo.Bairro;
                data["cidade"] = vo.Cidade;
                data["estado"] = vo.Estado;
                data["cep"] = vo.Cep;
                data["telefone_comercial"] = vo.Telefone_Comercial;

                var response = wb.UploadData(ConfigurationManager.AppSettings["UrlIncluir"].ToString(), "POST", );

                var js = new JavaScriptSerializer();
                var obj = js.Deserialize<dynamic>(Encoding.ASCII.GetString(response));
                retorno = (MensagemDeRetorno)js.Deserialize<MensagemDeRetorno>(new JavaScriptSerializer().Serialize(obj));
            }
        }
        catch (WebException ex)
        {
            StreamReader sr = new StreamReader(ex.Response.GetResponseStream(), true);
            var json = sr.ReadToEnd();

            var js = new JavaScriptSerializer();
            var obj = js.Deserialize<dynamic>(json);
            retorno = (MensagemDeRetorno)js.Deserialize<MensagemDeRetorno>(new JavaScriptSerializer().Serialize(obj));
        }

        return retorno;
    }
    */


    /*

    public static string UploadFileEx(string uploadfile, string url,
    string fileFormName, string contenttype, NameValueCollection querystring,
    CookieContainer cookies)
    {
        if ((fileFormName == null) ||
            (fileFormName.Length == 0))
        {
            fileFormName = "file";
        }

        if ((contenttype == null) ||
            (contenttype.Length == 0))
        {
            contenttype = "application/octet-stream";
        }


        string postdata;
        postdata = "?";
        if (querystring != null)
        {
            foreach (string key in querystring.Keys)
            {
                postdata += key + "=" + querystring.Get(key) + "&";
            }
        }
        Uri uri = new Uri(url + postdata);


        string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
        HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
        webrequest.CookieContainer = cookies;
        webrequest.ContentType = "multipart/form-data; boundary=" + boundary;
        webrequest.Method = "POST";


        // Build up the post message header
        StringBuilder sb = new StringBuilder();
        sb.Append("--");
        sb.Append(boundary);
        sb.Append("\r\n");
        sb.Append("Content-Disposition: form-data; name=\"");
        sb.Append(fileFormName);
        sb.Append("\"; filename=\"");
        sb.Append(Path.GetFileName(uploadfile));
        sb.Append("\"");
        sb.Append("\r\n");
        sb.Append("Content-Type: ");
        sb.Append(contenttype);
        sb.Append("\r\n");
        sb.Append("\r\n");

        string postHeader = sb.ToString();
        byte[] postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);

        // Build the trailing boundary string as a byte array
        // ensuring the boundary appears on a line by itself
        byte[] boundaryBytes =
               Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

        FileStream fileStream = new FileStream(uploadfile,
                                    FileMode.Open, FileAccess.Read);
        long length = postHeaderBytes.Length + fileStream.Length +
                                               boundaryBytes.Length;
        webrequest.ContentLength = length;

        Stream requestStream = webrequest.GetRequestStream();

        // Write out our post header
        requestStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);

        // Write out the file contents
        byte[] buffer = new Byte[checked((uint)Math.Min(4096,
                                 (int)fileStream.Length))];
        int bytesRead = 0;
        while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            requestStream.Write(buffer, 0, bytesRead);

        // Write out the trailing boundary
        requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
        WebResponse responce = webrequest.GetResponse();
        Stream s = responce.GetResponseStream();
        StreamReader sr = new StreamReader(s);

        return sr.ReadToEnd();
    }*/
}

