﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Raca
    {
        public Raca()
        {
            IdRaca = 0;
            IdEspecie = 0;
            Nome = "";
            Ordenacao = 9999;
        }

        public int IdRaca { get; set; }

        public int IdEspecie { get; set; }

        public string Nome { get; set; }

        public int Ordenacao { get; set; }
    }
}
