﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Animal
    {
        public Animal()
        {
            IdAnimal = 0;
            IdProprietario = Guid.Empty;
            Raca = null;
            Nome = "";
            Genero = "";
            DataNascimento = DateTime.MaxValue;
            DataInclusao = DateTime.Now;
        }

        public int IdAnimal { get; set; }

        public Guid IdProprietario { get; set; }

        public VO.Raca Raca { get; set; }

        public string Nome { get; set; }

        public string Genero { get; set; }

        public DateTime DataNascimento { get; set; }

        public DateTime DataInclusao { get; set; }

    }
}
