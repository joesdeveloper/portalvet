﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Imagem
    {
        public Imagem()
        {
            IdImagem = 0;
            Titulo = "";
            Url = "";
            DataInclusao = DateTime.Now;
        }

        public int IdImagem { get; set; }

        public string Titulo { get; set; }

        public string Url { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
