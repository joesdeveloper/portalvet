﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class MedicoVeterinario
    {
        string nome = "";
        string sobrenome = "";
        string cpf = "";
        string crmv = "";
        string crmv_uf = "";
        string sexo = "";
        string celular = "";
        string nome_clinica = "";
        string email = "";
        string especialidade_area_atuacao = "";
        string endereco = "";
        string numero = "";
        string cep = "";
        string bairro = "";
        string cidade = "";
        string estado = "";
        string complemento = "";
        string telefone_comercial = "";
        string logo = "";
        string total_pontos = "";
        string url_coroa_premiada = "";


        public MedicoVeterinario() { }

        public int IdMedicoVeterinario { get; set; }

        public string Id { get; set; }

        public string UserToken { get; set; }

        public string Nome
        {
            get { return nome.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    nome = value.TrimStart().TrimEnd();
                else
                    nome = "";
            }
        }

        public string Sobrenome
        {
            get { return sobrenome.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    sobrenome = value.TrimStart().TrimEnd();
                else
                    sobrenome = "";
            }
        }

        public string Cpf
        {
            get { return cpf.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    cpf = value.TrimStart().TrimEnd();
                else
                    cpf = "";
            }
        }

        public string Senha { get; set; }

        public string Crmv
        {
            get { return crmv.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    crmv = value.TrimStart().TrimEnd();
                else
                    crmv = "";
            }
        }

        public string Crmv_Uf
        {
            get { return crmv_uf.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    crmv_uf = value.TrimStart().TrimEnd();
                else
                    crmv_uf = "";
            }
        }

        public string Sexo
        {
            get { return sexo.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    sexo = value.TrimStart().TrimEnd();
                else
                    sexo = "";
            }
        }

        public string Celular
        {
            get { return celular.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    celular = value.TrimStart().TrimEnd();
                else
                    celular = "";
            }
        }

        public string Nome_Clinica
        {
            get { return nome_clinica.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    nome_clinica = value.TrimStart().TrimEnd();
                else
                    nome_clinica = "";
            }
        }

        public string Email
        {
            get { return email.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    email = value.TrimStart().TrimEnd();
                else
                    email = "";
            }
        }



        public string Especialidade_Area_Atuacao
        {
            get { return especialidade_area_atuacao.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    especialidade_area_atuacao = value.TrimStart().TrimEnd();
                else
                    especialidade_area_atuacao = "";
            }
        }

        public string Endereco
        {
            get { return endereco.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    endereco = value.TrimStart().TrimEnd();
                else
                    endereco = "";
            }
        }

        public string Numero
        {
            get { return numero.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    numero = value.TrimStart().TrimEnd();
                else
                    numero = "";
            }
        }

        public string Complemento
        {
            get { return complemento.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    complemento = value.TrimStart().TrimEnd();
                else
                    complemento = "";
            }
        }

        public string Bairro
        {
            get { return bairro.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    bairro = value.TrimStart().TrimEnd();
                else
                    bairro = "";
            }
        }

        public string Cidade
        {
            get { return cidade.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    cidade = value.TrimStart().TrimEnd();
                else
                    cidade = "";
            }
        }

        public string Estado
        {
            get { return estado.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    estado = value.TrimStart().TrimEnd();
                else
                    estado = "";
            }
        }

        public string Cep
        {
            get { return cep.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    cep = value.TrimStart().TrimEnd();
                else
                    cep = "";
            }
        }

        public string Telefone_Comercial
        {
            get { return telefone_comercial.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    telefone_comercial = value.TrimStart().TrimEnd();
                else
                    telefone_comercial = "";
            }
        }

        public string Logo 
        { 
            get { return logo.TrimStart().TrimEnd(); } 
            set {
                if (value != null)
                    logo = value.TrimStart().TrimEnd();
                else
                    logo = "";
            } 
        }



        public string Total_Pontos
        {
            get { return total_pontos.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    total_pontos = value.TrimStart().TrimEnd();
                else
                    total_pontos = "";
            }
        }

        public string Url_Coroa_Premiada
        {
            get { return url_coroa_premiada.TrimStart().TrimEnd(); }
            set
            {
                if (value != null)
                    url_coroa_premiada = value.TrimStart().TrimEnd();
                else
                    url_coroa_premiada = "";
            }
        }
    }
}
