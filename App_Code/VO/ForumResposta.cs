﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class ForumResposta
    {
        public ForumResposta()
        {
            IdResposta = 0;
            IdPergunta = 0;
            IdUsuario = 0;
            Nome = "";
            Texto = "";
            Ativo = false;
            DataInclusao = DateTime.Now;
        }


        public int IdResposta { get; set; }

        public int IdPergunta { get; set; }

        public int IdUsuario { get; set; }

        public string Nome { get; set; }

        public string Texto { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
