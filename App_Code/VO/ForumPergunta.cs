﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class ForumPergunta
    {
        public ForumPergunta()
        {
            IdPergunta = 0;
            IdForum = 0;
            IdUsuario = 0;
            Nome = "";
            Titulo = "";
            Texto = "";
            Ativo = false;
            TotalDeRespostas = 0;
            DataInclusao = DateTime.Now;
        }


        public int IdPergunta { get; set; }

        public int IdForum { get; set; }

        public int IdUsuario { get; set; }

        public string Nome { get; set; }

        public string Titulo { get; set; }

        public string Texto { get; set; }

        public int TotalDeRespostas { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
