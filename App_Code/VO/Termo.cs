﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Termo
    {
        public Termo()
        {
            IdTermo = 0;
            Nome = "";
            Total = 0;
        }

        public int IdTermo { get; set; }

        public string Nome { get; set; }

        public int Total { get; set; }
    }
}
