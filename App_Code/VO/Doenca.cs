﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Doenca
    {
        public Doenca()
        {
            IdDoenca = 0;
            IdEspecie = 0;
            Nome = "";
        }

        public int IdDoenca { get; set; }

        public int IdEspecie { get; set; }

        public string Nome { get; set; }
    }
}
