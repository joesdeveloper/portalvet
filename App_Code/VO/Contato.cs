﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Contato
    {
        public Contato()
        {
            Nome = "";
            Email = "";
            Telefone = "";
            Assunto = "";
            Mensagem = "";
            DataInclusao = DateTime.Now;
        }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public string Assunto { get; set; }

        public string Mensagem { get; set; }

        public DateTime DataInclusao { get; set; }

    }
}
