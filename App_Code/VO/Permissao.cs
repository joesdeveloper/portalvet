﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Permissao
    {
        public Permissao()
        {
            IdPermissao = 0;
            Nome = "";
        }

        public int IdPermissao { get; set; }

        public string Nome { get; set; }

    }
}
