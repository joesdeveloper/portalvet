﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VO
{
    public class Evento
    {
        string descricao = "";

        public Evento()
        {
            IdEvento = 0;
            //EventoImagem = null;
            Nome = "";
            Descricao = "";
            Local = "";
            Endereco = "";
            Numero = "";
            Complemento = "";
            Bairro = "";
            Cidade = "";
            Estado = "";
            Cep = "";
            DataEventoInicial = DateTime.MinValue;
            DataEventoFinal = DateTime.MinValue;
            Ativo = false;
            DataPublicacao = DateTime.Now;
            DataInclusao = DateTime.Now;
        }

        public int IdEvento { get; set; }

        //public VO.Imagem EventoImagem { get; set; }

        public string Nome { get; set; }

        public string Descricao { get { return descricao; } set { descricao = HttpUtility.HtmlDecode(value); } }

        public string Local { get; set; }

        public string Endereco { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Estado { get; set; }

        public string Cep { get; set; }

        public DateTime DataEventoInicial { get; set; }

        public DateTime DataEventoFinal { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataPublicacao { get; set; }

        public DateTime DataInclusao { get; set; }

    }
}
