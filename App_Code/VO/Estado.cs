﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Estado
    {
        public Estado()
        {
            Sigla = "";
            Nome = "";
        }

        public string Sigla { get; set; }

        public string Nome { get; set; }
    }
}
