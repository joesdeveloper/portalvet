﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Busca
    {
        public Busca()
        {
            Id = 0;
            Autor = null;
            Titulo = "";
            Texto = "";
            Tipo = "";
            Ativo = false;
            DataPublicacao = DateTime.Now;
            DataInclusao = DateTime.Now;
        }

        public int Id { get; set; }

        public VO.Autor Autor { get; set; }

        public string Titulo { get; set; }

        public string Texto { get; set; }

        public string Tipo { get; set; }
        
        public bool Ativo { get; set; }

        public DateTime DataPublicacao { get; set; }

        public DateTime DataInclusao { get; set; }

    }
}
