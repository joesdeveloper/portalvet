﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VO
{
    public class Produto
    {
        string nome = "";
        string informacao = "";

        public Produto()
        {
            IdProduto = 0;
            IdOutraVersao = 0;
            Categorias = null;
            //FotoDoProduto = null;
            Especie = null;
            Nome = "";
            Icones = "";
            Descricao = "";
            Embalagem = "";
            Informacao = "";
            EnergiaMetabolizavel = 0;
            FatorSobrepeso = 0;
            FatorPesoideial = 0;
            FatorMagro = 0;
            IndiceDeCalculo = 0;
            Tipo = null;
            Doenca = null;
            Obesidade = false;
            Cor = "";
            Termos = null;
            Ativo = false;
            LocalExibicao = "t";
            TabelaQuantidadeDiaria = "";
        }

        public int IdProduto { get; set; }

        public int IdOutraVersao { get; set; }

        //public VO.ProdutoCategoria Categoria { get; set; }

        public List<VO.ProdutoCategoria> Categorias { get; set; }

        //public VO.Imagem FotoDoProduto { get; set; }

        public VO.Especie Especie { get; set; }

        public string Nome { get { return nome.TrimStart().TrimEnd(); } set { nome = value.TrimStart().TrimEnd(); } }

        public string Icones { get; set; }

        public string Descricao { get; set; }

        public string Embalagem { get; set; }

        public string Informacao { get { return informacao; } set { informacao = HttpUtility.HtmlDecode(value); } }
        
        public int EnergiaMetabolizavel { get; set; }

        public decimal FatorSobrepeso { get; set; }

        public decimal FatorPesoideial { get; set; }

        public decimal FatorMagro { get; set; }

        public decimal IndiceDeCalculo { get; set; }

        public ProdutoTipo Tipo { get; set; }

        public List<Doenca> Doenca { get; set; }

        public bool Obesidade { get; set; }

        public string Cor { get; set; }

        public List<VO.Termo> Termos { get; set; }

        public bool Ativo { get; set; }

        public string LocalExibicao { get; set; }

        public string TabelaQuantidadeDiaria { get; set; }
        
    }
}
