﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class ImagemPorGaleria
    {
        public ImagemPorGaleria()
        {
            IdImagem = 0;
            IdGaleria = 0;
            Titulo = "";
            Descricao = "";
            Url = "";
            DataInclusao = DateTime.Now;
        }

        public int IdImagem { get; set; }

        public int IdGaleria { get; set; }

        public string Titulo { get; set; }

        public string Descricao { get; set; }

        public string Url { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
