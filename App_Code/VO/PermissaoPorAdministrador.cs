﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class PermissaoPorAdministrador
    {
        public PermissaoPorAdministrador()
        {
            IdPermissao = 0;
        }


        public Guid IdAdministrador { get; set; }
        public int IdPermissao { get; set; }

    }
}
