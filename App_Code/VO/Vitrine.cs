﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Vitrine
    {
        public Vitrine()
        {
            IdVitrine = 0;
            Titulo = "";
            Url = "";
            Target = "";
            Ativo = false;
            DataPublicacao = DateTime.Now;
            DataInclusao = DateTime.Now;
        }

        public int IdVitrine { get; set; }

        public string Titulo { get; set; }

        public string Url { get; set; }

        public string Target { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataPublicacao { get; set; }

        public DateTime DataInclusao { get; set; }

    }
}
