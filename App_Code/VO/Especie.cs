﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Especie
    {
        public Especie()
        {
            IdEspecie = 0;
            Nome = "";
            Imagem = "";
            Icone = "";
            Mascara = "";
            Orgao = "";
        }

        public int IdEspecie { get; set; }

        public string Nome { get; set; }

        public string Imagem { get; set; }

        public string Icone { get; set; }

        public string Mascara { get; set; }

        public string Orgao { get; set; }
    }
}
