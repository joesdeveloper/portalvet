﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class ReceitaObesidade
    {
        public ReceitaObesidade()
        {
            IdReceita = 0;
            IdTipoDeRelatorio = "o";
            TipoDeRelatorio = "";
            IdEspecie = 0;
            Especie = "";
            IdRaca = 0;
            Raca = "";
            IdVeterinario = 0;
            userToken = "";
            NomeClinica = "";
            NomeDoVeterinario = "";
            SobrenomeDoVeterinario = "";
            Crmv = "";
            CrmvUf = "";
            EmailDoVeterinario = "";
            EspecialidadeAreaAtuacao = "";
            Endereco = "";
            Numero = "";
            Complemento = "";
            Bairro = "";
            Cidade = "";
            Cep = "";
            Estado = "";
            TelefoneComercial = "";
            NomeDoProprietario = "";
            EmailDoProprietario = "";
            NomeDoAnimal = "";
            PesoAtual = 0.0;
            SexoIndice = 0;
            Sexo = "";
            Porte = "";
            ScoreCorporal = 0.0;
            IdDoenca = 0;
            Doenca = "";
            QuantidadeEnergetica = 0;
            PesoPretendido = 0.0;
            PerdaPorSemana = 0.0;
            QuantidadeDeSemanas = 0;
            IdAlimentoSeco = 0;
            IdAlimentoUmido = 0;
            AlimentoSecoNome = "";
            AlimentoUmidoNome = "";
            AlimentoSecoEM = 0.0;
            AlimentoUmidoEM = 0.0;
            AlimentoSecoQuantidade = 0.0;
            AlimentoUmidoQuantidade = 0.0;
            RecomendacoesAdicionais = "";
            Impresso = false;
            EnviadoPorEmail = false;
            DataInclusao = DateTime.Now;
        }

        public int IdReceita { get; set; }

        public string IdTipoDeRelatorio { get; set; }

        public string TipoDeRelatorio { get; set; }
        
        public int IdEspecie { get; set; }

        public string Especie { get; set; }

        public int IdRaca { get; set; }

        public string Raca { get; set; }

        public int IdVeterinario { get; set; }

        public string userToken { get; set; }

        public string NomeClinica { get; set; }

        public string NomeDoVeterinario { get; set; }

        public string SobrenomeDoVeterinario { get; set; }

        public string Crmv { get; set; }

        public string CrmvUf { get; set; }

        public string EmailDoVeterinario { get; set; }

        public string EspecialidadeAreaAtuacao { get; set; }

        public string Endereco { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Cep { get; set; }

        public string Estado { get; set; }

        public string TelefoneComercial { get; set; }

        public string NomeDoProprietario { get; set; }

        public string EmailDoProprietario { get; set; }
        
        public string NomeDoAnimal { get; set; }

        public double PesoAtual { get; set; }

        public int SexoIndice { get; set; }

        public string Sexo { get; set; }

        public string Porte { get; set; }

        public double ScoreCorporal { get; set; }

        public int IdDoenca { get; set; }

        public string Doenca { get; set; }

        public int QuantidadeEnergetica { get; set; }

        public double PesoPretendido { get; set; }

        public double PerdaPorSemana { get; set; }

        public int QuantidadeDeSemanas { get; set; }

        public int IdAlimentoSeco { get; set; }

        public int IdAlimentoUmido { get; set; }

        public string AlimentoSecoNome { get; set; }

        public string AlimentoUmidoNome { get; set; }

        public double AlimentoSecoEM { get; set; }

        public double AlimentoUmidoEM { get; set; }

        public double AlimentoSecoQuantidade { get; set; }

        public double AlimentoUmidoQuantidade { get; set; }

        public string RecomendacoesAdicionais { get; set; }

        public bool Impresso { get; set; }

        public bool EnviadoPorEmail { get; set; }
        
        public DateTime DataInclusao { get; set; }
    }
}
