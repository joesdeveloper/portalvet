﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Proprietario
    {
        public Proprietario()
        {
            IdProprietario = Guid.Empty;
            IdMedicoVeterinario = Guid.Empty;
            Nome = "";
            Cpf = "";
            Rg = "";
            Email = "";
            ComercialDdd = "";
            Comercial = "";
            TelefoneDdd = "";
            Telefone = "";
            CelularDdd = "";
            Celular = "";
            Endereco = "";
            Numero = "";
            Complemento = "";
            Bairro = "";
            Cidade = "";
            Estado = "";
            Cep = "";
            Ativo = false;
            DataInclusao = DateTime.Now;
        }

        public Guid IdProprietario { get; set; }

        public Guid IdMedicoVeterinario { get; set; }

        public string Nome { get; set; }

        public string Cpf { get; set; }

        public string Rg { get; set; }

        public string Email { get; set; }

        public string ComercialDdd { get; set; }

        public string Comercial { get; set; }

        public string TelefoneDdd { get; set; }

        public string Telefone { get; set; }

        public string CelularDdd { get; set; }

        public string Celular { get; set; }

        public string Endereco { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Estado { get; set; }

        public string Cep { get; set; }

        public DateTime DataNascimento { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
