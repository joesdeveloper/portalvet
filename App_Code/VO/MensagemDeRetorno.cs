﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VO
{
    public class MensagemDeRetorno
    {

        public bool error { get; set; }
        public int code { get; set; }
        public int status { get; set; }
        public string message { get; set; }
    }
}