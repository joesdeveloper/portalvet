﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class ProdutoCategoria
    {
        public ProdutoCategoria()
        {
            IdCategoria = 0;
            //IdEspecie = 0;
            Nome = "";
            Classe = "";
            Obesidade = false;
        }

        public int IdCategoria { get; set; }

        //public int IdEspecie { get; set; }

        public string Nome { get; set; }

        public string Classe { get; set; }

        public bool Obesidade { get; set; }
    }
}
