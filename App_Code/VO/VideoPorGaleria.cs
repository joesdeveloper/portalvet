﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class VideoPorGaleria
    {
        public VideoPorGaleria()
        {
            IdVideo = 0;
            IdGaleria = 0;
            Titulo = "";
            Url = "";
            DataInclusao = DateTime.Now;
        }

        public int IdVideo { get; set; }

        public int IdGaleria { get; set; }

        public string Titulo { get; set; }

        public string Url { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
