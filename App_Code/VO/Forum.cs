﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Forum
    {
        public Forum()
        {
            IdForum = 0;
            Nome = "";
            Descricao = "";
            UrlAmigavel = "";
            Ativo = false;
            TotalPerguntas = 0;
            TotalRespostas = 0;
            DataAtualizacao = DateTime.Now;
            DataInclusao = DateTime.Now;
        }

        public int IdForum { get; set; }

        public string Nome { get; set; }

        public string Descricao { get; set; }

        public string UrlAmigavel { get; set; }

        public int TotalPerguntas { get; set; }

        public int TotalRespostas { get; set; }

        public DateTime DataAtualizacao { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
