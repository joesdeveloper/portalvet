﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Galeria
    {
        public Galeria()
        {
            IdGaleria = 0;
            Titulo = "";
            Descricao = "";
            Imagens = null;
            Videos = null;
            Ativo = false;
            DataInclusao = DateTime.Now;
        }

        public int IdGaleria { get; set; }

        public string Titulo { get; set; }

        public string Descricao { get; set; }

        public List<VO.ImagemPorGaleria> Imagens { get; set; }

        public List<VO.VideoPorGaleria> Videos { get; set; }
        
        public bool Ativo { get; set; }

        public DateTime DataInclusao { get; set; }

    }
}
