﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class ArquivoPorArtigo
    {
        public ArquivoPorArtigo()
        {
            IdArquivo = 0;
            IdArtigo = 0;
            Titulo = "";
            Url = "";
            DataInclusao = DateTime.Now;
        }

        public int IdArquivo { get; set; }
        
        public int IdArtigo { get; set; }

        public string Titulo { get; set; }

        public string Url { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
