﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VO
{
    public class Artigo
    {
        string texto = "";

        public Artigo()
        {
            IdArtigo = 0;
            Relacionamento = null;
            IdTipo = 0;
            IdCategoria = 0;
            Autor = null;
            ArtigoAquivos = null;
            Destaque = false;
            Titulo = "";
            Texto = "";
            Ativo = false;
            Tipo = null;
            Termos = null;
            DataPublicacao = DateTime.Now;
            DataInclusao = DateTime.Now;
        }

        public int IdArtigo { get; set; }

        public VO.ProdutoCategoria Relacionamento { get; set; }
        
        public int IdTipo { get; set; }

        public int IdCategoria { get; set; }

        //public VO.Imagem ArtigoImagem { get; set; }

        public VO.Autor Autor { get; set; }

        public List<VO.ArquivoPorArtigo> ArtigoAquivos { get; set; }

        public bool Destaque { get; set; }

        public string Titulo { get; set; }

        public string Texto { get { return texto; } set { texto = HttpUtility.HtmlDecode(value); } }

        public bool Ativo { get; set; }

        public ArtigoTipo Tipo { get; set; }

        public List<VO.Termo> Termos { get; set; }

        public DateTime DataPublicacao { get; set; }
        
        public DateTime DataInclusao { get; set; }
    }
}
