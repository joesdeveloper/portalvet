﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VO
{
    public class Representante
    {
        string telefone1, telefone2, telefone3, telefone4, celular, fax = "";

        public Representante()
        {
            IdRepresentante = 0;
            Nome = "";
            Email1 = "";
            Email2 = "";
            Telefone1 = "";
            Telefone2 = "";
            Telefone3 = "";
            Telefone4 = "";
            Celular = "";
            Fax = "";
            NextelId = "";
            Endereco = "";
            Numero = "";
            Complemento = "";
            Bairro = "";
            Cidade = "";
            Estado = "";
            Cep = "";
            Ativo = false;
            DataInclusao = DateTime.Now;
        }

        public int IdRepresentante { get; set; }

        public string Nome { get; set; }

        public string Email1 { get; set; }

        public string Email2 { get; set; }

        public string Telefone1 { get { return telefone1; } set { telefone1 = value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""); } }

        public string Telefone2 { get { return telefone2; } set { telefone2 = value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""); } }

        public string Telefone3 { get { return telefone3; } set { telefone3 = value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""); } }

        public string Telefone4 { get { return telefone4; } set { telefone4 = value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""); } }

        public string Celular { get { return celular; } set { celular = value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""); } }

        public string Fax { get { return fax; } set { fax = value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""); } }

        public string NextelId { get; set; }

        public string Endereco { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Estado { get; set; }

        public string Cep { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataInclusao { get; set; }

    }
}
