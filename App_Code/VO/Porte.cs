﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Porte
    {
        public Porte()
        {
            IdPorte = 0;
            IdEspecie = 0;
            Nome = "";
        }


        public int IdPorte { get; set; }

        public int IdEspecie { get; set; }

        public string Nome { get; set; }
    }
}
