﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Administrador
    {
        public Administrador()
        {
            Nome = "";
            Email = "";
            Senha = "";
            Ativo = false;
            TotalAcessos = 0;
            DataInclusao = DateTime.Now;
        }

        public Guid IdAdministrador { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Senha { get; set; }

        public bool Ativo { get; set; }

        public int TotalAcessos { get; set; }

        public DateTime? DataUltimoAcesso { get; set; }

        public DateTime DataInclusao { get; set; }

    }
}
