﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class ProdutoTipo
    {
        public ProdutoTipo()
        {
            IdTipo = "";
            Nome = "";
        }

        public string IdTipo { get; set; }

        public string Nome { get; set; }
    }
}
