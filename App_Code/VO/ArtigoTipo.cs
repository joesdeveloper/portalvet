﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class ArtigoTipo
    {
        public ArtigoTipo()
        {
            IdTipo = 0;
            Nome = "";
            Css = "";
        }

        public int IdTipo { get; set; }

        public string Nome { get; set; }

        public string Css { get; set; }
    }
}
