﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VO
{
    public class PerguntaResposta
    {
        string resposta = "";

        public PerguntaResposta()
        {
            IdPerguntaResposta = 0;
            Categoria = null;
            Pergunta = "";
            Resposta = "";
            Ativo = false;
            DataInclusao = DateTime.Now;
        }

        public int IdPerguntaResposta { get; set; }

        public VO.PerguntaRespostaCategoria Categoria { get; set; }

        public string Pergunta { get; set; }

        public string Resposta { get { return resposta; } set { resposta = HttpUtility.HtmlDecode(value); } }

        public bool Ativo { get; set; }

        public DateTime DataInclusao { get; set; }
    }
}
