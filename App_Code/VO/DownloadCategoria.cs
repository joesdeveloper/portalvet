﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class DownloadCategoria
    {
        public DownloadCategoria()
        {
            IdCategoria = 0;
            Nome = "";
        }

        public int IdCategoria { get; set; }

        public string Nome { get; set; }
    }
}
