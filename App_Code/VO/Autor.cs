﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VO
{
    public class Autor
    {
        public Autor()
        {
            IdAutor = 0;
            Assinatura = "";
            Nome = "";
            Email = "";
            Curriculo = "";
        }

        public int IdAutor { get; set; }

        public string Assinatura { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Curriculo { get; set; }
    }
}
