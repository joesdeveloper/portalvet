﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Globalization;
using System.IO;

public partial class ultimas_noticias : System.Web.UI.Page
{
    BLL.Busca bBus = new BLL.Busca();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string caminho = HttpContext.Current.Request.PhysicalApplicationPath + @"arquivos\artigos\";

    const int registrosPorPagina = 10;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
            InicarSessao();
        }
    }

    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;
    }

    private void IniciarGridView()
    {
        InicarSessao();

        int paginaAtual = Convert.ToInt32(Session["paginaAtual"]);

        var lista = bBus.UltimasNoticias(100000);


        grdwListar.PageSize = registrosPorPagina;
        grdwListar.PageIndex = paginaAtual;
        grdwListar.DataSource = lista;
        grdwListar.DataBind();

    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");

            lblRegistro.Text = "";

            lblRegistro.Text += "<a class=\"item\" href=\"" + oVal.Pagina(DataBinder.Eval(e.Row.DataItem, "Tipo").ToString()) + "?id=" + DataBinder.Eval(e.Row.DataItem, "Id") + "\">\n";

            if (File.Exists(caminho + DataBinder.Eval(e.Row.DataItem, "Id") + @"\imagem.png"))
                lblRegistro.Text += "<div class=\"imagem\" style=\"background: transparent url('_servicos/gerar_imagem.aspx?i=arquivos/artigos/" + DataBinder.Eval(e.Row.DataItem, "Id") + "/imagem.png&w=300&h=300') no-repeat center center; background-size:cover;\"></div>\n";
            else
                lblRegistro.Text += "<div class=\"imagem\">\n<img src=\"layout/imagens/ico-congressos-e-seminarios-lateral.png\" /></div>\n";

            lblRegistro.Text +=
                "    <div class=\"dados\">\n" +
                "       <span class=\"titulo-do-item\">" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Titulo").ToString(), 55) + "</span>" +
                "       <div class=\"autor\">" + DataBinder.Eval(e.Row.DataItem, "Autor") + "</div>\n" +
                "       <p class=\"texto-do-item\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Texto").ToString()), 200) + "</p>" +
                "    </div>\n" +
                "</a>"
                ;
        }
    }
}