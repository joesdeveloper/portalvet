﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Security;

public partial class RoteamentoLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["ReturnUrl"] == null)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/");
        }
        else
        {
            var retorno = Request["ReturnUrl"].ToString().ToLower();
            
            if (retorno.IndexOf("_admin") > 0)
                Response.Redirect("~/_admin/login.aspx");
            else
                Response.Redirect("~/login.aspx?ReturnUrl=" + Request["ReturnUrl"]);
        }
    }
}