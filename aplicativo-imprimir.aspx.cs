using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class aplicativo_imprimir : System.Web.UI.Page
{
    UTIL.Validacao uVal = new UTIL.Validacao();

    IntegracaoWab oWab = new IntegracaoWab();

    string email = "";
    string assunto = "";
    string corpo = "";

    string mensagem = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["htmlImpressao"] != null)
        {
            pnlImpressao.InnerHtml = Session["htmlImpressao"].ToString();
        }
    }


    private void CaptarDados()
    {
        email = hdnEmail.Value;
        assunto = "PortalVet - App NutriVET";
        corpo = pnlImpressao.InnerHtml;
    }


    private bool ValidarDados()
    {
        mensagem = "";

        if (email.Trim().Length == 0)
            mensagem += @"O campo E-MAIL não pode ser nulo.\n";
        else
        {
            if (!uVal.Email(email))
                mensagem += @"O campo E-MAIL é inválido.\n";
        }

        if (mensagem.Length > 0)
        {
            mensagem = @"ATENÇÃO:\n\n" + mensagem;
            return false;
        }

        return true;
    }


    private void ExibirMensagem()
    {
        var script = "alert('" + mensagem + "');";

        if (mensagem.Trim().Length > 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", script, true);
    }


    protected void btnImprimir_Click(object sender, EventArgs e)
    {
        new BLL.Receitas().SalvarImpresso(Convert.ToInt32(Request.Cookies["MedicoVeterinarioReceita"]["id"]), true);
    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
        CaptarDados();

        if (ValidarDados())
        {
            //try
            //{
                new BLL.Receitas().SalvarEmail(Convert.ToInt32(Request.Cookies["MedicoVeterinarioReceita"]["id"]), true, email);
                
                EnviarEmail();

                mensagem = "Sua mensagem foi enviada com sucesso.";
            //}
            //catch
            //{
            //    mensagem = @"ERRO\n\nOcorreu um erro ao tentar enviar o e-mail, por favor, contate o administrador.";
            //}

        }

        ExibirMensagem();
    }


    private void EnviarEmail()
    {
        corpo =
            "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head runat=\"server\">" +
            "    <title></title>" +
            "    <style>" +
            "        * { font-family:Arial; font-size:12px; color:#333333; }" +
            "        body { font-family:Arial; font-size:12px; color:#333333; }" +
            "    </style>" +
            "</head>" +
            "<body>" +
            Session["htmlEmail"].ToString() +
            "<h2 style=\"color:#333; float:left; font-family:'DIN', 'Arial'; font-size:16px; width:100%; margin:30px 0 0 0; text-transform:uppercase;\">ONDE COMPRAR?</h2><br />" +
            "Acesse nosso site e encontre a loja mais pr&oacute;xima de voc&ecirc; <a href=\"http://buscalojas.royalcanin.com.br/\" target=\"_blank\" style=\";\">clicando aqui</a>" +
            "</body>" +
            "</html>";

        new UTIL.EnviarEmail(email, assunto, corpo, true);

        //GerarPdf(corpo);
    }



    //protected void GerarPdf(string html)
    //{
    //    Response.ClearContent();
    //    Response.ContentType = "application/pdf";
    //    //Response.ContentEncoding = System.Text.Encoding.UTF8;
    //    Response.AddHeader("content-disposition", "attachment; filename=Boleto.pdf");

    //    StringWriter stw = new StringWriter();

    //    HtmlTextWriter htextw = new HtmlTextWriter(stw);

    //    impressao.RenderControl(htextw);

    //    Document document = new Document();

    //    PdfWriter.GetInstance(document, Response.OutputStream);

    //    document.Open();

    //    StringReader str = new StringReader(stw.ToString());

    //    HTMLWorker htmlworker = new HTMLWorker(document);

    //    htmlworker.Parse(str);

    //    document.Close();

    //    Response.Write(document);
    //    Response.End();
    //}

    
}