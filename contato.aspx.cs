﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class contato : System.Web.UI.Page
{
    VO.Contato vCon = new VO.Contato();
    UTIL.Validacao uVal = new UTIL.Validacao();

    string mensagem = "";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private void LimparDados()
    {
        txtNome.Text = "";
        txtEmail.Text = "";
        txtTelefone.Text = "";
        txtAssunto.Text = "";
        txtMensagem.Text = "";
    }


    private void CaptarDados()
    {
        vCon.Nome = txtNome.Text;
        vCon.Email = txtEmail.Text;
        vCon.Telefone = txtTelefone.Text;
        vCon.Assunto = txtAssunto.Text;
        vCon.Mensagem = txtMensagem.Text;
    }


    private bool ValidarDados()
    {
        mensagem = "";

        if (vCon.Nome.Trim().Length == 0)
            mensagem += @"- O campo NOME não pode ser nulo.\n";

        if (vCon.Email.Trim().Length == 0)
            mensagem += @"- O campo E-MAIL não pode ser nulo.\n";
        else
        {
            if (!uVal.Email(vCon.Email))
                mensagem += @"- O campo E-MAIL é inválido.\n";
        }

        if (vCon.Telefone.Trim().Length == 0)
            mensagem += @"- O campo TELEFONE não pode ser nulo.\n";

        if (vCon.Assunto.Trim().Length == 0)
            mensagem += @"- O campo COMO PODEMOS AJUDAR? não pode ser nulo.\n";

        if (vCon.Mensagem.Trim().Length == 0)
            mensagem += @"- O campo SUA MENSAGEM não pode ser nulo.\n";

        if (mensagem.Length > 0)
        {
            mensagem = @"ATENÇÃO:\n\n" + mensagem;
            return false;
        }

        return true;
    }


    private void ExibirMensagem()
    {
        var script = "alert('" + mensagem + "');";

        if (mensagem.Trim().Length > 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", script, true);
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        if (ValidarDados())
        {
            EnviarEmail();
            mensagem = "Sua mensagem foi enviada com sucesso.";
            LimparDados();
        }
        ExibirMensagem();
    }


    private void EnviarEmail()
    {
        string Email = System.Configuration.ConfigurationManager.AppSettings["CONTATO_Email"];
        string Assunto = System.Configuration.ConfigurationManager.AppSettings["CONTATO_Assunto"];

        string corpo =
            "Portal Vet <br />---------------------------------------------------<br />" +
            "Nome: " + vCon.Nome + "<br />" +
            "E-mail: " + vCon.Email + "<br />" +
            "Telefone: " + vCon.Telefone + "<br />" +
            "Assunto: " + vCon.Assunto + "<br />" +
            "Sua mensagem: " + vCon.Mensagem + "<br />" +
            "Data da Inclusão: " + vCon.DataInclusao.ToString("dd/MM/yyyy HH:mm:ss") + "<br />"
            ;

        new UTIL.EnviarEmail(Email, Assunto, corpo, true);

    }

    protected void ddlEstado_Init(object sender, EventArgs e)
    {
        var bRep = new BLL.Representantes();

        var lista = bRep.Estados();

        ddlEstado.Items.Clear();
        ddlEstado.Items.Add(new ListItem("Selecione um estado", "0"));
        foreach (var registro in lista)
        {
            ddlEstado.Items.Add(new ListItem(registro.Nome, registro.Sigla));
        }
    }

}