﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class coroa_premiada : System.Web.UI.Page
{
    System.Security.Principal.IPrincipal usuario = System.Web.HttpContext.Current.User;

    IntegracaoWab oWab = new IntegracaoWab();

    protected void Page_Load(object sender, EventArgs e)
    {
        IniciariFrame();
    }

    private void IniciariFrame()
    {
        if (usuario.Identity.IsAuthenticated && usuario.IsInRole("MedicoVeterinario"))
        {
            // Recuperar dos dados do MV
            var vMed = oWab.RecuperarDados();

            // Verificar se o MV tem acesso a Coroa Premiada
            if (vMed.Url_Coroa_Premiada.Trim().Length > 0)
                pnlCoroaPremiada.Attributes.Add("src", vMed.Url_Coroa_Premiada);
        }
    }
}