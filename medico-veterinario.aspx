﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="medico-veterinario.aspx.cs" Inherits="medico_veterinario" %>
<%@ Register src="modulos/rodape.ascx" tagname="rodape" tagprefix="uc1" %>
<%@ Register src="modulos/cabecalho.ascx" tagname="cabecalho" tagprefix="uc2" %>
<%@ Register src="modulos/aside-app-prescricao.ascx" tagname="prescricao" tagprefix="uc3" %>
<%@ Register src="modulos/aside-pesquisar.ascx" tagname="pesquisar" tagprefix="uc4" %>
<%@ Register src="modulos/aside-menu.ascx" tagname="menu" tagprefix="uc5" %>
<%--<%@ Register src="modulos/aside-produtos-relacionados.ascx" tagname="aside" tagprefix="uc6" %>--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Portal Vet - Médico Veterinário</title>
    <link rel="shortcut icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link rel="icon" href="layout/imagens/favicon.ico" type="image/x-icon">
    <link href="layout/css/geral.css" rel="stylesheet" type="text/css" />
    <script src="layout/js/jquery.js" type="text/javascript"></script>
    <script src="layout/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
    <script src="layout/js/jquery.mask.min.js"></script>
    <script src="layout/js/geral.js" type="text/javascript"></script>
    <%--<script type="text/javascript">
        $(document).ready(function () {
            $('#slider1').tinycarousel({
                interval: true,
                intervalTime: 6000,
                bullets: true,
                buttons: false
            });
            $('#<%=txtCep.ClientID%>').mask('00000-000');
            $('#<%=txtTelefoneComercial.ClientID%>').mask('(00) 0000-0000');
            $('#<%=txtCelular.ClientID%>').mask('(00) 00000-0000');
            $('#<%=txtCpf.ClientID%>').mask('000.000.000-00');
            $('#<%=txtCpf.ClientID%>').blur(function () {
                var valor = $(this).val();
                if (valor.length > 0) {
                    if (!ValidarCpf(valor)) {
                        $(this).focus();
                        alert('ATENÇÃO\n\nO campo CPF é inválido.');
                    }
                }
            });
            function ValidarCpf(valor) {
                valor = valor.replace('.', '').replace('-', '').replace('.', '');
                var num, dig, som, i, res, digTem = 1;
                if (valor.length < 11)
                    return false;
                for (i = 0; i < valor.length - 1; i++)
                    if (valor.charAt(i) != valor.charAt(i + 1)) {
                        digTem = 0;
                        break;
                    }
                if (!digTem) {
                    num = valor.substring(0, 9);
                    dig = valor.substring(9);
                    som = 0;
                    for (i = 10; i > 1; i--)
                        som += num.charAt(10 - i) * i;
                    res = som % 11 < 2 ? 0 : 11 - som % 11;
                    if (res != dig.charAt(0))
                        return false;
                    num = valor.substring(0, 10);
                    som = 0;
                    for (i = 11; i > 1; i--)
                        som += num.charAt(11 - i) * i;
                    res = som % 11 < 2 ? 0 : 11 - som % 11;
                    if (res != dig.charAt(1))
                        return false;
                    return true;
                }
                else
                    return false;
            }
        });
	</script>--%>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="principal">
        <uc2:cabecalho ID="cabecalho1" runat="server" />

        <div id="conteudo">
            <section>
                <div class="medico-veterinario">
                    <h1>
                        <div>
                            <span>Médico</span> Veterinário
                        </div>
                    </h1>
                    <p><asp:Literal ID="lblDescricao" runat="server"></asp:Literal></p>
                   <%-- <div class="campos">
                        <div><span>Nome:</span><asp:TextBox ID="txtNome" runat="server" MaxLength="100"></asp:TextBox></div>
                        <div><span>Sobrenome:</span><asp:TextBox ID="txtSobrenome" runat="server" MaxLength="100"></asp:TextBox></div>
                        
                        <div><span>E-mail:</span><asp:TextBox ID="txtEmail" runat="server" MaxLength="100"></asp:TextBox></div>
                        <div><span>Confirmar e-mail:</span><asp:TextBox ID="txtConfEmail" runat="server" MaxLength="100"></asp:TextBox></div>

                        <div><span>Senha:</span><asp:TextBox ID="txtSenha1" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox></div>
                        <div><span>Confirmar senha:</span><asp:TextBox ID="txtSenha2" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox></div>

                        <div><span>CPF:</span><asp:TextBox ID="txtCpf" runat="server" MaxLength="14"></asp:TextBox></div>
                        <div><span>CRMV:</span><asp:TextBox ID="txtCrmv" runat="server" MaxLength="20"></asp:TextBox></div>

                        <div><span>Sexo:</span>
                            <asp:DropDownList ID="ddlSexo" runat="server">
                                <asp:ListItem Text="Feminino" Value="F"></asp:ListItem>
                                <asp:ListItem Text="Masculino" Value="M"></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div><span>Celular:</span><asp:TextBox ID="txtCelular" runat="server" MaxLength="15"></asp:TextBox></div>

                        <div><span>Nome da Clínica:</span><asp:TextBox ID="txtNomeDaClinica" runat="server" MaxLength="100"></asp:TextBox></div>

                        <div><span>Especialista/Área de Atuação:</span><asp:TextBox ID="txtEspecialidadeAreaAtuacao" runat="server" MaxLength="100" Width="68%"></asp:TextBox></div>

                        <div><span>Endereço:</span><asp:TextBox ID="txtEndereco" runat="server" MaxLength="30"></asp:TextBox></div>
                        <div><span>Número:</span><asp:TextBox ID="txtNumero" runat="server" MaxLength="5"></asp:TextBox></div>
                        <div><span>Bairro:</span><asp:TextBox ID="txtBairro" runat="server" MaxLength="30"></asp:TextBox></div>
                        <div><span>Cidade:</span><asp:TextBox ID="txtCidade" runat="server" MaxLength="30"></asp:TextBox></div>
                        <div><span>Estado:</span>
                            <asp:DropDownList ID="ddlEstado" runat="server" OnInit="ddlEstado_Init">
                            </asp:DropDownList>
                        </div>
                        <div><span>CEP:</span><asp:TextBox ID="txtCep" runat="server" MaxLength="9"></asp:TextBox></div>

                        <div><span>Telefone Comercial:</span><asp:TextBox ID="txtTelefoneComercial" runat="server" MaxLength="14" Width="78%"></asp:TextBox></div>
                        <div><span>Logo da Clínica:</span><asp:FileUpload ID="uplLogo" runat="server"></asp:FileUpload></div>
                        
                        <asp:Button ID="btnEnviar" runat="server" Text="enviar mensagem" OnClick="btnEnviar_Click"></asp:Button>

                    </div>--%>
                    <iframe runat="server" id="pnlCadastro">
                    </iframe>
                </div>
            </section>

            <aside>
                <uc3:prescricao ID="prescricao1" runat="server" />
                <uc4:pesquisar ID="pesquisar1" runat="server" />
                <uc5:menu ID="menu1" runat="server" />
                <%--<uc6:produtos_relacionados ID="produtos_relacionados1" runat="server" />--%>
            </aside>
        </div>
        <asp:Literal ID="lblTag" runat="server"></asp:Literal>
        <uc1:rodape ID="rodape1" runat="server" />
        
    </div>
    </form>
</body>
</html>

