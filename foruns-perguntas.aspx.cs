﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Globalization;

public partial class foruns_perguntas : System.Web.UI.Page
{
    System.Security.Principal.IPrincipal usuario = System.Web.HttpContext.Current.User;

    BLL.ForunsPerguntas bForPer = new BLL.ForunsPerguntas();
    VO.ForumPergunta vForPer = new VO.ForumPergunta();

    IntegracaoWab oWab = new IntegracaoWab();

    UTIL.Validacao oVal = new UTIL.Validacao();

    string _erro = "";
    const int registrosPorPagina = 15;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
            InicarSessao();
        }
    }

    #region Grid
    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;
    }

    private void IniciarGridView()
    {
        InicarSessao();

        var paginaAtual = Convert.ToInt32(Session["paginaAtual"]);
        var idForum = 0;

        if (Request["id"] != null)
        {
            idForum = Convert.ToInt32(Request["id"]);

            var lista = bForPer.Pesquisar(0, 15000, "", "DataInclusao", false, idForum);

            if (lista != null)
            {

                var listaAux = (from aux in lista
                                where aux.Ativo == true
                                select aux).ToList();

                grdwListar.PageSize = registrosPorPagina;
                grdwListar.PageIndex = paginaAtual;
                grdwListar.DataSource = listaAux;
                grdwListar.DataBind();
            }
        }

    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");
            lblRegistro.Text =
                "<a href=\"foruns-respostas.aspx?id=" + DataBinder.Eval(e.Row.DataItem, "IdPergunta") + "&idFor=" + Request["id"] + "\">" +
                "    <span class=\"titulo-do-item\">" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Titulo").ToString(), 70) + "</span>" +
                "    <p class=\"texto-do-item\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Texto").ToString()), 200) + "<br /><span style=\"color:#b91319; font-size:13px;\">Clique para comentar esse tópico</span></p>" +
                "</a>";
            
            var lblDataAtualizacao = (Literal)e.Row.FindControl("lblDataAtualizacao");

            var totalDeRespostas = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "TotalDeRespostas"));
            if (totalDeRespostas > 1)
            {
                lblDataAtualizacao.Text =
                    "<strong>" + totalDeRespostas + "</strong> respostas";
            }
            else
            {
                if (totalDeRespostas == 1)
                    lblDataAtualizacao.Text = "<strong>" + totalDeRespostas + "</strong> resposta";
                else
                    lblDataAtualizacao.Text = "aguardando respostas";
            }
        }
    }
    #endregion


    #region Topico

    private void LimparDados()
    {
        txtTexto.Text = "";
    }

    private void CaptarDados()
    {
        var vMed = oWab.RecuperarDados();

        if (usuario.Identity.IsAuthenticated && usuario.IsInRole("MedicoVeterinario"))
        {
            vForPer.IdUsuario = Convert.ToInt32(vMed.Id);
            vForPer.Nome = vMed.Nome + " " + vMed.Sobrenome;
        }
        else
            Response.Redirect("~/");

        vForPer.IdForum = Convert.ToInt32(Request["id"]);
        //vForPer.Nome = "Nome do veterinário";
        vForPer.Titulo = txtTitulo.Text;
        vForPer.Texto = txtTexto.Text;
        vForPer.Ativo = true;
    }

    private bool ValidarDados()
    {
        _erro = "";

        if (vForPer.Titulo.Trim().Length == 0)
            _erro += @"- O campo titulo não pode ser nulo.\n";

        if (vForPer.Texto.Trim().Length == 0)
            _erro += @"- O campo mensagem não pode ser nulo.\n";

        if (_erro.Length > 0)
        {
            _erro = @"ATENÇÃO:\n\n" + _erro;
            return false;
        }

        return true;
    }


    private void ExibirMensagem()
    {
        if (_erro.Trim().Length > 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", "alert('" + _erro + "');window.location.hash='#topico';", true);
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        if (ValidarDados())
        {
            bForPer.Incluir(vForPer);

            LimparDados();

            IniciarGridView();
        }

        ExibirMensagem();
    }
    #endregion
}