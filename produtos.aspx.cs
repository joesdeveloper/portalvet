﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;

public partial class produtos : System.Web.UI.Page
{
    BLL.Produtos bPro = new BLL.Produtos();
    BLL.Especies bEsp = new BLL.Especies();

    UTIL.Validacao oVal = new UTIL.Validacao();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string Icone()
    {
        if (Request["id"] == null)
            return "";

        var html = new StringBuilder();

        var lista = bEsp.Listar();

        var registro = (from aux in lista
                        where aux.IdEspecie != Convert.ToInt32(Request["id"])
                        select aux).Single();

        html.Append("<a href=\"produtos.aspx?id=" + registro.IdEspecie + "#inicio\" style=\"display:block; float:right; margin-right:20px;\"><img src=\"" + registro.Icone + "\" /></a>");

        return html.ToString();
    }

    public string Especies()
    {
        if (Request["id"] != null)
            return "";

        var html = new StringBuilder();

        var lista = bEsp.Listar();

        html.Append("<div class=\"racas\">");

        foreach (var registro in lista)
        {
            html.Append("    <a href=\"produtos.aspx?id=" + registro.IdEspecie + "#inicio\"><img src=\"" + registro.Imagem + "\" /></a>");
        }

        html.Append("</div> ");

        return html.ToString();
    }


    public string Produtos()
    {
        if (Request["id"] == null)
            return "";

        var idEspecie = Convert.ToInt32(Request["id"]);

        var lPro = bPro.Listar();

        var vEsp = bEsp.Consultar(idEspecie);

        var lCat = (from aux in bPro.ListarCategorias()
                    orderby aux.Nome
                    select aux );

        var html = new StringBuilder();
        var marcadores = new StringBuilder();

        //Response.Write(
        //        "<br />idEspecie: " + idEspecie +
        //        "<br />lCat: " + lCat.Count +
        //        "<br />lPro: " + lPro.Count
        //        );

        /* Marcadores para os orgaos */
        marcadores.Append("<div id=\"inicio\" class=\"" + vEsp.Orgao + "\">");

        /* Exibe os produtos por categorias */
        foreach(var vCat in lCat)
        {
            var lProAux = (from aux in lPro
                           where aux.Ativo == true && aux.Especie.IdEspecie == idEspecie && aux.Categorias.Any(c => c.IdCategoria == vCat.IdCategoria) && (aux.LocalExibicao == "t" || aux.LocalExibicao == "p")
                           orderby aux.Nome
                           select aux).ToList();

            // Monta os marcadores
            if (lProAux.Count > 0)
            {
                marcadores.Append(
                    "   <a data-id=\"#cat" + vCat.IdCategoria + "\" class=\"quadro-over " + vCat.Classe + "\"></a>" +
                    "   <div id=\"cat" + vCat.IdCategoria + "\" class=\"quadro\">" +
                    "       <div class=\"seta\"></div>" +
                    "       <div class=\"vermelho\">" +
                    "           <span class=\"titulo\">" + vCat.Nome + "</span> " +
                    "           <div class=\"branco\">"
                    );

                foreach (var vPro in lProAux)
                {
                    // Exibe o selo do produto se o produto for seco
                    if (vPro.Tipo.IdTipo == "S")
                        marcadores.Append("               <a href=\"#tab" + vCat.IdCategoria + "\" class=\"selo-do-produto\"><img src=\"arquivos/produtos/" + vPro.IdProduto + "/logo.png\"></a>");
                    else
                    {
                        // Exibe o selo do produto se o produto for umido e não tiver produto seco relacionado
                        if (vPro.IdOutraVersao == 0)
                            marcadores.Append("               <a href=\"#tab" + vCat.IdCategoria + "\" class=\"selo-do-produto\"><img src=\"arquivos/produtos/" + vPro.IdProduto + "/logo.png\"></a>");
                        else
                        {
                            // Exibe o selo do produto se o produto for umido e não tiver produto seco relaciona na mesma categoria
                            var vProOut = (from aux in lPro
                                           where aux.IdProduto == vPro.IdOutraVersao && aux.Categorias.Any(c => c.IdCategoria != vCat.IdCategoria)
                                           select aux).ToList();

                            if (vProOut.Count > 0)
                                marcadores.Append("               <a href=\"#tab" + vCat.IdCategoria + "\" class=\"selo-do-produto\"><img src=\"arquivos/produtos/" + vPro.IdProduto + "/logo.png\"></a>");
                        }
                    }
                }

                marcadores.Append(
                    "           </div> " +
                    "       </div>" +
                    "   </div>"
                    );

            }

            // Monta os produtos
            if (lProAux.Count > 0)
            {
                html.Append(
                        "<div class=\"margem\">" +
                        "<a href=\"#tab" + vCat.IdCategoria + "\" class=\"categoria\" id=\"cat" + vCat.IdCategoria + "\" name=\"cat" + vCat.IdCategoria + "\">" + vCat.Nome + "</a>" +
                        "<table id=\"tab" + vCat.IdCategoria + "\" class=\"lista\" cellspacing=\"0\">" +
                        "   <tr>" +
                        "       <td>"
                        );

                foreach (var vPro in lProAux)
                {
                    html.Append(
                           "           <a href=\"produto.aspx?id=" + vPro.IdProduto + "\" class=\"resposta-item\">" +
                           //"               <div class=\"imagem\" style=\"background-image:url('arquivos/produtos/" + vPro.IdProduto + "/foto.png');\"></div>" +
                           "               <div class=\"imagem\" style=\"background-image:url('_servicos/gerar_imagem.aspx?i=arquivos/produtos/" + vPro.IdProduto + "/foto.png&w=165&h=165');\"></div>" +
                           "               <p class=\"texto-nome\">" + vPro.Nome + "</p>" +
                           "           </a>"

                           );
                }

                html.Append(
                    "       </td>" +
                    "   </tr>" +
                    "</table>" +
                    "</div>"
                    );

            }
        }

        /* Marcadores para os orgaos */
        marcadores.Append("</div>");

        return marcadores.ToString() + html.ToString();
    }
}