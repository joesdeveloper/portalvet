﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Globalization;

public partial class foruns_respostas : System.Web.UI.Page
{
    System.Security.Principal.IPrincipal usuario = System.Web.HttpContext.Current.User;

    BLL.ForunsPerguntas bForPer = new BLL.ForunsPerguntas();
    BLL.ForunsRespostas bForRes = new BLL.ForunsRespostas();

    VO.ForumResposta vForRes = new VO.ForumResposta();

    IntegracaoWab oWab = new IntegracaoWab();

    UTIL.Validacao oVal = new UTIL.Validacao();
    string _erro = "";

    const int registrosPorPagina = 10;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
            InicarSessao();
        }
    }

    #region Iniciar Grid
    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;
    }

    private void IniciarGridView()
    {
        InicarSessao();

        var paginaAtual = Convert.ToInt32(Session["paginaAtual"]);
        var idPergunta = 0;

        if (Request["id"] != null)
        {
            idPergunta = Convert.ToInt32(Request["id"]);

            var vPer = bForPer.Consultar(idPergunta);

            lblPergunta.Text =
                    "<table class=\"lista\" cellspacing=\"0\">" +
                    "   <tr class=\"cabecalho\">" +
                    "       <th>" +
                    "           Pergunta" +
                    "       </th>" +
                    "   </tr>" +
                    "   <tr style=\"background:#fafafa;\">" +
                    "       <td>" +
                    "           <a class=\"resposta-item\">" +
                    "               <p class=\"titulo-da-pergunta\"><strong>" + vPer.Titulo + "</strong></p>" +
                    "               <p class=\"texto-nome\"><strong>" + vPer.Nome + "</strong></p>" +
                    "               <p class=\"texto-data\">" + oVal.Data(vPer.DataInclusao, new CultureInfo("pt-BR")) + " | " + vPer.DataInclusao.ToString("HH:mm") + "</p>" +
                    "               <hr />" +
                    "               <p class=\"texto-do-item\">" + vPer.Texto.Replace(Environment.NewLine, "<br />") + "<p>" +
                    "           </a>" +
                    "       </td>" +
                    "   </tr>" +
                    "</table>"
                    ;


            var lista = bForRes.Pesquisar(0, 15000, "", "DataInclusao", true, idPergunta);

            if (lista != null)
            {
                var listaAux = (from aux in lista
                                where aux.Ativo == true
                                select aux).ToList();

                grdwListar.PageSize = registrosPorPagina;
                grdwListar.PageIndex = paginaAtual;
                grdwListar.DataSource = listaAux;
                grdwListar.DataBind();
            }
        }

    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");
            lblRegistro.Text =
                "<a class=\"resposta-item\">" +
                "   <p class=\"texto-nome\"><strong>" + DataBinder.Eval(e.Row.DataItem, "Nome") + "</strong></p>" +
                "   <p class=\"texto-data\">" + oVal.Data(Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DataInclusao")), new CultureInfo("pt-BR")) + " | " + Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DataInclusao")).ToString("HH:mm") + "</p>" +
                "   <p class=\"texto-do-item\">" + oVal.Html(DataBinder.Eval(e.Row.DataItem, "Texto").ToString()).Replace(Environment.NewLine, "<br />") + "</p>" +
                "</a>";
            
        }
    }

    #endregion


    #region Mensagem

    private void LimparDados()
    {
        txtMensagem.Text = "";
    }

    private void CaptarDados()
    {
        var vMed = oWab.RecuperarDados();

        if (usuario.Identity.IsAuthenticated && usuario.IsInRole("MedicoVeterinario"))
        {
            vForRes.IdUsuario = Convert.ToInt32(vMed.Id);
            vForRes.Nome = vMed.Nome + " " + vMed.Sobrenome;
        }
        else
            Response.Redirect("~/");

        vForRes.IdPergunta = Convert.ToInt32(Request["id"]);
        //vForRes.Nome = "Nome do veterinário";
        vForRes.Texto = txtMensagem.Text;
        vForRes.Ativo = true;
    }

    private bool ValidarDados()
    {
        _erro = "";

        if (vForRes.Texto.Trim().Length == 0)
            _erro += @"- O campo mensagem não pode ser nulo.\n";

        if (_erro.Length > 0)
        {
            _erro = @"ATENÇÃO:\n\n" + _erro;
            return false;
        }

        return true;
    }


    private void ExibirMensagem()
    {
        if (_erro.Trim().Length > 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alerta", "alert('" + _erro + "');", true);
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        CaptarDados();

        if (ValidarDados())
        {
            bForRes.Incluir(vForRes);

            LimparDados();

            IniciarGridView();
        }

        ExibirMensagem();
    }
    #endregion
}