﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;

public partial class pesquisar : System.Web.UI.Page
{
    BLL.Busca bBus = new BLL.Busca();

    UTIL.Validacao oVal = new UTIL.Validacao();

    const int registrosPorPagina = 10;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IniciarGridView();
        }
    }

    private void InicarSessao()
    {
        if (Session["paginaAtual"] == null)
            Session["paginaAtual"] = 0;

        lblMensagem.Text = "";
    }

    private void IniciarGridView()
    {
        InicarSessao();

        var valor = "";
        
        int paginaAtual = Convert.ToInt32(Session["paginaAtual"]);

        if (Request["q"] != null)
            valor = Request["q"].ToString().TrimStart().TrimEnd();

        if (valor.Trim().Length == 0)
            lblMensagem.Text = "Dados da pesquisa inválidos";
        else
        {
            var lista = bBus.Pesquisar(0, registrosPorPagina, valor);

            if (lista == null)
                lblMensagem.Text = "A pesquisa não encontrou nenhum registro correspondente.";

            grdwListar.PageSize = registrosPorPagina;
            grdwListar.PageIndex = paginaAtual;
            grdwListar.DataSource = lista;
            grdwListar.DataBind();
        }
    }


    protected void grdwListar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["paginaAtual"] = e.NewPageIndex;
        IniciarGridView();
    }

    protected void grdwListar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblRegistro = (Literal)e.Row.FindControl("lblRegistro");

            //Response.Write(DataBinder.Eval(e.Row.DataItem, "Tipo").ToString().ToLower() + "<br />");

            lblRegistro.Text =
                "<a href=\"" + oVal.Pagina(DataBinder.Eval(e.Row.DataItem, "Tipo").ToString()) + "?id=" + DataBinder.Eval(e.Row.DataItem, "Id") + "\">" +
                "    <span class=\"titulo-do-item\">" + oVal.Substring(DataBinder.Eval(e.Row.DataItem, "Titulo").ToString(), 70) + "</span>" +
                "    <div class=\"autor\">" + DataBinder.Eval(e.Row.DataItem, "Autor.Assinatura") + "</div>\n" +
                "    <p class=\"texto-do-item\">" + oVal.Substring(oVal.Html(DataBinder.Eval(e.Row.DataItem, "Texto").ToString()), 200) + "</p>" +
                "</a>"
                ;
        }
    }
}